package local.source.actors;

import java.util.ArrayList;
import java.util.List;

import local.source.game_time;

public class actor_ai_demo_positions {

	private static actor_ai_demo_positions instance = null;
	List<interface_actor> actors = new ArrayList<interface_actor>();
	game_time gt = game_time.getInstance();
	List<String> positions = new ArrayList<String>();
	int lastTimePositionChanged = 0;
	int lengthOfTimeToDisplay = 10000;
	int positionIndex = 0;

	private actor_ai_demo_positions() {
		positions.add("stand");
		positions.add("walk-forward");
		positions.add("engage");
	}

	public void add(interface_actor a) {
		actors.add(a);
	}

	public static actor_ai_demo_positions getInstance() {
		if (instance == null) {
			instance = new actor_ai_demo_positions();
		}
		return instance;
	}

	public void update() {
		if (lastTimePositionChanged == 0) {
			lastTimePositionChanged = gt.getTime();
		}

		if (positionIndex >= positions.size()) {
			positionIndex = 0;
		}

		int now = gt.getTime();
		if ((lastTimePositionChanged + lengthOfTimeToDisplay) < now) {
			for (interface_actor a : actors) {
				a.move(positions.get(positionIndex));
			}
			lastTimePositionChanged = now;
			positionIndex++;
		}
	}

}
