package local.source.actors;

import java.util.ArrayList;
import java.util.List;
import org.joml.*;

import local.source.game_time;



public class manager_actors {

	private static manager_actors instance = null;
	game_time gt =game_time.getInstance();
	List<interface_actor> actors = new ArrayList<interface_actor>();
	interface_actor player;
	
	private manager_actors()
	{}
	
	public void add(interface_actor a)
	{ actors.add(a);  }
	
	public void draw()
	{ 
		for(interface_actor a : actors)
		{ a.draw();}		
	}
	
	public boolean isAnotherActorAtXYZ(interface_actor b)
	{
		boolean result = false;
		
		for(interface_actor a : actors)
		{
			if(a!=b)
			{
				
			}
		}
		
		return result;
	}
	
	public interface_actor getActorAtXYZ(Vector3f xyz, interface_actor b)
	{
		interface_actor result = null;
		
		for(interface_actor a : actors)
		{
			if(a!=b)
			{
				
			}
		}
		
		return result;
	}
	
	public static manager_actors getInstance()
	{
		if(instance==null)
		{instance = new manager_actors();}
		return instance;
	}	
	
	public  interface_actor getPlayer()
	{ return player ;}	

	public void remove(interface_actor b)
	{
		for(interface_actor a : actors)
		{
			if(a==b)
			{
				actors.remove(a);
				return;
			}
		}
	}
	
	public void setFacing(float x, float y, float z)
	{
		for(interface_actor a : actors)
		{
			if(a!=player)
			{ a.setFacing(x,y,z);}
		}
	}
	
//	public void setMVP(Matrix4f MVP)
//	{
//		for(interface_actor a : actors)
//		{a.setMVP(MVP);}
//	}
	
	public void setMaxStepDistance(int dist)
	{ for(interface_actor a:actors)
		{ a.setMaxStepDistance(dist);}
	}	
	
	public void setPlayer(interface_actor a)
	{ player = a;}
	
	public void testResetVertices()
	{
		for(interface_actor a : actors)
		{
			a.testResetVertices();
		}
	}
}
