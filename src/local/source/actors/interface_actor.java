package local.source.actors;




import org.joml.*;

import local.source.interface_engageable;
import local.source.interface_step_collisionable;
import local.source.attacks.interface_attackable;

// an actor is anything other than the landscape that can be drawn in the 3d part of the game
public interface interface_actor extends interface_attackable, interface_step_collisionable, interface_engageable{

//	public String actorName = "interface_actor";
//	public String className = "interface_actor";
	
	public void draw();	
	
//	public String getActorName();
	
	public Vector3f getCenterXYZ();
	
//	public String getClassName();
		
	public Vector3f getDiameterXYZ();
	
	public float getHPPercentageAsFloatZeroToOne();
	
	public Vector3f getStandingXYZ();
	
	public Vector3f getXYZ();	
	
	public void move(String position);
	
/*	public void moveAttack();
	public void moveEngage();
	public void moveJumpBackward();
	public void moveJumpForward();
	public void moveJumpLeftward();
	public void moveJumpRightward();
	public void moveJumpUpward();
	public void moveRunBackward();
	public void moveRunForward();
	public void moveRunLeftward();
	public void moveRunRightward();
	public void moveSquat();
	public void moveStand();
	public void moveSwimBackward();
	public void moveSwimDive();
	public void moveSwimForward();
	public void moveSwimLeftward();
	public void moveSwimRightward();
	public void moveWalkBackward();
	public void moveWalkForward();
	public void moveWalkLeftward();
	public void moveWalkRightward();
*/	
	
	public void registerWithActorManager();
	
	public void setActorName(String n);
	
	public void setFacing(float x, float y, float z);
	
	public void setHP(int h);
	
	public void setMaxStepDistance(int dist);
		
//	public void setMVP(Matrix4f mvp);
	
	public void testResetVertices();
}
