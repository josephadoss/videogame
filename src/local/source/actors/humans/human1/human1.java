package local.source.actors.humans.human1;



import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL32.*;
//import static org.lwjgl.opengl.GL40.*;
//import static org.lwjgl.opengl.GL45.*;

import java.nio.*;

import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;

import local.source.damage;
import local.source.manager_engageables;
import local.source.manager_mvp;
import local.source.manager_step_collisions;
import local.source.actors.interface_actor;
import local.source.actors.manager_actors;
import local.source.attacks.manager_attackables;
import local.source.worlds.manager_land;

import org.joml.Quaternionf;
import org.joml.Vector3f;

public class human1 //implements interface_actor 
{
	
	
	Vector3f centerXYZ = new Vector3f();
	Vector3f diameterXYZ = new Vector3f();
	Vector3f engageableDiameterXYZ = new Vector3f();

	

	public String actorName = "human1 default";
	public String className = "human1";

	// don't use width and length, make all vertices relative to heightZ

	manager_land lManage = manager_land.getInstance();
	manager_actors aManage = manager_actors.getInstance();
	float stepUpHeight = 1f;
	float stepDownHeight = 2f;

	enum bodyStates {
		standing, firstStep, walkingRForward, walkingLForward, walkingRForwardToStanding, walkingLForwardToStanding,
		// standToSquat, squatToStand,
		squating, punching, kicking
	};

	boolean showConsoleOutput = false;

	bodyStates bodyStateOld = bodyStates.standing;
	bodyStates bodyStateNew = bodyStates.standing;
	boolean isStateChange = false;

	int actionStart = (int) System.currentTimeMillis();
	int timeToCompleteStep = 1000;// milliseconds
	int timeToCompleteReturnToStanding = 1000;
	int timeToCompleteSquat = 1000;
	int timeToCompletePunch = 200;
	int timeToCompleteKick = 200;

	float headHeight = 0.125f;
	float halfHeadHeight = headHeight / 2.0f;
	float headSize = 1f / 8f / 2f;

	int maxStepDistance = -1;

	float[] jointsVerticesKick = { 0f, 0f, .5f, // crotch 0
			0f, 0f, .625f, // navel 1
			0f, 0f, .75f, // nip line 2
			0f, 0f, .875f, // neck center 3
			0f, 0f, .9375f, // head center 4
			0f, 0f, 1f, // head top 5
			0f, halfHeadHeight, .5f, // left hip 6
			0f, -halfHeadHeight, .5f, // right hip 7
			0f, halfHeadHeight, .25f, // left knee 8
			.25f, -halfHeadHeight, .5f, // right knee 9
			0f, halfHeadHeight, halfHeadHeight / 4f, // left ankle 10
			.5f - halfHeadHeight / 4f, -halfHeadHeight, .5f, // right ankle 11
			headHeight, halfHeadHeight, 0f, // left foot end 12
			.5f, -halfHeadHeight, .5f + headHeight, // right foot end 13
			0f, headHeight, 0.8125f, // left shoulder 14
			0f, -headHeight, 0.8125f, // right shoulder 15
			0f, headHeight, .625f, // left elbow 16
			0f, -headHeight, .625f, // right elbow 17
			0f, headHeight, 0.5f, // left wrist 18
			0f, -headHeight, 0.5f, // right wrist 19
			0f, headHeight, 0.5f - halfHeadHeight / 4f, // left hand end 20
			0f, -headHeight, 0.5f - halfHeadHeight / 4f // right hand end 21
	};

	float[] jointsVerticesPunch = { 0f, 0f, .5f, // crotch 0
			0f, 0f, .625f, // navel 1
			0f, 0f, .75f, // nip line 2
			0f, 0f, .875f, // neck center 3
			0f, 0f, .9375f, // head center 4
			0f, 0f, 1f, // head top 5
			0f, halfHeadHeight, .5f, // left hip 6
			0f, -halfHeadHeight, .5f, // right hip 7
			0f, halfHeadHeight, .25f, // left knee 8
			0f, -halfHeadHeight, .25f, // right knee 9
			0f, halfHeadHeight, halfHeadHeight / 4f, // left ankle 10
			0f, -halfHeadHeight, halfHeadHeight / 4f, // right ankle 11
			headHeight, halfHeadHeight, 0f, // left foot end 12
			headHeight, -halfHeadHeight, 0f, // right foot end 13
			0f, headHeight, 0.8125f, // left shoulder 14
			0f, -headHeight, 0.8125f, // right shoulder 15
			0f, headHeight, .625f, // left elbow 16
			headHeight, -headHeight, 0.8125f, // right elbow 17
			0f, headHeight, 0.5f, // left wrist 18
			headHeight * 2f, -headHeight, 0.8125f, // right wrist 19
			0f, headHeight, 0.5f - halfHeadHeight / 4f, // left hand end 20
			headHeight * 2f + halfHeadHeight / 4f, -headHeight, 0.8125f// right
																		// hand
																		// end
																		// 21
	};

	float squatCrotchZ = .3f;
	float[] jointsVerticesSquat = { 0f, 0f, squatCrotchZ, // crotch 0
			0f, 0f, squatCrotchZ + headHeight, // navel 1
			0f, 0f, squatCrotchZ + 2f * headHeight, // nip line 2
			0f, 0f, squatCrotchZ + 3f * headHeight, // neck center 3
			0f, 0f, squatCrotchZ + 3.5f * headHeight, // head center 4
			0f, 0f, squatCrotchZ + 4f * headHeight, // head top 5
			0f, halfHeadHeight, squatCrotchZ, // left hip 6
			0f, -halfHeadHeight, squatCrotchZ, // right hip 7
			headHeight, halfHeadHeight * 1.25f, squatCrotchZ / 2f, // left knee
																	// 8
			headHeight, -halfHeadHeight * 1.25f, squatCrotchZ / 2f, // right
																	// knee 9
			0f, halfHeadHeight, halfHeadHeight / 4f, // left ankle 10
			0f, -halfHeadHeight, halfHeadHeight / 4f, // right ankle 11
			headHeight, halfHeadHeight, 0f, // left foot end 12
			headHeight, -halfHeadHeight, 0f, // right foot end 13
			0f, headHeight, squatCrotchZ + headHeight * 2.75f, // left shoulder
																// 14
			0f, -headHeight, squatCrotchZ + headHeight * 2.75f, // right
																// shoulder 15
			0f, headHeight, squatCrotchZ + headHeight, // left elbow 16
			0f, -headHeight, squatCrotchZ + headHeight, // right elbow 17
			0f, headHeight, squatCrotchZ, // left wrist 18
			0f, -headHeight, squatCrotchZ, // right wrist 19
			0f, headHeight, squatCrotchZ - halfHeadHeight / 4f, // left hand end
																// 20
			0f, -headHeight, squatCrotchZ - halfHeadHeight / 4f // right hand
																// end 21
	};

	float[] jointsVerticesStand = { 0f, 0f, .5f, // crotch 0
			0f, 0f, .625f, // navel 1
			0f, 0f, .75f, // nip line 2
			0f, 0f, .875f, // neck center 3
			0f, 0f, .9375f, // head center 4
			0f, 0f, 1f, // head top 5
			0f, halfHeadHeight, .5f, // left hip 6
			0f, -halfHeadHeight, .5f, // right hip 7
			0f, halfHeadHeight, .25f, // left knee 8
			0f, -halfHeadHeight, .25f, // right knee 9
			0f, halfHeadHeight, halfHeadHeight / 4f, // left ankle 10
			0f, -halfHeadHeight, halfHeadHeight / 4f, // right ankle 11
			headHeight, halfHeadHeight, 0f, // left foot end 12
			headHeight, -halfHeadHeight, 0f, // right foot end 13
			0f, headHeight, 0.8125f, // left shoulder 14
			0f, -headHeight, 0.8125f, // right shoulder 15
			0f, headHeight, .625f, // left elbow 16
			0f, -headHeight, .625f, // right elbow 17
			0f, headHeight, 0.5f, // left wrist 18
			0f, -headHeight, 0.5f, // right wrist 19
			0f, headHeight, 0.5f - halfHeadHeight / 4f, // left hand end 20
			0f, -headHeight, 0.5f - halfHeadHeight / 4f // right hand end 21
	};

	float[] jointsVerticesStepLFootForward = { 0f, 0f, .5f, // crotch 0
			0f, 0f, .625f, // navel 1
			0f, 0f, .75f, // nip line 2
			0f, 0f, .875f, // neck center 3
			0f, 0f, .9375f, // head center 4
			0f, 0f, 1f, // head top 5
			0f, halfHeadHeight, .5f, // left hip 6
			0f, -halfHeadHeight, .5f, // right hip 7
			halfHeadHeight / 2f, halfHeadHeight, .25f, // left knee 8
			-halfHeadHeight / 2f, -halfHeadHeight, .25f, // right knee 9
			halfHeadHeight, halfHeadHeight, halfHeadHeight / 4f, // left ankle
																	// 10
			-halfHeadHeight * 3f, -halfHeadHeight, halfHeadHeight / 4f, // right
																		// ankle
																		// 11
			3f * halfHeadHeight, halfHeadHeight, 0f, // left foot end 12
			-halfHeadHeight, -halfHeadHeight, 0f, // right foot end 13
			0f, headHeight, 0.8125f, // left shoulder 14
			0f, -headHeight, 0.8125f, // right shoulder 15
			0f, headHeight, .625f, // left elbow 16
			0f, -headHeight, .625f, // right elbow 17
			0f, headHeight, 0.5f, // left wrist 18
			0f, -headHeight, 0.5f, // right wrist 19
			0f, headHeight, 0.5f - halfHeadHeight / 4f, // left hand end 20
			0f, -headHeight, 0.5f - halfHeadHeight / 4f // right hand end 21
	};

	float[] jointsVerticesStepRFootForward = { 0f, 0f, .5f, // crotch 0
			0f, 0f, .625f, // navel 1
			0f, 0f, .75f, // nip line 2
			0f, 0f, .875f, // neck center 3
			0f, 0f, .9375f, // head center 4
			0f, 0f, 1f, // head top 5
			0f, halfHeadHeight, .5f, // left hip 6
			0f, -halfHeadHeight, .5f, // right hip 7
			-halfHeadHeight / 2f, halfHeadHeight, .25f, // left knee 8
			halfHeadHeight / 2f, -halfHeadHeight, .25f, // right knee 9
			-halfHeadHeight * 3f, halfHeadHeight, halfHeadHeight / 4f, // left
																		// ankle
																		// 10
			halfHeadHeight, -halfHeadHeight, halfHeadHeight / 4f, // right
																	// ankle
																	// 11
			-halfHeadHeight, halfHeadHeight, 0f, // left foot end 12
			3f * halfHeadHeight, -halfHeadHeight, 0f, // right foot end 13
			0f, headHeight, 0.8125f, // left shoulder 14
			0f, -headHeight, 0.8125f, // right shoulder 15
			0f, headHeight, .625f, // left elbow 16
			0f, -headHeight, .625f, // right elbow 17
			0f, headHeight, 0.5f, // left wrist 18
			0f, -headHeight, 0.5f, // right wrist 19
			0f, headHeight, 0.5f - halfHeadHeight / 4f, // left hand end 20
			0f, -headHeight, 0.5f - halfHeadHeight / 4f // right hand end 21
	};

	float[] jointsVerticesDraw = jointsVerticesStand.clone();
	float[] jointsVerticesDrawOld = jointsVerticesStand.clone();
	float[] jointsVerticesDrawCurrent = jointsVerticesStand.clone();
	float[] jointsVerticesDrawNew = jointsVerticesStand.clone();

	byte[] jointsIndicesLines = { 0, 1, // crotch to navel
			1, 2, // navel to nip line
			2, 3, // nipples to neck center
			3, 4, // neck center to head center
			4, 5, // head center to head top
			0, 6, // crotch to left hip
			6, 8, // left hip to left knee
			8, 10, // left knee to left ankle
			10, 12, // left ankle to left foot end

			0, 7, // crotch to right hip
			7, 9, // right hip to right knee
			9, 11, // right knee to right ankle
			11, 13, // right ankle to right foot end

			2, 14, // nip line to left shoulder
			14, 16, // left shoulder to left elbow
			16, 18, // left elbow to left wrist
			18, 20, // left wrist to left hand end

			2, 15, // nip line to right shoulder
			15, 17, // right shoulder to right elbow
			17, 19, // right elbow to right wrist
			19, 21 // right wrist to right hand end

	};

	int jointsIndicesLinesCount;// = jointsIndicesLines.length;

	float jointsRed = 1f;
	float jointsBlue = 1f;
	float jointsGreen = 1f;

	public float skinRed = 1f;
	public float skinGreen = 0f;
	public float skinBlue = 0f;
	public float skinAlpha = 1.0f;

	public float skinHairRed = 0f;
	public float skinHairGreen = 0f;
	public float skinHairBlue = 1f;
	public float skinHairAlpha = 1.0f;

	int jointsVAOID = -1;
	int jointsVBOID = -1;
	int jointsVBOIID = -1;

	public int skinVBOID = -1;
	public int skinVAOID = -1;
	public int skinVBOIID = -1;

	public int skinHairVBOID = -1;
	public int skinHairVAOID = -1;
	public int skinHairVBOIID = -1;

	public int jointsGLMVPUniformLocation;
	public int jointsGLRGBAUniformLocation;
	public FloatBuffer jointsFB = BufferUtils.createFloatBuffer(16);

	public int skinGLMVPUniformLocation;
	public int skinGLRGBAUniformLocation;
	public FloatBuffer skinFB = BufferUtils.createFloatBuffer(16);

	public int skinHairGLMVPUniformLocation;
	public int skinHairGLRGBAUniformLocation;
	public FloatBuffer skinHairFB = BufferUtils.createFloatBuffer(16);

	public int jointsProgramId;
	public int jointsVertexShaderId;
	public int jointsFragmentShaderId;

	public int skinProgramId;
	public int skinVertexShaderId;
	public int skinFragmentShaderId;
	boolean isJointsIndexBound = false;

	public int skinHairProgramId;
	public int skinHairVertexShaderId;
	public int skinHairFragmentShaderId;

	//public Matrix4f MVP;

	// public boolean isDrawChange = true;
	public boolean isDrawSkin = true;
	public boolean isDrawJoints = true;

	public float x, y, z = 0.0f;
	public float heightZ = 2.0f;
	public float widthY = 1.0f;
	public float lengthX = 1.0f;
	public float turnDegree = 5.0f;
	public float degreeRotateZ = 0.0f;
	public float degreeRotateY = 0.0f;
	public float degreeRotateX = 0.0f;
	public float moveDistance = .0003f;// * headHeight;
	float distanceToExtendBeyondHandEnd = .3f; // this shouldn't exist, but I
												// need it to cover up the joint
												// cube of the hands
	float armWidth = 327f;

	// float[] skinVertices ;//= { .5f, .5f, 1f, -.5f, .5f, 1f, -.5f, -.5f, 1f,
	// .5f, -.5f, 1f, .5f, .5f, 0f, -.5f, .5f, 0f,
	// -.5f, -.5f, 0f, .5f, -.5f, 0f };
	float[] skinVerticesDraw;// = { .5f, .5f, 1f, -.5f, .5f, 1f, -.5f, -.5f, 1f,
								// .5f, -.5f, 1f, .5f, .5f, 0f, -.5f, .5f,
	// 0f, -.5f, -.5f, 0f, .5f, -.5f, 0f };
	byte[] skinIndices;// = new byte[] { 0, 1, 2, 0, 2, 3, 1, 5, 6, 1, 6, 2, 5,
						// 4, 7, 5, 7, 6, 4, 0, 3, 4, 3, 7, 0, 4, 5,
	// 0, 5, 1 };
	int skinIndicesCount;// = skinIndices.length;
	boolean isSkinIndexBound = false;

	// float[] skinHairVerticesDraw;
	// byte[] skinHairIndices;
	int skinHairIndicesCount;
	boolean isSkinHairIndexBound = false;

	// stop doing this
	// make these indices to the start of this data up in the skinVerticesDraw
	// and skinIndices arrays
	// then just crunch the relevant parts of that one array each time, instead
	// of making multiple ones and looping through them all multiple times each
	// draw

	float[] skinFaceVerticesDraw = new float[3 * 6];
	byte[] skinFaceIndices = new byte[] { 0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5

			// 0,2,1,
			// 0,3,2,
			// 0,4,3,
			// 0,5,4
	};

	float[] skinHairVerticesDraw = new float[3 * 7];
	byte[] skinHairIndices = new byte[] { 0, 1, 3, 1, 2, 3, 1, 6, 2, 2, 6, 5, 3, 2, 5, 3, 5, 4

			// 0,2,1,
			// 0,3,2,
			// 0,4,3,
			// 0,5,4
	};

	float[] skinRElbowToWristVerticesDraw = new float[3 * 17];
	byte[] skinRElbowToWristIndices = new byte[] { 0, 1, 2, 0, 2, 3, 1, 5, 6, 1, 6, 2, 5, 4, 7, 5, 7, 6, 4, 0, 3, 4, 3, 7, 0, 4, 5, 0, 5, 1, 8, 9, 12, 8, 13, 9, 8, 16, 13, 8, 12, 16, 12, 9, 10, 12, 10, 11, 9, 13, 14, 9, 14, 10, 13, 16, 15, 13, 15, 14, 16, 12, 11, 16, 11, 15 };

	float[] skinRShoulderToElbowVerticesDraw = new float[3 * 17];
	byte[] skinRShoulderToElbowIndices = new byte[] { 0, 1, 2, 0, 2, 3, 1, 5, 6, 1, 6, 2, 5, 4, 3, 5, 3, 6, 4, 0, 3, 4, 3, 7, 3, 2, 6, 3, 6, 7, 12, 14, 8, 14, 13, 8, 13, 9, 8, 9, 15, 8, 15, 16, 8, 16, 12, 8, 10, 9, 13, 10, 15, 9, 11, 14, 12, 11, 12, 16, 11, 10, 13, 11, 13, 14, 10, 11, 16, 11, 16, 15 };

	float[] skinRWristToHandEndVerticesDraw = new float[3 * 8];
	byte[] skinRWristToHandEndIndices = new byte[] { 0, 1, 2, 0, 2, 3, 1, 5, 6, 1, 6, 2, 5, 4, 7, 5, 7, 6, 4, 0, 3, 4, 3, 7, 0, 4, 5, 0, 5, 1, 3, 2, 6, 3, 6, 7 };

	float[] skinLElbowToWristVerticesDraw = new float[3 * 17];
	byte[] skinLElbowToWristIndices = new byte[] { 0, 1, 2, 0, 2, 3, 1, 5, 6, 1, 6, 2, 5, 4, 7, 5, 7, 6, 4, 0, 3, 4, 3, 7, 0, 4, 5, 0, 5, 1, 8, 9, 12, 8, 13, 9, 8, 16, 13, 8, 12, 16, 12, 9, 10, 12, 10, 11, 9, 13, 14, 9, 14, 10, 13, 16, 15, 13, 15, 14, 16, 12, 11, 16, 11, 15 };

	float[] skinLShoulderToElbowVerticesDraw = new float[3 * 17];
	byte[] skinLShoulderToElbowIndices = new byte[] { 0, 1, 2, 0, 2, 3, 1, 5, 6, 1, 6, 2, 5, 4, 3, 5, 3, 6, 4, 0, 3, 4, 3, 7, 3, 2, 6, 3, 6, 7, 12, 14, 8, 14, 13, 8, 13, 9, 8, 9, 15, 8, 15, 16, 8, 16, 12, 8, 10, 9, 13, 10, 15, 9, 11, 14, 12, 11, 12, 16, 11, 10, 13, 11, 13, 14, 10, 11, 16, 11, 16, 15 };

	float[] skinLWristToHandEndVerticesDraw = new float[3 * 8];
	byte[] skinLWristToHandEndIndices = new byte[] { 0, 1, 2, 0, 2, 3, 1, 5, 6, 1, 6, 2, 5, 4, 7, 5, 7, 6, 4, 0, 3, 4, 3, 7, 0, 4, 5, 0, 5, 1, 3, 2, 6, 3, 6, 7 };

	float[] skinNeckVerticesDraw = new float[3 * 8];
	byte[] skinNeckIndices = new byte[] { 0, 1, 2, 0, 2, 3, 1, 5, 6, 1, 6, 2, 5, 4, 7, 5, 7, 6, 4, 0, 3, 4, 3, 7 };

	float[] skinNeckToNipLineVerticesDraw = new float[3 * 9];
	byte[] skinNeckToNipLineIndices = new byte[] { 0, 1, 2, 0, 2, 3, 1, 5, 6, 1, 6, 2, 5, 4, 7, 5, 7, 6, 4, 0, 3, 4, 3, 7, 4, 5, 1, 4, 1, 0, 3, 2, 8, 2, 6, 8, 6, 7, 8, 7, 3, 8 };

	float[] skinTummyVerticesDraw = new float[3 * 8];
	byte[] skinTummyIndices = new byte[] { 0, 3, 1, 1, 3, 2, 0, 4, 7, 0, 7, 3, 4, 5, 6, 4, 6, 7, 5, 1, 2, 5, 2, 6 };

	float[] skinPelvisVerticesDraw = new float[3 * 9];
	byte[] skinPelvisIndices = new byte[] { 0, 4, 3, 0, 3, 1, 0, 1, 6, 0, 6, 5, 5, 6, 7, 5, 7, 8, 4, 8, 7, 4, 7, 3, 0, 5, 8, 0, 8, 4, 1, 3, 2, 3, 7, 2, 7, 6, 2, 6, 1, 2 };

	float[] skinLHipToKneeVerticesDraw = new float[3 * 17];
	byte[] skinLHipToKneeIndices = new byte[] { 0, 1, 2, 0, 2, 3, 1, 5, 6, 1, 6, 2, 5, 4, 3, 5, 3, 6, 4, 0, 3, 4, 3, 7, 3, 2, 6, 3, 6, 7, 12, 14, 8, 14, 13, 8, 13, 9, 8, 9, 15, 8, 15, 16, 8, 16, 12, 8, 10, 9, 13, 10, 15, 9, 11, 14, 12, 11, 12, 16, 11, 10, 13, 11, 13, 14, 10, 11, 16, 11, 16, 15 };

	float[] skinRHipToKneeVerticesDraw = new float[3 * 17];
	byte[] skinRHipToKneeIndices = new byte[] { 0, 1, 2, 0, 2, 3, 1, 5, 6, 1, 6, 2, 5, 4, 3, 5, 3, 6, 4, 0, 3, 4, 3, 7, 3, 2, 6, 3, 6, 7, 12, 14, 8, 14, 13, 8, 13, 9, 8, 9, 15, 8, 15, 16, 8, 16, 12, 8, 10, 9, 13, 10, 15, 9, 11, 14, 12, 11, 12, 16, 11, 10, 13, 11, 13, 14, 10, 11, 16, 11, 16, 15 };

	float[] skinRKneeToAnkleVerticesDraw = new float[3 * 17];
	byte[] skinRKneeToAnkleIndices = new byte[] { 0, 1, 2, 0, 2, 3, 1, 5, 6, 1, 6, 2, 5, 4, 3, 5, 3, 6, 4, 0, 3, 4, 3, 7, 3, 2, 6, 3, 6, 7, 12, 14, 8, 14, 13, 8, 13, 9, 8, 9, 15, 8, 15, 16, 8, 16, 12, 8, 10, 9, 13, 10, 15, 9, 11, 14, 12, 11, 12, 16, 11, 10, 13, 11, 13, 14, 10, 11, 16, 11, 16, 15 };

	float[] skinLKneeToAnkleVerticesDraw = new float[3 * 17];
	byte[] skinLKneeToAnkleIndices = new byte[] { 0, 1, 2, 0, 2, 3, 1, 5, 6, 1, 6, 2, 5, 4, 3, 5, 3, 6, 4, 0, 3, 4, 3, 7, 3, 2, 6, 3, 6, 7, 12, 14, 8, 14, 13, 8, 13, 9, 8, 9, 15, 8, 15, 16, 8, 16, 12, 8, 10, 9, 13, 10, 15, 9, 11, 14, 12, 11, 12, 16, 11, 10, 13, 11, 13, 14, 10, 11, 16, 11, 16, 15 };

	float[] skinRAnkleToFootEndVerticesDraw = new float[3 * 8];
	byte[] skinRAnkleToFootEndIndices = new byte[] { 0, 1, 2, 0, 2, 3, 1, 5, 6, 1, 6, 2, 5, 4, 7, 5, 7, 6, 4, 0, 3, 4, 3, 7, 0, 4, 5, 0, 5, 1, 3, 2, 6, 3, 6, 7 };

	float[] skinLAnkleToFootEndVerticesDraw = new float[3 * 8];
	byte[] skinLAnkleToFootEndIndices = new byte[] { 0, 1, 2, 0, 2, 3, 1, 5, 6, 1, 6, 2, 5, 4, 7, 5, 7, 6, 4, 0, 3, 4, 3, 7, 0, 4, 5, 0, 5, 1, 3, 2, 6, 3, 6, 7 };

	public human1() {
		// act = new actionHuman1();
		setRGB(.486f, .988f, 0f);
		setHeight(6f);

		registerWithActorManager();
		registerWithAttackableManager();
		registerWithEngageableManager();
		registerWithStepCollisionManager();

		this.glConstruct();
	}

	public float[] actionJoints() {
		int timeToComplete = 1000;
		// float[] oldJoints = jointsVerticesStand.clone();
		// float[] newJoints= jointsVerticesStand.clone();
		float[] vertices;
		float percentDone = 0f;
		int now = (int) System.currentTimeMillis();

		switch (bodyStateNew) {
		case standing:
			timeToComplete = timeToCompleteReturnToStanding;
			if (isStateChange) {
				jointsVerticesDrawOld = jointsVerticesDrawCurrent.clone();
				jointsVerticesDrawNew = jointsVerticesStand;
			}
			break;
		case firstStep:
			timeToComplete = timeToCompleteStep;
			if (isStateChange) {
				jointsVerticesDrawNew = jointsVerticesStepLFootForward;
				jointsVerticesDrawOld = jointsVerticesStand;
			}
			break;
		case walkingLForward:
			timeToComplete = timeToCompleteStep;
			if (isStateChange) {
				jointsVerticesDrawNew = jointsVerticesStepLFootForward;
				jointsVerticesDrawOld = jointsVerticesStepRFootForward;
			}
			break;
		case walkingRForward:
			timeToComplete = timeToCompleteStep;
			if (isStateChange) {
				jointsVerticesDrawNew = jointsVerticesStepRFootForward;
				jointsVerticesDrawOld = jointsVerticesStepLFootForward;
			}
			break;
		case walkingLForwardToStanding:
			timeToComplete = timeToCompleteReturnToStanding;
			if (isStateChange) {
				jointsVerticesDrawNew = jointsVerticesStand;
				jointsVerticesDrawOld = jointsVerticesStepLFootForward;
			}
			break;
		case walkingRForwardToStanding:
			timeToComplete = timeToCompleteReturnToStanding;
			if (isStateChange) {
				jointsVerticesDrawNew = jointsVerticesStand;
				jointsVerticesDrawOld = jointsVerticesStepRFootForward;
			}
			break;
		// case standToSquat:
		// timeToComplete = timeToCompleteSquat;
		// jointsVerticesDrawNew = jointsVerticesSquat;
		// jointsVerticesDrawOld = jointsVerticesStand;
		// break;
		// case squatToStand:
		// timeToComplete = timeToCompleteReturnToStanding;
		// jointsVerticesDrawNew = jointsVerticesStand;
		// jointsVerticesDrawOld = jointsVerticesSquat;
		// break;
		case squating:
			timeToComplete = timeToCompleteSquat;
			if (isStateChange) {
				jointsVerticesDrawOld = jointsVerticesDrawCurrent.clone();
				jointsVerticesDrawNew = jointsVerticesSquat;
			}
			// jointsVerticesDrawOld = jointsVerticesSquat;
			break;
		case punching:
			timeToComplete = timeToCompletePunch;
			if (isStateChange) {
				jointsVerticesDrawNew = jointsVerticesPunch;
				jointsVerticesDrawOld = jointsVerticesDrawCurrent.clone();
			}
			break;
		case kicking:
			timeToComplete = timeToCompleteKick;
			if (isStateChange) {
				jointsVerticesDrawNew = jointsVerticesKick;
				jointsVerticesDrawOld = jointsVerticesDrawCurrent.clone();
			}
			break;
		default:
			// timeToComplete = 1000;
			// newJoints = jointsVerticesStand.clone();
			// jointsVerticesDrawNew = jointsVerticesStand;
			break;
		}
		isStateChange = false;

		int timeEnd = actionStart + timeToComplete;
		// percentDone = (float)((timeEnd-actionStart) / timeToComplete);
		percentDone = (float) ((float) (now - actionStart) / (float) timeToComplete);
		// if()
		// {}

		if (percentDone > 1f) {

			percentDone = 1f;

			bodyStates bnew = bodyStateNew;
			switch (bodyStateNew) {

			case firstStep:
				bnew = bodyStates.walkingRForward;
				actionStart = now;
				isStateChange = true;
				break;
			case walkingLForward:
				bnew = bodyStates.walkingRForward;
				actionStart = now;
				isStateChange = true;
				break;
			case walkingRForward:
				bnew = bodyStates.walkingLForward;
				actionStart = now;
				isStateChange = true;
				break;
			case walkingLForwardToStanding:
				bnew = bodyStates.standing;
				actionStart = now;
				isStateChange = true;
				break;
			case walkingRForwardToStanding:
				bnew = bodyStates.standing;
				actionStart = now;
				isStateChange = true;
				break;
			case squating:
				bnew = bodyStates.squating;
				bodyStateOld = bodyStates.squating;
				break;
			default:
				break;
			}
			bodyStateNew = bnew;
		}

		vertices = new float[jointsVerticesDrawOld.length];
		for (int i = 0; i < jointsVerticesDrawOld.length; i++) {
			float val = (jointsVerticesDrawNew[i] - jointsVerticesDrawOld[i]) * percentDone;
			val = jointsVerticesDrawOld[i] + val;
			vertices[i] = val;
			jointsVerticesDrawCurrent[i] = val;
		}

		return vertices;
	}

	FloatBuffer verticesBufferJoints;

	public void bindJointsVertexData() {
		// if (isDrawChange)
		{

			if (isDrawJoints) {
				if (verticesBufferJoints == null) {
					verticesBufferJoints = BufferUtils.createFloatBuffer(jointsVerticesDraw.length);
				}
				verticesBufferJoints.put(jointsVerticesDraw).flip();

				if (jointsVAOID == -1) {
					jointsVAOID = glGenVertexArrays();
				}
				glBindVertexArray(jointsVAOID);

				if (jointsVBOID == -1) {
					jointsVBOID = glGenBuffers();
				}
				glBindBuffer(GL_ARRAY_BUFFER, jointsVBOID);
				glBufferData(GL_ARRAY_BUFFER, verticesBufferJoints, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);

				if (!isJointsIndexBound) {
					jointsIndicesLinesCount = jointsIndicesLines.length;
					ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(jointsIndicesLinesCount);
					indicesBuffer.put(jointsIndicesLines);
					indicesBuffer.flip();

					jointsVBOIID = glGenBuffers();
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, jointsVBOIID);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

					isJointsIndexBound = true;
				}
			}

			// isDrawChange = false;
		}
	}

	FloatBuffer verticesBufferSkin;

	public void bindSkinVertexData() {
		{
			if (isDrawSkin) {

				if (verticesBufferSkin == null) {
					verticesBufferSkin = BufferUtils.createFloatBuffer(skinVerticesDraw.length);
				}
				// println("bindSkinVertexData skinVerticesDraw.length:" +
				// skinVerticesDraw.length);
				verticesBufferSkin.put(skinVerticesDraw).flip();

				if (skinVAOID == -1) {
					skinVAOID = glGenVertexArrays();
				}
				glBindVertexArray(skinVAOID);

				if (skinVBOID == -1) {
					skinVBOID = glGenBuffers();
				}
				glBindBuffer(GL_ARRAY_BUFFER, skinVBOID);
				glBufferData(GL_ARRAY_BUFFER, verticesBufferSkin, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);

				if (!isSkinIndexBound) {
					skinIndicesCount = skinIndices.length;
					ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(skinIndicesCount);
					indicesBuffer.put(skinIndices);
					indicesBuffer.flip();

					skinVBOIID = glGenBuffers();
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinVBOIID);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

					isSkinIndexBound = true;
				}
			}
		}
	}

	FloatBuffer verticesBufferSkinHair;

	public void bindSkinHairVertexData() {
		{
			if (isDrawSkin) {
				if (verticesBufferSkinHair == null) {
					verticesBufferSkinHair = BufferUtils.createFloatBuffer(skinHairVerticesDraw.length);
				}
				// println("bindSkinVertexData skinHairVerticesDraw.length:" +
				// skinHairVerticesDraw.length);
				verticesBufferSkinHair.put(skinHairVerticesDraw).flip();

				if (skinHairVAOID == -1) {
					skinHairVAOID = glGenVertexArrays();
				}
				glBindVertexArray(skinHairVAOID);

				if (skinHairVBOID == -1) {
					skinHairVBOID = glGenBuffers();
				}
				glBindBuffer(GL_ARRAY_BUFFER, skinHairVBOID);
				glBufferData(GL_ARRAY_BUFFER, verticesBufferSkinHair, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);

				if (!isSkinHairIndexBound) {
					skinHairIndicesCount = skinHairIndices.length;
					ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(skinHairIndicesCount);
					indicesBuffer.put(skinHairIndices);
					indicesBuffer.flip();

					skinHairVBOIID = glGenBuffers();
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinHairVBOIID);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

					isSkinHairIndexBound = true;
				}
			}
		}
	}

	public void bindVertexData() {
		if (isDrawJoints) {
			bindJointsVertexData();
		}

		if (isDrawSkin) {
			bindSkinVertexData();
			bindSkinHairVertexData();
		}
	}

	public void calculateJointsVertices() {
		// System.out.println("human1.move(); called");

		double radian = Math.toRadians(degreeRotateZ);
		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		float fX, fY, fZ;

		int indexCount = 0;
		float[] vertices = {};// .clone();// act.getVertices();

		jointsVerticesDraw = actionJoints();
		vertices = jointsVerticesDraw;

		for (int i = vertices.length; indexCount < i; indexCount += 3) {

			fX = vertices[indexCount];// * lengthX/* X */;
			fY = vertices[indexCount + 1];// * widthY/* Y */;
			fZ = vertices[indexCount + 2];// * heightZ/* Z */;
			setVertices(indexCount, cosRadian, sinRadian, fX, fY, fZ, vertices);
		}

		if (isDrawSkin) {
			// calculate skin vertices based off the jointsVerticesDraw array
			calculateSkinRElbowToWrist();
			calculateSkinLElbowToWrist();
			calculateSkinLWristToHandEnd();
			calculateSkinRWristToHandEnd();
			calculateSkinLShoulderToElbow();
			calculateSkinRShoulderToElbow();
			calculateSkinNeckToNipLine();
			calculateSkinNeck();
			calculateSkinFace();
			calculateSkinTummy();
			calculateSkinPelvis();
			calculateSkinLHipToKnee();
			calculateSkinRHipToKnee();
			calculateSkinRKneeToAnkle();
			calculateSkinLKneeToAnkle();
			calculateSkinRAnkleToFootEnd();
			calculateSkinLAnkleToFootEnd();

			int vertLength = skinLElbowToWristVerticesDraw.length;
			int indLength = skinLElbowToWristIndices.length;
			vertLength += skinRElbowToWristVerticesDraw.length;
			// System.out.println("vertLength:" + vertLength);
			indLength += skinRElbowToWristIndices.length;
			// System.out.println("indLength:" + indLength);
			vertLength += skinLWristToHandEndVerticesDraw.length;
			// System.out.println("vertLength:" + vertLength);
			indLength += skinLWristToHandEndIndices.length;
			// System.out.println("indLength:" + indLength);
			vertLength += skinRWristToHandEndVerticesDraw.length;
			indLength += skinRWristToHandEndIndices.length;

			vertLength += skinLShoulderToElbowVerticesDraw.length;
			indLength += skinLShoulderToElbowIndices.length;

			vertLength += skinRShoulderToElbowVerticesDraw.length;
			indLength += skinRShoulderToElbowIndices.length;

			vertLength += skinNeckToNipLineVerticesDraw.length;
			indLength += skinNeckToNipLineIndices.length;

			vertLength += skinNeckVerticesDraw.length;
			indLength += skinNeckIndices.length;

			vertLength += skinFaceVerticesDraw.length;
			indLength += skinFaceIndices.length;

			vertLength += skinTummyVerticesDraw.length;
			indLength += skinTummyIndices.length;

			vertLength += skinPelvisVerticesDraw.length;
			indLength += skinPelvisIndices.length;

			vertLength += skinLHipToKneeVerticesDraw.length;
			indLength += skinLHipToKneeIndices.length;

			vertLength += skinRHipToKneeVerticesDraw.length;
			indLength += skinRHipToKneeIndices.length;

			vertLength += skinRKneeToAnkleVerticesDraw.length;
			indLength += skinRKneeToAnkleIndices.length;

			vertLength += skinLKneeToAnkleVerticesDraw.length;
			indLength += skinLKneeToAnkleIndices.length;

			vertLength += skinRAnkleToFootEndVerticesDraw.length;
			indLength += skinRAnkleToFootEndIndices.length;

			vertLength += skinLAnkleToFootEndVerticesDraw.length;
			indLength += skinLAnkleToFootEndIndices.length;

			// !!!!!! Once you've drawn the entire person, figure out how many
			// vertices and indicies there are in total and create the arrays at
			// the top of the class
			if (skinVerticesDraw == null) {
				skinVerticesDraw = new float[vertLength];
				skinIndices = new byte[indLength];
			}
			int iVert = 0;
			int iInd = 0;
			byte indexOffset = 0;

			for (int i = 0; i < skinLElbowToWristVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinLElbowToWristVerticesDraw[i];
			}
			for (int i = 0; i < skinLElbowToWristIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinLElbowToWristIndices[i] + indexOffset);
			}
			indexOffset += skinLElbowToWristVerticesDraw.length / 3;

			for (int i = 0; i < skinRElbowToWristVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinRElbowToWristVerticesDraw[i];
			}
			for (int i = 0; i < skinRElbowToWristIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinRElbowToWristIndices[i] + indexOffset);
			}
			indexOffset += skinRElbowToWristVerticesDraw.length / 3;

			for (int i = 0; i < skinLWristToHandEndVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinLWristToHandEndVerticesDraw[i];
			}
			for (int i = 0; i < skinLWristToHandEndIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinLWristToHandEndIndices[i] + indexOffset);
			}
			indexOffset += skinLWristToHandEndVerticesDraw.length / 3;

			for (int i = 0; i < skinRWristToHandEndVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinRWristToHandEndVerticesDraw[i];
			}
			for (int i = 0; i < skinRWristToHandEndIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinRWristToHandEndIndices[i] + indexOffset);
			}
			indexOffset += skinRWristToHandEndVerticesDraw.length / 3;

			for (int i = 0; i < skinLShoulderToElbowVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinLShoulderToElbowVerticesDraw[i];
			}
			for (int i = 0; i < skinLShoulderToElbowIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinLShoulderToElbowIndices[i] + indexOffset);
			}
			indexOffset += skinLShoulderToElbowVerticesDraw.length / 3;

			for (int i = 0; i < skinRShoulderToElbowVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinRShoulderToElbowVerticesDraw[i];
			}
			for (int i = 0; i < skinRShoulderToElbowIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinRShoulderToElbowIndices[i] + indexOffset);
			}
			indexOffset += skinRShoulderToElbowVerticesDraw.length / 3;

			for (int i = 0; i < skinNeckToNipLineVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinNeckToNipLineVerticesDraw[i];
			}
			for (int i = 0; i < skinNeckToNipLineIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinNeckToNipLineIndices[i] + indexOffset);
			}
			indexOffset += skinNeckToNipLineVerticesDraw.length / 3;

			for (int i = 0; i < skinNeckVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinNeckVerticesDraw[i];
			}
			for (int i = 0; i < skinNeckIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinNeckIndices[i] + indexOffset);
			}
			indexOffset += skinNeckVerticesDraw.length / 3;

			for (int i = 0; i < skinFaceVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinFaceVerticesDraw[i];
			}
			for (int i = 0; i < skinFaceIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinFaceIndices[i] + indexOffset);
			}
			indexOffset += skinFaceVerticesDraw.length / 3;

			for (int i = 0; i < skinTummyVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinTummyVerticesDraw[i];
			}
			for (int i = 0; i < skinTummyIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinTummyIndices[i] + indexOffset);
			}
			indexOffset += skinTummyVerticesDraw.length / 3;

			for (int i = 0; i < skinPelvisVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinPelvisVerticesDraw[i];
			}
			for (int i = 0; i < skinPelvisIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinPelvisIndices[i] + indexOffset);
			}
			indexOffset += skinPelvisVerticesDraw.length / 3;

			for (int i = 0; i < skinLHipToKneeVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinLHipToKneeVerticesDraw[i];
			}
			for (int i = 0; i < skinLHipToKneeIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinLHipToKneeIndices[i] + indexOffset);
			}
			indexOffset += skinLHipToKneeVerticesDraw.length / 3;

			for (int i = 0; i < skinRHipToKneeVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinRHipToKneeVerticesDraw[i];
			}
			for (int i = 0; i < skinRHipToKneeIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinRHipToKneeIndices[i] + indexOffset);
			}
			indexOffset += skinRHipToKneeVerticesDraw.length / 3;

			for (int i = 0; i < skinRKneeToAnkleVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinRKneeToAnkleVerticesDraw[i];
			}
			for (int i = 0; i < skinRKneeToAnkleIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinRKneeToAnkleIndices[i] + indexOffset);
			}
			indexOffset += skinRKneeToAnkleVerticesDraw.length / 3;

			for (int i = 0; i < skinLKneeToAnkleVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinLKneeToAnkleVerticesDraw[i];
			}
			for (int i = 0; i < skinLKneeToAnkleIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinLKneeToAnkleIndices[i] + indexOffset);
			}
			indexOffset += skinLKneeToAnkleVerticesDraw.length / 3;

			for (int i = 0; i < skinRAnkleToFootEndVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinRAnkleToFootEndVerticesDraw[i];
			}
			for (int i = 0; i < skinRAnkleToFootEndIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinRAnkleToFootEndIndices[i] + indexOffset);
			}
			indexOffset += skinRAnkleToFootEndVerticesDraw.length / 3;

			for (int i = 0; i < skinLAnkleToFootEndVerticesDraw.length; i++) {
				skinVerticesDraw[iVert++] = skinLAnkleToFootEndVerticesDraw[i];
			}
			for (int i = 0; i < skinLAnkleToFootEndIndices.length; i++) {
				skinIndices[iInd++] = (byte) (skinLAnkleToFootEndIndices[i] + indexOffset);
			}
			indexOffset += skinLAnkleToFootEndVerticesDraw.length / 3;

			isSkinIndexBound = false;
			skinIndicesCount = skinIndices.length;

			calculateSkinHair();
			isSkinHairIndexBound = false;
			skinHairIndicesCount = skinHairIndices.length;

		}

	}

	public void calculateSkinFace() {
		println("human1.calculateSkinFace");

		int index = 3 * 3; // neck
		float neckX = jointsVerticesDrawCurrent[index];
		float neckY = jointsVerticesDrawCurrent[index + 1];
		float neckZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 5; // head top
		float topX = jointsVerticesDrawCurrent[index];
		float topY = jointsVerticesDrawCurrent[index + 1];
		float topZ = jointsVerticesDrawCurrent[index + 2];

		float dist = headSize;

		float[] array = skinFaceVerticesDraw;

		index = 0;
		// 0
		array[index++] = dist + topX;
		array[index++] = topY;
		array[index++] = topZ;
		// 1
		array[index++] = topX;
		array[index++] = -dist + topY;
		array[index++] = topZ;
		// 2
		array[index++] = neckX;
		array[index++] = -dist + neckY;
		array[index++] = neckZ;
		// 3
		array[index++] = dist + neckX;
		array[index++] = neckY;
		array[index++] = neckZ;
		// 4
		array[index++] = neckX;
		array[index++] = dist + neckY;
		array[index++] = neckZ;
		// 5
		array[index++] = topX;
		array[index++] = dist + topY;
		array[index++] = topZ;

		/*
		 * float deltaX = beginX - endX; float deltaZ = beginZ - endZ; // float
		 * angleYRadian = Math.atan2(deltaX,deltaZ); float angleYRadian =
		 * Math.atan2(deltaZ, deltaX); float cosRadian = Math.cos(angleYRadian);
		 * float sinRadian = Math.sin(angleYRadian); // rotate around y axis for
		 * (int i = 0; i < array.length; i += 3) { float fX = array[i]; float fY
		 * = array[i + 1]; float fZ = array[i + 2];
		 * 
		 * float dX = fZ * cosRadian - fX * sinRadian; float dY = fY; float dZ =
		 * fZ * sinRadian + fX * cosRadian;
		 * 
		 * array[i] = (float) (dX); array[i + 1] = (float) (dY); array[i + 2] =
		 * (float) (dZ);
		 * 
		 * }
		 * 
		 * for (int i = 0; i < array.length; i += 3) { array[i] += beginX;
		 * array[i + 1] += beginY; array[i + 2] += beginZ; }
		 */

		// float[] skinVerticesRef = skinLElbowToWristVerticesDraw.clone();
		double radian = Math.toRadians(degreeRotateZ);
		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = array[i];// * lengthX/* X */;
			float fY = array[i + 1];// * widthY/* Y */;
			float fZ = array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (dX);
			array[i + 1] = (dY);
			array[i + 2] = (dZ);
		}

	}

	public void calculateSkinHair() {
		println("human1.calculateSkinHair");

		int index = 3 * 3; // neck
		float neckX = jointsVerticesDrawCurrent[index];
		float neckY = jointsVerticesDrawCurrent[index + 1];
		float neckZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 5; // head top
		float topX = jointsVerticesDrawCurrent[index];
		float topY = jointsVerticesDrawCurrent[index + 1];
		float topZ = jointsVerticesDrawCurrent[index + 2];

		float dist = headSize;

		float[] array = skinHairVerticesDraw;

		index = 0;
		// 0
		array[index++] = dist + topX;
		array[index++] = topY;
		array[index++] = topZ;
		// 1
		array[index++] = topX;
		array[index++] = dist + topY;
		array[index++] = topZ;
		// 2
		array[index++] = -dist + topX;
		array[index++] = topY;
		array[index++] = topZ;
		// 3
		array[index++] = topX;
		array[index++] = -dist + topY;
		array[index++] = topZ;
		// 4
		array[index++] = neckX;
		array[index++] = -dist + neckY;
		array[index++] = neckZ;
		// 5
		array[index++] = -dist + neckX;
		array[index++] = neckY;
		array[index++] = neckZ;
		// 6
		array[index++] = neckX;
		array[index++] = dist + neckY;
		array[index++] = neckZ;

		/*
		 * float deltaX = beginX - endX; float deltaZ = beginZ - endZ; // float
		 * angleYRadian = Math.atan2(deltaX,deltaZ); float angleYRadian =
		 * Math.atan2(deltaZ, deltaX); float cosRadian = Math.cos(angleYRadian);
		 * float sinRadian = Math.sin(angleYRadian); // rotate around y axis for
		 * (int i = 0; i < array.length; i += 3) { float fX = array[i]; float fY
		 * = array[i + 1]; float fZ = array[i + 2];
		 * 
		 * float dX = fZ * cosRadian - fX * sinRadian; float dY = fY; float dZ =
		 * fZ * sinRadian + fX * cosRadian;
		 * 
		 * array[i] = (float) (dX); array[i + 1] = (float) (dY); array[i + 2] =
		 * (float) (dZ);
		 * 
		 * }
		 * 
		 * for (int i = 0; i < array.length; i += 3) { array[i] += beginX;
		 * array[i + 1] += beginY; array[i + 2] += beginZ; }
		 */

		// float[] skinVerticesRef = skinLElbowToWristVerticesDraw.clone();
		double radian = Math.toRadians(degreeRotateZ);
		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = array[i];// * lengthX/* X */;
			float fY = array[i + 1];// * widthY/* Y */;
			float fZ = array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (dX);
			array[i + 1] = (dY);
			array[i + 2] = (dZ);
		}

	}

	public void calculateSkinLAnkleToFootEnd() {
		println("human1.calculateSkinLAnkleToFootEnd");
		int index = 3 * 10;
		float beginX = jointsVerticesDrawCurrent[index];
		float beginY = jointsVerticesDrawCurrent[index + 1];
		float beginZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 12;
		float endX = jointsVerticesDrawCurrent[index];
		float endY = jointsVerticesDrawCurrent[index + 1];
		float endZ = jointsVerticesDrawCurrent[index + 2];

		float newX = (float) Math.pow((beginX - endX), 2);
		float newY = (float) Math.pow((beginY - endY), 2);
		float newZ = (float) Math.pow((beginZ - endZ), 2);
		float dist = (float) Math.sqrt(newX + newY + newZ);

		float halfDist = dist / 4;/// 2f;
		float quartDist = dist / 4f;

		float armWidth = dist / 7f;
		float lowerArmWidth = dist / 4f;

		float[] array = skinLAnkleToFootEndVerticesDraw;

		float extraZ = distanceToExtendBeyondHandEnd * dist * 2;

		index = 0;
		// 0
		array[index++] = halfDist;
		array[index++] = -halfDist;
		array[index++] = 0;
		// 1
		array[index++] = -halfDist;
		array[index++] = -halfDist;
		array[index++] = 0;
		// 2
		array[index++] = -halfDist;// System.out.print("\t\t2x:"+-halfDist);
		array[index++] = -halfDist;// System.out.print("\t\t2y:"+-halfDist);
		array[index++] = -dist - extraZ;// System.out.println("\t\t2z:"+-dist);
		// 3
		array[index++] = halfDist;// System.out.print("\t\t3x:"+halfDist);
		array[index++] = -halfDist;// System.out.print("\t\t3y:"+-halfDist);
		array[index++] = -dist - extraZ;// System.out.println("\t\t3z:"+-dist);
		// 4
		array[index++] = halfDist;
		array[index++] = halfDist;
		array[index++] = 0;
		// 5
		array[index++] = -halfDist;
		array[index++] = halfDist;
		array[index++] = 0;
		// 6
		array[index++] = -halfDist;
		array[index++] = halfDist;
		array[index++] = -dist - extraZ;
		// 7
		array[index++] = halfDist;
		array[index++] = halfDist;
		array[index++] = -dist - extraZ;

		float deltaX = beginX - endX;
		float deltaZ = beginZ - endZ;
		// float angleYRadian = Math.atan2(deltaX,deltaZ);
		double angleYRadian = Math.atan2(deltaZ, deltaX);
		float cosRadian = (float) Math.cos(angleYRadian);
		float sinRadian = (float) Math.sin(angleYRadian);
		// rotate around y axis
		for (int i = 0; i < array.length; i += 3) {
			float fX = array[i];
			float fY = array[i + 1];
			float fZ = array[i + 2];

			float dX = fZ * cosRadian - fX * sinRadian;
			float dY = fY;
			float dZ = fZ * sinRadian + fX * cosRadian;

			array[i] = (dX);
			array[i + 1] = (dY);
			array[i + 2] = (dZ);

		}

		for (int i = 0; i < array.length; i += 3) {
			array[i] += beginX;
			array[i + 1] += beginY;
			array[i + 2] += beginZ;
		}

		// float[] skinVerticesRef = skinLElbowToWristVerticesDraw.clone();
		double radian = Math.toRadians(degreeRotateZ);
		cosRadian = (float) Math.cos(radian);
		sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = (float) array[i];// * lengthX/* X */;
			float fY = (float) array[i + 1];// * widthY/* Y */;
			float fZ = (float) array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (dX);
			array[i + 1] = (dY);
			array[i + 2] = (dZ);

			/*
			 * if (i == 0) { System.out.print("\t\tfinal0x:" + dX);
			 * System.out.print("\t\tfinal0y:" + dY);
			 * System.out.println("\t\tfinal0z:" + dZ); }
			 * 
			 * if (i == 3) { System.out.print("\t\tfinal2x:" + dX);
			 * System.out.print("\t\tfinal2y:" + dY);
			 * System.out.println("\t\tfinal2z:" + dZ); }
			 * 
			 * if (i == 6) { System.out.print("\t\tfinal3x:" + dX);
			 * System.out.print("\t\tfinal3y:" + dY);
			 * System.out.println("\t\tfinal3z:" + dZ); }
			 */
		}
	}

	public void calculateSkinLElbowToWrist() {
		println("human1.calculateSkinLElbowToWrist");
		int index = 3 * 16;
		float beginX = jointsVerticesDrawCurrent[index];
		float beginY = jointsVerticesDrawCurrent[index + 1];
		float beginZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 18;
		float endX = jointsVerticesDrawCurrent[index];
		float endY = jointsVerticesDrawCurrent[index + 1];
		float endZ = jointsVerticesDrawCurrent[index + 2];

		float newX = (float) Math.pow((beginX - endX), 2);
		float newY = (float) Math.pow((beginY - endY), 2);
		float newZ = (float) Math.pow((beginZ - endZ), 2);
		float dist = (float) Math.sqrt(newX + newY + newZ);

		float halfDist = dist / 2f;
		float quartDist = dist / 4f;

		float lowerArmWidth = dist / 4f;

		float[] array = skinLElbowToWristVerticesDraw;

		index = 0;
		array[index++] = armWidth;
		array[index++] = -armWidth;
		array[index++] = 0;
		array[index++] = -armWidth;
		array[index++] = -armWidth;
		array[index++] = 0;
		array[index++] = -armWidth;
		array[index++] = -armWidth;
		array[index++] = -halfDist;
		array[index++] = armWidth;
		array[index++] = -armWidth;
		array[index++] = -halfDist;
		array[index++] = armWidth;
		array[index++] = armWidth;
		array[index++] = 0;
		array[index++] = -armWidth;
		array[index++] = armWidth;
		array[index++] = 0;
		array[index++] = -armWidth;
		array[index++] = armWidth;
		array[index++] = -halfDist;
		array[index++] = armWidth;
		array[index++] = armWidth;
		array[index++] = -halfDist;
		array[index++] = 0f;
		array[index++] = 0f;
		array[index++] = -halfDist + (halfDist / 6f);
		array[index++] = -lowerArmWidth;
		array[index++] = lowerArmWidth;
		array[index++] = -halfDist - (halfDist / 6f);
		array[index++] = -lowerArmWidth;
		array[index++] = lowerArmWidth;
		array[index++] = -dist;
		array[index++] = lowerArmWidth;
		array[index++] = lowerArmWidth;
		array[index++] = -dist;
		array[index++] = lowerArmWidth;
		array[index++] = lowerArmWidth;
		array[index++] = -halfDist - (halfDist / 6f);
		array[index++] = -lowerArmWidth;
		array[index++] = -lowerArmWidth;
		array[index++] = -halfDist - (halfDist / 6f);
		array[index++] = -lowerArmWidth;
		array[index++] = -lowerArmWidth;
		array[index++] = -dist;
		array[index++] = lowerArmWidth;
		array[index++] = -lowerArmWidth;
		array[index++] = -dist;
		array[index++] = lowerArmWidth;
		array[index++] = -lowerArmWidth;
		array[index++] = -halfDist - (halfDist / 6f);

		float deltaX = beginX - endX;
		float deltaZ = beginZ - endZ;
		// float angleYRadian = Math.atan2(deltaX,deltaZ);
		float angleYRadian = (float) Math.atan2(deltaZ, deltaX);
		float cosRadian = (float) Math.cos(angleYRadian);
		float sinRadian = (float) Math.sin(angleYRadian);
		// rotate around y axis
		for (int i = 0; i < array.length; i += 3) {
			float fX = array[i];
			float fY = array[i + 1];
			float fZ = array[i + 2];

			float dX = fZ * cosRadian - fX * sinRadian;
			float dY = fY;
			float dZ = fZ * sinRadian + fX * cosRadian;

			array[i] = (dX);
			array[i + 1] = (dY);
			array[i + 2] = (dZ);

		}

		for (int i = 0; i < array.length; i += 3) {
			array[i] += beginX;
			array[i + 1] += beginY;
			array[i + 2] += beginZ;
		}

		// float[] skinVerticesRef = skinLElbowToWristVerticesDraw.clone();
		float radian = (float) Math.toRadians(degreeRotateZ);
		cosRadian = (float) Math.cos(radian);
		sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = array[i];// * lengthX/* X */;
			float fY = array[i + 1];// * widthY/* Y */;
			float fZ = array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (dX);
			array[i + 1] = (dY);
			array[i + 2] = (dZ);
		}

		// skinLElbowToWristVerticesDraw = array.clone();

	}

	public void calculateSkinLHipToKnee() {
		println("human1.calculateSkinLHipToKnee");
		int index = 3 * 6;
		float beginX = jointsVerticesDrawCurrent[index];
		float beginY = jointsVerticesDrawCurrent[index + 1];
		float beginZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 8;
		float endX = jointsVerticesDrawCurrent[index];
		float endY = jointsVerticesDrawCurrent[index + 1];
		float endZ = jointsVerticesDrawCurrent[index + 2];

		float newX = (float) Math.pow((beginX - endX), 2);
		float newY = (float) Math.pow((beginY - endY), 2);
		float newZ = (float) Math.pow((beginZ - endZ), 2);
		float dist = (float) Math.sqrt(newX + newY + newZ);

		float halfDist = dist / 2f;
		float quartDist = dist / 4f;
		// float width = armWidth/2f;

		float lowerArmWidth = dist / 4f;

		float[] array = skinLHipToKneeVerticesDraw;

		index = 0;
		// 0
		array[index++] = armWidth;
		array[index++] = -armWidth;
		array[index++] = 0f;
		// 1
		array[index++] = -armWidth;
		array[index++] = -armWidth;
		array[index++] = 0f;
		// 2
		array[index++] = -armWidth;
		array[index++] = -armWidth;
		array[index++] = -dist;
		// 3
		array[index++] = armWidth;
		array[index++] = -armWidth;
		array[index++] = -dist;
		// 4
		array[index++] = armWidth;
		array[index++] = armWidth;
		array[index++] = 0f;
		// 5
		array[index++] = -armWidth;
		array[index++] = armWidth;
		array[index++] = 0f;
		// 6
		array[index++] = -armWidth;
		array[index++] = armWidth;
		array[index++] = -dist;
		// 7
		array[index++] = armWidth;
		array[index++] = armWidth;
		array[index++] = -dist;
		// 8
		array[index++] = 0f;
		array[index++] = 0f;
		array[index++] = -halfDist;
		// 9
		array[index++] = -halfDist / 3f;
		array[index++] = 0f;
		array[index++] = 0f;
		// 10
		array[index++] = -halfDist / 3f;
		array[index++] = 0f;
		array[index++] = dist / 4f;
		// 11
		array[index++] = halfDist / 3f;
		array[index++] = 0f;
		array[index++] = dist / 4f;
		// 12
		array[index++] = halfDist / 3f;
		array[index++] = 0f;
		array[index++] = 0f;
		// 13
		array[index++] = -halfDist / 3f;
		array[index++] = -dist / 4f;
		array[index++] = 0f;
		// 14
		array[index++] = halfDist / 3f;
		array[index++] = -dist / 4f;
		array[index++] = 0f;
		// 15
		array[index++] = -halfDist / 3f;
		array[index++] = dist / 4f;
		array[index++] = 0f;
		// 16
		array[index++] = halfDist / 3f;
		array[index++] = dist / 4f;
		array[index++] = 0f;

		float deltaX = beginX - endX;
		float deltaZ = beginZ - endZ;
		// float angleYRadian = Math.atan2(deltaX,deltaZ);
		float angleYRadian = (float) Math.atan2(deltaZ, deltaX);
		float cosRadian = (float) Math.cos(angleYRadian);
		float sinRadian = (float) Math.sin(angleYRadian);
		// rotate around y axis
		for (int i = 0; i < array.length; i += 3) {
			float fX = array[i];
			float fY = array[i + 1];
			float fZ = array[i + 2];

			float dX = fZ * cosRadian - fX * sinRadian;
			float dY = fY;
			float dZ = fZ * sinRadian + fX * cosRadian;

			array[i] = (dX);
			array[i + 1] = (dY);
			array[i + 2] = (dZ);

		}

		for (int i = 0; i < array.length; i += 3) {
			array[i] += beginX;
			array[i + 1] += beginY;
			array[i + 2] += beginZ;
		}

		// float[] skinVerticesRef = skinLElbowToWristVerticesDraw.clone();
		float radian = (float) Math.toRadians(degreeRotateZ);
		cosRadian = (float) Math.cos(radian);
		sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = array[i];// * lengthX/* X */;
			float fY = array[i + 1];// * widthY/* Y */;
			float fZ = array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (dX);
			array[i + 1] = (dY);
			array[i + 2] = (dZ);
		}

		// skinLElbowToWristVerticesDraw = array.clone();

	}

	public void calculateSkinRKneeToAnkle() {
		println("human1.calculateSkinRKneeToAnkle");
		int index = 3 * 9;
		float beginX = jointsVerticesDrawCurrent[index];
		float beginY = jointsVerticesDrawCurrent[index + 1];
		float beginZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 11;
		float endX = jointsVerticesDrawCurrent[index];
		float endY = jointsVerticesDrawCurrent[index + 1];
		float endZ = jointsVerticesDrawCurrent[index + 2];

		float newX = (float) Math.pow((beginX - endX), 2);
		float newY = (float) Math.pow((beginY - endY), 2);
		float newZ = (float) Math.pow((beginZ - endZ), 2);
		float dist = (float) Math.sqrt(newX + newY + newZ);

		float halfDist = dist / 2f;
		float quartDist = dist / 4f;

		float lowerArmWidth = dist / 4f;

		float[] array = skinRKneeToAnkleVerticesDraw;

		index = 0;
		// 0
		array[index++] = armWidth;
		array[index++] = -armWidth;
		array[index++] = 0f;
		// 1
		array[index++] = -armWidth;
		array[index++] = -armWidth;
		array[index++] = 0f;
		// 2
		array[index++] = -armWidth;
		array[index++] = -armWidth;
		array[index++] = -dist;
		// 3
		array[index++] = armWidth;
		array[index++] = -armWidth;
		array[index++] = -dist;
		// 4
		array[index++] = armWidth;
		array[index++] = armWidth;
		array[index++] = 0f;
		// 5
		array[index++] = -armWidth;
		array[index++] = armWidth;
		array[index++] = 0f;
		// 6
		array[index++] = -armWidth;
		array[index++] = armWidth;
		array[index++] = -dist;
		// 7
		array[index++] = armWidth;
		array[index++] = armWidth;
		array[index++] = -dist;
		// 8
		array[index++] = 0f;
		array[index++] = 0f;
		array[index++] = -halfDist;
		// 9
		array[index++] = -halfDist / 3f;
		array[index++] = 0f;
		array[index++] = 0f;
		// 10
		array[index++] = -halfDist / 3f;
		array[index++] = 0f;
		array[index++] = dist / 4f;
		// 11
		array[index++] = halfDist / 3f;
		array[index++] = 0f;
		array[index++] = dist / 4f;
		// 12
		array[index++] = halfDist / 3f;
		array[index++] = 0f;
		array[index++] = 0f;
		// 13
		array[index++] = -halfDist / 3f;
		array[index++] = -dist / 4f;
		array[index++] = 0f;
		// 14
		array[index++] = halfDist / 3f;
		array[index++] = -dist / 4f;
		array[index++] = 0f;
		// 15
		array[index++] = -halfDist / 3f;
		array[index++] = dist / 4f;
		array[index++] = 0f;
		// 16
		array[index++] = halfDist / 3f;
		array[index++] = dist / 4f;
		array[index++] = 0f;

		float deltaX = beginX - endX;
		float deltaZ = beginZ - endZ;
		// float angleYRadian = Math.atan2(deltaX,deltaZ);
		float angleYRadian = (float) Math.atan2(deltaZ, deltaX);
		float cosRadian = (float) Math.cos(angleYRadian);
		float sinRadian = (float) Math.sin(angleYRadian);
		// rotate around y axis
		for (int i = 0; i < array.length; i += 3) {
			float fX = array[i];
			float fY = array[i + 1];
			float fZ = array[i + 2];

			float dX = fZ * cosRadian - fX * sinRadian;
			float dY = fY;
			float dZ = fZ * sinRadian + fX * cosRadian;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);

		}

		for (int i = 0; i < array.length; i += 3) {
			array[i] += beginX;
			array[i + 1] += beginY;
			array[i + 2] += beginZ;
		}

		// float[] skinVerticesRef = skinLElbowToWristVerticesDraw.clone();
		float radian = (float) Math.toRadians(degreeRotateZ);
		cosRadian = (float) Math.cos(radian);
		sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = (float) array[i];// * lengthX/* X */;
			float fY = (float) array[i + 1];// * widthY/* Y */;
			float fZ = (float) array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);
		}

		// skinLElbowToWristVerticesDraw = array.clone();

	}

	public void calculateSkinLShoulderToElbow() {
		println("human1.calculateSkinLShoulderToElbow");
		int index = 3 * 14;
		float beginX = jointsVerticesDrawCurrent[index];
		float beginY = jointsVerticesDrawCurrent[index + 1];
		float beginZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 16;
		float endX = jointsVerticesDrawCurrent[index];
		float endY = jointsVerticesDrawCurrent[index + 1];
		float endZ = jointsVerticesDrawCurrent[index + 2];

		float newX = (float) Math.pow((beginX - endX), 2);
		float newY = (float) Math.pow((beginY - endY), 2);
		float newZ = (float) Math.pow((beginZ - endZ), 2);
		float dist = (float) Math.sqrt(newX + newY + newZ);

		float halfDist = dist / 2f;
		float quartDist = dist / 4f;

		float lowerArmWidth = dist / 4f;

		float[] array = skinLShoulderToElbowVerticesDraw;

		index = 0;
		// 0
		array[index++] = armWidth;
		array[index++] = -armWidth;
		array[index++] = 0f;
		// 1
		array[index++] = -armWidth;
		array[index++] = -armWidth;
		array[index++] = 0f;
		// 2
		array[index++] = -armWidth;
		array[index++] = -armWidth;
		array[index++] = -dist;
		// 3
		array[index++] = armWidth;
		array[index++] = -armWidth;
		array[index++] = -dist;
		// 4
		array[index++] = armWidth;
		array[index++] = armWidth;
		array[index++] = 0f;
		// 5
		array[index++] = -armWidth;
		array[index++] = armWidth;
		array[index++] = 0f;
		// 6
		array[index++] = -armWidth;
		array[index++] = armWidth;
		array[index++] = -dist;
		// 7
		array[index++] = armWidth;
		array[index++] = armWidth;
		array[index++] = -dist;
		// 8
		array[index++] = 0f;
		array[index++] = 0f;
		array[index++] = -halfDist;
		// 9
		array[index++] = -halfDist;
		array[index++] = 0f;
		array[index++] = 0f;
		// 10
		array[index++] = -halfDist / 3f;
		array[index++] = 0f;
		array[index++] = dist / 4f;
		// 11
		array[index++] = halfDist / 3f;
		array[index++] = 0f;
		array[index++] = dist / 4f;
		// 12
		array[index++] = halfDist;
		array[index++] = 0f;
		array[index++] = 0f;
		// 13
		array[index++] = -halfDist / 3f;
		array[index++] = -dist / 4f;
		array[index++] = 0f;
		// 14
		array[index++] = halfDist / 3f;
		array[index++] = -dist / 4f;
		array[index++] = 0f;
		// 15
		array[index++] = -halfDist / 3f;
		array[index++] = dist / 4f;
		array[index++] = 0f;
		// 16
		array[index++] = halfDist / 3f;
		array[index++] = dist / 4f;
		array[index++] = 0f;

		float deltaX = beginX - endX;
		float deltaZ = beginZ - endZ;
		// float angleYRadian = Math.atan2(deltaX,deltaZ);
		float angleYRadian = (float) Math.atan2(deltaZ, deltaX);
		float cosRadian = (float) Math.cos(angleYRadian);
		float sinRadian = (float) Math.sin(angleYRadian);
		// rotate around y axis
		for (int i = 0; i < array.length; i += 3) {
			float fX = array[i];
			float fY = array[i + 1];
			float fZ = array[i + 2];

			float dX = fZ * cosRadian - fX * sinRadian;
			float dY = fY;
			float dZ = fZ * sinRadian + fX * cosRadian;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);

		}

		for (int i = 0; i < array.length; i += 3) {
			array[i] += beginX;
			array[i + 1] += beginY;
			array[i + 2] += beginZ;
		}

		// float[] skinVerticesRef = skinLElbowToWristVerticesDraw.clone();
		float radian = (float) Math.toRadians(degreeRotateZ);
		cosRadian = (float) Math.cos(radian);
		sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = (float) array[i];// * lengthX/* X */;
			float fY = (float) array[i + 1];// * widthY/* Y */;
			float fZ = (float) array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);
		}

		// skinLElbowToWristVerticesDraw = array.clone();

	}

	public void calculateSkinLWristToHandEnd() {
		println("human1.calculateSkinLWristToHandEnd");
		int index = 3 * 18;
		float beginX = jointsVerticesDrawCurrent[index];
		float beginY = jointsVerticesDrawCurrent[index + 1];
		float beginZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 20;
		float endX = jointsVerticesDrawCurrent[index];
		float endY = jointsVerticesDrawCurrent[index + 1];
		float endZ = jointsVerticesDrawCurrent[index + 2];

		float newX = (float) Math.pow((beginX - endX), 2);
		float newY = (float) Math.pow((beginY - endY), 2);
		float newZ = (float) Math.pow((beginZ - endZ), 2);
		float dist = (float) Math.sqrt(newX + newY + newZ);

		float halfDist = dist * 2;/// 2f;
		float quartDist = dist / 4f;

		float lowerArmWidth = dist / 4f;

		float[] array = skinLWristToHandEndVerticesDraw;

		float extraZ = distanceToExtendBeyondHandEnd * dist * 2;

		index = 0;
		// 0
		array[index++] = halfDist;
		array[index++] = -halfDist;
		array[index++] = 0;
		// 1
		array[index++] = -halfDist;
		array[index++] = -halfDist;
		array[index++] = 0;
		// 2
		array[index++] = -halfDist;// System.out.print("\t\t2x:"+-halfDist);
		array[index++] = -halfDist;// System.out.print("\t\t2y:"+-halfDist);
		array[index++] = -dist - extraZ;// System.out.println("\t\t2z:"+-dist);
		// 3
		array[index++] = halfDist;// System.out.print("\t\t3x:"+halfDist);
		array[index++] = -halfDist;// System.out.print("\t\t3y:"+-halfDist);
		array[index++] = -dist - extraZ;// System.out.println("\t\t3z:"+-dist);
		// 4
		array[index++] = halfDist;
		array[index++] = halfDist;
		array[index++] = 0;
		// 5
		array[index++] = -halfDist;
		array[index++] = halfDist;
		array[index++] = 0;
		// 6
		array[index++] = -halfDist;
		array[index++] = halfDist;
		array[index++] = -dist - extraZ;
		// 7
		array[index++] = halfDist;
		array[index++] = halfDist;
		array[index++] = -dist - extraZ;

		float deltaX = beginX - endX;
		float deltaZ = beginZ - endZ;
		// float angleYRadian = Math.atan2(deltaX,deltaZ);
		float angleYRadian = (float) Math.atan2(deltaZ, deltaX);
		float cosRadian = (float) Math.cos(angleYRadian);
		float sinRadian = (float) Math.sin(angleYRadian);
		// rotate around y axis
		for (int i = 0; i < array.length; i += 3) {
			float fX = array[i];
			float fY = array[i + 1];
			float fZ = array[i + 2];

			float dX = fZ * cosRadian - fX * sinRadian;
			float dY = fY;
			float dZ = fZ * sinRadian + fX * cosRadian;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);

		}

		for (int i = 0; i < array.length; i += 3) {
			array[i] += beginX;
			array[i + 1] += beginY;
			array[i + 2] += beginZ;
		}

		// float[] skinVerticesRef = skinLElbowToWristVerticesDraw.clone();
		float radian = (float) Math.toRadians(degreeRotateZ);
		cosRadian = (float) Math.cos(radian);
		sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = (float) array[i];// * lengthX/* X */;
			float fY = (float) array[i + 1];// * widthY/* Y */;
			float fZ = (float) array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);

			/*
			 * if (i == 0) { printlon("\t\tfinal0x:" + dX);
			 * printon("\t\tfinal0y:" + dY); println("\t\tfinal0z:" + dZ); }
			 * 
			 * if (i == 3) { print("\t\tfinal2x:" + dX); print("\t\tfinal2y:" +
			 * dY); println("\t\tfinal2z:" + dZ); }
			 * 
			 * if (i == 6) { printon("\t\tfinal3x:" + dX);
			 * printon("\t\tfinal3y:" + dY); println("\t\tfinal3z:" + dZ); }
			 */
		}
	}

	public void calculateSkinRElbowToWrist() {
		println("human1.calculateSkinRElbowToWrist");
		int index = 3 * 17;
		float beginX = jointsVerticesDrawCurrent[index];
		float beginY = jointsVerticesDrawCurrent[index + 1];
		float beginZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 19;
		float endX = jointsVerticesDrawCurrent[index];
		float endY = jointsVerticesDrawCurrent[index + 1];
		float endZ = jointsVerticesDrawCurrent[index + 2];

		float newX = (float) Math.pow((beginX - endX), 2);
		float newY = (float) Math.pow((beginY - endY), 2);
		float newZ = (float) Math.pow((beginZ - endZ), 2);
		float dist = (float) Math.sqrt(newX + newY + newZ);

		float halfDist = dist / 2f;
		float quartDist = dist / 4f;

		float lowerArmWidth = dist / 4f;

		float[] array = skinRElbowToWristVerticesDraw;
		byte[] indices;// = skinRElbowToWristIndices;

		index = 0;

		array[index++] = armWidth;
		array[index++] = -armWidth;
		array[index++] = 0;
		array[index++] = -armWidth;
		array[index++] = -armWidth;
		array[index++] = 0;
		array[index++] = -armWidth;
		array[index++] = -armWidth;
		array[index++] = -halfDist;
		array[index++] = armWidth;
		array[index++] = -armWidth;
		array[index++] = -halfDist;
		array[index++] = armWidth;
		array[index++] = armWidth;
		array[index++] = 0;
		array[index++] = -armWidth;
		array[index++] = armWidth;
		array[index++] = 0;
		array[index++] = -armWidth;
		array[index++] = armWidth;
		array[index++] = -halfDist;
		array[index++] = armWidth;
		array[index++] = armWidth;
		array[index++] = -halfDist;
		array[index++] = 0f;
		array[index++] = 0f;
		array[index++] = -halfDist + (halfDist / 6f);
		array[index++] = -lowerArmWidth;
		array[index++] = lowerArmWidth;
		array[index++] = -halfDist - (halfDist / 6f);
		array[index++] = -lowerArmWidth;
		array[index++] = lowerArmWidth;
		array[index++] = -dist;
		array[index++] = lowerArmWidth;
		array[index++] = lowerArmWidth;
		array[index++] = -dist;
		array[index++] = lowerArmWidth;
		array[index++] = lowerArmWidth;
		array[index++] = -halfDist - (halfDist / 6f);
		array[index++] = -lowerArmWidth;
		array[index++] = -lowerArmWidth;
		array[index++] = -halfDist - (halfDist / 6f);
		array[index++] = -lowerArmWidth;
		array[index++] = -lowerArmWidth;
		array[index++] = -dist;
		array[index++] = lowerArmWidth;
		array[index++] = -lowerArmWidth;
		array[index++] = -dist;
		array[index++] = lowerArmWidth;
		array[index++] = -lowerArmWidth;
		array[index++] = -halfDist - (halfDist / 6f);

		float deltaX = beginX - endX;
		float deltaZ = beginZ - endZ;
		// float angleYRadian = Math.atan2(deltaX,deltaZ);
		float angleYRadian = (float) Math.atan2(deltaZ, deltaX);
		float cosRadian = (float) Math.cos(angleYRadian);
		float sinRadian = (float) Math.sin(angleYRadian);
		// rotate around y axis
		for (int i = 0; i < array.length; i += 3) {
			float fX = array[i];
			float fY = array[i + 1];
			float fZ = array[i + 2];

			float dX = fZ * cosRadian - fX * sinRadian;
			float dY = fY;
			float dZ = fZ * sinRadian + fX * cosRadian;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);

		}

		for (int i = 0; i < array.length; i += 3) {
			array[i] += beginX;
			array[i + 1] += beginY;
			array[i + 2] += beginZ;
		}

		// float[] skinVerticesRef = array.clone();
		float radian = (float) Math.toRadians(degreeRotateZ);
		cosRadian = (float) Math.cos(radian);
		sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = (float) array[i];// * lengthX/* X */;
			float fY = (float) array[i + 1];// * widthY/* Y */;
			float fZ = (float) array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);
		}

		// skinRElbowToWristVerticesDraw = array.clone();
		// skinRElbowToWristIndices = indices.clone();

	}

	public void calculateSkinRHipToKnee() {
		println("human1.calculateSkinRHipToKnee");
		int index = 3 * 7;
		float beginX = jointsVerticesDrawCurrent[index];
		float beginY = jointsVerticesDrawCurrent[index + 1];
		float beginZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 9;
		float endX = jointsVerticesDrawCurrent[index];
		float endY = jointsVerticesDrawCurrent[index + 1];
		float endZ = jointsVerticesDrawCurrent[index + 2];

		float newX = (float) Math.pow((beginX - endX), 2);
		float newY = (float) Math.pow((beginY - endY), 2);
		float newZ = (float) Math.pow((beginZ - endZ), 2);
		float dist = (float) Math.sqrt(newX + newY + newZ);

		float halfDist = dist / 2f;
		float quartDist = dist / 4f;

		float lowerArmWidth = dist / 4f;

		float[] array = skinRHipToKneeVerticesDraw;

		index = 0;
		// 0
		array[index++] = armWidth;
		array[index++] = -armWidth;
		array[index++] = 0f;
		// 1
		array[index++] = -armWidth;
		array[index++] = -armWidth;
		array[index++] = 0f;
		// 2
		array[index++] = -armWidth;
		array[index++] = -armWidth;
		array[index++] = -dist;
		// 3
		array[index++] = armWidth;
		array[index++] = -armWidth;
		array[index++] = -dist;
		// 4
		array[index++] = armWidth;
		array[index++] = armWidth;
		array[index++] = 0f;
		// 5
		array[index++] = -armWidth;
		array[index++] = armWidth;
		array[index++] = 0f;
		// 6
		array[index++] = -armWidth;
		array[index++] = armWidth;
		array[index++] = -dist;
		// 7
		array[index++] = armWidth;
		array[index++] = armWidth;
		array[index++] = -dist;
		// 8
		array[index++] = 0f;
		array[index++] = 0f;
		array[index++] = -halfDist;
		// 9
		array[index++] = -halfDist / 3f;
		array[index++] = 0f;
		array[index++] = 0f;
		// 10
		array[index++] = -halfDist / 3f;
		array[index++] = 0f;
		array[index++] = dist / 4f;
		// 11
		array[index++] = halfDist / 3f;
		array[index++] = 0f;
		array[index++] = dist / 4f;
		// 12
		array[index++] = halfDist / 3f;
		array[index++] = 0f;
		array[index++] = 0f;
		// 13
		array[index++] = -halfDist / 3f;
		array[index++] = -dist / 4f;
		array[index++] = 0f;
		// 14
		array[index++] = halfDist / 3f;
		array[index++] = -dist / 4f;
		array[index++] = 0f;
		// 15
		array[index++] = -halfDist / 3f;
		array[index++] = dist / 4f;
		array[index++] = 0f;
		// 16
		array[index++] = halfDist / 3f;
		array[index++] = dist / 4f;
		array[index++] = 0f;

		float deltaX = beginX - endX;
		float deltaZ = beginZ - endZ;
		// float angleYRadian = Math.atan2(deltaX,deltaZ);
		float angleYRadian = (float) Math.atan2(deltaZ, deltaX);
		float cosRadian = (float) Math.cos(angleYRadian);
		float sinRadian = (float) Math.sin(angleYRadian);
		// rotate around y axis
		for (int i = 0; i < array.length; i += 3) {
			float fX = array[i];
			float fY = array[i + 1];
			float fZ = array[i + 2];

			float dX = fZ * cosRadian - fX * sinRadian;
			float dY = fY;
			float dZ = fZ * sinRadian + fX * cosRadian;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);

		}

		for (int i = 0; i < array.length; i += 3) {
			array[i] += beginX;
			array[i + 1] += beginY;
			array[i + 2] += beginZ;
		}

		// float[] skinVerticesRef = skinLElbowToWristVerticesDraw.clone();
		float radian = (float) Math.toRadians(degreeRotateZ);
		cosRadian = (float) Math.cos(radian);
		sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = (float) array[i];// * lengthX/* X */;
			float fY = (float) array[i + 1];// * widthY/* Y */;
			float fZ = (float) array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);
		}

		// skinLElbowToWristVerticesDraw = array.clone();

	}

	public void calculateSkinRAnkleToFootEnd() {
		println("human1.calculateSkinRAnkleToFootEnd");
		int index = 3 * 11;
		float beginX = jointsVerticesDrawCurrent[index];
		float beginY = jointsVerticesDrawCurrent[index + 1];
		float beginZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 13;
		float endX = jointsVerticesDrawCurrent[index];
		float endY = jointsVerticesDrawCurrent[index + 1];
		float endZ = jointsVerticesDrawCurrent[index + 2];

		float newX = (float) Math.pow((beginX - endX), 2);
		float newY = (float) Math.pow((beginY - endY), 2);
		float newZ = (float) Math.pow((beginZ - endZ), 2);
		float dist = (float) Math.sqrt(newX + newY + newZ);

		float halfDist = dist / 4;/// 2f;
		float quartDist = dist / 4f;

		float armWidth = dist / 7f;
		float lowerArmWidth = dist / 4f;

		float[] array = skinRAnkleToFootEndVerticesDraw;

		float extraZ = distanceToExtendBeyondHandEnd * dist * 2;

		index = 0;
		// 0
		array[index++] = halfDist;
		array[index++] = -halfDist;
		array[index++] = 0;
		// 1
		array[index++] = -halfDist;
		array[index++] = -halfDist;
		array[index++] = 0;
		// 2
		array[index++] = -halfDist;// System.out.print("\t\t2x:"+-halfDist);
		array[index++] = -halfDist;// System.out.print("\t\t2y:"+-halfDist);
		array[index++] = -dist - extraZ;// System.out.println("\t\t2z:"+-dist);
		// 3
		array[index++] = halfDist;// System.out.print("\t\t3x:"+halfDist);
		array[index++] = -halfDist;// System.out.print("\t\t3y:"+-halfDist);
		array[index++] = -dist - extraZ;// System.out.println("\t\t3z:"+-dist);
		// 4
		array[index++] = halfDist;
		array[index++] = halfDist;
		array[index++] = 0;
		// 5
		array[index++] = -halfDist;
		array[index++] = halfDist;
		array[index++] = 0;
		// 6
		array[index++] = -halfDist;
		array[index++] = halfDist;
		array[index++] = -dist - extraZ;
		// 7
		array[index++] = halfDist;
		array[index++] = halfDist;
		array[index++] = -dist - extraZ;

		float deltaX = beginX - endX;
		float deltaZ = beginZ - endZ;
		// float angleYRadian = Math.atan2(deltaX,deltaZ);
		float angleYRadian = (float) Math.atan2(deltaZ, deltaX);
		float cosRadian = (float) Math.cos(angleYRadian);
		float sinRadian = (float) Math.sin(angleYRadian);
		// rotate around y axis
		for (int i = 0; i < array.length; i += 3) {
			float fX = array[i];
			float fY = array[i + 1];
			float fZ = array[i + 2];

			float dX = fZ * cosRadian - fX * sinRadian;
			float dY = fY;
			float dZ = fZ * sinRadian + fX * cosRadian;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);

		}

		for (int i = 0; i < array.length; i += 3) {
			array[i] += beginX;
			array[i + 1] += beginY;
			array[i + 2] += beginZ;
		}

		// float[] skinVerticesRef = skinLElbowToWristVerticesDraw.clone();
		float radian = (float) Math.toRadians(degreeRotateZ);
		cosRadian = (float) Math.cos(radian);
		sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = (float) array[i];// * lengthX/* X */;
			float fY = (float) array[i + 1];// * widthY/* Y */;
			float fZ = (float) array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);

			/*
			 * if (i == 0) { System.out.print("\t\tfinal0x:" + dX);
			 * System.out.print("\t\tfinal0y:" + dY);
			 * System.out.println("\t\tfinal0z:" + dZ); }
			 * 
			 * if (i == 3) { System.out.print("\t\tfinal2x:" + dX);
			 * System.out.print("\t\tfinal2y:" + dY);
			 * System.out.println("\t\tfinal2z:" + dZ); }
			 * 
			 * if (i == 6) { System.out.print("\t\tfinal3x:" + dX);
			 * System.out.print("\t\tfinal3y:" + dY);
			 * System.out.println("\t\tfinal3z:" + dZ); }
			 */
		}
	}

	public void calculateSkinLKneeToAnkle() {
		println("human1.calculateSkinLKneeToAnkle");
		int index = 3 * 8;
		float beginX = jointsVerticesDrawCurrent[index];
		float beginY = jointsVerticesDrawCurrent[index + 1];
		float beginZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 10;
		float endX = jointsVerticesDrawCurrent[index];
		float endY = jointsVerticesDrawCurrent[index + 1];
		float endZ = jointsVerticesDrawCurrent[index + 2];

		float newX = (float) Math.pow((beginX - endX), 2);
		float newY = (float) Math.pow((beginY - endY), 2);
		float newZ = (float) Math.pow((beginZ - endZ), 2);
		float dist = (float) Math.sqrt(newX + newY + newZ);

		float halfDist = dist / 2f;
		float quartDist = dist / 4f;

		float lowerArmWidth = dist / 4f;

		float[] array = skinLKneeToAnkleVerticesDraw;

		index = 0;
		// 0
		array[index++] = armWidth;
		array[index++] = -armWidth;
		array[index++] = 0f;
		// 1
		array[index++] = -armWidth;
		array[index++] = -armWidth;
		array[index++] = 0f;
		// 2
		array[index++] = -armWidth;
		array[index++] = -armWidth;
		array[index++] = -dist;
		// 3
		array[index++] = armWidth;
		array[index++] = -armWidth;
		array[index++] = -dist;
		// 4
		array[index++] = armWidth;
		array[index++] = armWidth;
		array[index++] = 0f;
		// 5
		array[index++] = -armWidth;
		array[index++] = armWidth;
		array[index++] = 0f;
		// 6
		array[index++] = -armWidth;
		array[index++] = armWidth;
		array[index++] = -dist;
		// 7
		array[index++] = armWidth;
		array[index++] = armWidth;
		array[index++] = -dist;
		// 8
		array[index++] = 0f;
		array[index++] = 0f;
		array[index++] = -halfDist;
		// 9
		array[index++] = -halfDist / 3f;
		array[index++] = 0f;
		array[index++] = 0f;
		// 10
		array[index++] = -halfDist / 3f;
		array[index++] = 0f;
		array[index++] = dist / 4f;
		// 11
		array[index++] = halfDist / 3f;
		array[index++] = 0f;
		array[index++] = dist / 4f;
		// 12
		array[index++] = halfDist / 3f;
		array[index++] = 0f;
		array[index++] = 0f;
		// 13
		array[index++] = -halfDist / 3f;
		array[index++] = -dist / 4f;
		array[index++] = 0f;
		// 14
		array[index++] = halfDist / 3f;
		array[index++] = -dist / 4f;
		array[index++] = 0f;
		// 15
		array[index++] = -halfDist / 3f;
		array[index++] = dist / 4f;
		array[index++] = 0f;
		// 16
		array[index++] = halfDist / 3f;
		array[index++] = dist / 4f;
		array[index++] = 0f;

		float deltaX = beginX - endX;
		float deltaZ = beginZ - endZ;
		// float angleYRadian = Math.atan2(deltaX,deltaZ);
		float angleYRadian = (float) Math.atan2(deltaZ, deltaX);
		float cosRadian = (float) Math.cos(angleYRadian);
		float sinRadian = (float) Math.sin(angleYRadian);
		// rotate around y axis
		for (int i = 0; i < array.length; i += 3) {
			float fX = array[i];
			float fY = array[i + 1];
			float fZ = array[i + 2];

			float dX = fZ * cosRadian - fX * sinRadian;
			float dY = fY;
			float dZ = fZ * sinRadian + fX * cosRadian;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);

		}

		for (int i = 0; i < array.length; i += 3) {
			array[i] += beginX;
			array[i + 1] += beginY;
			array[i + 2] += beginZ;
		}

		// float[] skinVerticesRef = skinLElbowToWristVerticesDraw.clone();
		float radian = (float) Math.toRadians(degreeRotateZ);
		cosRadian = (float) Math.cos(radian);
		sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = (float) array[i];// * lengthX/* X */;
			float fY = (float) array[i + 1];// * widthY/* Y */;
			float fZ = (float) array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);
		}

		// skinLElbowToWristVerticesDraw = array.clone();

	}

	public void calculateSkinRShoulderToElbow() {
		println("human1.calculateSkinRShoulderToElbow");
		int index = 3 * 15;
		float beginX = jointsVerticesDrawCurrent[index];
		float beginY = jointsVerticesDrawCurrent[index + 1];
		float beginZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 17;
		float endX = jointsVerticesDrawCurrent[index];
		float endY = jointsVerticesDrawCurrent[index + 1];
		float endZ = jointsVerticesDrawCurrent[index + 2];

		float newX = (float) Math.pow((beginX - endX), 2);
		float newY = (float) Math.pow((beginY - endY), 2);
		float newZ = (float) Math.pow((beginZ - endZ), 2);
		float dist = (float) Math.sqrt(newX + newY + newZ);

		float halfDist = dist / 2f;
		float quartDist = dist / 4f;

		float lowerArmWidth = dist / 4f;

		float[] array = skinRShoulderToElbowVerticesDraw;

		index = 0;
		// 0
		array[index++] = armWidth;
		array[index++] = -armWidth;
		array[index++] = 0f;
		// 1
		array[index++] = -armWidth;
		array[index++] = -armWidth;
		array[index++] = 0f;
		// 2
		array[index++] = -armWidth;
		array[index++] = -armWidth;
		array[index++] = -dist;
		// 3
		array[index++] = armWidth;
		array[index++] = -armWidth;
		array[index++] = -dist;
		// 4
		array[index++] = armWidth;
		array[index++] = armWidth;
		array[index++] = 0f;
		// 5
		array[index++] = -armWidth;
		array[index++] = armWidth;
		array[index++] = 0f;
		// 6
		array[index++] = -armWidth;
		array[index++] = armWidth;
		array[index++] = -dist;
		// 7
		array[index++] = armWidth;
		array[index++] = armWidth;
		array[index++] = -dist;
		// 8
		array[index++] = 0f;
		array[index++] = 0f;
		array[index++] = -halfDist;
		// 9
		array[index++] = -halfDist;
		array[index++] = 0f;
		array[index++] = 0f;
		// 10
		array[index++] = -halfDist / 3f;
		array[index++] = 0f;
		array[index++] = dist / 4f;
		// 11
		array[index++] = halfDist / 3f;
		array[index++] = 0f;
		array[index++] = dist / 4f;
		// 12
		array[index++] = halfDist;
		array[index++] = 0f;
		array[index++] = 0f;
		// 13
		array[index++] = -halfDist / 3f;
		array[index++] = -dist / 4f;
		array[index++] = 0f;
		// 14
		array[index++] = halfDist / 3f;
		array[index++] = -dist / 4f;
		array[index++] = 0f;
		// 15
		array[index++] = -halfDist / 3f;
		array[index++] = dist / 4f;
		array[index++] = 0f;
		// 16
		array[index++] = halfDist / 3f;
		array[index++] = dist / 4f;
		array[index++] = 0f;

		float deltaX = beginX - endX;
		float deltaZ = beginZ - endZ;
		// float angleYRadian = Math.atan2(deltaX,deltaZ);
		float angleYRadian = (float) Math.atan2(deltaZ, deltaX);
		float cosRadian = (float) Math.cos(angleYRadian);
		float sinRadian = (float) Math.sin(angleYRadian);
		// rotate around y axis
		for (int i = 0; i < array.length; i += 3) {
			float fX = array[i];
			float fY = array[i + 1];
			float fZ = array[i + 2];

			float dX = fZ * cosRadian - fX * sinRadian;
			float dY = fY;
			float dZ = fZ * sinRadian + fX * cosRadian;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);

		}

		for (int i = 0; i < array.length; i += 3) {
			array[i] += beginX;
			array[i + 1] += beginY;
			array[i + 2] += beginZ;
		}

		// float[] skinVerticesRef = skinLElbowToWristVerticesDraw.clone();
		float radian = (float) Math.toRadians(degreeRotateZ);
		cosRadian = (float) Math.cos(radian);
		sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = (float) array[i];// * lengthX/* X */;
			float fY = (float) array[i + 1];// * widthY/* Y */;
			float fZ = (float) array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);
		}

		// skinLElbowToWristVerticesDraw = array.clone();

	}

	public void calculateSkinRWristToHandEnd() {
		println("human1.calculateSkinRWristToHandEnd");
		int index = 3 * 19;
		float beginX = jointsVerticesDrawCurrent[index];
		float beginY = jointsVerticesDrawCurrent[index + 1];
		float beginZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 21;
		float endX = jointsVerticesDrawCurrent[index];
		float endY = jointsVerticesDrawCurrent[index + 1];
		float endZ = jointsVerticesDrawCurrent[index + 2];

		float newX = (float) Math.pow((beginX - endX), 2);
		float newY = (float) Math.pow((beginY - endY), 2);
		float newZ = (float) Math.pow((beginZ - endZ), 2);
		float dist = (float) Math.sqrt(newX + newY + newZ);

		float halfDist = dist * 2;/// 2f;
		float quartDist = dist / 4f;

		float armWidth = dist / 7f;
		float lowerArmWidth = dist / 4f;

		float[] array = skinRWristToHandEndVerticesDraw;

		float extraZ = distanceToExtendBeyondHandEnd * dist * 2;

		index = 0;
		// 0
		array[index++] = halfDist;
		array[index++] = -halfDist;
		array[index++] = 0;
		// 1
		array[index++] = -halfDist;
		array[index++] = -halfDist;
		array[index++] = 0;
		// 2
		array[index++] = -halfDist;// System.out.print("\t\t2x:"+-halfDist);
		array[index++] = -halfDist;// System.out.print("\t\t2y:"+-halfDist);
		array[index++] = -dist - extraZ;// System.out.println("\t\t2z:"+-dist);
		// 3
		array[index++] = halfDist;// System.out.print("\t\t3x:"+halfDist);
		array[index++] = -halfDist;// System.out.print("\t\t3y:"+-halfDist);
		array[index++] = -dist - extraZ;// System.out.println("\t\t3z:"+-dist);
		// 4
		array[index++] = halfDist;
		array[index++] = halfDist;
		array[index++] = 0;
		// 5
		array[index++] = -halfDist;
		array[index++] = halfDist;
		array[index++] = 0;
		// 6
		array[index++] = -halfDist;
		array[index++] = halfDist;
		array[index++] = -dist - extraZ;
		// 7
		array[index++] = halfDist;
		array[index++] = halfDist;
		array[index++] = -dist - extraZ;

		float deltaX = beginX - endX;
		float deltaZ = beginZ - endZ;
		// float angleYRadian = Math.atan2(deltaX,deltaZ);
		float angleYRadian = (float) Math.atan2(deltaZ, deltaX);
		float cosRadian = (float) Math.cos(angleYRadian);
		float sinRadian = (float) Math.sin(angleYRadian);
		// rotate around y axis
		for (int i = 0; i < array.length; i += 3) {
			float fX = array[i];
			float fY = array[i + 1];
			float fZ = array[i + 2];

			float dX = fZ * cosRadian - fX * sinRadian;
			float dY = fY;
			float dZ = fZ * sinRadian + fX * cosRadian;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);

		}

		for (int i = 0; i < array.length; i += 3) {
			array[i] += beginX;
			array[i + 1] += beginY;
			array[i + 2] += beginZ;
		}

		// float[] skinVerticesRef = skinLElbowToWristVerticesDraw.clone();
		float radian = (float) Math.toRadians(degreeRotateZ);
		cosRadian = (float) Math.cos(radian);
		sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = (float) array[i];// * lengthX/* X */;
			float fY = (float) array[i + 1];// * widthY/* Y */;
			float fZ = (float) array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);

			/*
			 * if (i == 0) { System.out.print("\t\tfinal0x:" + dX);
			 * System.out.print("\t\tfinal0y:" + dY);
			 * System.out.println("\t\tfinal0z:" + dZ); }
			 * 
			 * if (i == 3) { System.out.print("\t\tfinal2x:" + dX);
			 * System.out.print("\t\tfinal2y:" + dY);
			 * System.out.println("\t\tfinal2z:" + dZ); }
			 * 
			 * if (i == 6) { System.out.print("\t\tfinal3x:" + dX);
			 * System.out.print("\t\tfinal3y:" + dY);
			 * System.out.println("\t\tfinal3z:" + dZ); }
			 */
		}
	}

	public void calculateSkinNeck() {
		println("human1.calculateSkinNeck");

		int index = 3 * 3;
		float neckX = jointsVerticesDrawCurrent[index];
		float neckY = jointsVerticesDrawCurrent[index + 1];
		float neckZ = jointsVerticesDrawCurrent[index + 2];

		float distWidth = 1f / 8f / 3f / 2f;
		float distHeight = 1f / 8f;

		float[] array = skinNeckVerticesDraw;

		index = 0;
		// 0
		array[index++] = distWidth + neckX;
		array[index++] = distWidth + neckY;
		array[index++] = distHeight + neckZ;
		// 1
		array[index++] = distWidth + neckX;
		array[index++] = -distWidth + neckY;
		array[index++] = distHeight + neckZ;
		// 2
		array[index++] = distWidth + neckX;
		array[index++] = -distWidth + neckY;
		array[index++] = -distHeight + neckZ;
		// 3
		array[index++] = distWidth + neckX;
		array[index++] = distWidth + neckY;
		array[index++] = -distHeight + neckZ;
		// 4
		array[index++] = -distWidth + neckX;
		array[index++] = distWidth + neckY;
		array[index++] = distHeight + neckZ;
		// 5
		array[index++] = -distWidth + neckX;
		array[index++] = -distWidth + neckY;
		array[index++] = distHeight + neckZ;
		// 6
		array[index++] = -distWidth + neckX;
		array[index++] = -distWidth + neckY;
		array[index++] = -distHeight + neckZ;
		// 7
		array[index++] = -distWidth + neckX;
		array[index++] = distWidth + neckY;
		array[index++] = -distHeight + neckZ;

		/*
		 * float deltaX = beginX - endX; float deltaZ = beginZ - endZ; // float
		 * angleYRadian = Math.atan2(deltaX,deltaZ); float angleYRadian =
		 * Math.atan2(deltaZ, deltaX); float cosRadian = Math.cos(angleYRadian);
		 * float sinRadian = Math.sin(angleYRadian); // rotate around y axis for
		 * (int i = 0; i < array.length; i += 3) { float fX = array[i]; float fY
		 * = array[i + 1]; float fZ = array[i + 2];
		 * 
		 * float dX = fZ * cosRadian - fX * sinRadian; float dY = fY; float dZ =
		 * fZ * sinRadian + fX * cosRadian;
		 * 
		 * array[i] = (float) (dX); array[i + 1] = (float) (dY); array[i + 2] =
		 * (float) (dZ);
		 * 
		 * }
		 * 
		 * for (int i = 0; i < array.length; i += 3) { array[i] += beginX;
		 * array[i + 1] += beginY; array[i + 2] += beginZ; }
		 */

		// float[] skinVerticesRef = skinLElbowToWristVerticesDraw.clone();
		float radian = (float) Math.toRadians(degreeRotateZ);
		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = (float) array[i];// * lengthX/* X */;
			float fY = (float) array[i + 1];// * widthY/* Y */;
			float fZ = (float) array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);
		}

	}

	public void calculateSkinNeckToNipLine() {
		println("human1.calculateSkinNeckToNipLine");

		int index = 3 * 14;
		float shoulderRX = jointsVerticesDrawCurrent[index];
		println("shoulderRX:" + shoulderRX);
		float shoulderRY = jointsVerticesDrawCurrent[index + 1];
		println("shoulderRY:" + shoulderRY);
		float shoulderRZ = jointsVerticesDrawCurrent[index + 2];
		println("shoulderRZ:" + shoulderRZ);

		index = 3 * 15;
		float shoulderLX = jointsVerticesDrawCurrent[index];
		float shoulderLY = jointsVerticesDrawCurrent[index + 1];
		float shoulderLZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 2;
		float nipX = jointsVerticesDrawCurrent[index];
		float nipY = jointsVerticesDrawCurrent[index + 1];
		float nipZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 1;
		float navelX = jointsVerticesDrawCurrent[index];
		float navelY = jointsVerticesDrawCurrent[index + 1];
		float navelZ = jointsVerticesDrawCurrent[index + 2];

		// float headDist = (float)heightZ/8f;
		float shoulderReduce = 1f / 8f / 4f;
		println("shoulderReduce:" + shoulderReduce);
		float chestThickness = 1f / 8f / 4f;
		println("chestThickness:" + chestThickness);

		float[] array = skinNeckToNipLineVerticesDraw;

		index = 0;
		// 0
		array[index++] = shoulderLX + chestThickness;
		array[index++] = shoulderLY - shoulderReduce;
		array[index++] = shoulderLZ;
		// 1
		array[index++] = shoulderRX + chestThickness;
		array[index++] = shoulderRY + shoulderReduce;
		array[index++] = shoulderRZ;
		// 2
		array[index++] = shoulderRX + chestThickness;
		array[index++] = shoulderRY + shoulderReduce;
		array[index++] = nipZ;
		// 3
		array[index++] = shoulderLX + chestThickness;
		array[index++] = shoulderLY - shoulderReduce;
		array[index++] = nipZ;
		// 4
		array[index++] = shoulderLX - chestThickness;
		array[index++] = shoulderLY - shoulderReduce;
		array[index++] = shoulderLZ;
		// 5
		array[index++] = shoulderRX - chestThickness;
		array[index++] = shoulderRY + shoulderReduce;
		array[index++] = shoulderRZ;
		// 6
		array[index++] = shoulderRX - chestThickness;
		array[index++] = shoulderRY + shoulderReduce;
		array[index++] = nipZ;
		// 7
		array[index++] = shoulderLX - chestThickness;
		array[index++] = shoulderLY - shoulderReduce;
		array[index++] = nipZ;
		// 8
		array[index++] = navelX;
		array[index++] = navelY;
		array[index++] = navelZ;

		/*
		 * float deltaX = beginX - endX; float deltaZ = beginZ - endZ; // float
		 * angleYRadian = Math.atan2(deltaX,deltaZ); float angleYRadian =
		 * Math.atan2(deltaZ, deltaX); float cosRadian = Math.cos(angleYRadian);
		 * float sinRadian = Math.sin(angleYRadian); // rotate around y axis for
		 * (int i = 0; i < array.length; i += 3) { float fX = array[i]; float fY
		 * = array[i + 1]; float fZ = array[i + 2];
		 * 
		 * float dX = fZ * cosRadian - fX * sinRadian; float dY = fY; float dZ =
		 * fZ * sinRadian + fX * cosRadian;
		 * 
		 * array[i] = (float) (dX); array[i + 1] = (float) (dY); array[i + 2] =
		 * (float) (dZ);
		 * 
		 * }
		 * 
		 * for (int i = 0; i < array.length; i += 3) { array[i] += beginX;
		 * array[i + 1] += beginY; array[i + 2] += beginZ; }
		 */

		// float[] skinVerticesRef = skinLElbowToWristVerticesDraw.clone();
		float radian = (float) Math.toRadians(degreeRotateZ);
		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = (float) array[i];// * lengthX/* X */;
			float fY = (float) array[i + 1];// * widthY/* Y */;
			float fZ = (float) array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);
		}

	}

	public void calculateSkinPelvis() {
		println("human1.calculateSkinPelvis");

		int index = 3 * 6;
		float hipLX = jointsVerticesDrawCurrent[index];
		float hipLY = jointsVerticesDrawCurrent[index + 1];
		float hipLZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 7;
		float hipRX = jointsVerticesDrawCurrent[index];
		float hipRY = jointsVerticesDrawCurrent[index + 1];
		float hipRZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 0;
		float crotchX = jointsVerticesDrawCurrent[index];
		float crotchY = jointsVerticesDrawCurrent[index + 1];
		float crotchZ = jointsVerticesDrawCurrent[index + 2];

		// float headDist = (float)heightZ/8f;
		float waistWidth = (1f / 8f / 4f);
		float waistLength = (1f / 8f / 4f);
		float waistHeight = (1f / 8f / 4f);

		float[] array = skinPelvisVerticesDraw;

		index = 0;
		// 0
		array[index++] = hipLX + waistLength;
		array[index++] = hipLY + waistWidth;
		array[index++] = hipLZ + waistHeight;
		// 1
		array[index++] = hipLX + waistLength;
		array[index++] = hipLY + waistWidth;
		array[index++] = hipLZ - waistHeight;
		// 2
		array[index++] = crotchX;// +waistLength;
		array[index++] = crotchY;// +waistWidth;
		array[index++] = crotchZ - waistHeight * 2;// Z+waistHeight;
		// 3
		array[index++] = hipRX + waistLength;
		array[index++] = hipRY - waistWidth;
		array[index++] = hipRZ - waistHeight;
		// 4
		array[index++] = hipRX + waistLength;
		array[index++] = hipRY - waistWidth;
		array[index++] = hipRZ + waistHeight;
		// 5
		array[index++] = hipLX - waistLength;
		array[index++] = hipLY + waistWidth;
		array[index++] = hipLZ + waistHeight;
		// 6
		array[index++] = hipLX - waistLength;
		array[index++] = hipLY + waistWidth;
		array[index++] = hipLZ - waistHeight;
		// 7
		array[index++] = hipRX - waistLength;
		array[index++] = hipRY - waistWidth;
		array[index++] = hipRZ - waistHeight;
		// 8
		array[index++] = hipRX - waistLength;
		array[index++] = hipRY - waistWidth;
		array[index++] = hipRZ + waistHeight;

		float radian = (float) Math.toRadians(degreeRotateZ);
		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = (float) array[i];// * lengthX/* X */;
			float fY = (float) array[i + 1];// * widthY/* Y */;
			float fZ = (float) array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);
		}

	}

	public void calculateSkinTummy() {
		println("human1.calculateSkinTummy");

		int index = 3 * 2;
		float nipX = jointsVerticesDrawCurrent[index];
		float nipY = jointsVerticesDrawCurrent[index + 1];
		float nipZ = jointsVerticesDrawCurrent[index + 2];

		index = 3 * 0;
		float crotchX = jointsVerticesDrawCurrent[index];
		float crotchY = jointsVerticesDrawCurrent[index + 1];
		float crotchZ = jointsVerticesDrawCurrent[index + 2];

		// float headDist = (float)heightZ/8f;
		float waistWidth = 1f / 8f / 4f;
		float waistLength = 1f / 8f / 4f;

		float[] array = skinTummyVerticesDraw;

		index = 0;
		// 0
		array[index++] = nipX + waistLength;
		array[index++] = nipY - waistWidth;
		array[index++] = nipZ;
		// 1
		array[index++] = nipX + waistLength;
		array[index++] = nipY + waistWidth;
		array[index++] = nipZ;
		// 2
		array[index++] = crotchX + waistLength;
		array[index++] = crotchY + waistWidth;
		array[index++] = crotchZ;
		// 3
		array[index++] = crotchX + waistLength;
		array[index++] = crotchY - waistWidth;
		array[index++] = crotchZ;
		// 4
		array[index++] = nipX - waistLength;
		array[index++] = nipY - waistWidth;
		array[index++] = nipZ;
		// 5
		array[index++] = nipX - waistLength;
		array[index++] = nipY + waistWidth;
		array[index++] = nipZ;
		// 6
		array[index++] = crotchX - waistLength;
		array[index++] = crotchY + waistWidth;
		array[index++] = crotchZ;
		// 7
		array[index++] = crotchX - waistLength;
		array[index++] = crotchY - waistWidth;
		array[index++] = crotchZ;

		float radian = (float) Math.toRadians(degreeRotateZ);
		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		// rotate Z axis
		for (int i = 0; i < array.length; i += 3) {

			float fX = (float) array[i];// * lengthX/* X */;
			float fY = (float) array[i + 1];// * widthY/* Y */;
			float fZ = (float) array[i + 2];// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= heightZ;
			dY *= heightZ;
			dZ *= heightZ;

			dX += x;
			dY += y;
			dZ += z;

			array[i] = (float) (dX);
			array[i + 1] = (float) (dY);
			array[i + 2] = (float) (dZ);
		}

	}

	// boolean doCalc = true;
	public void draw() {

		// if(doCalc)
		{
			calculateJointsVertices();
			// doCalc=false;
		}

		if (isDrawJoints) {
			drawJoints();
		}

		if (isDrawSkin) {
			drawSkin();
			drawSkinHair();
		}
	}

	public void drawJoints() {// System.out.println("human.drawJoints");
		bindJointsVertexData();
		glUseProgram(jointsProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(jointsGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(jointsFB));
		glUniform4f(jointsGLRGBAUniformLocation, jointsRed, jointsGreen, jointsBlue, 1.0f);

		glBindVertexArray(jointsVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, jointsVBOIID);
		glDrawElements(GL_LINES, jointsIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glDrawElements(GL_POINTS, jointsIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);
	}

	public void drawSkin() {
		println("human.drawSkin");
		bindSkinVertexData();
		println("human.drawSkin just finished bindSkinVertexData");
		glUseProgram(skinProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(skinGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(skinFB));
		glUniform4f(skinGLRGBAUniformLocation, skinRed, skinGreen, skinBlue, skinAlpha);

		glBindVertexArray(skinVAOID);
		glEnableVertexAttribArray(0);

		// skinIndicesCount = skinIndices.length;
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinVBOIID);
		glDrawElements(GL_TRIANGLES, skinIndicesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		println("human.drawSkin skinIndicesCount:" + skinIndicesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);
	}

	public void drawSkinHair() {
		println("human.drawSkinHair");
		bindSkinHairVertexData();
		println("human.drawSkinHair just finished bindSkinHairVertexData");
		glUseProgram(skinHairProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(skinHairGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(skinHairFB));
		glUniform4f(skinHairGLRGBAUniformLocation, skinHairRed, skinHairGreen, skinHairBlue, skinHairAlpha);

		glBindVertexArray(skinHairVAOID);
		glEnableVertexAttribArray(0);

		// skinIndicesCount = skinIndices.length;
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinHairVBOIID);
		glDrawElements(GL_TRIANGLES, skinHairIndicesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		println("human.drawSkinHair skinHairIndicesCount:" + skinHairIndicesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);
	}

	public void engaged()
	{ System.out.println("human1.engaged()");}
	
	public void faceLeft() {
		degreeRotateZ = (degreeRotateZ + turnDegree) % 360.0f;
		// move();
	}

	public void facePoint(float newX, float newY, float newZ) {
		float dX = newX - x;
		float dY = newY - y;
		degreeRotateZ = (float) (Math.atan2(dY, dX) * 180.0f / Math.PI);
	}

	public void faceRight() {
		float change = (degreeRotateZ - turnDegree);
		degreeRotateZ = (float) (change <= 0.0d ? (360.0d - turnDegree) : change % 360.0d);
		// move();
	}

	public String getActorName() {
		return this.actorName;
	}

	public Vector3f getCenterXYZ()
	{ return centerXYZ;}
	
	public String getClassName()
	{ return this.className;}

	public Vector3f getDiameterXYZ()
	{ return diameterXYZ;}
	
	public Vector3f getEngageableDiameterXYZ()
	{ return engageableDiameterXYZ;}
	
	public float getFacingDegrees() {
		return degreeRotateZ;
	}

	private float getFacingX() {
		return (float) (moveDistance * Math.cos(Math.toRadians(degreeRotateZ)));
	}

	private float getFacingY() {
		return (float) (moveDistance * Math.sin(Math.toRadians(degreeRotateZ)));
	}

	private float getForwardX() {
		return x + getFacingX();
	}

	private float getForwardY() {
		return y + getFacingY();
	}

	private float getLeftX() {
		return (float) (moveDistance * Math.cos(Math.toRadians(degreeRotateZ + 90f)));
	}

	private float getLeftY() {
		return (float) (moveDistance * Math.sin(Math.toRadians(degreeRotateZ + 90f)));
	}

	private float getRightX() {
		return (float) (moveDistance * Math.cos(Math.toRadians(degreeRotateZ - 90f)));
	}

	private float getRightY() {
		return (float) (moveDistance * Math.sin(Math.toRadians(degreeRotateZ - 90f)));
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getZ() {
		return z;
	}

	public Vector3f getXYZ() {
		Vector3f v = new Vector3f();
		v.x = this.x;
		v.y = this.y;
		v.z = this.z;
		return v;
	}

	public void glCleanup() {
		glUseProgram(0);
		if (jointsProgramId != 0) {
			if (jointsVertexShaderId != 0) {
				glDetachShader(jointsProgramId, jointsVertexShaderId);
			}
			if (jointsFragmentShaderId != 0) {
				glDetachShader(jointsProgramId, jointsFragmentShaderId);
			}
			glDeleteProgram(jointsProgramId);
		}

		if (skinProgramId != 0) {
			if (skinVertexShaderId != 0) {
				glDetachShader(skinProgramId, skinVertexShaderId);
			}
			if (skinFragmentShaderId != 0) {
				glDetachShader(skinProgramId, skinFragmentShaderId);
			}
			glDeleteProgram(skinProgramId);
		}

		if (skinHairProgramId != 0) {
			if (skinHairVertexShaderId != 0) {
				glDetachShader(skinHairProgramId, skinHairVertexShaderId);
			}
			if (skinHairFragmentShaderId != 0) {
				glDetachShader(skinHairProgramId, skinHairFragmentShaderId);
			}
			glDeleteProgram(skinHairProgramId);
		}
	}

	public void glConstruct() {
		if (isDrawJoints) {
			glConstructJoints();
		}

		if (isDrawSkin) {
			glConstructSkin();
			glConstructSkinHair();
		}

	}

	public void glConstructJoints() {
		try {
			try {
				jointsProgramId = glCreateProgram();

				glCreateVertexShaderJoints("#version 130\nuniform mat4 MVP; in vec3 position; void main() { gl_Position =MVP* vec4(position, 1.0);  gl_PointSize = 3.0; }");

				glCreateFragmentShaderJoints("#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(jointsProgramId);

				glUseProgram(jointsProgramId);
				jointsGLMVPUniformLocation = glGetUniformLocation(jointsProgramId, "MVP");
				jointsGLRGBAUniformLocation = glGetUniformLocation(jointsProgramId, "RGBA");
				glUseProgram(0);

				glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (jointsProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glConstructSkin() {
		try {
			try {
				skinProgramId = glCreateProgram();

				glCreateVertexShaderSkin("#version 130\nuniform mat4 MVP; in vec3 position; void main() { gl_Position =MVP* vec4(position, 1.0); }");

				glCreateFragmentShaderSkin("#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(skinProgramId);

				glUseProgram(skinProgramId);
				skinGLMVPUniformLocation = glGetUniformLocation(skinProgramId, "MVP");
				skinGLRGBAUniformLocation = glGetUniformLocation(skinProgramId, "RGBA");
				glUseProgram(0);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (skinProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glConstructSkinHair() {
		try {
			try {
				skinHairProgramId = glCreateProgram();

				glCreateVertexShaderSkinHair("#version 130\nuniform mat4 MVP; in vec3 position; void main() { gl_Position =MVP* vec4(position, 1.0); }");

				glCreateFragmentShaderSkinHair("#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(skinHairProgramId);

				glUseProgram(skinHairProgramId);
				skinHairGLMVPUniformLocation = glGetUniformLocation(skinHairProgramId, "MVP");
				skinHairGLRGBAUniformLocation = glGetUniformLocation(skinHairProgramId, "RGBA");
				glUseProgram(0);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (skinHairProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glCreateFragmentShaderJoints(String shaderCode) throws Exception {
		jointsFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, jointsProgramId);
	}

	public void glCreateFragmentShaderSkin(String shaderCode) throws Exception {
		skinFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, skinProgramId);
	}

	public void glCreateFragmentShaderSkinHair(String shaderCode) throws Exception {
		skinHairFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, skinHairProgramId);
	}

	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShaderJoints(String shaderCode) throws Exception {
		jointsVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, jointsProgramId);
	}

	public void glCreateVertexShaderSkin(String shaderCode) throws Exception {
		skinVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, skinProgramId);
	}

	public void glCreateVertexShaderSkinHair(String shaderCode) throws Exception {
		skinHairVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, skinHairProgramId);
	}

	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}

	public void moveBackward() {

		if (bodyStateNew != bodyStates.firstStep && bodyStateNew != bodyStates.walkingRForwardToStanding && bodyStateNew != bodyStates.walkingRForward && bodyStateNew != bodyStates.walkingLForwardToStanding && bodyStateNew != bodyStates.walkingLForward) {
			{
				bodyStateOld = bodyStateNew;
				bodyStateNew = bodyStates.firstStep;
				actionStart = (int) System.currentTimeMillis();
				isStateChange = true;
			}
		}

		float facingX = x - getFacingX();
		float facingY = y - getFacingY();
		float facingZ = z;
		if (lManage != null) {
			facingZ = lManage.getZ(facingX, facingY);
			if (facingZ - z > stepUpHeight || z - facingZ > stepDownHeight) {
				return;
			}
		}

		x = facingX;
		y = facingY;
		z = facingZ;

		if (x < 0f) {
			x = 0f;
		}

		if (y < 0f) {
			y = 0f;
		}

		if (y > maxStepDistance) {
			y = maxStepDistance;
		}

		if (x > maxStepDistance) {
			x = maxStepDistance;
		}
	}

	public void moveForward() {

		if (bodyStateNew != bodyStates.firstStep && bodyStateNew != bodyStates.walkingRForwardToStanding && bodyStateNew != bodyStates.walkingRForward && bodyStateNew != bodyStates.walkingLForwardToStanding && bodyStateNew != bodyStates.walkingLForward) {

			{
				bodyStateOld = bodyStateNew;
				bodyStateNew = bodyStates.firstStep;
				actionStart = (int) System.currentTimeMillis();
				isStateChange = true;
			}
		}

		/*
		 * x += getFacingX(); y += getFacingY();
		 */

		float facingX = x + getFacingX();
		float facingY = y + getFacingY();
		float facingZ = z;
		if (lManage != null) {
			facingZ = lManage.getZ(facingX, facingY);
			if (facingZ - z > stepUpHeight || z - facingZ > stepDownHeight)// if(Math.abs(z-facingZ)>stepHeight)
			{
				return;
			}
		}

		x = facingX;
		y = facingY;
		z = facingZ;

		if (x < 0f) {
			x = 0f;
		}

		if (y < 0f) {
			y = 0f;
		}

		if (y > maxStepDistance) {
			y = maxStepDistance;
		}

		if (x > maxStepDistance) {
			x = maxStepDistance;
		}

	}

	public void moveDown() {
		y -= moveDistance;
		// move();
	}

	public void moveKick() {
		if (bodyStateNew != bodyStates.kicking) {
			bodyStateOld = bodyStateNew;
			bodyStateNew = bodyStates.kicking;
			actionStart = (int) System.currentTimeMillis();
			isStateChange = true;
		}
	}

	public void moveLeft() {
		/*
		 * x += getLeftX(); y += getLeftY();
		 */
		// move();

		float facingX = x + getLeftX();
		float facingY = y + getLeftY();
		float facingZ = z;
		if (lManage != null) {
			facingZ = lManage.getZ(facingX, facingY);
			if (facingZ - z > stepUpHeight || z - facingZ > stepDownHeight)// if(Math.abs(z-facingZ)>stepHeight)
			{
				return;
			}
		}

		x = facingX;
		y = facingY;
		z = facingZ;

		if (x < 0f) {
			x = 0f;
		}

		if (y < 0f) {
			y = 0f;
		}

		if (y > maxStepDistance) {
			y = maxStepDistance;
		}

		if (x > maxStepDistance) {
			x = maxStepDistance;
		}
	}

	public void movePunch() {
		if (bodyStateNew != bodyStates.punching) {
			bodyStateOld = bodyStateNew;
			bodyStateNew = bodyStates.punching;
			actionStart = (int) System.currentTimeMillis();
			isStateChange = true;
		}
	}

	public void moveRight() {
		/*
		 * x += getRightX(); y += getRightY();
		 */
		// move();

		float facingX = x + getRightX();
		float facingY = y + getRightY();
		float facingZ = z;
		if (lManage != null) {
			facingZ = lManage.getZ(facingX, facingY);
			if (facingZ - z > stepUpHeight || z - facingZ > stepDownHeight)// if(Math.abs(z-facingZ)>stepHeight)
			{
				return;
			}
		}

		x = facingX;
		y = facingY;
		z = facingZ;

		if (x < 0f) {
			x = 0f;
		}

		if (y < 0f) {
			y = 0f;
		}

		if (y > maxStepDistance) {
			y = maxStepDistance;
		}

		if (x > maxStepDistance) {
			x = maxStepDistance;
		}
	}

	public void moveSquat() {
		if (bodyStateNew != bodyStates.squating) {
			bodyStateOld = bodyStateNew;
			bodyStateNew = bodyStates.squating;
			actionStart = (int) System.currentTimeMillis();
			isStateChange = true;
		}
	}

	public void moveToStanding() {
		if (bodyStateNew != bodyStates.standing) {
			bodyStateOld = bodyStateNew;
			bodyStateNew = bodyStates.standing;
			actionStart = (int) System.currentTimeMillis();
			isStateChange = true;
		}
	}

	public void moveUp() {
		y += moveDistance;
		// move();
	}

	public void printHeight() {
		println("height:" + heightZ);
	}

	private void println(String s) {
		if (this.showConsoleOutput) {
			System.out.println(s);
		}
	}

	public void printStateInfo() {
		String newState = "";
		String oldState = "";

		println("old state:" + oldState + "\t\tnew state:" + newState);
	}

	public void printXYZ() {
		println("X:" + x + "\t\ty:" + y + "\t\tz:" + z + "\t\tdegreeRotateZ:" + degreeRotateZ);
	}

	public void registerWithActorManager() {
		//manager_actors.getInstance().add(this);
	}

	public void registerWithAttackableManager() {
		//manager_attackables.getInstance().add(this);
	}

	public void registerWithEngageableManager() {
		//manager_engageables.getInstance().add(this);
	}

	public void registerWithStepCollisionManager() {
		//manager_step_collisions.getInstance().add(this);
	}

	public void setActorName(String n) {
		this.actorName = n;
	}

	public void setHeight(float x) {
		heightZ = x;
		moveDistance = heightZ * .1f;
		armWidth = (float) (heightZ / 400f);
		
		this.diameterXYZ.x = heightZ/7f;
		this.diameterXYZ.y= heightZ/7f;
		this.diameterXYZ.z= heightZ/2f;
		
		
		this.engageableDiameterXYZ.x = heightZ / 2f ;
		this.engageableDiameterXYZ.y = heightZ / 2f;
		this.engageableDiameterXYZ.z = heightZ / 1.5f;
	}

	public void setLandManager(manager_land l) {
		lManage = l;
	}

	public void setMaxStepDistance(int newDist) {
		maxStepDistance = newDist;
	}

//	public void setMVP(Matrix4f mvp) {
//		MVP = mvp;
//	}

	public void setRGB(float r, float g, float b) {
		skinRed = r;
		skinGreen = g;
		skinBlue = b;
	}

	public void setVertices(int indexCounter, float cosRadian, float sinRadian, float fX, float fY, float fZ, float[] array) {

		// System.out.println("");

		float dX = fX * cosRadian - fY * sinRadian;
		float dY = fY * cosRadian + fX * sinRadian;
		float dZ = fZ;
		// System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" +
		// dY + "\t\tdZ:" + dZ);

		dX *= heightZ;
		dY *= heightZ;
		dZ *= heightZ;
		// System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" +
		// dY + "\t\tdZ:" + dZ);

		dX += x;
		dY += y;
		dZ += z;
		// System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" +
		// dY + "\t\tdZ:" + dZ);

		array[indexCounter++] = (dX);
		array[indexCounter++] = (dY);
		array[indexCounter] = (dZ);

	}

	public void setXYZ(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		// move();
		
		this.centerXYZ.x =x;
		this.centerXYZ.y=y;
		this.centerXYZ.z=z + heightZ/2f;	
	
		
	}

	public void setXY(float newX, float newY) {
		x = newX;
		y = newY;
	}

	public void setZ(float newZ) {
		z = newZ;
	}

	public void takeDamage(damage d)
	{
		System.out.println("human1 : Ouch, I just took "+d.getPhysicalDamage()+" of physical damage");
	}
}
