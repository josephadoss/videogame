package local.source.actors.humans.human2;

import org.joml.Vector3f;

public class human2_body_skinMesh_band {

	Vector3f FRVertex = new Vector3f(0f,0f,0f);
	Vector3f FLVertex = new Vector3f(0f,0f,0f);
	Vector3f BRVertex = new Vector3f(0f,0f,0f);
	Vector3f BLVertex = new Vector3f(0f,0f,0f);

	Vector3f FRRotate = new Vector3f(0f,0f,0f);
	Vector3f FLRotate = new Vector3f(0f,0f,0f);
	Vector3f BRRotate = new Vector3f(0f,0f,0f);
	Vector3f BLRotate = new Vector3f(0f,0f,0f);

	Vector3f joint;// = new Vector3f();
	Vector3f jointRotate;

	float rlWidth, fbWidth;
	float dist;
	float z;
	
	float r,g,b;

	human2_body_skinMesh_band below;

	public human2_body_skinMesh_band() {
		/*FRRotate.z = 90 - 20;
		BRRotate.z = 90 + 20;
		FLRotate.z = 90 - 20 - 180;
		BLRotate.z = 90 + 20 - 180;

		FRRotate.x = 0;
		BRRotate.x = 0;
		FLRotate.x = 0;
		BLRotate.x = 0;*/
		
		//FRRotate.y = 0;
		//BRRotate.y = 0;
		//FLRotate.y = 0;
		//BLRotate.y = 0;
		r=1.0f;
		g=0.0f;
		b=0.0f;
	}
	
	public float getBlue()
	{ return b;}
	
	public float getFBWidth()
	{return fbWidth;}

	public float getGreen()
	{return g;}
	
	public float getRed()
	{return r;}
	
	public float getRLWidth()
	{return rlWidth;}
	
	public float getZ()
	{return z;}
		
	public void setBRRotate(float newX, float newY, float newZ) {
		BRRotate.x = newX;
		BRRotate.y = newY;
		BRRotate.z = newZ;
	}

	public void setBLRotate(float newX, float newY, float newZ) {
		BLRotate.x = newX;
		BLRotate.y = newY;
		BLRotate.z = newZ;
	}

	public void setDistance(float d) {
		dist = d;
	}
	
	public void setFBWidth(float w)
	{fbWidth=w;}

	public void setFRRotate(float newX, float newY, float newZ) {
		FRRotate.x = newX;
		FRRotate.y = newY;
		FRRotate.z = newZ;
	}


	public void setFLRotate(float newX, float newY, float newZ) {
		FLRotate.x = newX;
		FLRotate.y = newY;
		FLRotate.z = newZ;
	}


	public void setJoint(Vector3f newJ) {
		joint = newJ;
	}
	
	public void setJointRotate(Vector3f newJ) {
		jointRotate = newJ;
	}

	public void setLRWidth(float w)
	{rlWidth=w;}
	
	public void setRGB(float nr, float ng, float nb)
	{ r=nr; g=ng; b=nb;}
	
	public void setZFromJoint(float newZ)
	{z=newZ;}
}
