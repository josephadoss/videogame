package local.source.actors.humans.human2;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL32.*;
import static org.lwjgl.opengl.GL40.*;
import static org.lwjgl.stb.STBImage.*;
import org.lwjgl.system.MemoryStack;

import local.source.manager_mvp;

import java.nio.*;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.opengl.GLUtil;
import org.lwjgl.system.Callback;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.glfw.GLFW.*;

import static org.lwjgl.system.MemoryUtil.*;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;

import static org.lwjgl.system.MemoryUtil.memUTF8;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

import org.lwjgl.PointerBuffer;

public class human2_eye_textured {
	Vector3f vertexHeadBottom;
	Vector3f vertexHeadTop;
	Vector3f rotateHeadBottom;

	float heightZ, degreeRotateZ, x, y, z;
	float skinRed=0f;
	float skinBlue=1f;
	float skinGreen=0f;
	
	int skinEyeIndicesCount;

	int[] skinEyeIndicesDraw = { //0, 2, 1 //
			//, 0, 3, 2//
			 0,1,2//
			 ,0,2,3//
	};

	float[] skinEyeVerticesDraw = {
			// vertices //textures
			0f, 0f, 0f, 1f, 1f, // 0 Bottom Right , outward bottom left
			0f, 0f, 0f, 1f, 0f, // 1 top right , outward top left
			10f, 10f, 0f, 0f, 0f, // 2 top left , outward top right
			-10f, -30f, 0f, 0f, 1f // 3 bottom left , outward bottom right
	};

	int skinEyeVAOID = -1;
	int skinEyeVBOID = -1;
	int skinEyeVBOIID = -1;
	int textureID = -1;

	public int skinEyeGLMVPUniformLocation;
	public int skinEyeGLRGBAUniformLocation;

	public FloatBuffer skinEyeFB = BufferUtils.createFloatBuffer(20);// (16);

	public int skinEyeProgramId;
	public int skinEyeVertexShaderId;
	public int skinEyeFragmentShaderId;
	public int skinEyeGLTextureCoordLocation;
	public int skinEyeGLTextureImageUniform;

	boolean isSkinEyeIndexBound = false;

	//public Matrix4f MVP;

	FloatBuffer verticesBufferSkinEye;

	Quaternionf dest1 = new Quaternionf();
	Quaternionf dest2 = new Quaternionf();
	Vector3f v = new Vector3f();

	float distTR, distTL, distBR, distBL;

	//float distArray[] = new float[25];

	float eyeColorRed = 0.545f;
	float eyeColorGreen = 0.271f;
	float eyeColorBlue = 0.075f;

	Vector3f topLeftVertex;
	Vector3f topRightVertex;
	Vector3f botLeftVertex;
	Vector3f botRightVertex;

	Vector3f topLeftRotate;
	Vector3f topRightRotate;
	Vector3f botLeftRotate;
	Vector3f botRightRotate;

	IntBuffer textureWidth = BufferUtils.createIntBuffer(1);
	IntBuffer textureHeight = BufferUtils.createIntBuffer(1);
	IntBuffer textureComponents = BufferUtils.createIntBuffer(1);
	ByteBuffer textureData;

	String textureSourceLeft = "resources" + File.separatorChar + "actors" + File.separatorChar + "states" + File.separatorChar + "human2" + File.separatorChar + "eyes" + File.separatorChar + "eye-default-Left.png";
	String textureSourceRight = "resources" + File.separatorChar + "actors" + File.separatorChar + "states" + File.separatorChar + "human2" + File.separatorChar + "eyes" + File.separatorChar + "eye-default-right.png";

	String sideOfFace = "left";

	public human2_eye_textured(String lr) {
		//System.out.println("human2_eye_textured just entered constructor with arg:" + lr);
		String textureSource;
		if (lr == "left") {
			textureSource = textureSourceLeft;
		} else {
			textureSource = textureSourceRight;
		}
		sideOfFace = lr;

		//System.out.println("human2_eye_textured using this value for textureSource:" + textureSource);

		try {
			textureData = stbi_load_from_memory(ioResourceToByteBuffer(textureSource, 20 * 1024), textureWidth, textureHeight, textureComponents, 4);
		} catch (Exception e) {
			System.out.println("human2_eye_textured constructor : error thrown making the texture message:" + e.getMessage() + " stacktrace:" + e.getStackTrace());
		}

		try {
			glConstruct();
		} catch (Exception e) {
			System.out.println("error caught in human2_eye_textured constructor around glConstruct() message:" + e.getMessage() + " stacktrace:" + e.getStackTrace());
		}
	}

	public float[] actionJoints() {

		float[] vertices = skinEyeVerticesDraw;
		int index = 0;

		vertices[index++] = botRightVertex.x;
		vertices[index++] = botRightVertex.y;
		vertices[index++] = botRightVertex.z;
		//if(sideOfFace=="left"){
		vertices[index++] = 0f;
		vertices[index++] = 1f;
		//}else{
		//	vertices[index++] = 0f;
		//	vertices[index++] = 0f;
		//}

		vertices[index++] = topRightVertex.x;
		vertices[index++] = topRightVertex.y;
		vertices[index++] = topRightVertex.z;
		//if(sideOfFace=="left"){
		vertices[index++] = 0f;
		vertices[index++] = 0f;
		//}else{
		//	vertices[index++] = 0f;
		//	vertices[index++] = 1f;
		//}

		vertices[index++] = topLeftVertex.x;
		vertices[index++] = topLeftVertex.y;
		vertices[index++] = topLeftVertex.z;
		//if(sideOfFace=="left"){
		vertices[index++] = 1f;
		vertices[index++] = 0f;
		//}else{
		//	vertices[index++] = 1f;
		//	vertices[index++] = 1f;
		//}

		vertices[index++] = botLeftVertex.x;
		vertices[index++] = botLeftVertex.y;
		vertices[index++] = botLeftVertex.z;
		//if(sideOfFace=="left"){
		vertices[index++] = 1f;
		vertices[index++] = 1f;
		//}else{
		//	vertices[index++] = 1f;
		//	vertices[index++] = 0f;
		//}

		/*
		 * Vector3f vvertex ;//= vertexChin0; Vector3f vrotate ;//= rotateChin0;
		 * Vector3f vjoint = vertexHeadBottom; //
		 * System.out.println("human2_eye.actionJoints vertexHeadBottom x:"
		 * +vjoint.x+" y:"+vjoint.y+" z:"+vjoint.z); Vector3f vjointrotate =
		 * rotateHeadBottom;
		 * 
		 * float nx, ny, nz;
		 * 
		 * 
		 * // bottom right ( outward bottom left ) v.x = 0f; v.y = 0f; v.z =
		 * distBR;
		 * 
		 * vvertex = botRightVertex; vrotate = botRightRotate;
		 * 
		 * nx = vrotate.x; ny = vjointrotate.y + vrotate.y; nz = vjointrotate.z
		 * + vrotate.z; calcuateJointsQuaternions(nx, ny, nz); vvertex.x = v.x +
		 * vjoint.x; vvertex.y = v.y + vjoint.y; vvertex.z = v.z + vjoint.z;
		 * 
		 * vertices[index++] = vvertex.x; vertices[index++] = vvertex.y;
		 * vertices[index++] = vvertex.z;
		 * 
		 * 
		 * 
		 * 
		 * // top right ( outward top left ) v.x = 0f; v.y = 0f; v.z = distTR;
		 * 
		 * vvertex = topRightVertex; vrotate = topRightRotate;
		 * 
		 * nx = vrotate.x; ny = vjointrotate.y + vrotate.y; nz = vjointrotate.z
		 * + vrotate.z; calcuateJointsQuaternions(nx, ny, nz); vvertex.x = v.x +
		 * vjoint.x; vvertex.y = v.y + vjoint.y; vvertex.z = v.z + vjoint.z;
		 * 
		 * vertices[index++] = vvertex.x; vertices[index++] = vvertex.y;
		 * vertices[index++] = vvertex.z;
		 * 
		 * 
		 * 
		 * 
		 * 
		 * // top left ( outward top right ) v.x = 0f; v.y = 0f; v.z = distTL;
		 * 
		 * vvertex = topLeftVertex; vrotate = topLeftRotate;
		 * 
		 * nx = vrotate.x; ny = vjointrotate.y + vrotate.y; nz = vjointrotate.z
		 * + vrotate.z; calcuateJointsQuaternions(nx, ny, nz); vvertex.x = v.x +
		 * vjoint.x; vvertex.y = v.y + vjoint.y; vvertex.z = v.z + vjoint.z;
		 * 
		 * vertices[index++] = vvertex.x; vertices[index++] = vvertex.y;
		 * vertices[index++] = vvertex.z;
		 * 
		 * 
		 * 
		 * 
		 * // bottom left ( outward bottom right ) v.x = 0f; v.y = 0f; v.z =
		 * distBL;
		 * 
		 * vvertex = botLeftVertex; vrotate = botLeftRotate;
		 * 
		 * nx = vrotate.x; ny = vjointrotate.y + vrotate.y; nz = vjointrotate.z
		 * + vrotate.z; calcuateJointsQuaternions(nx, ny, nz); vvertex.x = v.x +
		 * vjoint.x; vvertex.y = v.y + vjoint.y; vvertex.z = v.z + vjoint.z;
		 * 
		 * vertices[index++] = vvertex.x; vertices[index++] = vvertex.y;
		 * vertices[index++] = vvertex.z;
		 */

		return vertices;
	}

	public void bindSkinEyeVertexData() {
		// if (isDrawChange)
		{

			if (true) {
				if (verticesBufferSkinEye == null) {
					verticesBufferSkinEye = BufferUtils.createFloatBuffer(skinEyeVerticesDraw.length);
				}
				verticesBufferSkinEye.put(skinEyeVerticesDraw).flip();

				if (textureID == -1) {
					textureID = glGenTextures();

					glBindTexture(GL_TEXTURE_2D, textureID);
					// glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
					// glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

					//System.out.println("about to do glTexImage2D");
					glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureWidth.get(), textureHeight.get(), 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData);
					stbi_image_free(textureData);
					//System.out.println("done with glTexImage2D");

					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

					// System.out.println("texture width and height :
					// "+textureWidth.get() + " " +textureHeight.get());
					if (textureData == null) {
						System.out.println("about to make texture with null textureData");
					}

				}
				// glBindTexture(GL_TEXTURE_2D, textureID);

				if (skinEyeVAOID == -1) {
					skinEyeVAOID = glGenVertexArrays();
				}
				glBindVertexArray(skinEyeVAOID);

				if (skinEyeVBOID == -1) {
					skinEyeVBOID = glGenBuffers();
				}
				glBindBuffer(GL_ARRAY_BUFFER, skinEyeVBOID);
				glBufferData(GL_ARRAY_BUFFER, verticesBufferSkinEye, GL_DYNAMIC_DRAW);
				// works without texture vals in vertices[]
				// glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0); // the
				// one that works without texture
				
				glVertexAttribPointer(0, 3, GL_FLOAT, false, 20, 0);
				glVertexAttribPointer(1, 2, GL_FLOAT, false, 20, 12);
				//glVertexAttribPointer(1, 2, GL_FLOAT, false, 20, 12);

				
				// glVertexAttribPointer(2, 2, GL_FLOAT, false, 8*4, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				// glBindVertexArray(0);
				// glEnableVertexAttribArray(2);

				if (!isSkinEyeIndexBound) {
					skinEyeIndicesCount = skinEyeIndicesDraw.length;
					IntBuffer indicesBuffer = BufferUtils.createIntBuffer(skinEyeIndicesCount);
					indicesBuffer.put(skinEyeIndicesDraw);
					indicesBuffer.flip();

					skinEyeVBOIID = glGenBuffers();
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinEyeVBOIID);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

					isSkinEyeIndexBound = true;
				}
			}

			// isDrawChange = false;
		}
	}

	public void calcuateJointsQuaternions(float rX, float rY, float rZ) {
		dest1.w = 1f;
		dest1.x = 0f;
		dest1.y = 0f;
		dest1.z = 0f;

		dest2.w = 1f;
		dest2.x = 0f;
		dest2.y = 0f;
		dest2.z = 0f;

		dest2.rotate((float) Math.toRadians(rX), (float) Math.toRadians(rY), (float) Math.toRadians(rZ), dest1);

		// dest1.normalize();
		// dest1.invert();
		dest1.transform(v);
	}

	public void calculateJointsVertices() {
		// System.out.println("human1.move(); called");

		double radian = Math.toRadians(degreeRotateZ);
		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		float fX, fY, fZ;

		int indexCount = 0;
		float[] vertices = {};// .clone();// act.getVertices();

		// jointsVerticesDraw =
		actionJoints();
		vertices = skinEyeVerticesDraw;

		for (int i = vertices.length; indexCount < i; indexCount += 5) {

			fX = vertices[indexCount];
			fY = vertices[indexCount + 1];
			fZ = vertices[indexCount + 2];
			setVertices(indexCount, cosRadian, sinRadian, fX, fY, fZ, vertices);
		}

		// for(int i = 0; i < vertices.length; i++)
		// { System.out.println("skinEye draw vertex : i:"+i+"
		// val:"+vertices[i]);}

	}

	public void draw() {
		calculateJointsVertices();
		drawSkinEye();
	}

	public void drawSkinEye() {// System.out.println("human.drawJoints");
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_ALPHA_TEST);
		 glEnable(GL_BLEND);
		 glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		 glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND); //System.out.println("rgb " + skinRed + " " + skinBlue + " " +skinGreen);
		//glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		 glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
		glUseProgram(skinEyeProgramId);
		bindSkinEyeVertexData();

		glUniformMatrix4fv(skinEyeGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(skinEyeFB));
		glUniform4f(skinEyeGLRGBAUniformLocation, skinRed, skinGreen, skinBlue, 1.0f);
		// glUniform

		glBindVertexArray(skinEyeVAOID);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinEyeVBOIID);
		glBindTexture(GL_TEXTURE_2D, textureID);
		glDrawElements(GL_TRIANGLES, skinEyeIndicesCount, GL_UNSIGNED_INT, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glBindVertexArray(0);

		// glBindTexture(GL_TEXTURE_2D,textureID);

		glUseProgram(0);

		// System.out.println("just drew eye");
		//glDisable(GL_BLEND);
		// glDisable(GL_TEXTURE_2D);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		//glCullFace(GL_FRONT);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_ALPHA_TEST);
		 glDisable(GL_BLEND);
	}

	public void glCleanup() {
		glUseProgram(0);
		/*
		 * if (skinEyeProgramId != 0) { if (jointsVertexShaderId != 0) {
		 * glDetachShader(jointsProgramId, jointsVertexShaderId); } if
		 * (jointsFragmentShaderId != 0) { glDetachShader(jointsProgramId,
		 * jointsFragmentShaderId); } glDeleteProgram(jointsProgramId); }
		 */
	}

	public void glConstruct() {

		glConstructSkinEye();

	}

	public void glConstructSkinEye() {
		try {
			try {
				skinEyeProgramId = glCreateProgram();

				glCreateVertexShaderSkinEye("#version 130         \n"//
						+ "uniform mat4 MVP;                       \n"//
						+ "in vec3 position;                       \n"//
						+ "in vec2 texcoord;                       \n"//
						// +"in vec4 in_color; \n"//
						// +"out vec4 pass_color; \n"//
						+ "out vec2 textureCoord;              \n"//
						+ "void main() {                           \n"//
						+ "     gl_Position =MVP* vec4(position, 1.0); \n"//
						// +" pass_color=in_color; \n"//
						// +" pass_color=vec4(1.0,1.0,1.0,1.0); \n"//
						+ "       textureCoord = texcoord;           \n"//
						+ "}");

				glCreateFragmentShaderSkinEye("#version 130       \n"//
						+ "uniform vec4 RGBA; \n"//
						+ "in vec2 textureCoord;              \n"//
						+ "uniform sampler2D texImage;             \n"//
						+ "out vec4 out_color;                     \n"//
						+ "void main() {                           \n"//
						// +" vec4 textureColor =
						// texture(texImage,textureCoord);\n"//
						// +" out_color=pass_color*textureColor; \n"//
						// +" out_color=vec4(1f,1f,1f,1f); \n"//
						// + " out_color=
						// vec4(1.0,1.0,1.0,1.0);\n"//texture(texImage,textureCoord);\n"//
						//+ "   out_color= texture(texImage,textureCoord)*RGBA;\n"//
						+ "   out_color= texture(texImage,textureCoord);\n"//
						+ "}   ");

				glLink(skinEyeProgramId);

				glUseProgram(skinEyeProgramId);
				skinEyeGLMVPUniformLocation = glGetUniformLocation(skinEyeProgramId, "MVP");
				skinEyeGLRGBAUniformLocation = glGetUniformLocation(skinEyeProgramId, "RGBA");
				skinEyeGLTextureCoordLocation = glGetAttribLocation(skinEyeProgramId, "texcoord");
				skinEyeGLTextureImageUniform = glGetUniformLocation(skinEyeProgramId, "texImage");

				glUseProgram(0);

				// glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (skinEyeProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glCreateFragmentShaderSkinEye(String shaderCode) throws Exception {
		skinEyeFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, skinEyeProgramId);
	}

	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShaderSkinEye(String shaderCode) throws Exception {
		skinEyeVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, skinEyeProgramId);
	}

	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}

	public ByteBuffer ioResourceToByteBuffer(String resource, int bufferSize) throws IOException {
		// System.out.println("ioResourceToByteBuffer entered");
		ByteBuffer buffer; // System.out.println("ioResourceToByteBuffer buffer
							// made");
		
		File file = new File(resource);// new File(url.getFile());
										// System.out.println("ioResourceToByteBuffer
										// file created");
		URL url = file.toURI().toURL();

		if (url == null) {
			System.out.println("ioResourceToByteBuffer url is null");
		}

		// System.out.println("ioResourceToByteBuffer url:"+url.toString());

		if (file.isFile()) { // System.out.println("ioResourcetoByteBuffer file
								// is a file");
			FileInputStream fis = new FileInputStream(file);
			FileChannel fc = fis.getChannel();
			buffer = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size()); 
			fc.close();
			fis.close();
		} else { // System.out.println("ioResourcetoByteBuffer file is not a
					// file");
			buffer = BufferUtils.createByteBuffer(bufferSize);
			InputStream source = url.openStream();
			if (source == null)
				throw new FileNotFoundException(resource);
			try {
				ReadableByteChannel rbc = Channels.newChannel(source);
				try {
					while (true) {
						int bytes = rbc.read(buffer);
						if (bytes == -1)
							break;
						if (buffer.remaining() == 0)
							buffer = resizeBuffer(buffer, buffer.capacity() * 2);
					}
					buffer.flip(); // System.out.println("didn't figure out
									// buffersize myself ");
				} finally {
					rbc.close();
				}
			} finally {
				source.close();
			}
		}
		return buffer;
	}

	public void quaternionRotate(float dist, float[] joints, int index, Vector3f vv, Vector3f vvOffset) {
		v.x = 0f;
		v.y = 0f;
		v.z = dist;
		calcuateJointsQuaternions(joints[index++], joints[index++], joints[index++]);
		// float jx,jy,jz;
		// jx = joints[index++];
		// jy = joints[index++];
		// jz = joints[index++];
		// calcuateJointsQuaternions(jx,jy,jz);
		// System.out.println("jx:"+jx+"\tjy:"+jy+"\tjz:"+jz);
		if (vvOffset == null) {
			vv.x = v.x;
			vv.y = v.y;
			vv.z = v.z;
			// System.out.println("vv.x:"+vv.x+"\tvv.y:"+vv.y+"\tvv.z:"+vv.z);
		} else {
			vv.x = v.x + vvOffset.x;
			vv.y = v.y + vvOffset.y;
			vv.z = v.z + vvOffset.z;
		}
	}

	private ByteBuffer resizeBuffer(ByteBuffer buffer, int newCapacity) {
		ByteBuffer newBuffer = BufferUtils.createByteBuffer(newCapacity);
		buffer.flip();
		newBuffer.put(buffer);
		return newBuffer;
	}

	public void setDistances(float tr, float tl, float br, float bl) {
		distTR = tr;
		distTL = tl;
		distBR = br;
		distBL = bl;

	}

	public void setHeight(float newH) {
		heightZ = newH;
	}

	public void setKeyRotations(Vector3f topLeftRotate, Vector3f topRightRotate, Vector3f botLeftRotate, Vector3f botRightRotate) {
		this.topLeftRotate = topLeftRotate;
		this.topRightRotate = topRightRotate;
		this.botLeftRotate = botLeftRotate;
		this.botRightRotate = botRightRotate;
	}

	public void setKeyVertices(Vector3f topLeftVertex, Vector3f topRightVertex, Vector3f botLeftVertex, Vector3f botRightVertex) {
		this.topLeftVertex = topLeftVertex;
		this.topRightVertex = topRightVertex;
		this.botLeftVertex = botLeftVertex;
		this.botRightVertex = botRightVertex;
	}

	public void setKeyHeadPoints(Vector3f headBottom, Vector3f headTop, Vector3f rotate) {
		vertexHeadBottom = headBottom;
		vertexHeadTop = headTop;
		rotateHeadBottom = rotate;
		// degreeRotateZ = rotateZ;
		// System.out.println("human2_eye.setKeyHeadPoints");
	}

	// public void setMasterVertexValues()

//	public void setMVP(Matrix4f mvp) {
//		MVP = mvp;
//	}

	public void setRGB(float r, float g, float b) {
		skinRed = r;
		skinGreen = g;
		skinBlue = b;
	}

	public void setVertices(int indexCounter, float cosRadian, float sinRadian, float fX, float fY, float fZ, float[] array) {

		// System.out.println("");

		float dX = fX * cosRadian - fY * sinRadian;
		float dY = fY * cosRadian + fX * sinRadian;
		float dZ = fZ;
		// System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" +
		// dY + "\t\tdZ:" + dZ);

		dX *= heightZ;
		dY *= heightZ;
		dZ *= heightZ;
		// System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" +
		// dY + "\t\tdZ:" + dZ);

		dX += x;
		dY += y;
		dZ += z;
		// System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" +
		// dY + "\t\tdZ:" + dZ);

		array[indexCounter++] = (dX);
		array[indexCounter++] = (dY);
		array[indexCounter] = (dZ);

		// System.out.println("heightZ:"+heightZ);
		// System.out.println("cos:"+sinRadian);
		// System.out.println("sin:"+cosRadian);

	}

	public void setXYZR(float x, float y, float z, float r) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.degreeRotateZ = r;
		// move();
		// System.out.println("x:"+x+" y:"+y+" z:"+z+" r:"+r);
	}

}
