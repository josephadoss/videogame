package local.source.actors.humans.human2;

import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VALIDATE_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glDeleteProgram;
import static org.lwjgl.opengl.GL20.glDetachShader;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;
import static org.lwjgl.opengl.GL20.glGetProgrami;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform4f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;
//import static org.lwjgl.opengl.GL32.GL_GEOMETRY_SHADER;
import static org.lwjgl.opengl.GL32.GL_PROGRAM_POINT_SIZE;
import static org.lwjgl.stb.STBImage.*;

import static org.lwjgl.opengl.GL11.*;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.opengl.GLUtil;
import org.lwjgl.system.Callback;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.stb.STBImage.*;
import static org.lwjgl.system.MemoryUtil.*;

import static org.lwjgl.BufferUtils.*;

import org.lwjgl.*;

import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.file.*;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.stb.STBImage.*;
import static org.lwjgl.system.MemoryUtil.NULL;

import de.matthiasmann.twl.utils.PNGDecoder;
import local.source.game_time;
import local.source.manager_mvp;

public class human2_head {

	game_time gt = game_time.getInstance();

	
	float hairRed = 1.0f;
	float hairGreen=0.0f;
	float hairBlue=0.0f;
	
	byte[] skinHairIndicesDraw = { /// front of head
			//0, 1, 8 //
			//, 0, 11, 1//
			//, 2, 20, 14//
			//, 2, 3, 20//
			//, 2, 17, 23//
			//'', 2, 23, 3//
			 3, 4, 20//
			, 3, 23, 4//
			//1,17,2//
			//,1,11,17//
			//8,2,14//
			//,8,1,2//
			// right side of head
			//, 0, 12, 11//
			//, 0, 13, 12//
			//, 0, 7, 13//
			//, 11, 12, 18//
			//, 11, 18, 17//
			////////, 17, 18, 24//
			////////, 17, 24, 23//
			//,23,17,18
			,24,23,18
			, 23, 24, 4//
			, 12, 13, 19//
			, 12, 19, 18//
			, 18, 19, 25//
			, 18, 25, 24//
			, 24, 25, 4//
			, 13, 7, 6//
			, 13, 6, 19//
			, 19, 6, 5//
			, 19, 5, 25//
			, 25, 5, 4
			// left side of head
			//, 0, 8, 9//
			//, 0, 9, 10//
			//, 0, 10, 7//
			//, 9, 8, 14//
			//, 9, 14, 15//
			//, 15, 14, 20//
			, 15, 20, 21//
			, 21, 20, 4//
			, 10, 9, 15//
			, 10, 15, 16//
			, 16, 15, 21//
			, 16, 21, 22//
			, 22, 21, 4//
			, 7, 10, 16//
			, 7, 16, 6//
			, 6, 16, 22//
			, 6, 22, 5//
			, 5, 22, 4
	};
	
	
	
	byte[] skinIndicesDraw = {
			// front of head
			0, 1, 8 //
			, 0, 11, 1//
			, 2, 20, 14//
			, 2, 3, 20//
			, 2, 17, 23//
			, 2, 23, 3//
			//, 3, 4, 20//
			//, 3, 23, 4//
			//,1,17,2// eye
			//,1,11,17//
			//,8,2,14//
			//,8,1,2// eye
			// right side of head
			, 0, 12, 11//
			, 0, 13, 12//
			, 0, 7, 13//
			, 11, 12, 18//
			, 11, 18, 17//
			////////, 17, 18, 24//
			////////, 17, 24, 23//
			,23,17,18
			//,24,23,18
			//, 23, 24, 4//
			//, 12, 13, 19//
			//, 12, 19, 18//
			//, 18, 19, 25//
			//, 18, 25, 24//
			//, 24, 25, 4//
			//, 13, 7, 6//
			//, 13, 6, 19//
			//, 19, 6, 5//
			//, 19, 5, 25//
			//, 25, 5, 4
			// left side of head
			, 0, 8, 9//
			, 0, 9, 10//
			, 0, 10, 7//
			, 9, 8, 14//
			, 9, 14, 15//
			, 15, 14, 20//
			//, 15, 20, 21//
			//, 21, 20, 4//
			//, 10, 9, 15//
			//, 10, 15, 16//
			//, 16, 15, 21//
			//, 16, 21, 22//
			//, 22, 21, 4//
			//, 7, 10, 16//
			//, 7, 16, 6//
			//, 6, 16, 22//
			//, 6, 22, 5//
			//, 5, 22, 4

	};

	byte[] jointsIndicesLines = { 0, 1 // chin to nose
			, 1, 2 // nose to eyebrow center
			, 2, 3 // eyebrowcenter to foreheadtop
			, 3, 4 // foreheadtop to topofhead
			, 4, 5 // topofhead to backOfHeadTop
			, 5, 6 // backOfHeadTop to backOfHeadMid
			, 6, 7 // backHeadMid to backHeadBot
			, 1, 8// nose to frontNoseLeft
			, 8, 9 // frontNoseLeft to midNoseLeft
			, 9, 10// midNoseLeft to backnoseLeft
			, 10, 7 // backNoseLeft to backOfHead
			, 1, 11// nose to frontNoseRight
			, 11, 12 // frontNoseRight to midNoseRight
			, 12, 13// midNoseright to backNoseRight
			, 13, 7 // backNoseRight to backOfHead

			, 2, 14 // eyebrow to front eyebrow left
			, 14, 15 // front eyebrow left to mid eyebrow left
			, 15, 16 // mid eyebrow left to back eyebrow left
			, 16, 6 // back eyebrow left to backHeadMid
			, 2, 17 // eyebrow to front eyebrow right
			, 17, 18 // front eyebrow right to mid eyebrow right
			, 18, 19 // mid eyebrow right to back eyebrow right
			, 19, 6 // back eyebrow right to backHeadMid

			, 3, 20 // to front forhead left
			, 20, 21 // front forehead left to mid forehead left
			, 21, 22 // mid forehead left to back forhead left
			, 22, 7 // back forehead left to backHeadMid
			, 3, 23 // forehead to front forhead right
			, 23, 24 // front forehead right to mid forehead right
			, 24, 25 // mid forehead right to back forhead right
			, 25, 7 // back forehead right to backHeadMid
	};

	float[] jointsVerticesDraw = { 0f, 0f, 0f // chin
			, 0f, 0f, 0f // nose
			, 0f, 0f, 0f // eyebrow center
			, 0f, 0f, 0f // forheadtop
			, 0f, 0f, 0f // top of head
			, 0f, 0f, 0f // back of head top
			, 0f, 0f, 0f // back of head mid
			, 0f, 0f, 0f // back of head bot
			, 0f, 0f, 0f // 8 front nose left
			, 0f, 0f, 0f // 9 mid nose left
			, 0f, 0f, 0f // 10 back nose left
			, 0f, 0f, 0f// 11 front nose right
			, 0f, 0f, 0f // 12 mid nose right
			, 0f, 0f, 0f // 13 back nose right
			, 0f, 0f, 0f // 14 front eyeB left
			, 0f, 0f, 0f // 15 mid eyeB left
			, 0f, 0f, 0f // 16 back eyeB left
			, 0f, 0f, 0f // 17 front eyeB right
			, 0f, 0f, 0f // 18 mid eyeB right
			, 0f, 0f, 0f // 19 back eyeB right
			, 0f, 0f, 0f // 20 front forehead left
			, 0f, 0f, 0f // 21 mid forehead left
			, 0f, 0f, 0f // 22 back forehead left
			, 0f, 0f, 0f // 23 front forehead right
			, 0f, 0f, 0f // 24 mid forehead right
			, 0f, 0f, 0f // 25 back forehead right
			,0f,0f,0f// 26 specal eye texture
			,0f,0f,0f// 27 specal eye texture
			,0f,0f,0f// 28 specal eye texture
			,0f,0f,0f// 29 specal eye texture
			,0f,0f,0f// 30 specal eye texture
			,0f,0f,0f// 31 specal eye texture
			};

	
	float x, y, z;
	public float degreeRotateZ = 0.0f;

	float[] rotationsOld;
	float[] rotationsNew;
	float[] rotationsDraw;
	int timeToComplete;
	int timeStarted;

	int jointsIndicesLinesCount;// = jointsIndicesLines.length;

	float jointsRed = 1f;
	float jointsBlue = 1f;
	float jointsGreen = 1f;

	int jointsVAOID = -1;
	int jointsVBOID = -1;
	int jointsVBOIID = -1;

	int skinIndicesLinesCount;// = jointsIndicesLines.length;
	int skinEyeLIndicesLinesCount;// = jointsIndicesLines.length;
	int skinEyeRIndicesLinesCount;// = jointsIndicesLines.length;

	float skinRed = 1f;
	float skinBlue = 1f;
	float skinGreen = 1f;

	int skinVAOID = -1;
	int skinVBOID = -1;
	int skinVBOIID = -1;

	int skinHairIndicesLinesCount;// = jointsIndicesLines.length;
	
	int skinHairVAOID = -1;
	int skinHairVBOID = -1;
	int skinHairVBOIID = -1;

	

	public int jointsGLMVPUniformLocation;
	public int jointsGLRGBAUniformLocation;

	public int skinGLMVPUniformLocation;
	public int skinGLRGBAUniformLocation;

	public int skinHairGLMVPUniformLocation;
	public int skinHairGLRGBAUniformLocation;
	

	public FloatBuffer jointsFB = BufferUtils.createFloatBuffer(16);
	public FloatBuffer skinFB = BufferUtils.createFloatBuffer(16);
	public FloatBuffer skinHairFB = BufferUtils.createFloatBuffer(16);

	public float heightZ = 2.0f;

	Quaternionf dest1 = new Quaternionf();
	Quaternionf dest2 = new Quaternionf();
	Vector3f v = new Vector3f();

	public int jointsProgramId;
	public int jointsVertexShaderId;
	public int jointsFragmentShaderId;

	public int skinProgramId;
	public int skinVertexShaderId;
	public int skinFragmentShaderId;

	public int skinHairProgramId;
	public int skinHairVertexShaderId;
	public int skinHairFragmentShaderId;


	boolean isJointsIndexBound = false;
	boolean isSkinIndexBound = false;
	boolean isSkinHairIndexBound=false;
	

	//public Matrix4f MVP;

	public boolean isDrawJoints = false;
	public boolean isDrawSkin = true;

	FloatBuffer verticesBufferJoints;
	FloatBuffer verticesBufferSkin;
	FloatBuffer verticesBufferSkinHair;
	

	Vector3f vertexHeadBottom;
	Vector3f vertexHeadTop;
	Vector3f rotateHeadBottom;

	Vector3f[] vertexHeadArray = new Vector3f[32];
	Vector3f vertexChin0 = new Vector3f();
	Vector3f vertexNose1 = new Vector3f();
	Vector3f vertexEyebrowCenter2 = new Vector3f();
	Vector3f vertexForeheadTop3 = new Vector3f();
	Vector3f vertexTopHead4 = new Vector3f();
	Vector3f vertexBackHeadTop5 = new Vector3f();
	Vector3f vertexBackHeadMid6 = new Vector3f();
	Vector3f vertexBackHeadBot7 = new Vector3f();
	Vector3f vertexNoseFrontLeft8 = new Vector3f();
	Vector3f vertexNoseMidLeft9 = new Vector3f();
	Vector3f vertexNoseBackLeft10 = new Vector3f();
	Vector3f vertexNoseFrontRight11 = new Vector3f();
	Vector3f vertexNoseMidRight12 = new Vector3f();
	Vector3f vertexNoseBackRight13 = new Vector3f();
	Vector3f vertexEyeBFrontLeft14 = new Vector3f();
	Vector3f vertexEyeBMidLeft15 = new Vector3f();
	Vector3f vertexEyeBBackLeft16 = new Vector3f();
	Vector3f vertexEyeBFrontRight17 = new Vector3f();
	Vector3f vertexEyeBMidRight18 = new Vector3f();
	Vector3f vertexEyeBBackRight19 = new Vector3f();
	Vector3f vertexForeHeadFrontLeft20 = new Vector3f();
	Vector3f vertexForeHeadMidLeft21 = new Vector3f();
	Vector3f vertexForeHeadBackLeft22 = new Vector3f();
	Vector3f vertexForeHeadFrontRight23 = new Vector3f();
	Vector3f vertexForeHeadMidRight24 = new Vector3f();
	Vector3f vertexForeHeadBackRight25 = new Vector3f();

	Vector3f[] rotateHeadArray = new Vector3f[32];
	Vector3f rotateChin0 = new Vector3f(0f, 90f, 0f);
	Vector3f rotateNose1 = new Vector3f(0f, 75f, 0f);
	Vector3f rotateEyebrowCenter2 = new Vector3f(0f, 50f, 0f);
	Vector3f rotateForeheadTop3 = new Vector3f(0f, 30f, 0f);
	Vector3f rotateTopHead4 = new Vector3f(0f, 0f, 0f);
	Vector3f rotateBackHeadTop5 = new Vector3f(0f, -30f, 0f);
	Vector3f rotateBackHeadMid6 = new Vector3f(0f, -60f, 0f);
	Vector3f rotateBackHeadBot7 = new Vector3f(0f, -70f, 0f);

	Vector3f rotateNoseFrontLeft8 = new Vector3f(0f, 75f, 70f);
	Vector3f rotateNoseMidLeft9 = new Vector3f(0f, 75f, 150f);
	Vector3f rotateNoseBackLeft10 = new Vector3f(0f, -70f, -70f);
	Vector3f rotateNoseFrontRight11 = new Vector3f(0f, 75f, -70);
	Vector3f rotateNoseMidRight12 = new Vector3f(0f, 75f, -150f);
	Vector3f rotateNoseBackRight13 = new Vector3f(0f, -70f, 70f);

	Vector3f rotateEyeBFrontLeft14 = new Vector3f(0f, 50f, 70f);
	Vector3f rotateEyeBMidLeft15 = new Vector3f(0f, 50f, 150f);
	Vector3f rotateEyeBBackLeft16 = new Vector3f(0f, -50f, -70f);
	Vector3f rotateEyeBFrontRight17 = new Vector3f(0f, 50f, -70f);
	Vector3f rotateEyeBMidRight18 = new Vector3f(0f, 50f, -150f);
	Vector3f rotateEyeBBackRight19 = new Vector3f(0f, -50f, 70f);

	Vector3f rotateForeHeadFrontLeft20 = new Vector3f(0f, 30f, 70f);
	Vector3f rotateForeHeadMidLeft21 = new Vector3f(0f, 30f, 150f);
	Vector3f rotateForeHeadBackLeft22 = new Vector3f(0f, -30f, -70f);
	Vector3f rotateForeHeadFrontRight23 = new Vector3f(0f, 30f, -70f);
	Vector3f rotateForeHeadMidRight24 = new Vector3f(0f, 30f, -150f);
	Vector3f rotateForeHeadBackRight25 = new Vector3f(0f, -30f, 70f);
	
	

	Vector3f vertexEye1 = new Vector3f(0f,0f,0f);
	Vector3f vertexEye2 = new Vector3f(0f,0f,0f);
	Vector3f vertexEye8 = new Vector3f(0f,0f,0f);
	Vector3f vertexEye11 = new Vector3f(0f,0f,0f);
	Vector3f vertexEye14 = new Vector3f(0f,0f,0f);
	Vector3f vertexEye17 = new Vector3f(0f,0f,0f);
	
	Vector3f rotateEye1 = new Vector3f(0f,75f,0f);
	Vector3f rotateEye2 = new Vector3f(0f,50f,0f);
	Vector3f rotateEye8 = new Vector3f(0f,75f,70f);
	Vector3f rotateEye11 = new Vector3f(0f,75f,-70f);
	Vector3f rotateEye14 = new Vector3f(0f,50f,70f);
	Vector3f rotateEye17 = new Vector3f(0f,50f,-70f);
	
	
	float[] distArray = new float[32];
	float headHeight = 1f;
	float distChin = 0.5f;
	float distNose = 0.6f;
	float distEyebrowCenter = 0.7f;
	float distForeheadTop = 1f;
	float distTopHead = 1f;
	float distBackHeadTop = 1f;
	float distBackHeadMid = 0.6f;
	float distBackHeadBot = 0.5f;
	float distNoseFrontLeft = 0.6f;
	float distNoseMidLeft = 0.5f;
	float distNoseBackLeft = 0.6f;

	float distEyeBFrontLeft = 0.8f;
	float distEyeBMidLeft = 0.7f;
	float distEyeBBackLeft = 0.8f;

	float distForeHeadFrontLeft = 1f;
	float distForeHeadMidLeft = 0.9f;
	float distForeHeadBackLeft = 1f;

	
	int tempHeadIndex = 32;
	
	//human2_eye eyeR, eyeL;
	human2_eye_textured eyeRTex = new human2_eye_textured("right");
	human2_eye_textured eyeLTex = new human2_eye_textured("left");
	//human2_hair hair = new human2_hair();

	public human2_head(float hHeight, float regHeight) {

	
		heightZ = regHeight;
		headHeight = hHeight; // System.out.println("headHeight : "+headHeight);
		distChin = headHeight * distChin;
		distNose = headHeight * distNose;
		distEyebrowCenter *= headHeight;
		distForeheadTop *= headHeight;
		distTopHead *= headHeight;
		distBackHeadTop *= headHeight;
		distBackHeadMid *= headHeight;
		distBackHeadBot *= headHeight;
		distNoseFrontLeft *= headHeight;
		distNoseMidLeft *= headHeight;
		distNoseBackLeft *= headHeight;

		distEyeBFrontLeft *= headHeight;
		distEyeBMidLeft *= headHeight;
		distEyeBBackLeft *= headHeight;

		distForeHeadFrontLeft *= headHeight;
		distForeHeadMidLeft *= headHeight;
		distForeHeadBackLeft *= headHeight;


		
		float dist = 0f;
		distArray[0] = distChin;
		distArray[1] = distNose;
		distArray[2] = distEyebrowCenter;
		distArray[3] = distForeheadTop;
		distArray[4] = distTopHead;
		distArray[5] = distBackHeadTop;
		distArray[6] = distBackHeadMid;
		distArray[7] = distBackHeadBot;
		distArray[8] = distNoseFrontLeft;
		distArray[9] = distNoseMidLeft;
		distArray[10] = distNoseBackLeft;
		distArray[11] = distNoseFrontLeft;
		distArray[12] = distNoseMidLeft;
		distArray[13] = distNoseBackLeft;
		distArray[14] = distEyeBFrontLeft;
		distArray[15] = distEyeBMidLeft;
		distArray[16] = distEyeBBackLeft;
		distArray[17] = distEyeBFrontLeft;
		distArray[18] = distEyeBMidLeft;
		distArray[19] = distEyeBBackLeft;
		distArray[20] = distForeHeadFrontLeft;
		distArray[21] = distForeHeadMidLeft;
		distArray[22] = distForeHeadBackLeft;
		distArray[23] = distForeHeadFrontLeft;
		distArray[24] = distForeHeadMidLeft;
		distArray[25] = distForeHeadBackLeft;
		float eyeDist = 0.00f;
		distArray[26]=distArray[1]+eyeDist;
		distArray[27]=distArray[2]+eyeDist*3;
		distArray[28]=distArray[8]+eyeDist*3;
		distArray[29]=distArray[11]+eyeDist*3;
		distArray[30]=distArray[14]+eyeDist;
		distArray[31]=distArray[17]+eyeDist*3;
	
		

//		System.out.println("human2_head constructor about to construct eyes");
		//eyeR=new human2_eye("right");
		//eyeL=new human2_eye("left");
//		System.out.println("human2_head constructor done constructing eyes");
		//eyeR.setRotationsRight();
		//eyeL.setRotationsLeft();
		
		//eyeR.setHeight(heightZ);
		//eyeL.setHeight(heightZ);
		
//		eyeR.setDistances(distArray[17],distArray[2],distArray[11],distArray[1]);
//		eyeL.setDistances(distArray[2],distArray[14],distArray[1],distArray[8]);

		//eyeR.setDistances(distArray[2],distArray[17],distArray[1],distArray[11]);
		//eyeL.setDistances(distArray[14],distArray[2],distArray[8],distArray[1]);

		
		vertexHeadArray[0] = vertexChin0;
		vertexHeadArray[1] = vertexNose1;
		vertexHeadArray[2] = vertexEyebrowCenter2;
		vertexHeadArray[3] = vertexForeheadTop3;
		vertexHeadArray[4] = vertexTopHead4;
		vertexHeadArray[5] = vertexBackHeadTop5;
		vertexHeadArray[6] = vertexBackHeadMid6;
		vertexHeadArray[7] = vertexBackHeadBot7;
		vertexHeadArray[8] = vertexNoseFrontLeft8;
		vertexHeadArray[9] = vertexNoseMidLeft9;
		vertexHeadArray[10] = vertexNoseBackLeft10;
		vertexHeadArray[11] = vertexNoseFrontRight11;
		vertexHeadArray[12] = vertexNoseMidRight12;
		vertexHeadArray[13] = vertexNoseBackRight13;
		vertexHeadArray[14] = vertexEyeBFrontLeft14;
		vertexHeadArray[15] = vertexEyeBMidLeft15;
		vertexHeadArray[16] = vertexEyeBBackLeft16;
		vertexHeadArray[17] = vertexEyeBFrontRight17;
		vertexHeadArray[18] = vertexEyeBMidRight18;
		vertexHeadArray[19] = vertexEyeBBackRight19;
		vertexHeadArray[20] = vertexForeHeadFrontLeft20;
		vertexHeadArray[21] = vertexForeHeadMidLeft21;
		vertexHeadArray[22] = vertexForeHeadBackLeft22;
		vertexHeadArray[23] = vertexForeHeadFrontRight23;
		vertexHeadArray[24] = vertexForeHeadMidRight24;
		vertexHeadArray[25] = vertexForeHeadBackRight25;
		vertexHeadArray[26] =  vertexEye1 ;//= new Vector3f(0f,0f,0f);
		vertexHeadArray[27] =  vertexEye2 ;//= new Vector3f(0f,0f,0f);
		vertexHeadArray[28] =  vertexEye8;// = new Vector3f(0f,0f,0f);
		vertexHeadArray[29] =  vertexEye11;// = new Vector3f(0f,0f,0f);
		vertexHeadArray[30] =  vertexEye14;// = new Vector3f(0f,0f,0f);
		vertexHeadArray[31] =  vertexEye17;// = new Vector3f(0f,0f,0f);
		

		
		
		rotateHeadArray[0] = rotateChin0;
		rotateHeadArray[1] = rotateNose1;
		rotateHeadArray[2] = rotateEyebrowCenter2;
		rotateHeadArray[3] = rotateForeheadTop3;
		rotateHeadArray[4] = rotateTopHead4;
		rotateHeadArray[5] = rotateBackHeadTop5;
		rotateHeadArray[6] = rotateBackHeadMid6;
		rotateHeadArray[7] = rotateBackHeadBot7;
		rotateHeadArray[8] = rotateNoseFrontLeft8;
		rotateHeadArray[9] = rotateNoseMidLeft9;
		rotateHeadArray[10] = rotateNoseBackLeft10;
		rotateHeadArray[11] = rotateNoseFrontRight11;
		rotateHeadArray[12] = rotateNoseMidRight12;
		rotateHeadArray[13] = rotateNoseBackRight13;
		rotateHeadArray[14] = rotateEyeBFrontLeft14;
		rotateHeadArray[15] = rotateEyeBMidLeft15;
		rotateHeadArray[16] = rotateEyeBBackLeft16;
		rotateHeadArray[17] = rotateEyeBFrontRight17;
		rotateHeadArray[18] = rotateEyeBMidRight18;
		rotateHeadArray[19] = rotateEyeBBackRight19;
		rotateHeadArray[20] = rotateForeHeadFrontLeft20;
		rotateHeadArray[21] = rotateForeHeadMidLeft21;
		rotateHeadArray[22] = rotateForeHeadBackLeft22;
		rotateHeadArray[23] = rotateForeHeadFrontRight23;
		rotateHeadArray[24] = rotateForeHeadMidRight24;
		rotateHeadArray[25] = rotateForeHeadBackRight25;
		rotateHeadArray[26] =  rotateEye1;//= new Vector3f(0f,75f,0f);
		rotateHeadArray[27] =  rotateEye2;// = new Vector3f(0f,50f,0f);
		rotateHeadArray[28] =  rotateEye8;// = new Vector3f(0f,75f,70f);
		rotateHeadArray[29] =  rotateEye11;// = new Vector3f(0f,75f,-70f);
		rotateHeadArray[30] =  rotateEye14;// = new Vector3f(0f,50f,70f);
		rotateHeadArray[31] =  rotateEye17;// = new Vector3f(0f,50f,-70f);

		this.glConstruct();
		
		//eyeL.setKeyVals(vertexHeadArray[2],vertexHeadArray[14],vertexHeadArray[1],vertexHeadArray[8]);
		//eyeR.setKeyVals(vertexHeadArray[17],vertexHeadArray[2],vertexHeadArray[11],vertexHeadArray[1]);

		eyeLTex.setKeyVertices(vertexHeadArray[30],vertexHeadArray[27],vertexHeadArray[28],vertexHeadArray[26]);
		eyeRTex.setKeyVertices(vertexHeadArray[27],vertexHeadArray[31],vertexHeadArray[26],vertexHeadArray[29]);
		
		eyeLTex.setKeyRotations(rotateHeadArray[2],rotateHeadArray[14],rotateHeadArray[1],rotateHeadArray[8]);
		eyeRTex.setKeyRotations(rotateHeadArray[17],rotateHeadArray[2],rotateHeadArray[11],rotateHeadArray[1]);

		eyeRTex.setDistances(distArray[2],distArray[17],distArray[1],distArray[11]);
		eyeLTex.setDistances(distArray[14],distArray[2],distArray[8],distArray[1]);

		eyeRTex.setHeight(heightZ);
		eyeLTex.setHeight(heightZ);
	}

	public float[] actionJoints() {

		Vector3f vvertex = vertexChin0;
		Vector3f vrotate = rotateChin0;
		Vector3f vjoint = vertexHeadBottom;      //  System.out.println("human2_head.actionJoints vertexHeadBottom x:"+vjoint.x+" y:"+vjoint.y+" z:"+vjoint.z);
		Vector3f vjointrotate = rotateHeadBottom;

		float nx, ny, nz;

		for (int i = 0; i < tempHeadIndex/* vertexHeadArray.length */; i++) {
			v.x = 0f;
			v.y = 0f;
			v.z = distArray[i];

			vvertex = vertexHeadArray[i];
			vrotate = rotateHeadArray[i];

			nx = vrotate.x;
			ny = vjointrotate.y + vrotate.y;
			nz = vjointrotate.z + vrotate.z;
			calcuateJointsQuaternions(nx, ny, nz);
			vvertex.x = v.x + vjoint.x;
			vvertex.y = v.y + vjoint.y;
			vvertex.z = v.z + vjoint.z;				
		}

		float[] vertices = jointsVerticesDraw;
		int index = 0;

		Vector3f using = vertexChin0;

		for (int i = 0; i < tempHeadIndex/* vertexHeadArray.length */; i++) {
			using = vertexHeadArray[i];
			vertices[index++] = using.x;
			vertices[index++] = using.y;
			vertices[index++] = using.z;
		}

		//eyeLTex.setKeyVertices(vertexHeadArray[2],vertexHeadArray[14],vertexHeadArray[1],vertexHeadArray[8]);
		//eyeRTex.setKeyVertices(vertexHeadArray[17],vertexHeadArray[2],vertexHeadArray[11],vertexHeadArray[1]);

		eyeLTex.setKeyVertices(vertexHeadArray[30],vertexHeadArray[27],vertexHeadArray[28],vertexHeadArray[26]);
		eyeRTex.setKeyVertices(vertexHeadArray[27],vertexHeadArray[31],vertexHeadArray[26],vertexHeadArray[29]);

		//hair.setCalculatedVertices(jointsVerticesDraw);

		return vertices;
	}

	public void bindJointsVertexData() {
		// if (isDrawChange)
		{

			if (isDrawJoints) {
				if (verticesBufferJoints == null) {
					verticesBufferJoints = BufferUtils.createFloatBuffer(jointsVerticesDraw.length);
				}
				verticesBufferJoints.put(jointsVerticesDraw).flip();

				if (jointsVAOID == -1) {
					jointsVAOID = glGenVertexArrays();
				}
				glBindVertexArray(jointsVAOID);

				if (jointsVBOID == -1) {
					jointsVBOID = glGenBuffers();
				}
				glBindBuffer(GL_ARRAY_BUFFER, jointsVBOID);
				glBufferData(GL_ARRAY_BUFFER, verticesBufferJoints, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);

				if (!isJointsIndexBound) {
					jointsIndicesLinesCount = jointsIndicesLines.length;
					ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(jointsIndicesLinesCount);
					indicesBuffer.put(jointsIndicesLines);
					indicesBuffer.flip();

					jointsVBOIID = glGenBuffers();
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, jointsVBOIID);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

					isJointsIndexBound = true;
				}
			}

			// isDrawChange = false;
		}
	}


	public void bindSkinHairVertexData() {
		// if (isDrawChange)
		{

			if (isDrawSkin) {
				if (verticesBufferSkinHair == null) {
					verticesBufferSkinHair = BufferUtils.createFloatBuffer(jointsVerticesDraw.length);
				}
				verticesBufferSkinHair.put(jointsVerticesDraw).flip();

				if (skinHairVAOID == -1) {
					skinHairVAOID = glGenVertexArrays();
				}
				glBindVertexArray(skinHairVAOID);

				if (skinHairVBOID == -1) {
					skinHairVBOID = glGenBuffers();
				}
				glBindBuffer(GL_ARRAY_BUFFER, skinHairVBOID);
				glBufferData(GL_ARRAY_BUFFER, verticesBufferSkinHair, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);

				if (!isSkinHairIndexBound) {
					skinHairIndicesLinesCount = skinHairIndicesDraw.length;
					ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(skinHairIndicesLinesCount);
					indicesBuffer.put(skinHairIndicesDraw);
					indicesBuffer.flip();

					skinHairVBOIID = glGenBuffers();
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinHairVBOIID);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

					isSkinHairIndexBound = true;
				}
			}

			// isDrawChange = false;
		}
	}
	
	public void bindSkinVertexData() {
		// if (isDrawChange)
		{

			if (isDrawSkin) {
				if (verticesBufferSkin == null) {
					verticesBufferSkin = BufferUtils.createFloatBuffer(jointsVerticesDraw.length);
				}
				verticesBufferSkin.put(jointsVerticesDraw).flip();

				if (skinVAOID == -1) {
					skinVAOID = glGenVertexArrays();
				}
				glBindVertexArray(skinVAOID);

				if (skinVBOID == -1) {
					skinVBOID = glGenBuffers();
				}
				glBindBuffer(GL_ARRAY_BUFFER, skinVBOID);
				glBufferData(GL_ARRAY_BUFFER, verticesBufferSkin, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);

				if (!isSkinIndexBound) {
					skinIndicesLinesCount = skinIndicesDraw.length;
					ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(skinIndicesLinesCount);
					indicesBuffer.put(skinIndicesDraw);
					indicesBuffer.flip();

					skinVBOIID = glGenBuffers();
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinVBOIID);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

					isSkinIndexBound = true;
				}
			}

			// isDrawChange = false;
		}
	}

	public void bindVertexData() {
		if (isDrawJoints) {
			bindJointsVertexData();
		}

		if (isDrawSkin) {
			bindSkinVertexData();
			//bindSkinEyeLVertexData();
			//bindSkinEyeRVertexData();
			
			//eyeR.bindSkinEyeVertexData();
			//eyeL.bindSkinEyeVertexData();
			
			eyeRTex.bindSkinEyeVertexData();
			eyeLTex.bindSkinEyeVertexData();
		}
	}

	public void calcuateJointsQuaternions(float rX, float rY, float rZ) {
		dest1.w = 1f;
		dest1.x = 0f;
		dest1.y = 0f;
		dest1.z = 0f;

		dest2.w = 1f;
		dest2.x = 0f;
		dest2.y = 0f;
		dest2.z = 0f;

		dest2.rotate((float) Math.toRadians(rX), (float) Math.toRadians(rY), (float) Math.toRadians(rZ), dest1);

		// dest1.normalize();
		// dest1.invert();
		dest1.transform(v);
	}

	public void calculateJointsVertices() {
		// System.out.println("human1.move(); called");

		double radian = Math.toRadians(degreeRotateZ);
		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		float fX, fY, fZ;

		int indexCount = 0;
		float[] vertices = {};// .clone();// act.getVertices();

		
		
		// jointsVerticesDraw =
		actionJoints();
		vertices = jointsVerticesDraw;

		for (int i = vertices.length; indexCount < i; indexCount += 3) {

			fX = vertices[indexCount];// * lengthX/* X */;
			fY = vertices[indexCount + 1];// * widthY/* Y */;
			fZ = vertices[indexCount + 2];// * heightZ/* Z */;
			setVertices(indexCount, cosRadian, sinRadian, fX, fY, fZ, vertices);
		}

		int index = 0;

	
		// for (int i = 0; i < vertices.length; i++) {
		// System.out.println("vertices index:"+i + " val:"+vertices[i]);
		// }
	}

	public void draw() {

		// if(doCalc)
		{
			calculateJointsVertices();
			// doCalc=false;
		}

		if (isDrawJoints) {
			drawJoints();
		}

		if (isDrawSkin) {
			 
			 drawSkinHair();
			 drawSkin();
			//drawSkinEyeL();
			//drawSkinEyeR();
			
			//eyeR.draw();
			//eyeL.draw();
			 
			 eyeRTex.draw();
			 eyeLTex.draw();
			 
			 //hair.draw();
		}

	}

	public void drawJoints() {// System.out.println("human.drawJoints");
		bindJointsVertexData();
		glUseProgram(jointsProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(jointsGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(jointsFB));
		glUniform4f(jointsGLRGBAUniformLocation, jointsRed, jointsGreen, jointsBlue, 1.0f);

		glBindVertexArray(jointsVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, jointsVBOIID);
		glDrawElements(GL_LINES, jointsIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glDrawElements(GL_POINTS, jointsIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);
	}

	public void drawSkin() {// System.out.println("human.drawJoints");
		bindSkinVertexData();
		glUseProgram(skinProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(skinGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(skinFB));
		glUniform4f(skinGLRGBAUniformLocation, skinRed, skinGreen, skinBlue, 1.0f);

		glBindVertexArray(skinVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinVBOIID);
		glDrawElements(GL_TRIANGLES, skinIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
	}

	
	public void drawSkinHair() {// System.out.println("human.drawJoints");
		bindSkinHairVertexData();
		glUseProgram(skinHairProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(skinHairGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(skinHairFB));
		glUniform4f(skinHairGLRGBAUniformLocation, hairRed, hairGreen, hairBlue, 1.0f);

		glBindVertexArray(skinHairVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinHairVBOIID);
		glDrawElements(GL_TRIANGLES, skinHairIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
	}

	

	public void glCleanup() {
		glUseProgram(0);
		if (jointsProgramId != 0) {
			if (jointsVertexShaderId != 0) {
				glDetachShader(jointsProgramId, jointsVertexShaderId);
			}
			if (jointsFragmentShaderId != 0) {
				glDetachShader(jointsProgramId, jointsFragmentShaderId);
			}
			glDeleteProgram(jointsProgramId);
		}
	}

	public void glConstruct() {
		if (isDrawJoints) {
			glConstructJoints();
		}

		if (isDrawSkin) {
		
			glConstructSkinHair();	
			glConstructSkin();	
		}
	}

	public void glConstructJoints() {
		try {
			try {
				jointsProgramId = glCreateProgram();

				glCreateVertexShaderJoints("#version 130\nuniform mat4 MVP; in vec3 position; void main() { gl_Position =MVP* vec4(position, 1.0);  gl_PointSize = 3.0; }");

				glCreateFragmentShaderJoints("#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(jointsProgramId);

				glUseProgram(jointsProgramId);
				jointsGLMVPUniformLocation = glGetUniformLocation(jointsProgramId, "MVP");
				jointsGLRGBAUniformLocation = glGetUniformLocation(jointsProgramId, "RGBA");
				glUseProgram(0);

				glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (jointsProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glConstructSkin() {
		try {
			try {
				skinProgramId = glCreateProgram();

				glCreateVertexShaderSkin("#version 130                   \n" //
						+ "uniform mat4 MVP;                             \n"//
						+ "in vec3 in_position;                          \n"//
						+ "void main()                                   \n"//
						+ "{                                             \n"//
						+ "gl_Position =MVP* vec4(in_position, 1.0);     \n"//
						//+ "gl_PointSize = 3.0;                           \n"//
						+ "}                                               ");

				glCreateFragmentShaderSkin("#version 130 \n"//
						+ "uniform vec4 RGBA; \n"//
						+ "out vec4 out_color;  \n"//
						+ "void main() { out_color = RGBA; }");

				glLink(skinProgramId);

				glUseProgram(skinProgramId);
				skinGLMVPUniformLocation = glGetUniformLocation(skinProgramId, "MVP");
				skinGLRGBAUniformLocation = glGetUniformLocation(skinProgramId, "RGBA");
				glUseProgram(0);

				// glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (skinProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glConstructSkinHair() {
		try {
			try {
				skinHairProgramId = glCreateProgram();

				glCreateVertexShaderSkinHair("#version 130                   \n" //
						+ "uniform mat4 MVP;                             \n"//
						+ "in vec3 in_position;                          \n"//
						+ "void main()                                   \n"//
						+ "{                                             \n"//
						+ "gl_Position =MVP* vec4(in_position, 1.0);     \n"//
						//+ "gl_PointSize = 3.0;                           \n"//
						+ "}                                               ");

				glCreateFragmentShaderSkinHair("#version 130     \n"//
						+ "uniform vec4 RGBA; \n"//
						+ "out vec4 out_color;  \n"//
						+ "void main() { out_color = RGBA; }");

				glLink(skinHairProgramId);

				glUseProgram(skinHairProgramId);
				skinHairGLMVPUniformLocation = glGetUniformLocation(skinHairProgramId, "MVP");
				skinHairGLRGBAUniformLocation = glGetUniformLocation(skinHairProgramId, "RGBA");
				glUseProgram(0);

				// glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught in human2_head.glConstructSkinHair:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (skinHairProgramId == 0) {
				throw new Exception("Could not create Shader for human2_head.glConstructSkinHair");
			}

		} catch (Exception e) {
			System.out.println("exception caught in  for human2_head.glConstructSkinHair " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}


	public void glCreateFragmentShaderJoints(String shaderCode) throws Exception {
		jointsFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, jointsProgramId);
	}

	public void glCreateFragmentShaderSkin(String shaderCode) throws Exception {
		skinFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, skinProgramId);
	}

	public void glCreateFragmentShaderSkinHair(String shaderCode) throws Exception {
		skinHairFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, skinHairProgramId);
	}



	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShaderJoints(String shaderCode) throws Exception {
		jointsVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, jointsProgramId);
	}

	public void glCreateVertexShaderSkin(String shaderCode) throws Exception {
		skinVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, skinProgramId);
	}
	
	public void glCreateVertexShaderSkinHair(String shaderCode) throws Exception {
		skinHairVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, skinHairProgramId);
	}


	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}
	/*
	 * public static ByteBuffer ioResourceToByteBuffer(String resource, int
	 * bufferSize) throws IOException { ByteBuffer buffer;
	 * 
	 * Path path = Paths.get(resource); if (Files.isReadable(path)) { try
	 * (SeekableByteChannel fc = Files.newByteChannel(path)) { buffer =
	 * BufferUtils.createByteBuffer((int)fc.size() + 1); while (fc.read(buffer)
	 * != -1) { ; } } } else { try ( InputStream source = new
	 * FileInputStream(resource);//= //
	 * IOUtil.class.getClassLoader().getResourceAsStream(resource);
	 * ReadableByteChannel rbc = Channels.newChannel(source) ) { buffer =
	 * createByteBuffer(bufferSize);
	 * 
	 * while (true) { int bytes = rbc.read(buffer); if (bytes == -1) { break; }
	 * if (buffer.remaining() == 0) { buffer = resizeBuffer(buffer,
	 * buffer.capacity() * 2); } } } }
	 * 
	 * buffer.flip(); return buffer; }
	 */

	public void quaternionRotate(float dist, float[] joints, int index, Vector3f vv, Vector3f vvOffset) {
		v.x = 0f;
		v.y = 0f;
		v.z = dist;
		calcuateJointsQuaternions(joints[index++], joints[index++], joints[index++]);
		// float jx,jy,jz;
		// jx = joints[index++];
		// jy = joints[index++];
		// jz = joints[index++];
		// calcuateJointsQuaternions(jx,jy,jz);
		// System.out.println("jx:"+jx+"\tjy:"+jy+"\tjz:"+jz);
		if (vvOffset == null) {
			vv.x = v.x;
			vv.y = v.y;
			vv.z = v.z;
			// System.out.println("vv.x:"+vv.x+"\tvv.y:"+vv.y+"\tvv.z:"+vv.z);
		} else {
			vv.x = v.x + vvOffset.x;
			vv.y = v.y + vvOffset.y;
			vv.z = v.z + vvOffset.z;
		}
	}

	/*
	 * private static ByteBuffer resizeBuffer(ByteBuffer buffer, int
	 * newCapacity) { ByteBuffer newBuffer =
	 * BufferUtils.createByteBuffer(newCapacity); buffer.flip();
	 * newBuffer.put(buffer); return newBuffer; }
	 */
	public void setKeyHeadPoints(Vector3f headBottom, Vector3f headTop, Vector3f rotate) {
		vertexHeadBottom = headBottom;
		vertexHeadTop = headTop;
		rotateHeadBottom = rotate;
		// degreeRotateZ = rotateZ;
		//eyeR.setKeyHeadPoints(headBottom,headTop,rotate);
		//eyeL.setKeyHeadPoints(headBottom,headTop,rotate);
		
		eyeRTex.setKeyHeadPoints(headBottom,headTop,rotate);
		eyeLTex.setKeyHeadPoints(headBottom,headTop,rotate);
	}

//	public void setMVP(Matrix4f mvp) {
//		MVP = mvp;
//		//eyeR.setMVP(MVP);
//		//eyeL.setMVP(MVP);
//		
//		eyeRTex.setMVP(MVP);
//		eyeLTex.setMVP(MVP);
//	}

	public void setRGB(float r, float g, float b) {
		skinRed = r;
		skinGreen = g;
		skinBlue = b;
		
		//.setRGB(r,g,b);
		//eyeL.setRGB(r,g,b);
		
		eyeRTex.setRGB(r,g,b);
		eyeLTex.setRGB(r,g,b);
	}

	public void setVertices(int indexCounter, float cosRadian, float sinRadian, float fX, float fY, float fZ, float[] array) {

		// System.out.println("");

		float dX = fX * cosRadian - fY * sinRadian;
		float dY = fY * cosRadian + fX * sinRadian;
		float dZ = fZ;
		// System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" +
		// dY + "\t\tdZ:" + dZ);

		dX *= heightZ;
		dY *= heightZ;
		dZ *= heightZ;
		// System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" +
		// dY + "\t\tdZ:" + dZ);

		dX += x;
		dY += y;
		dZ += z;
		// System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" +
		// dY + "\t\tdZ:" + dZ);

		array[indexCounter++] = (dX);
		array[indexCounter++] = (dY);
		array[indexCounter] = (dZ);

	}

	public void setXYZR(float x, float y, float z, float r) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.degreeRotateZ = r;
		// move();
		//eyeR.setXYZR(x,y,z,r);
		//eyeL.setXYZR(x,y,z,r);
		
		eyeRTex.setXYZR(x,y,z,r);
		eyeLTex.setXYZR(x,y,z,r);
	}

}
