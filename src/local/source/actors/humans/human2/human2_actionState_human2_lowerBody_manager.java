package local.source.actors.humans.human2;

import java.util.HashMap;


public class human2_actionState_human2_lowerBody_manager {
	private static human2_actionState_human2_lowerBody_manager instance = null;
	

	HashMap<String,human2_actionState_human2_lowerBody_state> hashMapStates = new HashMap<String,human2_actionState_human2_lowerBody_state>();

	
	private human2_actionState_human2_lowerBody_manager()
	{}
	
	public static human2_actionState_human2_lowerBody_manager getInstance()
	{
		if(instance==null)
		{ instance = new human2_actionState_human2_lowerBody_manager();}
		return instance;		
	}
	
	public human2_actionState_human2_lowerBody_state get( String key)
	{
		human2_actionState_human2_lowerBody_state result = hashMapStates.get(key);
		
		if(result==null)
		{
			result =  new human2_actionState_human2_lowerBody_state(key);
			this.put(key, result);
		}
	
		return result;
	}
	

	
	public void put(String key, human2_actionState_human2_lowerBody_state val)
	{
		if(!hashMapStates.containsKey(key))
		{ hashMapStates.put(key, val);}
	}

}
