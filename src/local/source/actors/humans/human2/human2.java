package local.source.actors.humans.human2;


import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL32.*;
//import static org.lwjgl.opengl.GL40.*;
//import static org.lwjgl.opengl.GL45.*;

import java.nio.*;

import org.lwjgl.BufferUtils;

import local.source.damage;
import local.source.game_time;
import local.source.manager_engageables;
import local.source.manager_mvp;
import local.source.manager_step_collisions;
import local.source.actors.interface_actor;
import local.source.actors.manager_actors;
import local.source.attacks.manager_attackables;
import local.source.worlds.manager_land;

import org.joml.QuaternionfInterpolator;
import org.joml.Quaternionf;
import org.joml.Matrix4f;
import org.joml.Vector3f;

public class human2 //implements interface_actor 
{
	
	
	Vector3f centerXYZ = new Vector3f();
	Vector3f diameterXYZ = new Vector3f();
	Vector3f engageableDiameterXYZ = new Vector3f();

	
	
	public String actorName="human2 default";
	public String className = "human2";

	boolean shouldUpdateHealthBar = false;
	int maxHealthPoints = 1000;
	int healthPoints = 50;

	Vector3f vertexGround = new Vector3f(0f, 0f, 0f);
	Vector3f vertexCrotch = new Vector3f();
	Vector3f vertexLHip = new Vector3f();
	Vector3f vertexRHip = new Vector3f();
	Vector3f vertexLKnee = new Vector3f();
	Vector3f vertexRKnee = new Vector3f();
	Vector3f vertexLAnkle = new Vector3f();
	Vector3f vertexRAnkle = new Vector3f();
	Vector3f vertexLFoot = new Vector3f();
	Vector3f vertexRFoot = new Vector3f();
	Vector3f vertexBackNavelLine = new Vector3f();
	Vector3f vertexBackNipLine = new Vector3f();
	Vector3f vertexNeckCenter = new Vector3f();
	Vector3f vertexHeadTop = new Vector3f();
	Vector3f vertexLShoulder = new Vector3f();
	Vector3f vertexRShoulder = new Vector3f();
	Vector3f vertexLElbow = new Vector3f();
	Vector3f vertexRElbow = new Vector3f();
	Vector3f vertexLWrist = new Vector3f();
	Vector3f vertexRWrist = new Vector3f();
	Vector3f vertexLHand = new Vector3f();
	Vector3f vertexRHand = new Vector3f();

	Vector3f vertexGroundRotate = new Vector3f(0f, 0f, 0f);
	Vector3f vertexCrotchRotate = new Vector3f();
	Vector3f vertexLHipRotate = new Vector3f();
	Vector3f vertexRHipRotate = new Vector3f();
	Vector3f vertexLKneeRotate = new Vector3f();
	Vector3f vertexRKneeRotate = new Vector3f();
	Vector3f vertexLAnkleRotate = new Vector3f();
	Vector3f vertexRAnkleRotate = new Vector3f();
	Vector3f vertexLFootRotate = new Vector3f();
	Vector3f vertexRFootRotate = new Vector3f();
	Vector3f vertexBackNavelLineRotate = new Vector3f();
	Vector3f vertexBackNipLineRotate = new Vector3f();
	Vector3f vertexNeckCenterRotate = new Vector3f();
	Vector3f vertexHeadTopRotate = new Vector3f();
	Vector3f vertexLShoulderRotate = new Vector3f();
	Vector3f vertexRShoulderRotate = new Vector3f();
	Vector3f vertexLElbowRotate = new Vector3f();
	Vector3f vertexRElbowRotate = new Vector3f();
	Vector3f vertexLWristRotate = new Vector3f();
	Vector3f vertexRWristRotate = new Vector3f();
	Vector3f vertexLHandRotate = new Vector3f();
	Vector3f vertexRHandRotate = new Vector3f();

	// when you start buliding the skin mesh, use those Vector3f objects to get
	// the basics and maybe make little objects with a collection of those
	/// then build the array from that
	// the idea being, make it flexable for all the crazy changes you know
	// you're going to be making before this is done

	byte[] skinMeshTorsoIndicesLines = { 0, 1 //
			, 1, 2 //
			, 0, 2 //
			, 2, 3 //
			, 4, 5//
			, 5, 1//
			, 4, 1//
			, 1, 0//
			, 7, 6//
			, 6, 5//
			, 7, 5//
			, 5, 4//
			, 3, 2//
			, 2, 6//
			, 3, 6//
			, 6, 7//
			, 4, 0// the tops
			, 0, 3//
			, 3, 7//
			, 7, 4 };

	byte[] skinMeshTorsoIndicesDraw = { 0, 1, 2 //
			, 0, 2, 3 //
			, 4, 5, 1//
			, 4, 1, 0//
			, 7, 6, 5//
			, 7, 5, 4//
			, 3, 2, 6//
			, 3, 6, 7//
	};

	byte[] skinArmIndicesDraw = { 0, 2, 1//
			, 0, 1, 3//
			, 0, 3, 4//
			, 0, 4, 2//
			, 1, 2, 6//
			, 1, 6, 5//
			, 3, 1, 5//
			, 3, 5, 7//
			, 4, 3, 7//
			, 4, 7, 8//
			, 2, 4, 8//
			, 2, 8, 6//
			, 9, 5, 6//
			, 9, 7, 5//
			, 9, 8, 7//
			, 9, 6, 8// };
	};

	byte[] skinLArmShoulderToElbowIndicesDraw = { 0, 2, 1//
			, 0, 1, 3//
			, 0, 3, 4//
			, 0, 4, 2//
			, 1, 2, 6//
			, 1, 6, 5//
			, 3, 1, 5//
			, 3, 5, 7//
			, 4, 3, 7//
			, 4, 7, 8//
			, 2, 4, 8//
			, 2, 8, 6//
			, 9, 5, 6//
			, 9, 7, 5//
			, 9, 8, 7//
			, 9, 6, 8// };
	};

	byte[] skinLArmElbowToWristIndicesDraw = { 0, 2, 1//
			, 0, 1, 3//
			, 0, 3, 4//
			, 0, 4, 2//
			, 1, 2, 6//
			, 1, 6, 5//
			, 3, 1, 5//
			, 3, 5, 7//
			, 4, 3, 7//
			, 4, 7, 8//
			, 2, 4, 8//
			, 2, 8, 6//
			, 9, 5, 6//
			, 9, 7, 5//
			, 9, 8, 7//
			, 9, 6, 8// };
	};

	byte[] skinRArmShoulderToElbowIndicesDraw = { 0, 2, 1//
			, 0, 1, 3//
			, 0, 3, 4//
			, 0, 4, 2//
			, 1, 2, 6//
			, 1, 6, 5//
			, 3, 1, 5//
			, 3, 5, 7//
			, 4, 3, 7//
			, 4, 7, 8//
			, 2, 4, 8//
			, 2, 8, 6//
			, 9, 5, 6//
			, 9, 7, 5//
			, 9, 8, 7//
			, 9, 6, 8// };
	};

	byte[] skinRArmElbowToWristIndicesDraw = { 0, 2, 1//
			, 0, 1, 3//
			, 0, 3, 4//
			, 0, 4, 2//
			, 1, 2, 6//
			, 1, 6, 5//
			, 3, 1, 5//
			, 3, 5, 7//
			, 4, 3, 7//
			, 4, 7, 8//
			, 2, 4, 8//
			, 2, 8, 6//
			, 9, 5, 6//
			, 9, 7, 5//
			, 9, 8, 7//
			, 9, 6, 8// };
	};

	byte[] skinLHandIndicesDraw = { 0, 2, 1//
			, 0, 1, 3//
			, 0, 3, 4//
			, 0, 4, 2//
			, 1, 2, 6//
			, 1, 6, 5//
			, 3, 1, 5//
			, 3, 5, 7//
			, 4, 3, 7//
			, 4, 7, 8//
			, 2, 4, 8//
			, 2, 8, 6//
			, 9, 5, 6//
			, 9, 7, 5//
			, 9, 8, 7//
			, 9, 6, 8// };
	};

	byte[] skinRHandIdicesDraw = { 0, 2, 1//
			, 0, 1, 3//
			, 0, 3, 4//
			, 0, 4, 2//
			, 1, 2, 6//
			, 1, 6, 5//
			, 3, 1, 5//
			, 3, 5, 7//
			, 4, 3, 7//
			, 4, 7, 8//
			, 2, 4, 8//
			, 2, 8, 6//
			, 9, 5, 6//
			, 9, 7, 5//
			, 9, 8, 7//
			, 9, 6, 8// };
	};

	byte[] skinTorsoIndicesDraw = { 0, 2, 1 //
			, 0, 3, 2 //
			, 4, 1, 5//
			, 4, 0, 1//
			, 7, 5, 6//
			, 7, 4, 5//
			, 3, 6, 2//
			, 3, 7, 6//
	};

	float[] skinMeshTorsoVerticesDraw = new float[2 * 4 * 3];

	float[] skinLArmSholderToElbowVerticesCalc = new float[10 * 3];
	float[] skinRArmSholderToElbowVerticesCalc = new float[10 * 3];
	float[] skinLArmElbowToWristVerticesCalc = new float[10 * 3];
	float[] skinRArmElbowToWristVerticesCalc = new float[10 * 3];
	float[] skinLHandVerticesCalc = new float[10 * 3];
	float[] skinRHandVerticesCalc = new float[10 * 3];

	float[] skinLLegHipToKneeVerticesCalc = new float[10 * 3];
	float[] skinRLegHipToKneeVerticesCalc = new float[10 * 3];
	float[] skinLLegKneeToAnkleVerticesCalc = new float[10 * 3];
	float[] skinRLegKneeToAnkleVerticesCalc = new float[10 * 3];
	float[] skinLFootVerticesCalc = new float[10 * 3];
	float[] skinRFootVerticesCalc = new float[10 * 3];

	float[] skinNeckVerticesCalc = new float[10 * 3];

	float[] skinArmVerticesDraw = new float[10 * 3];

	float skinColorRed = 1f;
	float skinColorGreen = 0.894f;
	float skinColorBlue = 0.710f;

	float skinSpecialRed = skinColorRed;
	float skinSpecialGreen = skinColorGreen;
	float skinSpecialBlue = skinColorBlue;

	float skinShirtColorRed = 0.0f;
	float skinShirtColorGreen = 0.0f;
	float skinShirtColorBlue = 1.0f;

	float skinPantsColorRed = 0.0f;
	float skinPantsColorGreen = 1.0f;
	float skinPantsColorBlue = 0.0f;

	float skinShoeColorRed = 0.647059f;
	float skinShoeColorGreen = 0.164706f;
	float skinShoeColorBlue = 0.164706f;

	int skinMeshTorsoVAOID = -1;
	int skinMeshTorsoVBOID = -1;
	int skinMeshTorsoVBOIID = -1;

	int skinTorsoVAOID = -1;
	int skinTorsoVBOID = -1;
	int skinTorsoVBOIID = -1;

	int skinArmVAOID = -1;
	int skinArmVBOID = -1;
	int skinArmVBOIID = -1;

	public int skinTorsoGLMVPUniformLocation;
	public int skinTorsoGLRGBAUniformLocation;
	public FloatBuffer skinTorsoFB = BufferUtils.createFloatBuffer(16);

	public int skinTorsoProgramId;
	public int skinTorsoVertexShaderId;
	public int skinTorsoFragmentShaderId;

	boolean isSkinTorsoIndexBound = false;

	public int skinArmGLMVPUniformLocation;
	public int skinArmGLRGBAUniformLocation;
	public FloatBuffer skinArmFB = BufferUtils.createFloatBuffer(16);

	public int skinArmProgramId;
	public int skinArmVertexShaderId;
	public int skinArmFragmentShaderId;

	boolean isSkinArmIndexBound = false;

	public int skinMeshTorsoGLMVPUniformLocation;
	public int skinMeshTorsoGLRGBAUniformLocation;
	public FloatBuffer skinMeshTorsoFB = BufferUtils.createFloatBuffer(16);

	public int skinMeshTorsoProgramId;
	public int skinMeshTorsoVertexShaderId;
	public int skinMeshTorsoFragmentShaderId;

	boolean isSkinMeshTorsoIndexBound = false;

	public boolean isDrawSkinMesh = true;
	public boolean isDrawSkin = true;

	FloatBuffer verticesBufferSkinMeshTorso;
	FloatBuffer verticesBufferSkinTorso;
	FloatBuffer verticesBufferSkinArm;

	Vector3f vertexSkinMesh = new Vector3f(0f, 0f, 0f);

	int skinTorsoIndicesLinesCount;
	int skinMeshTorsoIndicesLinesCount;
	int skinArmIndicesLinesCount;

	human2_body_skinMesh_band[] skinMeshBandsArray = { new human2_body_skinMesh_band(), new human2_body_skinMesh_band(), new human2_body_skinMesh_band(), new human2_body_skinMesh_band(), new human2_body_skinMesh_band(), new human2_body_skinMesh_band(), new human2_body_skinMesh_band(), new human2_body_skinMesh_band(), new human2_body_skinMesh_band(), new human2_body_skinMesh_band(), new human2_body_skinMesh_band(), new human2_body_skinMesh_band(), new human2_body_skinMesh_band(), new human2_body_skinMesh_band(), new human2_body_skinMesh_band(), new human2_body_skinMesh_band() };

	game_time gt = game_time.getInstance();
	// don't use width and length, make all vertices relative to heightZ

	manager_land lManage = manager_land.getInstance();
	manager_actors aManage = manager_actors.getInstance();
	float stepUpHeight = 1f;
	float stepDownHeight = 2f;

	// enum bodyStatesLower {
	// standing, firstStepForward, firstStepBackward, walkingRForward,
	// walkingLForward, squating
	// };

	public enum bodyStatesUpper {
		normal, engage, standingHandsFree, walkingLLegForward, walkingRLegForward, handsFreePunch1, handsFreePunch2, handsFreeEngage, swordSwing1, swordSwing2, swordHold
	};

	enum bodyStatesUpperWeaponStatus {
		normal, handsFree, sword
	};

	int myBodyStateUpperWeaponStatus = bodyStatesUpperWeaponStatus.handsFree.ordinal();
	boolean isAttacking = false;

	boolean showConsoleOutput = false;

	int actionLowerStart = gt.getTime();
	int actionUpperStart = gt.getTime();

	int timeWeaponChanged = gt.getTime();

	float headHeight = 1f / 8f;

	int maxStepDistance = -1;

	byte[] jointsIndicesLines = { 0, 1 // ground to crotch
			, 1, 2 // crotch to l hip
			, 1, 3 // crotch to r hip
			, 2, 4 // l hip to l knee
			, 3, 5 // r hip to r knee
			, 4, 6 // l knee to l ankle
			, 5, 7 // r knee to r ankle
			, 6, 8 // l ankle to l foot
			, 7, 9 // r ankle to r foot
			, 1, 10 // crotch to navel
			, 10, 11 // navel to nips
			, 11, 12 // nips to neck
			, 12, 13 // neck to top of head
			, 11, 14 // nip line to l shoulder
			, 11, 15 // nip line to r shoulder
			, 14, 16 // l shoulder to l elbow
			, 15, 17 // r shoulder to r elbow
			, 16, 18 // l elbow to l wrist
			, 17, 19 // r elbow to r wrist
			, 18, 20 // l wrist to l hand
			, 19, 21 // r wrist to r hand
	};

	float[] jointsVerticesDraw = { 0f, 0f, 0f // ground 0
			, 0f, 0f, .5f // crotch 1
			, 0f, 0f, .5f // l hip 2
			, 0f, 0f, .5f // r hip 3
			, 0f, 0f, .25f // l knee 4
			, 0f, 0f, .25f // r knee 5
			, 0f, 0f, 0f // l ankle 6
			, 0f, 0f, 0f // r ankle 7
			, 0f, 0f, 0f // l foot 8
			, 0f, 0f, 0f // r foot 9
			, 0f, 0f, 0f // backNavelLine 10
			, 0f, 0f, 0f // backNipLine 11
			, 0f, 0f, 0f // back NeckLine 12
			, 0f, 0f, 0f // headTop 13
			, 0f, 0f, 0f // l shoulder 14
			, 0f, 0f, 0f // r shoulder 15
			, 0f, 0f, 0f // l elbow 16
			, 0f, 0f, 0f // r elbow 17
			, 0f, 0f, 0f // l wrist 18
			, 0f, 0f, 0f // r wrist 19
			, 0f, 0f, 0f // l hand 20
			, 0f, 0f, 0f // r hand 21

	};

	int indexLHand = 20;

	boolean isBodyStateLowerChangeable = true;
	boolean isBodyStateUpperChangeable = true;

	float distCrotchToNavelLine = headHeight;
	float distNavelLineToNipLine = headHeight;
	float distNipLineToNeckLine = headHeight;
	float distNeckLineToHeadTop = headHeight;
	float distNipToLShoulder = headHeight;
	float distNipToRShoulder = headHeight;

	float distLShoulderToLElbow = headHeight * 1.5f;
	float distRShoulderToRElbow = headHeight * 1.5f;
	float distLElbowToLWrist = headHeight * 1.5f;
	float distRElbowToRWrist = headHeight * 1.5f;
	float distLWristToLHand = headHeight * .25f;
	float distRWristToRHand = headHeight * .25f;
	float distGroundToCrotchDraw = .5f;

	float distCrotchToLHip = headHeight / 2f;// crotch to l hip 0
	float distCrotchToRHip = headHeight / 2f;
	float distLHipToLKnee = headHeight * 2f;
	float distRHipToRKnee = headHeight * 2f;
	float distLKneeToLAnkle = headHeight * 2f;// l knee to l ankle 4
	float distRKneeToRAnkle = headHeight * 2f;// r knee to r ankle 5
	float distLAnkleToLFoot = headHeight;// l ankle to l foot 6
	float distRAnkleToRFoot = headHeight;

	float distSkinLegUpper = headHeight / 4f;
	float distSkinLegKnee = headHeight / 5f;
	float distSkinLegAnkle = headHeight / 5f;
	float distSkinLegFoot = headHeight / 5f;

	float distSkinNeck = headHeight / 8f;

	float distSkinArmShoulder = headHeight / 4f;
	float distSkinArmElbow = headHeight / 5f;
	float distSkinArmHand = headHeight / 6f;

	float[] jointsLowerBodyRotate = { 0f, 0f, 0f // ground to crotch
			, 0f, 0f, 0f // crotch to l hip
			, 0f, 0f, 0f // crotch to r hip
			, 0f, 0f, 0f // l hip to l knee
			, 0f, 0f, 0f // r hip to r knee
			, 0f, 0f, 0f // l knee to l ankle
			, 0f, 0f, 0f // r knee to r ankle
			, 0f, 0f, 0f // l ankle to l foot
			, 0f, 0f, 0f// r ankle to r foot
	};

	Vector3f[] jointsLowerBodyRotateVectors = { vertexCrotchRotate, vertexLHipRotate, vertexRHipRotate, vertexLKneeRotate, vertexRKneeRotate, vertexLAnkleRotate, vertexRAnkleRotate, vertexLFootRotate, vertexRFootRotate };

	human2_actionState_human2_lowerBody_manager h2bslManager = human2_actionState_human2_lowerBody_manager.getInstance();

	human2_actionState_human2_lowerBody_state h2bslStand = h2bslManager.get("human2_lowerBody_stand");
	human2_actionState_human2_lowerBody_state h2bslSquat = h2bslManager.get("human2_lowerBody_squat");
	human2_actionState_human2_lowerBody_state h2bslWalkFirstStep = h2bslManager.get("human2_lowerBody_firstStep");
	human2_actionState_human2_lowerBody_state h2bslWalkLForward = h2bslManager.get("human2_lowerBody_walkLForward");
	human2_actionState_human2_lowerBody_state h2bslWalkRForward = h2bslManager.get("human2_lowerBody_walkRForward");
	human2_actionState_human2_lowerBody_state h2bslLayDown = h2bslManager.get("human2_lowerBody_layDown");

	human2_actionState_human2_lowerBody_state h2bslCurrent = h2bslStand;
	human2_actionState_human2_lowerBody_state h2bslPrevious = h2bslStand;
	human2_actionState_human2_lowerBody_state h2bslDefault = h2bslStand;

	human2_actionState_weapon_parent aswhfp = new human2_actionState_weapon_handsFree_punch();
	human2_actionState_weapon_parent aswsh = new human2_actionState_weapon_sword_hold();
	human2_actionState_weapon_parent aswss1 = new human2_actionState_weapon_sword_swing1();
	human2_actionState_weapon_parent aswss2 = new human2_actionState_weapon_sword_swing2();

	human2_actionState_human2_upperBody_manager h2bsuManager = human2_actionState_human2_upperBody_manager.getInstance();

	human2_actionState_human2_upperBody_state h2bsuNormal = h2bsuManager.get("human2_upperBody_handsFree");
	human2_actionState_human2_upperBody_state h2bsuStandingHandsFree = h2bsuManager.get("human2_upperBody_handsFree");
	human2_actionState_human2_upperBody_state h2bsuWalkingLLegForward = h2bsuManager.get("human2_upperBody_handsFree_walkLForward");
	human2_actionState_human2_upperBody_state h2bsuWalkingRLegForward = h2bsuManager.get("human2_upperBody_handsFree_walkRForward");
	human2_actionState_human2_upperBody_state h2bsuHandsFreePunch1 = h2bsuManager.get("human2_upperBody_handsFree_punch1");
	human2_actionState_human2_upperBody_state h2bsuHandsFreePunch2 = h2bsuManager.get("human2_upperBody_handsFree_punch2");
	human2_actionState_human2_upperBody_state h2bsuEngage = h2bsuManager.get("human2_upperBody_engage");
	human2_actionState_human2_upperBody_state h2bsuLayDown = h2bsuManager.get("human2_upperBody_layDown");

	human2_actionState_human2_upperBody_state h2bsuSwordHold = h2bsuManager.get("human2_upperBody_sword_hold");
	human2_actionState_human2_upperBody_state h2bsuSwordSwing1 = h2bsuManager.get("human2_upperBody_sword_swing1");
	human2_actionState_human2_upperBody_state h2bsuSwordSwing2 = h2bsuManager.get("human2_upperBody_sword_swing2");

	human2_actionState_human2_upperBody_state h2bsuNew = h2bsuStandingHandsFree;
	human2_actionState_human2_upperBody_state h2bsuOld = h2bsuStandingHandsFree;
	human2_actionState_human2_upperBody_state h2bsuAttackEnd = h2bsuStandingHandsFree;
	human2_actionState_human2_upperBody_state h2bsuDefault = h2bsuStandingHandsFree;

	human2_attack fist = new human2_attack();// .setHuman2BodyStateUpper("human2_upperBody_handsFree_punch1");
	human2_attack swing = new human2_attack();
	human2_weapon_parent myWeapon;// = new weaponHuman2Fist();
	human2_weapon_parent weaponFist = new human2_weapon_fist(fist);
	human2_weapon_parent weaponSword = new human2_weapon_sword(swing);

	float[] jointsUpperBodyRotate = { 0f, 0f, 0f, // crotch to navel
			0f, 0f, 0f, // navel to nips
			0f, 0f, 0f, // nips to neck
			0f, 0f, 0f, // neck to top of head
			0f, 0f, 0f, // nip to l shoulder
			0f, 0f, 0f, // nip to r shoulder
			0f, 0f, 0f, // l shoulder to l elbow
			0f, 0f, 0f, // r shoulder to r elbow
			0f, 0f, 0f, // l elbow to l wrist
			0f, 0f, 0f, // r elbow to r wrist
			0f, 0f, 0f, // l wrist to l hand
			0f, 0f, 0f// r wrist to r hand
	};

	Vector3f[] jointsUpperBodyRotateVectors = { vertexBackNavelLineRotate, vertexBackNipLineRotate, vertexNeckCenterRotate, vertexHeadTopRotate, vertexLShoulderRotate, vertexRShoulderRotate, vertexLElbowRotate, vertexRElbowRotate, vertexLWristRotate, vertexRWristRotate, vertexLHandRotate, vertexRHandRotate };

	int indexUpperBodyCrotchToNavel = 0;
	int indexUpperBodyNavelToNips = 1;
	int indexUpperBodyNipsToNeck = 2;
	int indexUpperBodyNeckToTopOfHead = 3;
	int indexUpperBodyNipToLShoulder = 4;
	int indexUpperBodyNipToRShoulder = 5;
	int indexUpperBodyLShoulderToLElbow = 6;
	int indexUpperBodyRShoulderToRElbow = 7;
	int indexUpperBodyLElbowToLWrist = 8;
	int indexUpperBodyRElbowToRWrist = 9;
	int indexUpperBodyLWristToLHand = 10;
	int indexUpperBodyRWristToRHand = 11;

	int jointsIndicesLinesCount;// = jointsIndicesLines.length;

	float jointsRed = 1f;
	float jointsBlue = 1f;
	float jointsGreen = 1f;

	int jointsVAOID = -1;
	int jointsVBOID = -1;
	int jointsVBOIID = -1;

	public int jointsGLMVPUniformLocation;
	public int jointsGLRGBAUniformLocation;
	public FloatBuffer jointsFB = BufferUtils.createFloatBuffer(16);

	public int jointsProgramId;
	public int jointsVertexShaderId;
	public int jointsFragmentShaderId;

	boolean isJointsIndexBound = false;

	//public Matrix4f MVP;

	public boolean isDrawJoints = false;

	public float x, y, z = 0.0f;
	public float heightZ = 2.0f;
	public float widthY = 1.0f;
	public float lengthX = 1.0f;
	public float turnDegree = 5.0f;
	public float degreeRotateZ = 0.0f;
	public float degreeRotateY = 0.0f;
	public float degreeRotateX = 0.0f;
	public float moveDistance = .0003f;// * headHeight;
	// float armWidth = 327f;

	Quaternionf dest1 = new Quaternionf();
	Quaternionf dest2 = new Quaternionf();
	Vector3f v = new Vector3f();

	FloatBuffer verticesBufferJoints;

	float percentDoneLower = 0f;
	float percentDoneUpper = 0f;

	human2_head head;// = new human2_head();

	public human2() { // System.out.println("entered human2()");
		setRGB(((float) 251 / 256), ((float) 222 / 256), ((float) 134 / 256));
		setHeight(6f);

		head = new human2_head(this.headHeight, heightZ);
		head.setRGB(skinColorRed, skinColorGreen, skinColorBlue);

		registerWithActorManager();
		registerWithAttackableManager();
		registerWithEngageableManager();
		registerWithStepCollisionManager();
		// System.out.println("human2() added to aManage");
		this.glConstruct();
		// System.out.println("human2() glConstruct used");

		// h2bsuEngage.setNextState(h2bsuStandingHandsFree);

		h2bsuHandsFreePunch1.setNextState(h2bsuHandsFreePunch2);
		h2bsuWalkingLLegForward.setNextState(h2bsuWalkingRLegForward);
		h2bsuWalkingRLegForward.setNextState(h2bsuWalkingLLegForward);
		h2bsuSwordSwing1.setNextState(h2bsuSwordSwing2);

		myWeapon = weaponSword;
		h2bsuWalkingLLegForward.setWeaponActionState(aswsh);
		h2bsuWalkingRLegForward.setWeaponActionState(aswsh);

		myBodyStateUpperWeaponStatus = myWeapon.getUpperBodyStateWeaponStatus();

		// h2bslStand.setNextState(h2bslStand);
		// h2bslSquat.setNextState(h2bslSquat);
		// h2bslWalkFirstStep.setNextState(h2bslWalkRForward);
		// h2bslWalkLForward.setNextState(h2bslWalkRForward);
		// h2bslWalkRForward.setNextState(h2bslWalkLForward);
		// System.out.println("leaving human2()");

		fist.setHuman2BodyStateUpper("human2_upperBody_handsFree_punch1");
		swing.setHuman2BodyStateUpper("human2_upperBody_sword_swing1");

		h2bsuNormal.setWeaponActionState(aswhfp);
		h2bsuStandingHandsFree.setWeaponActionState(aswhfp);
		h2bsuWalkingLLegForward.setWeaponActionState(aswhfp);
		h2bsuWalkingRLegForward.setWeaponActionState(aswhfp);
		h2bsuHandsFreePunch1.setWeaponActionState(aswhfp);
		h2bsuHandsFreePunch2.setWeaponActionState(aswhfp);
		h2bsuEngage.setWeaponActionState(aswhfp);

		h2bsuSwordHold.setWeaponActionState(aswsh);
		h2bsuSwordSwing1.setWeaponActionState(aswss1);
		h2bsuSwordSwing2.setWeaponActionState(aswss2);

		// System.out.println("human2() set a bunch of upper/lower body and
		// weapon stuff");

		// System.out.println("human2() about to do for loop on
		// skinmeshBandsArray");
		for (int i = 0; i < 7; i++) {
			skinMeshBandsArray[i].setJoint(vertexBackNavelLine);
			skinMeshBandsArray[i].setJointRotate(vertexBackNavelLineRotate);
		}
		for (int i = 7; i < 10; i++) {
			skinMeshBandsArray[i].setJoint(vertexBackNipLine);
			skinMeshBandsArray[i].setJointRotate(vertexBackNipLineRotate);
		}
		for (int i = 10; i < 16; i++) {
			skinMeshBandsArray[i].setJoint(vertexNeckCenter);
			skinMeshBandsArray[i].setJointRotate(vertexNeckCenterRotate);
		}
		// System.out.println("human2() done with for loop");
		// System.out.println("human2() about to set a bunch of stuff for
		// skinMeshBandsArray");
		// skinMeshBandsArray[0].setZFromJoint(-headHeight);
		// skinMeshBandsArray[1].setZFromJoint(headHeight);

		skinMeshBandsArray[0].setFRRotate(0, 180f, 0);
		skinMeshBandsArray[1].setFRRotate(0, 180f, 0);
		skinMeshBandsArray[2].setFRRotate(0, 180f, 0);
		skinMeshBandsArray[3].setFRRotate(0, 180f, 0);
		skinMeshBandsArray[4].setFRRotate(0, 180f, 0);
		skinMeshBandsArray[5].setFRRotate(0, 180f, 0);
		skinMeshBandsArray[6].setFRRotate(0, 180f, 0);
		skinMeshBandsArray[7].setFRRotate(0, 180f, 0);
		skinMeshBandsArray[8].setFRRotate(0, 180f, 0);
		skinMeshBandsArray[9].setFRRotate(0, 180f, 0);
		skinMeshBandsArray[10].setFRRotate(0, 180f, 0);
		skinMeshBandsArray[11].setFRRotate(0, 180f, 0);
		skinMeshBandsArray[12].setFRRotate(0, 180f, 0);
		skinMeshBandsArray[13].setFRRotate(0, 180f, 0);
		skinMeshBandsArray[14].setFRRotate(0, 180f, 0);
		skinMeshBandsArray[15].setFRRotate(0, 180f, 0);

		skinMeshBandsArray[0].setFLRotate(0, 180f, 0);
		skinMeshBandsArray[1].setFLRotate(0, 180f, 0);
		skinMeshBandsArray[2].setFLRotate(0, 180f, 0);
		skinMeshBandsArray[3].setFLRotate(0, 180f, 0);
		skinMeshBandsArray[4].setFLRotate(0, 180f, 0);
		skinMeshBandsArray[5].setFLRotate(0, 180f, 0);
		skinMeshBandsArray[6].setFLRotate(0, 180f, 0);
		skinMeshBandsArray[7].setFLRotate(0, 180f, 0);
		skinMeshBandsArray[8].setFLRotate(0, 180f, 0);
		skinMeshBandsArray[9].setFLRotate(0, 180f, 0);
		skinMeshBandsArray[10].setFLRotate(0, 180f, 0);
		skinMeshBandsArray[11].setFLRotate(0, 180f, 0);
		skinMeshBandsArray[12].setFLRotate(0, 180f, 0);
		skinMeshBandsArray[13].setFLRotate(0, 180f, 0);
		skinMeshBandsArray[14].setFLRotate(0, 180f, 0);
		skinMeshBandsArray[15].setFLRotate(0, 180f, 0);

		skinMeshBandsArray[0].setBRRotate(0, 180f, 0);
		skinMeshBandsArray[1].setBRRotate(0, 180f, 0);
		skinMeshBandsArray[2].setBRRotate(0, 180f, 0);
		skinMeshBandsArray[3].setBRRotate(0, 180f, 0);
		skinMeshBandsArray[4].setBRRotate(0, 180f, 0);
		skinMeshBandsArray[5].setBRRotate(0, 180f, 0);
		skinMeshBandsArray[6].setBRRotate(0, 180f, 0);
		skinMeshBandsArray[7].setBRRotate(0, 180f, 0);
		skinMeshBandsArray[8].setBRRotate(0, 180f, 0);
		skinMeshBandsArray[9].setBRRotate(0, 180f, 0);
		skinMeshBandsArray[10].setBRRotate(0, 180f, 0);
		skinMeshBandsArray[11].setBRRotate(0, 180f, 0);
		skinMeshBandsArray[12].setBRRotate(0, 180f, 0);
		skinMeshBandsArray[13].setBRRotate(0, 180f, 0);
		skinMeshBandsArray[14].setBRRotate(0, 180f, 0);
		skinMeshBandsArray[15].setBRRotate(0, 180f, 0);

		skinMeshBandsArray[0].setBLRotate(0, 180f, 0);
		skinMeshBandsArray[1].setBLRotate(0, 180f, 0);
		skinMeshBandsArray[2].setBLRotate(0, 180f, 0);
		skinMeshBandsArray[3].setBLRotate(0, 180f, 0);
		skinMeshBandsArray[4].setBLRotate(0, 180f, 0);
		skinMeshBandsArray[5].setBLRotate(0, 180f, 0);
		skinMeshBandsArray[6].setBLRotate(0, 180f, 0);
		skinMeshBandsArray[7].setBLRotate(0, 180f, 0);
		skinMeshBandsArray[8].setBLRotate(0, 180f, 0);
		skinMeshBandsArray[9].setBLRotate(0, 180f, 0);
		skinMeshBandsArray[10].setBLRotate(0, 180f, 0);
		skinMeshBandsArray[11].setBLRotate(0, 180f, 0);
		skinMeshBandsArray[12].setBLRotate(0, 180f, 0);
		skinMeshBandsArray[13].setBLRotate(0, 180f, 0);
		skinMeshBandsArray[14].setBLRotate(0, 180f, 0);
		skinMeshBandsArray[15].setBLRotate(0, 180f, 0);

		int index = 0;
		float lrWidth = headHeight / 1.5f;

		skinMeshBandsArray[0].setLRWidth(0);
		skinMeshBandsArray[1].setLRWidth(lrWidth * .1f);
		skinMeshBandsArray[2].setLRWidth(lrWidth * .9f);
		skinMeshBandsArray[3].setLRWidth(lrWidth * .9f);
		skinMeshBandsArray[4].setLRWidth(lrWidth * .85f);
		skinMeshBandsArray[5].setLRWidth(lrWidth * .8f);
		skinMeshBandsArray[6].setLRWidth(lrWidth * .78f);
		skinMeshBandsArray[7].setLRWidth(lrWidth * .8f);
		skinMeshBandsArray[8].setLRWidth(lrWidth * .8f);
		skinMeshBandsArray[9].setLRWidth(lrWidth * .85f);
		skinMeshBandsArray[10].setLRWidth(lrWidth * .9f);
		skinMeshBandsArray[11].setLRWidth(lrWidth);
		skinMeshBandsArray[12].setLRWidth(lrWidth);
		skinMeshBandsArray[13].setLRWidth(lrWidth);
		skinMeshBandsArray[14].setLRWidth(lrWidth * 0.8f);
		skinMeshBandsArray[15].setLRWidth(0);
		// System.out.println("human2() done with fifth set");

		float defaultWidth = 0f;
		skinMeshBandsArray[0].setFBWidth(0);
		skinMeshBandsArray[1].setFBWidth(defaultWidth);
		skinMeshBandsArray[2].setFBWidth(headHeight / 6f);
		skinMeshBandsArray[3].setFBWidth(headHeight / 6f);
		skinMeshBandsArray[4].setFBWidth(headHeight / 7f);
		skinMeshBandsArray[5].setFBWidth(headHeight / 8);
		skinMeshBandsArray[6].setFBWidth(headHeight / 9);
		skinMeshBandsArray[7].setFBWidth(headHeight / 7);
		skinMeshBandsArray[8].setFBWidth(headHeight / 6);
		skinMeshBandsArray[9].setFBWidth(headHeight / 5.5f);
		skinMeshBandsArray[10].setFBWidth(headHeight / 5.5f);
		skinMeshBandsArray[11].setFBWidth(headHeight / 5f);
		skinMeshBandsArray[12].setFBWidth(headHeight / 5f);
		skinMeshBandsArray[13].setFBWidth(headHeight / 6f);
		skinMeshBandsArray[14].setFBWidth(headHeight / 7f);
		skinMeshBandsArray[15].setFBWidth(0);
		// System.out.println("human2() done with sixth set");

		index = 16;
		float defaultHeight = 40 * headHeight / 16f;
		skinMeshBandsArray[0].setDistance(headHeight * 1.2f);
		skinMeshBandsArray[1].setDistance(headHeight * 1.2f);
		skinMeshBandsArray[2].setDistance(headHeight);
		skinMeshBandsArray[3].setDistance(headHeight / 2f);
		skinMeshBandsArray[4].setDistance(headHeight * .4f);
		skinMeshBandsArray[5].setDistance(headHeight / 5f);
		skinMeshBandsArray[6].setDistance(headHeight / 7f);
		skinMeshBandsArray[7].setDistance(headHeight);
		skinMeshBandsArray[8].setDistance(headHeight / 2f);
		skinMeshBandsArray[9].setDistance(headHeight / 5f);
		skinMeshBandsArray[10].setDistance(headHeight / 1.2f);
		skinMeshBandsArray[11].setDistance(headHeight / 1.5f);
		skinMeshBandsArray[12].setDistance(headHeight / 2f);
		skinMeshBandsArray[13].setDistance(headHeight / 3f);
		skinMeshBandsArray[14].setDistance(headHeight / 7f);
		skinMeshBandsArray[15].setDistance(headHeight / 7f);
		// System.out.println("human2() done with seventh set");

		skinMeshBandsArray[0].setRGB(1f, 0f, 0f);
		skinMeshBandsArray[1].setRGB(1f, 0.1f, 0f);
		skinMeshBandsArray[2].setRGB(1f, 0.2f, 0f);
		skinMeshBandsArray[3].setRGB(1f, 0.3f, 0f);
		skinMeshBandsArray[4].setRGB(1f, 0.4f, 0f);
		skinMeshBandsArray[5].setRGB(1f, 0.5f, 0f);
		skinMeshBandsArray[6].setRGB(1f, 0.6f, 0f);
		skinMeshBandsArray[7].setRGB(1f, 0.7f, 0f);
		skinMeshBandsArray[8].setRGB(1f, 0.8f, 0f);
		skinMeshBandsArray[9].setRGB(1f, 0.9f, 0f);
		skinMeshBandsArray[10].setRGB(1f, 1f, 0.1f);
		skinMeshBandsArray[11].setRGB(1f, 1f, 0.2f);
		skinMeshBandsArray[12].setRGB(1f, 1f, 0.3f);
		skinMeshBandsArray[13].setRGB(1f, 1f, 0.4f);
		skinMeshBandsArray[14].setRGB(1f, 1f, 0.5f);
		skinMeshBandsArray[15].setRGB(1f, 1f, 0.6f);

	}

	public float[] actionJoints() {
		int timeToCompleteLower = 1000;
		int timeToCompleteUpper = 1000;

		float[] vertices;

		int now = gt.getTime();

		// timeToCompleteLower = h2bslNew.getTimeToComplete();
		timeToCompleteLower = h2bslCurrent.getTimeToComplete();
		timeToCompleteUpper = h2bsuNew.getTimeToComplete();

		percentDoneLower = (float) ((float) (now - actionLowerStart) / (float) timeToCompleteLower);
		percentDoneUpper = (float) ((float) (now - actionUpperStart) / (float) timeToCompleteUpper);

		if (percentDoneLower > 1f) {
			isBodyStateLowerChangeable = true;
			percentDoneLower = 0f;
			h2bslPrevious = h2bslCurrent;
			if (h2bslCurrent.getNextState() != null) {
				h2bslCurrent = h2bslCurrent.getNextState();
			} else {
				h2bslCurrent = h2bslDefault;
			}
			actionLowerStart = gt.getTime();
		}

		if (percentDoneUpper > 1f) {
			percentDoneUpper = 0f;

			isBodyStateUpperChangeable = true;
			h2bsuOld = h2bsuNew;
			if (h2bsuNew.getNextState() != null) {
				h2bsuNew = h2bsuNew.getNextState();
			} else {
				h2bsuNew = h2bsuDefault;
			}
			actionUpperStart = gt.getTime();
			isAttacking = h2bsuNew.isAttack();
		}

		float o, n;
		for (int i = 0; i < jointsLowerBodyRotate.length; i++) {

			o = h2bslPrevious.getArray()[i];// h2bslOld.getArray()[i];
			n = h2bslCurrent.getArray()[i];// h2bslNew.getArray()[i];
			jointsLowerBodyRotate[i] = o + ((n - o) * percentDoneLower);

			int index = i / 3;
			int m = i % 3;

			switch (m) {
			case 0:
				jointsLowerBodyRotateVectors[index].x = jointsLowerBodyRotate[i];
				break;
			case 1:
				jointsLowerBodyRotateVectors[index].y = jointsLowerBodyRotate[i];
				break;
			case 2:
				jointsLowerBodyRotateVectors[index].z = jointsLowerBodyRotate[i];
				break;
			default:
				break;
			}
			;
		}

		for (int i = 0; i < jointsUpperBodyRotate.length; i++) {
			o = h2bsuOld.getArray()[i];
			n = h2bsuNew.getArray()[i];
			jointsUpperBodyRotate[i] = o + ((n - o) * percentDoneUpper);

			int index = i / 3;
			int m = i % 3;

			switch (m) {
			case 0:
				jointsUpperBodyRotateVectors[index].x = jointsUpperBodyRotate[i];
				break;
			case 1:
				jointsUpperBodyRotateVectors[index].y = jointsUpperBodyRotate[i];
				break;
			case 2:
				jointsUpperBodyRotateVectors[index].z = jointsUpperBodyRotate[i];
				break;
			default:
				break;
			}
			;
		}

		n = h2bslCurrent.getDistGroundToCrotch();// h2bslNew.getDistGroundToCrotch();
		o = h2bslPrevious.getDistGroundToCrotch();// h2bslOld.getDistGroundToCrotch();
		distGroundToCrotchDraw = o + ((n - o) * percentDoneLower);/// 2f;

		int index = 0;
		// int indexDist = 0;
		vertices = jointsVerticesDraw;
		// ground to crotch

		float dist = distGroundToCrotchDraw;
		quaternionRotate(dist, jointsLowerBodyRotate, index, vertexCrotch, null);
		index += 3;

		// crotch to left hip
		dist = distCrotchToLHip;
		quaternionRotate(dist, jointsLowerBodyRotate, index, vertexLHip, vertexCrotch);
		index += 3;

		// crotch to right
		dist = distCrotchToRHip;
		quaternionRotate(dist, jointsLowerBodyRotate, index, vertexRHip, vertexCrotch);
		index += 3;

		// left hip to left knee
		dist = distLHipToLKnee;
		quaternionRotate(dist, jointsLowerBodyRotate, index, vertexLKnee, vertexLHip);
		index += 3;

		// right hip to right knee
		dist = distRHipToRKnee;
		quaternionRotate(dist, jointsLowerBodyRotate, index, vertexRKnee, vertexRHip);
		index += 3;

		// left knee to left ankle
		dist = distLKneeToLAnkle;
		quaternionRotate(dist, jointsLowerBodyRotate, index, vertexLAnkle, vertexLKnee);
		index += 3;

		// right knee to right ankle
		dist = distRKneeToRAnkle;
		quaternionRotate(dist, jointsLowerBodyRotate, index, vertexRAnkle, vertexRKnee);
		index += 3;

		// left ankle to left foot
		dist = distLAnkleToLFoot;
		quaternionRotate(dist, jointsLowerBodyRotate, index, vertexLFoot, vertexLAnkle);
		index += 3;

		// right ankle to right foot
		dist = distRAnkleToRFoot;
		quaternionRotate(dist, jointsLowerBodyRotate, index, vertexRFoot, vertexRAnkle);
		index = 0;

		dist = distCrotchToNavelLine;
		quaternionRotate(dist, jointsUpperBodyRotate, index, vertexBackNavelLine, vertexCrotch);
		index += 3;

		dist = distNavelLineToNipLine;
		quaternionRotate(dist, jointsUpperBodyRotate, index, vertexBackNipLine, vertexBackNavelLine);
		index += 3;

		dist = distNipLineToNeckLine;
		quaternionRotate(dist, jointsUpperBodyRotate, index, vertexNeckCenter, vertexBackNipLine);
		index += 3;

		dist = distNeckLineToHeadTop;
		quaternionRotate(dist, jointsUpperBodyRotate, index, vertexHeadTop, vertexNeckCenter);
		index += 3;

		dist = distNipToLShoulder;
		quaternionRotate(dist, jointsUpperBodyRotate, index, vertexLShoulder, vertexBackNipLine);
		index += 3;

		dist = distNipToRShoulder;
		quaternionRotate(dist, jointsUpperBodyRotate, index, vertexRShoulder, vertexBackNipLine);
		index += 3;

		dist = distLShoulderToLElbow;
		quaternionRotate(dist, jointsUpperBodyRotate, index, vertexLElbow, vertexLShoulder);
		index += 3;

		dist = distRShoulderToRElbow;
		quaternionRotate(dist, jointsUpperBodyRotate, index, vertexRElbow, vertexRShoulder);
		index += 3;

		dist = distLElbowToLWrist;
		quaternionRotate(dist, jointsUpperBodyRotate, index, vertexLWrist, vertexLElbow);
		index += 3;

		dist = distRElbowToRWrist;
		quaternionRotate(dist, jointsUpperBodyRotate, index, vertexRWrist, vertexRElbow);
		index += 3;

		dist = distLWristToLHand;
		quaternionRotate(dist, jointsUpperBodyRotate, index, vertexLHand, vertexLWrist);
		index += 3;

		dist = distRWristToRHand;
		quaternionRotate(dist, jointsUpperBodyRotate, index, vertexRHand, vertexRWrist);

		// now working on skin vertices
		for (int i = (skinMeshBandsArray.length - 1); i > 0; i--) {
			v.x = 0f;
			v.y = 0f;
			v.z = skinMeshBandsArray[i].dist;// System.out.println("dist :
												// "+v.z);

			Vector3f vvertex = skinMeshBandsArray[i].FRVertex;
			Vector3f vrotate = skinMeshBandsArray[i].FRRotate;
			Vector3f vjoint = skinMeshBandsArray[i].joint;
			Vector3f vjointrotate = skinMeshBandsArray[i].jointRotate;

			float nx, ny, nz;
			nx = vrotate.x;// vjointrotate.x + vrotate.x;
			ny = vjointrotate.y + vrotate.y;
			nz = vrotate.z;// vjointrotate.z + vrotate.z;
			calcuateJointsQuaternions(nx, ny, nz);
			vvertex.x = v.x + vjoint.x + skinMeshBandsArray[i].getFBWidth();
			vvertex.y = v.y + vjoint.y - skinMeshBandsArray[i].getRLWidth();
			vvertex.z = v.z + vjoint.z;// + skinMeshBandsArray[i].getZ();

			// System.out.println("i : "+i+" vrotate x:"+vrotate.x + "
			// y:"+vrotate.y + " z:"+vrotate.z + " vvertex x:"+vvertex.x+"
			// y:"+vvertex.y+" z:"+vvertex.z);

			v.x = 0f;
			v.y = 0f;
			v.z = skinMeshBandsArray[i].dist;// System.out.println("dist :
												// "+v.z);

			vvertex = skinMeshBandsArray[i].FLVertex;
			vrotate = skinMeshBandsArray[i].FLRotate;
			vjoint = skinMeshBandsArray[i].joint;
			vjointrotate = skinMeshBandsArray[i].jointRotate;

			nx = vrotate.x;// vjointrotate.x + vrotate.x;
			ny = vjointrotate.y + vrotate.y;
			nz = vrotate.z;// vjointrotate.z + vrotate.z;
			calcuateJointsQuaternions(nx, ny, nz);
			vvertex.x = v.x + vjoint.x + skinMeshBandsArray[i].getFBWidth();
			vvertex.y = v.y + vjoint.y + skinMeshBandsArray[i].getRLWidth();
			vvertex.z = v.z + vjoint.z;// + skinMeshBandsArray[i].getZ();

			// System.out.println("i : "+i+" vrotate x:"+vrotate.x + "
			// y:"+vrotate.y + " z:"+vrotate.z + " vvertex x:"+vvertex.x+"
			// y:"+vvertex.y+" z:"+vvertex.z);
			v.x = 0f;
			v.y = 0f;
			v.z = skinMeshBandsArray[i].dist;// System.out.println("dist :
												// "+v.z);

			vvertex = skinMeshBandsArray[i].BRVertex;
			vrotate = skinMeshBandsArray[i].BRRotate;
			vjoint = skinMeshBandsArray[i].joint;
			vjointrotate = skinMeshBandsArray[i].jointRotate;

			nx = vrotate.x;// vjointrotate.x + vrotate.x;
			ny = vjointrotate.y + vrotate.y;
			nz = vrotate.z;// vjointrotate.z + vrotate.z;
			calcuateJointsQuaternions(nx, ny, nz);
			vvertex.x = v.x + vjoint.x - skinMeshBandsArray[i].getFBWidth();
			vvertex.y = v.y + vjoint.y - skinMeshBandsArray[i].getRLWidth();
			vvertex.z = v.z + vjoint.z;// + skinMeshBandsArray[i].getZ();

			// System.out.println("i : "+i+" vrotate x:"+vrotate.x + "
			// y:"+vrotate.y + " z:"+vrotate.z + " vvertex x:"+vvertex.x+"
			// y:"+vvertex.y+" z:"+vvertex.z);

			v.x = 0f;
			v.y = 0f;
			v.z = skinMeshBandsArray[i].dist; // System.out.println("dist :
												// "+v.z);

			vvertex = skinMeshBandsArray[i].BLVertex;
			vrotate = skinMeshBandsArray[i].BLRotate;
			vjoint = skinMeshBandsArray[i].joint;
			vjointrotate = skinMeshBandsArray[i].jointRotate;

			nx = vrotate.x;// vjointrotate.x + vrotate.x;
			ny = vjointrotate.y + vrotate.y;
			nz = vrotate.z;// vjointrotate.z + vrotate.z;
			calcuateJointsQuaternions(nx, ny, nz);
			vvertex.x = v.x + vjoint.x - skinMeshBandsArray[i].getFBWidth();
			vvertex.y = v.y + vjoint.y + skinMeshBandsArray[i].getRLWidth();
			vvertex.z = v.z + vjoint.z;// + skinMeshBandsArray[i].getZ();

			// System.out.println("v.x:"+v.x+" v.y:"+v.y+" v.z:"+v.z);
			// System.out.println("i : "+i+" vrotate x:"+vrotate.x + "
			// y:"+vrotate.y + " z:"+vrotate.z + " vvertex x:"+vvertex.x+"
			// y:"+vvertex.y+" z:"+vvertex.z);
		}
		// System.out.println("vertexCrotch : x:"+vertexCrotch.x+"
		// y:"+vertexCrotch.y+" z:"+vertexCrotch.z);
		// System.out.println("vertexSkinMesh : x:"+vertexSkinMesh.x+"
		// y:"+vertexSkinMesh.y+" z:"+vertexSkinMesh.z);
		// System.out.println("index count of skinMesh : " +
		// skinMeshIndicesLinesCount);
		// System.out.println("");

		Vector3f vertexSkinArmUp0 = new Vector3f();
		Vector3f vertexSkinArmUpFL2 = new Vector3f();
		Vector3f vertexSkinArmUpFR1 = new Vector3f();
		Vector3f vertexSkinArmUpBL4 = new Vector3f();
		Vector3f vertexSkinArmUpBR3 = new Vector3f();

		Vector3f vertexSkinArmDown9 = new Vector3f();
		Vector3f vertexSkinArmDownFL6 = new Vector3f();
		Vector3f vertexSkinArmDownFR5 = new Vector3f();
		Vector3f vertexSkinArmDownBL8 = new Vector3f();
		Vector3f vertexSkinArmDownBR7 = new Vector3f();

		dist = 0f;
		Vector3f jointUp = new Vector3f();
		Vector3f jointDown = new Vector3f();
		Vector3f jointDownRotate = new Vector3f();
		float[] armArray = new float[10 * 3];

		for (int z = 0; z < 13; z++) {
			switch (z) {
			case 0:
				armArray = skinLArmSholderToElbowVerticesCalc;
				dist = distSkinArmShoulder;
				jointUp = vertexLShoulder;
				jointDown = vertexLElbow;
				jointDownRotate = vertexLElbowRotate;
				break;
			case 1:
				armArray = skinRArmSholderToElbowVerticesCalc;
				dist = distSkinArmShoulder;
				jointUp = vertexRShoulder;
				jointDown = vertexRElbow;
				jointDownRotate = vertexRElbowRotate;
				break;
			case 2:
				armArray = skinLArmElbowToWristVerticesCalc;
				dist = distSkinArmElbow;
				jointUp = vertexLElbow;
				jointDown = vertexLWrist;
				jointDownRotate = vertexLWristRotate;
				break;
			case 3:
				armArray = skinRArmElbowToWristVerticesCalc;
				dist = distSkinArmElbow;
				jointUp = vertexRElbow;
				jointDown = vertexRWrist;
				jointDownRotate = vertexRWristRotate;
				break;
			case 4:
				armArray = skinLHandVerticesCalc;
				dist = distSkinArmHand;
				jointUp = vertexLWrist;
				jointDown = vertexLHand;
				jointDownRotate = vertexLHandRotate;
				break;
			case 5:
				armArray = skinRHandVerticesCalc;
				dist = distSkinArmHand;
				jointUp = vertexRWrist;
				jointDown = vertexRHand;
				jointDownRotate = vertexRHandRotate;
				break;
			case 6:
				armArray = skinLLegHipToKneeVerticesCalc;
				dist = distSkinLegUpper;
				jointUp = vertexLHip;
				jointDown = vertexLKnee;
				jointDownRotate = vertexLKneeRotate;
				break;
			case 7:
				armArray = skinRLegHipToKneeVerticesCalc;
				dist = distSkinLegUpper;
				jointUp = vertexRHip;
				jointDown = vertexRKnee;
				jointDownRotate = vertexRKneeRotate;
				break;
			case 8:
				armArray = skinLLegKneeToAnkleVerticesCalc;
				dist = distSkinLegKnee;
				jointUp = vertexLKnee;
				jointDown = vertexLAnkle;
				jointDownRotate = vertexLAnkleRotate;
				break;
			case 9:
				armArray = skinRLegKneeToAnkleVerticesCalc;
				dist = distSkinLegKnee;
				jointUp = vertexRKnee;
				jointDown = vertexRAnkle;
				jointDownRotate = vertexRAnkleRotate;
				break;
			case 10:
				armArray = skinLFootVerticesCalc;
				dist = distSkinLegFoot;
				jointUp = vertexLAnkle;
				jointDown = vertexLFoot;
				jointDownRotate = vertexLFootRotate;
				break;
			case 11:
				armArray = skinRFootVerticesCalc;
				dist = distSkinLegFoot;
				jointUp = vertexRAnkle;
				jointDown = vertexRFoot;
				jointDownRotate = vertexRFootRotate;
				break;
			case 12:
				armArray = skinNeckVerticesCalc;
				dist = distSkinNeck;
				jointUp = vertexNeckCenter;
				jointDown = vertexBackNipLine;
				jointDownRotate = vertexNeckCenterRotate;
				break;
			default:
				break;
			}

			v.x = 0f;
			v.y = 0f;
			v.z = dist;

			Vector3f vvertex = vertexSkinArmUp0;
			Vector3f vrotate = new Vector3f(0, -180, 0);
			Vector3f vjoint = jointUp;
			Vector3f vjointrotate = jointDownRotate;

			float nx, ny, nz;
			nx = vjointrotate.x + vrotate.x;
			ny = vjointrotate.y + vrotate.y;
			nz = vjointrotate.z + vrotate.z;
			calcuateJointsQuaternions(nx, ny, nz);
			vvertex.x = v.x + vjoint.x;
			vvertex.y = v.y + vjoint.y;
			vvertex.z = v.z + vjoint.z;

			v.x = 0f;
			v.y = 0f;
			v.z = dist;
			vvertex = vertexSkinArmUpFR1;
			vrotate = new Vector3f(0, -90, 0);
			vjoint = jointUp;
			vjointrotate = jointDownRotate;
			nx = vjointrotate.x + vrotate.x;
			ny = vjointrotate.y + vrotate.y;
			nz = vjointrotate.z + vrotate.z;
			calcuateJointsQuaternions(nx, ny, nz);
			vvertex.x = v.x + vjoint.x;
			vvertex.y = v.y + vjoint.y - dist;
			vvertex.z = v.z + vjoint.z;

			v.x = 0f;
			v.y = 0f;
			v.z = dist;
			vvertex = vertexSkinArmUpFL2;
			vrotate = new Vector3f(0, -90, 0);
			vjoint = jointUp;
			vjointrotate = jointDownRotate;
			nx = vjointrotate.x + vrotate.x;
			ny = vjointrotate.y + vrotate.y;
			nz = vjointrotate.z + vrotate.z;
			calcuateJointsQuaternions(nx, ny, nz);
			vvertex.x = v.x + vjoint.x;
			vvertex.y = v.y + vjoint.y + dist;
			vvertex.z = v.z + vjoint.z;

			v.x = 0f;
			v.y = 0f;
			v.z = dist;
			vvertex = vertexSkinArmUpBL4;
			vrotate = new Vector3f(0, 90, 0);
			vjoint = jointUp;
			vjointrotate = jointDownRotate;
			nx = vjointrotate.x + vrotate.x;
			ny = vjointrotate.y + vrotate.y;
			nz = vjointrotate.z + vrotate.z;
			calcuateJointsQuaternions(nx, ny, nz);
			vvertex.x = v.x + vjoint.x;
			vvertex.y = v.y + vjoint.y + dist;
			vvertex.z = v.z + vjoint.z;

			v.x = 0f;
			v.y = 0f;
			v.z = dist;
			vvertex = vertexSkinArmUpBR3;
			vrotate = new Vector3f(0, 90, 0);
			vjoint = jointUp;
			vjointrotate = jointDownRotate;
			nx = vjointrotate.x + vrotate.x;
			ny = vjointrotate.y + vrotate.y;
			nz = vjointrotate.z + vrotate.z;
			calcuateJointsQuaternions(nx, ny, nz);
			vvertex.x = v.x + vjoint.x;
			vvertex.y = v.y + vjoint.y - dist;
			vvertex.z = v.z + vjoint.z;

			v.x = 0f;
			v.y = 0f;
			v.z = dist;
			vvertex = vertexSkinArmDownFL6;
			vrotate = new Vector3f(0, -90, 0);
			vjoint = jointDown;
			vjointrotate = jointDownRotate;
			nx = vjointrotate.x + vrotate.x;
			ny = vjointrotate.y + vrotate.y;
			nz = vjointrotate.z + vrotate.z;
			calcuateJointsQuaternions(nx, ny, nz);
			vvertex.x = v.x + vjoint.x;
			vvertex.y = v.y + vjoint.y + dist;
			vvertex.z = v.z + vjoint.z;

			v.x = 0f;
			v.y = 0f;
			v.z = dist;
			vvertex = vertexSkinArmDownFR5;
			vrotate = new Vector3f(0, -90, 0);
			vjoint = jointDown;
			vjointrotate = jointDownRotate;
			nx = vjointrotate.x + vrotate.x;
			ny = vjointrotate.y + vrotate.y;
			nz = vjointrotate.z + vrotate.z;
			calcuateJointsQuaternions(nx, ny, nz);
			vvertex.x = v.x + vjoint.x;
			vvertex.y = v.y + vjoint.y - dist;
			vvertex.z = v.z + vjoint.z;

			v.x = 0f;
			v.y = 0f;
			v.z = dist;
			vvertex = vertexSkinArmDownBL8;
			vrotate = new Vector3f(0, 90, 0);
			vjoint = jointDown;
			vjointrotate = jointDownRotate;
			nx = vjointrotate.x + vrotate.x;
			ny = vjointrotate.y + vrotate.y;
			nz = vjointrotate.z + vrotate.z;
			calcuateJointsQuaternions(nx, ny, nz);
			vvertex.x = v.x + vjoint.x;
			vvertex.y = v.y + vjoint.y + dist;
			vvertex.z = v.z + vjoint.z;

			v.x = 0f;
			v.y = 0f;
			v.z = dist;
			vvertex = vertexSkinArmDownBR7;
			vrotate = new Vector3f(0, 90, 0);
			vjoint = jointDown;
			vjointrotate = jointDownRotate;
			nx = vjointrotate.x + vrotate.x;
			ny = vjointrotate.y + vrotate.y;
			nz = vjointrotate.z + vrotate.z;
			calcuateJointsQuaternions(nx, ny, nz);
			vvertex.x = v.x + vjoint.x;
			vvertex.y = v.y + vjoint.y - dist;
			vvertex.z = v.z + vjoint.z;

			v.x = 0f;
			v.y = 0f;
			v.z = dist;
			vvertex = vertexSkinArmDown9;
			vrotate = new Vector3f(0, 180, 0);
			vjoint = jointDown;
			vjointrotate = jointDownRotate;
			nx = vjointrotate.x + vrotate.x;
			ny = vjointrotate.y + vrotate.y;
			nz = vjointrotate.z + vrotate.z;
			calcuateJointsQuaternions(nx, ny, nz);
			vvertex.x = v.x + vjoint.x;
			vvertex.y = v.y + vjoint.y;
			vvertex.z = v.z + vjoint.z;

			// System.out.println("vertexLElbowRotate " + vertexLElbowRotate.x +
			// " " +vertexLElbowRotate.y + " " +vertexLElbowRotate.z + " " );

			index = 0;
			armArray[index++] = vertexSkinArmUp0.x;
			armArray[index++] = vertexSkinArmUp0.y;
			armArray[index++] = vertexSkinArmUp0.z;

			armArray[index++] = vertexSkinArmUpFR1.x;
			armArray[index++] = vertexSkinArmUpFR1.y;
			armArray[index++] = vertexSkinArmUpFR1.z;

			armArray[index++] = vertexSkinArmUpFL2.x;
			armArray[index++] = vertexSkinArmUpFL2.y;
			armArray[index++] = vertexSkinArmUpFL2.z;

			armArray[index++] = vertexSkinArmUpBR3.x;
			armArray[index++] = vertexSkinArmUpBR3.y;
			armArray[index++] = vertexSkinArmUpBR3.z;

			armArray[index++] = vertexSkinArmUpBL4.x;
			armArray[index++] = vertexSkinArmUpBL4.y;
			armArray[index++] = vertexSkinArmUpBL4.z;

			armArray[index++] = vertexSkinArmDownFR5.x;
			armArray[index++] = vertexSkinArmDownFR5.y;
			armArray[index++] = vertexSkinArmDownFR5.z;

			armArray[index++] = vertexSkinArmDownFL6.x;
			armArray[index++] = vertexSkinArmDownFL6.y;
			armArray[index++] = vertexSkinArmDownFL6.z;

			armArray[index++] = vertexSkinArmDownBR7.x;
			armArray[index++] = vertexSkinArmDownBR7.y;
			armArray[index++] = vertexSkinArmDownBR7.z;

			armArray[index++] = vertexSkinArmDownBL8.x;
			armArray[index++] = vertexSkinArmDownBL8.y;
			armArray[index++] = vertexSkinArmDownBL8.z;

			armArray[index++] = vertexSkinArmDown9.x;
			armArray[index++] = vertexSkinArmDown9.y;
			armArray[index++] = vertexSkinArmDown9.z;

		}

		int i = 0;
		// ground
		vertices[i++] = vertexGround.x;
		vertices[i++] = vertexGround.y;
		vertices[i++] = vertexGround.z;
		// ground to crotch
		vertices[i++] = vertexCrotch.x;
		vertices[i++] = vertexCrotch.y;
		vertices[i++] = vertexCrotch.z;

		// crotch to left hip
		vertices[i++] = vertexLHip.x;
		vertices[i++] = vertexLHip.y;
		vertices[i++] = vertexLHip.z;

		// crotch to right hip
		vertices[i++] = vertexRHip.x;
		vertices[i++] = vertexRHip.y;
		vertices[i++] = vertexRHip.z;

		// left hip to left knee
		vertices[i++] = vertexLKnee.x;
		vertices[i++] = vertexLKnee.y;
		vertices[i++] = vertexLKnee.z;

		// right hip to right knee
		vertices[i++] = vertexRKnee.x;
		vertices[i++] = vertexRKnee.y;
		vertices[i++] = vertexRKnee.z;

		// left knee to left ankle
		vertices[i++] = vertexLAnkle.x;
		vertices[i++] = vertexLAnkle.y;
		vertices[i++] = vertexLAnkle.z;

		// right knee to right ankle
		vertices[i++] = vertexRAnkle.x;
		vertices[i++] = vertexRAnkle.y;
		vertices[i++] = vertexRAnkle.z;

		// left ankle to left foot
		vertices[i++] = vertexLFoot.x;
		vertices[i++] = vertexLFoot.y;
		vertices[i++] = vertexLFoot.z;

		// right ankle to right foot
		vertices[i++] = vertexRFoot.x;
		vertices[i++] = vertexRFoot.y;
		vertices[i++] = vertexRFoot.z;

		// crotch to navel
		vertices[i++] = vertexBackNavelLine.x;
		vertices[i++] = vertexBackNavelLine.y;
		vertices[i++] = vertexBackNavelLine.z;

		// navel to nips
		vertices[i++] = vertexBackNipLine.x;
		vertices[i++] = vertexBackNipLine.y;
		vertices[i++] = vertexBackNipLine.z;

		// nips to neck center
		vertices[i++] = vertexNeckCenter.x;
		vertices[i++] = vertexNeckCenter.y;
		vertices[i++] = vertexNeckCenter.z;

		// neck center to head top
		vertices[i++] = vertexHeadTop.x;
		vertices[i++] = vertexHeadTop.y;
		vertices[i++] = vertexHeadTop.z;

		// l shoulder
		vertices[i++] = vertexLShoulder.x;
		vertices[i++] = vertexLShoulder.y;
		vertices[i++] = vertexLShoulder.z;

		// r shoulder
		vertices[i++] = vertexRShoulder.x;
		vertices[i++] = vertexRShoulder.y;
		vertices[i++] = vertexRShoulder.z;

		// l elbow
		vertices[i++] = vertexLElbow.x;
		vertices[i++] = vertexLElbow.y;
		vertices[i++] = vertexLElbow.z;

		// r elbow
		vertices[i++] = vertexRElbow.x;
		vertices[i++] = vertexRElbow.y;
		vertices[i++] = vertexRElbow.z;

		// l wrist
		vertices[i++] = vertexLWrist.x;
		vertices[i++] = vertexLWrist.y;
		vertices[i++] = vertexLWrist.z;

		// r wrist
		vertices[i++] = vertexRWrist.x;
		vertices[i++] = vertexRWrist.y;
		vertices[i++] = vertexRWrist.z;

		// l hand
		vertices[i++] = vertexLHand.x;
		vertices[i++] = vertexLHand.y;
		vertices[i++] = vertexLHand.z;

		// r hand
		vertices[i++] = vertexRHand.x;
		vertices[i++] = vertexRHand.y;
		vertices[i++] = vertexRHand.z;

		// System.out.println("rotates:" + vertexLShoulderRotate.x + " " +
		// vertexLShoulderRotate.y + " " + vertexLShoulderRotate.z + " " );

		return vertices;
	}

	public void bindJointsVertexData() {
		// if (isDrawChange)
		{

			if (isDrawJoints) {
				if (verticesBufferJoints == null) {
					verticesBufferJoints = BufferUtils.createFloatBuffer(jointsVerticesDraw.length);
				}
				verticesBufferJoints.put(jointsVerticesDraw).flip();

				if (jointsVAOID == -1) {
					jointsVAOID = glGenVertexArrays();
				}
				glBindVertexArray(jointsVAOID);

				if (jointsVBOID == -1) {
					jointsVBOID = glGenBuffers();
				}
				glBindBuffer(GL_ARRAY_BUFFER, jointsVBOID);
				glBufferData(GL_ARRAY_BUFFER, verticesBufferJoints, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);

				if (!isJointsIndexBound) {
					jointsIndicesLinesCount = jointsIndicesLines.length;
					ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(jointsIndicesLinesCount);
					indicesBuffer.put(jointsIndicesLines);
					indicesBuffer.flip();

					jointsVBOIID = glGenBuffers();
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, jointsVBOIID);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

					isJointsIndexBound = true;
				}
			}

			// isDrawChange = false;
		}
	}

	public void bindSkinArmVertexData() {
		// if (isDrawChange)
		{

			if (isDrawSkin) {
				if (verticesBufferSkinArm == null) {
					verticesBufferSkinArm = BufferUtils.createFloatBuffer(skinArmVerticesDraw.length);
				}
				verticesBufferSkinArm.put(skinArmVerticesDraw).flip();

				if (skinArmVAOID == -1) {
					skinArmVAOID = glGenVertexArrays();
				}
				glBindVertexArray(skinArmVAOID);

				if (skinArmVBOID == -1) {
					skinArmVBOID = glGenBuffers();
				}
				glBindBuffer(GL_ARRAY_BUFFER, skinArmVBOID);
				glBufferData(GL_ARRAY_BUFFER, verticesBufferSkinArm, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);

				if (!isSkinArmIndexBound) {
					skinArmIndicesLinesCount = skinArmIndicesDraw.length;
					ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(skinArmIndicesLinesCount);
					indicesBuffer.put(skinArmIndicesDraw);
					indicesBuffer.flip();

					skinArmVBOIID = glGenBuffers();
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinArmVBOIID);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

					isSkinArmIndexBound = true;
				}
			}

			// isDrawChange = false;
		}
	}

	public void bindSkinMeshTorsoVertexData() {
		// if (isDrawChange)
		{

			if (isDrawSkinMesh) {
				if (verticesBufferSkinMeshTorso == null) {
					verticesBufferSkinMeshTorso = BufferUtils.createFloatBuffer(skinMeshTorsoVerticesDraw.length);
				}
				verticesBufferSkinMeshTorso.put(skinMeshTorsoVerticesDraw).flip();

				if (skinMeshTorsoVAOID == -1) {
					skinMeshTorsoVAOID = glGenVertexArrays();
				}
				glBindVertexArray(skinMeshTorsoVAOID);

				if (skinMeshTorsoVBOID == -1) {
					skinMeshTorsoVBOID = glGenBuffers();
				}
				glBindBuffer(GL_ARRAY_BUFFER, skinMeshTorsoVBOID);
				glBufferData(GL_ARRAY_BUFFER, verticesBufferSkinMeshTorso, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);

				if (!isSkinMeshTorsoIndexBound) {
					skinMeshTorsoIndicesLinesCount = skinMeshTorsoIndicesLines.length;
					ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(skinMeshTorsoIndicesLinesCount);
					indicesBuffer.put(skinMeshTorsoIndicesLines);
					indicesBuffer.flip();

					skinMeshTorsoVBOIID = glGenBuffers();
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinMeshTorsoVBOIID);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

					isSkinMeshTorsoIndexBound = true;
				}
			}

			// isDrawChange = false;
		}
	}

	public void bindSkinTorsoVertexData() {
		// if (isDrawChange)
		{

			if (isDrawSkinMesh) {
				if (verticesBufferSkinTorso == null) {
					verticesBufferSkinTorso = BufferUtils.createFloatBuffer(skinMeshTorsoVerticesDraw.length);
				}
				verticesBufferSkinTorso.put(skinMeshTorsoVerticesDraw).flip();

				if (skinTorsoVAOID == -1) {
					skinTorsoVAOID = glGenVertexArrays();
				}
				glBindVertexArray(skinTorsoVAOID);

				if (skinTorsoVBOID == -1) {
					skinTorsoVBOID = glGenBuffers();
				}
				glBindBuffer(GL_ARRAY_BUFFER, skinTorsoVBOID);
				glBufferData(GL_ARRAY_BUFFER, verticesBufferSkinTorso, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);

				if (!isSkinTorsoIndexBound) {
					skinTorsoIndicesLinesCount = skinTorsoIndicesDraw.length;
					ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(skinTorsoIndicesLinesCount);
					indicesBuffer.put(skinTorsoIndicesDraw);
					indicesBuffer.flip();

					skinTorsoVBOIID = glGenBuffers();
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinTorsoVBOIID);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

					isSkinTorsoIndexBound = true;
				}
			}

			// isDrawChange = false;
		}
	}

	public void bindVertexData() {
		if (isDrawJoints) {
			bindJointsVertexData();
		}

		if (isDrawSkinMesh) {
			bindSkinMeshTorsoVertexData();
		}

		if (isDrawSkin) {
			bindSkinTorsoVertexData();
			bindSkinArmVertexData();
		}
	}

	public void calcuateJointsQuaternions(float rX, float rY, float rZ) {
		dest1.w = 1f;
		dest1.x = 0f;
		dest1.y = 0f;
		dest1.z = 0f;

		dest2.w = 1f;
		dest2.x = 0f;
		dest2.y = 0f;
		dest2.z = 0f;

		dest2.rotate((float) Math.toRadians(rX), (float) Math.toRadians(rY), (float) Math.toRadians(rZ), dest1);

		// dest1.normalize();
		// dest1.invert();
		dest1.transform(v);
	}

	public void calculateJointsVertices() {
		// System.out.println("human1.move(); called");

		double radian = Math.toRadians(degreeRotateZ);
		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		float fX, fY, fZ;

		int indexCount = 0;
		float[] vertices = {};// .clone();// act.getVertices();

		// jointsVerticesDraw =
		actionJoints();
		vertices = jointsVerticesDraw;

		for (int i = vertices.length; indexCount < i; indexCount += 3) {

			fX = vertices[indexCount];// * lengthX/* X */;
			fY = vertices[indexCount + 1];// * widthY/* Y */;
			fZ = vertices[indexCount + 2];// * heightZ/* Z */;
			setVertices(indexCount, cosRadian, sinRadian, fX, fY, fZ, vertices);
		}

		indexCount = 0;
		vertices = skinMeshTorsoVerticesDraw;
		for (int i = vertices.length; indexCount < i; indexCount += 3) {

			fX = vertices[indexCount];// * lengthX/* X */;
			fY = vertices[indexCount + 1];// * widthY/* Y */;
			fZ = vertices[indexCount + 2];// * heightZ/* Z */;
			setVertices(indexCount, cosRadian, sinRadian, fX, fY, fZ, vertices);
		}
	}

	public void changeWeapon() {
		int now = gt.getTime();
		if (timeWeaponChanged + 500 < now) {
			timeWeaponChanged = now;
			if (myWeapon == weaponSword) {
				myWeapon = weaponFist;
				myBodyStateUpperWeaponStatus = bodyStatesUpperWeaponStatus.handsFree.ordinal();
				h2bsuWalkingLLegForward.setWeaponActionState(aswhfp);
				h2bsuWalkingRLegForward.setWeaponActionState(aswhfp);
				h2bsuDefault = h2bsuSwordHold;
			} else {
				myWeapon = weaponSword;
				myBodyStateUpperWeaponStatus = bodyStatesUpperWeaponStatus.sword.ordinal();
				h2bsuWalkingLLegForward.setWeaponActionState(aswsh);
				h2bsuWalkingRLegForward.setWeaponActionState(aswsh);
				h2bsuDefault = h2bsuStandingHandsFree;
			}
		}
	}

	public void draw() { // System.out.println("human2.draw() just entered" );
		// if(doCalc)
		{
			calculateJointsVertices();
			// doCalc=false;
		}

		if (isDrawJoints) {
			drawJoints();
		}

		if (isDrawSkinMesh || isDrawSkin) {

			for (int i = (skinMeshBandsArray.length - 1); i > 1; i--) {
				int index = 0;
				int k = i - 1;
				if (i == 0) {
					k = 0;
				}
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[i].FRVertex.x;
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[i].FRVertex.y;
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[i].FRVertex.z;

				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[k].FRVertex.x;
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[k].FRVertex.y;
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[k].FRVertex.z;

				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[k].FLVertex.x;
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[k].FLVertex.y;
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[k].FLVertex.z;

				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[i].FLVertex.x;
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[i].FLVertex.y;
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[i].FLVertex.z;

				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[i].BRVertex.x;
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[i].BRVertex.y;
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[i].BRVertex.z;

				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[k].BRVertex.x;
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[k].BRVertex.y;
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[k].BRVertex.z;

				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[k].BLVertex.x;
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[k].BLVertex.y;
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[k].BLVertex.z;

				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[i].BLVertex.x;
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[i].BLVertex.y;
				skinMeshTorsoVerticesDraw[index++] = skinMeshBandsArray[i].BLVertex.z;

				double radian = Math.toRadians(degreeRotateZ);
				float cosRadian = (float) Math.cos(radian);
				float sinRadian = (float) Math.sin(radian);

				float fX, fY, fZ;

				int indexCount = 0;
				float[] vertices = skinMeshTorsoVerticesDraw;
				for (int ix = vertices.length; indexCount < ix; indexCount += 3) {

					fX = vertices[indexCount];// * lengthX/* X */;
					fY = vertices[indexCount + 1];// * widthY/* Y */;
					fZ = vertices[indexCount + 2];// * heightZ/* Z */;
					setVertices(indexCount, cosRadian, sinRadian, fX, fY, fZ, vertices);
				}

				// for (int j = 0; j < skinMeshTorsoVerticesDraw.length; j++) {
				// System.out.println("i:" + i + " j:" + j + " val:" +
				// skinMeshTorsoVerticesDraw[j]);
				// }

				// skinMeshRed = skinMeshBandsArray[i].getRed();
				// skinMeshGreen = skinMeshBandsArray[i].getGreen();
				// skinMeshBlue = skinMeshBandsArray[i].getBlue();
				if (i > 5) {
					skinSpecialRed = skinShirtColorRed;
					skinSpecialGreen = skinShirtColorGreen;
					skinSpecialBlue = skinShirtColorBlue;
				} else {
					skinSpecialRed = skinPantsColorRed;
					skinSpecialGreen = skinPantsColorGreen;
					skinSpecialBlue = skinPantsColorBlue;
				}

				if (isDrawSkinMesh) {
					drawSkinMeshTorso();
				}

				if (isDrawSkin) {
					drawSkinTorso();
				}
			}

			if (isDrawSkin) // draw arms and hands... six in all that need to be
							// passed to skinArmVerticesDraw array and call draw
							// on that
			{

				// for each of the six
				for (int z = 0; z < 13; z++) {
					switch (z) {
					case 0:
						skinArmVerticesDraw = skinLArmSholderToElbowVerticesCalc.clone();
						skinSpecialRed = skinShirtColorRed;
						skinSpecialGreen = skinShirtColorGreen;
						skinSpecialBlue = skinShirtColorBlue;
						break;
					case 1:
						skinArmVerticesDraw = skinRArmSholderToElbowVerticesCalc.clone();
						skinSpecialRed = skinShirtColorRed;
						skinSpecialGreen = skinShirtColorGreen;
						skinSpecialBlue = skinShirtColorBlue;
						break;
					case 2:
						skinArmVerticesDraw = skinLArmElbowToWristVerticesCalc.clone();
						skinSpecialRed = skinShirtColorRed;
						skinSpecialGreen = skinShirtColorGreen;
						skinSpecialBlue = skinShirtColorBlue;
						break;
					case 3:
						skinArmVerticesDraw = skinRArmElbowToWristVerticesCalc.clone();
						skinSpecialRed = skinShirtColorRed;
						skinSpecialGreen = skinShirtColorGreen;
						skinSpecialBlue = skinShirtColorBlue;
						break;
					case 4:
						skinArmVerticesDraw = skinLHandVerticesCalc.clone();
						skinSpecialRed = skinColorRed;
						skinSpecialGreen = skinColorGreen;
						skinSpecialBlue = skinColorBlue;
						break;
					case 5:
						skinArmVerticesDraw = skinRHandVerticesCalc.clone();
						skinSpecialRed = skinColorRed;
						skinSpecialGreen = skinColorGreen;
						skinSpecialBlue = skinColorBlue;
						break;
					case 6:
						skinArmVerticesDraw = skinLLegHipToKneeVerticesCalc.clone();
						skinSpecialRed = skinPantsColorRed;
						skinSpecialGreen = skinPantsColorGreen;
						skinSpecialBlue = skinPantsColorBlue;
						break;
					case 7:
						skinArmVerticesDraw = skinRLegHipToKneeVerticesCalc.clone();
						skinSpecialRed = skinPantsColorRed;
						skinSpecialGreen = skinPantsColorGreen;
						skinSpecialBlue = skinPantsColorBlue;
						break;
					case 8:
						skinArmVerticesDraw = skinLLegKneeToAnkleVerticesCalc.clone();
						skinSpecialRed = skinPantsColorRed;
						skinSpecialGreen = skinPantsColorGreen;
						skinSpecialBlue = skinPantsColorBlue;
						break;
					case 9:
						skinArmVerticesDraw = skinRLegKneeToAnkleVerticesCalc.clone();
						skinSpecialRed = skinPantsColorRed;
						skinSpecialGreen = skinPantsColorGreen;
						skinSpecialBlue = skinPantsColorBlue;
						break;
					case 10:
						skinArmVerticesDraw = skinLFootVerticesCalc.clone();
						skinSpecialRed = skinShoeColorRed;
						skinSpecialGreen = skinShoeColorGreen;
						skinSpecialBlue = skinShoeColorBlue;
						break;
					case 11:
						skinArmVerticesDraw = skinRFootVerticesCalc.clone();
						skinSpecialRed = skinShoeColorRed;
						skinSpecialGreen = skinShoeColorGreen;
						skinSpecialBlue = skinShoeColorBlue;
						break;
					case 12:
						skinArmVerticesDraw = skinNeckVerticesCalc.clone();
						skinSpecialRed = skinColorRed;
						skinSpecialGreen = skinColorGreen;
						skinSpecialBlue = skinColorBlue;
						break;
					default:
						break;
					}

					// for (int i = 0; i < skinArmVerticesDraw.length; i++)
					// {

					// }
					double radian = Math.toRadians(degreeRotateZ);
					float cosRadian = (float) Math.cos(radian);
					float sinRadian = (float) Math.sin(radian);

					float fX, fY, fZ;

					int indexCount = 0;
					float[] vertices = skinArmVerticesDraw;
					for (int ix = vertices.length; indexCount < ix; indexCount += 3) {

						fX = vertices[indexCount];// * lengthX/* X */;
						fY = vertices[indexCount + 1];// * widthY/* Y */;
						fZ = vertices[indexCount + 2];// * heightZ/* Z */;
						setVertices(indexCount, cosRadian, sinRadian, fX, fY, fZ, vertices);
					}
					// draw
					drawSkinArm();
				}
			}
			// vertexNeckCenter.z *=heightZ;
			// vertexHeadTop.z *=heightZ;
			head.setKeyHeadPoints(vertexNeckCenter, vertexHeadTop, vertexNeckCenterRotate);
			head.setXYZR(x, y, z, degreeRotateZ);
			head.draw();
		}

		if (myWeapon != null) {
			float nX = jointsVerticesDraw[indexLHand * 3];
			float nY = jointsVerticesDraw[indexLHand * 3 + 1];
			float nZ = jointsVerticesDraw[indexLHand * 3 + 2];

			myWeapon.setXYZR(nX, nY, nZ, degreeRotateZ);

			float[] n = { 1 };
			float[] o = { 1 };
			// if (h2bsuNew != null)
			{
				// if (h2bsuNew.getWeaponActionState() != null)
				{
					// if (h2bsuNew.getWeaponActionState().getArray() != null)
					{
						// System.out.println("human2.draw() about to get new
						// weapon state array" );
						n = h2bsuNew.getWeaponActionState().getArray();
					}
				}
			}
			// if (h2bsuOld != null)
			{
				// if (h2bsuOld.getWeaponActionState() != null)
				{
					// if (h2bsuOld.getWeaponActionState().getArray() != null)
					{
						// System.out.println("human2.draw() about to get old
						// weapon state array" );
						o = h2bsuOld.getWeaponActionState().getArray();
					}
				}
			}

			// if (n.length > 1 && o.length > 1)
			{
				// System.out.println("human2.draw() about to rotate weapon
				// state arrays " );
				myWeapon.setRotations(n, o, h2bsuNew.timeToComplete, actionUpperStart);
				// System.out.println("human2.draw() rotated weapon state
				// arrays" );
			}

			// System.out.println("human2.draw() about draw weapon" );
			myWeapon.draw();
			// System.out.println("human2.draw() drew weapon and returning from
			// human2.draw()" );
		}
	}

	public void drawJoints() {// System.out.println("human.drawJoints");
		bindJointsVertexData();
		glUseProgram(jointsProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(jointsGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(jointsFB));
		glUniform4f(jointsGLRGBAUniformLocation, jointsRed, jointsGreen, jointsBlue, 1.0f);

		glBindVertexArray(jointsVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, jointsVBOIID);
		glDrawElements(GL_LINES, jointsIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glDrawElements(GL_POINTS, jointsIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);
	}

	public void drawSkinArm() { // ''System.out.println("human.drawSkinMesh");
		bindSkinArmVertexData();
		glUseProgram(skinArmProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(skinArmGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(skinArmFB));
		glUniform4f(skinArmGLRGBAUniformLocation, skinSpecialRed, skinSpecialGreen, skinSpecialBlue, 1.0f);

		glBindVertexArray(skinArmVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinArmVBOIID);
		glDrawElements(GL_TRIANGLES, skinArmIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		// glDrawElements(GL_POINTS, skinMeshIndicesLinesCount,
		// GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);
	}

	public void drawSkinMeshTorso() { // ''System.out.println("human.drawSkinMesh");
		bindSkinMeshTorsoVertexData();
		glUseProgram(skinMeshTorsoProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(skinMeshTorsoGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(skinMeshTorsoFB));
		glUniform4f(skinMeshTorsoGLRGBAUniformLocation, skinSpecialRed, skinSpecialGreen, skinSpecialBlue, 1.0f);

		glBindVertexArray(skinMeshTorsoVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinMeshTorsoVBOIID);
		glDrawElements(GL_LINES, skinMeshTorsoIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glDrawElements(GL_POINTS, skinMeshTorsoIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);
	}

	public void drawSkinTorso() { // ''System.out.println("human.drawSkinMesh");
		bindSkinTorsoVertexData();
		glUseProgram(skinTorsoProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(skinTorsoGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(skinTorsoFB));
		glUniform4f(skinMeshTorsoGLRGBAUniformLocation, skinSpecialRed, skinSpecialGreen, skinSpecialBlue, 1.0f);

		glBindVertexArray(skinTorsoVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinTorsoVBOIID);
		glDrawElements(GL_TRIANGLES, skinTorsoIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		// glDrawElements(GL_POINTS, skinMeshIndicesLinesCount,
		// GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);
	}

	public void engaged()
	{ System.out.println("human2.engaged()");}
	
	public void faceLeft() {
		degreeRotateZ = (degreeRotateZ + turnDegree) % 360.0f;
		// move();
	}

	public void facePoint(float newX, float newY, float newZ) {
		float dX = newX - x;
		float dY = newY - y;
		degreeRotateZ = (float) (Math.atan2(dY, dX) * 180.0f / Math.PI);
	}

	public void faceRight() {
		float change = (degreeRotateZ - turnDegree);
		degreeRotateZ = (float) (change <= 0.0d ? (360.0d - turnDegree) : change % 360.0d);
		// move();
	}

	public String getActorName()
	{return this.actorName;}
	
	public Vector3f getCenterXYZ()
	{ return centerXYZ;}
	
	public String getClassName()
	{ return this.className;}

	public Vector3f getDiameterXYZ()
	{ return diameterXYZ;}
		
	public Vector3f getEngageableDiameterXYZ()
	{ return engageableDiameterXYZ;}
	
	public float getFacingDegrees() {
		return degreeRotateZ;
	}

	private float getFacingX() {
		return (float) (moveDistance * Math.cos(Math.toRadians(degreeRotateZ)));
	}

	private float getFacingY() {
		return (float) (moveDistance * Math.sin(Math.toRadians(degreeRotateZ)));
	}

	private float getForwardX() {
		return x + getFacingX();
	}

	private float getForwardY() {
		return y + getFacingY();
	}

	public float getHeight() {
		return heightZ;
	}

	private float getLeftX() {
		return (float) (moveDistance * Math.cos(Math.toRadians(degreeRotateZ + 90f)));
	}

	private float getLeftY() {
		return (float) (moveDistance * Math.sin(Math.toRadians(degreeRotateZ + 90f)));
	}

	private float getRightX() {
		return (float) (moveDistance * Math.cos(Math.toRadians(degreeRotateZ - 90f)));
	}

	private float getRightY() {
		return (float) (moveDistance * Math.sin(Math.toRadians(degreeRotateZ - 90f)));
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getZ() {
		return z;
	}

	public Vector3f getXYZ()
	{
		Vector3f v = new Vector3f();
		v.x=this.x;
		v.y=this.y;
		v.z=this.z;
		return v;
	}
	
	
	public void glCleanup() {
		glUseProgram(0);
		if (jointsProgramId != 0) {
			if (jointsVertexShaderId != 0) {
				glDetachShader(jointsProgramId, jointsVertexShaderId);
			}
			if (jointsFragmentShaderId != 0) {
				glDetachShader(jointsProgramId, jointsFragmentShaderId);
			}
			glDeleteProgram(jointsProgramId);
		}

		if (skinMeshTorsoProgramId != 0) {
			if (skinMeshTorsoVertexShaderId != 0) {
				glDetachShader(skinMeshTorsoProgramId, skinMeshTorsoVertexShaderId);
			}
			if (skinMeshTorsoFragmentShaderId != 0) {
				glDetachShader(skinMeshTorsoProgramId, skinMeshTorsoFragmentShaderId);
			}
			glDeleteProgram(skinMeshTorsoProgramId);
		}
	}

	public void glConstruct() {
		if (isDrawJoints) {
			glConstructJoints();
		}

		if (isDrawSkinMesh) {
			glConstructSkinMeshTorso();
		}

		if (isDrawSkin) {
			glConstructSkinTorso();
			glConstructSkinArm();
		}
	}

	public void glConstructJoints() {
		try {
			try {
				jointsProgramId = glCreateProgram();

				glCreateVertexShaderJoints("#version 130\nuniform mat4 MVP; in vec3 position; void main() { gl_Position =MVP* vec4(position, 1.0);  gl_PointSize = 3.0; }");

				glCreateFragmentShaderJoints("#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(jointsProgramId);

				glUseProgram(jointsProgramId);
				jointsGLMVPUniformLocation = glGetUniformLocation(jointsProgramId, "MVP");
				jointsGLRGBAUniformLocation = glGetUniformLocation(jointsProgramId, "RGBA");
				glUseProgram(0);

				glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (jointsProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glConstructSkinArm() {
		try {
			try {
				skinArmProgramId = glCreateProgram();

				glCreateVertexShaderSkinArm("#version 130\nuniform mat4 MVP; in vec3 position; void main() { gl_Position =MVP* vec4(position, 1.0);   }");

				glCreateFragmentShaderSkinArm("#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(skinArmProgramId);

				glUseProgram(skinArmProgramId);
				skinArmGLMVPUniformLocation = glGetUniformLocation(skinArmProgramId, "MVP");
				skinArmGLRGBAUniformLocation = glGetUniformLocation(skinArmProgramId, "RGBA");
				glUseProgram(0);

				// glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (skinArmProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glConstructSkinMeshTorso() {
		try {
			try {
				skinMeshTorsoProgramId = glCreateProgram();

				glCreateVertexShaderSkinMeshTorso("#version 130\nuniform mat4 MVP; in vec3 position; void main() { gl_Position =MVP* vec4(position, 1.0);  gl_PointSize = 3.0; }");

				glCreateFragmentShaderSkinMeshTorso("#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(skinMeshTorsoProgramId);

				glUseProgram(skinMeshTorsoProgramId);
				skinMeshTorsoGLMVPUniformLocation = glGetUniformLocation(skinMeshTorsoProgramId, "MVP");
				skinMeshTorsoGLRGBAUniformLocation = glGetUniformLocation(skinMeshTorsoProgramId, "RGBA");
				glUseProgram(0);

				glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (skinMeshTorsoProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glConstructSkinTorso() {
		try {
			try {
				skinTorsoProgramId = glCreateProgram();

				glCreateVertexShaderSkinTorso("#version 130\nuniform mat4 MVP; in vec3 position; void main() { gl_Position =MVP* vec4(position, 1.0);   }");

				glCreateFragmentShaderSkinTorso("#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(skinTorsoProgramId);

				glUseProgram(skinTorsoProgramId);
				skinTorsoGLMVPUniformLocation = glGetUniformLocation(skinTorsoProgramId, "MVP");
				skinTorsoGLRGBAUniformLocation = glGetUniformLocation(skinTorsoProgramId, "RGBA");
				glUseProgram(0);

				// glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (skinTorsoProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glCreateFragmentShaderJoints(String shaderCode) throws Exception {
		jointsFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, jointsProgramId);
	}

	public void glCreateFragmentShaderSkinArm(String shaderCode) throws Exception {
		skinArmFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, skinArmProgramId);
	}

	public void glCreateFragmentShaderSkinMeshTorso(String shaderCode) throws Exception {
		skinMeshTorsoFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, skinMeshTorsoProgramId);
	}

	public void glCreateFragmentShaderSkinTorso(String shaderCode) throws Exception {
		skinTorsoFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, skinTorsoProgramId);
	}

	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShaderJoints(String shaderCode) throws Exception {
		jointsVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, jointsProgramId);
	}

	public void glCreateVertexShaderSkinArm(String shaderCode) throws Exception {
		skinArmVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, skinArmProgramId);
	}

	public void glCreateVertexShaderSkinMeshTorso(String shaderCode) throws Exception {
		skinMeshTorsoVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, skinMeshTorsoProgramId);
	}

	public void glCreateVertexShaderSkinTorso(String shaderCode) throws Exception {
		skinTorsoVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, skinTorsoProgramId);
	}

	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}

	public void healthAdd(int add) {		
			this.healthPoints += add;
			if (this.healthPoints > maxHealthPoints) {
				healthPoints = maxHealthPoints;
			}
			shouldUpdateHealthBar = true;		
	}

	public int healthGetHP() {
		return healthPoints;
	}

	public int healthGetHPPercentage() {
		return (int) (healthPoints / maxHealthPoints * 100);
	}

	public float healthGetHPPercentageAsFloatZeroToOne() {
		return (float) healthPoints / (float) maxHealthPoints;
	}

	public int healthGetMaxHP() {
		return maxHealthPoints;
	}

	public boolean healthGetShouldUpdateHealthBar() {
		boolean result = shouldUpdateHealthBar;

		if (shouldUpdateHealthBar) {
			shouldUpdateHealthBar = false;
		}

		return result;
	}

	public void healthRegen()
	{
		if(healthPoints>0)
		{healthAdd(1);}
	}
	
	public void healthSubtract(int sub) {
		healthPoints -= sub;
		if (healthPoints < 0) {
			healthPoints = 0;
		}
		shouldUpdateHealthBar = true;
	}

	public void moveAttack() { // System.out.println("entered
								// human2.moveAttack()");
		if (!isAttacking) {// System.out.println("is not attacking");
			isBodyStateUpperChangeable = false;

			isAttacking = true;

			h2bsuAttackEnd = h2bsuNew;
			h2bsuOld = h2bsuNew;
			h2bsuNew = myWeapon.getAttack().getHuman2BodyStateUpper();
			actionUpperStart = gt.getTime();

		}
	}

	public void moveBackward() {
		/*
		 * if (isBodyStateLowerChangeable) { isBodyStateLowerChangeable = false;
		 * if (h2bslNew.getEnumVal() !=
		 * bodyStatesLower.firstStepBackward.ordinal() && h2bslNew.getEnumVal()
		 * != bodyStatesLower.walkingRForward.ordinal() && h2bslNew.getEnumVal()
		 * != bodyStatesLower.walkingLForward.ordinal()) { { actionLowerStart =
		 * gt.getTime(); printStateInfo(); } }
		 * 
		 * float facingX = x - getFacingX(); float facingY = y - getFacingY();
		 * float facingZ = z; if (lManage != null) { facingZ =
		 * lManage.getZ(facingX, facingY); if (facingZ - z > stepUpHeight || z -
		 * facingZ > stepDownHeight) { return; } }
		 * 
		 * x = facingX; y = facingY; z = facingZ;
		 * 
		 * if (x < 0f) { x = 0f; }
		 * 
		 * if (y < 0f) { y = 0f; }
		 * 
		 * if (y > maxStepDistance) { y = maxStepDistance; }
		 * 
		 * if (x > maxStepDistance) { x = maxStepDistance; } }
		 */
	}

	public void moveEngage() { // System.out.println("engage :
								// h2bsuNew.getState() : " +h2bsuNew.getState()
								// );
		if (!isAttacking && !h2bsuNew.getState().contains("engage") && !h2bsuOld.getState().contains("engage")) {
			// System.out.println("--------------engaging");
			isBodyStateUpperChangeable = false;
			h2bsuEngage.setNextState(h2bsuNew);
			h2bsuOld = h2bsuNew;
			h2bsuNew = h2bsuEngage;
			actionUpperStart = gt.getTime();
		}
	}

	public void moveForward() {
		if (isBodyStateLowerChangeable) {
			isBodyStateLowerChangeable = false;
			// if (h2bslCurrent.getEnumVal()/* h2bslNew.getEnumVal() */ !=
			// bodyStatesLower.firstStepForward.ordinal() &&
			// h2bslCurrent.getEnumVal()/* h2bslNew.getEnumVal() */ !=
			// bodyStatesLower.walkingLForward.ordinal() &&
			// h2bslCurrent.getEnumVal()/* h2bslNew.getEnumVal() */ !=
			// bodyStatesLower.walkingRForward.ordinal())
			if (!h2bslCurrent.isWalking()) {
				{
					// h2bslOld = h2bslNew;
					// h2bslNew = h2bslWalkFirstStep;

					h2bslPrevious = h2bslCurrent;
					h2bslCurrent = h2bslWalkFirstStep;
					// interruptWithNewLowerBodyAction=true;
					actionLowerStart = gt.getTime();// +h2bslCurrent.getTimeToComplete();

					// actionLowerStart = gt.getTime();
					printStateInfo();

					h2bsuOld = h2bsuNew;
					h2bsuNew = h2bsuWalkingRLegForward;
					actionUpperStart = gt.getTime();// actionLowerStart;
					isBodyStateUpperChangeable = false;

				}
			}

		}

		// System.out.println("h2bslCurrent.getState() :
		// "+h2bslCurrent.getState() );
		if (h2bslCurrent.isWalking() && isBodyStateUpperChangeable && !isAttacking && false) {
			// if (myBodyStateUpperWeaponStatus ==
			// bodyStatesUpperWeaponStatus.handsFree.ordinal())
			{
				if (!h2bsuNew.getState().contains("walk"))// h2bsuNew.getEnumVal()
															// !=
															// bodyStatesUpper.walkingLLegForward.ordinal()
															// &&
															// h2bsuNew.getEnumVal()
															// !=
															// bodyStatesUpper.walkingRLegForward.ordinal())
				{
					h2bsuOld = h2bsuNew;
					// if (h2bslCurrent.getEnumVal() ==
					// bodyStatesLower.walkingLForward.ordinal() ||
					// h2bslCurrent.getEnumVal() ==
					// bodyStatesLower.firstStepForward.ordinal())
					// System.out.println("h2bslCurrent.getState() :
					// "+h2bslCurrent.getState() );
					if (!h2bslCurrent.getState().contains("firstStep") && !h2bslCurrent.getState().contains("walkLForward")) {
						h2bsuNew = h2bsuWalkingRLegForward;// System.out.println("just
															// set upper to
															// RLegForward");
					} else {
						h2bsuNew = h2bsuWalkingLLegForward;// System.out.println("just
															// set upper to
															// LLegForward");
					}
					actionUpperStart = gt.getTime();// actionLowerStart;
					isBodyStateUpperChangeable = false;
				}
			}
		}

		float facingX = x + getFacingX();
		float facingY = y + getFacingY();
		float facingZ = z;
		if (lManage != null) {
			facingZ = lManage.getZ(facingX, facingY);
			if (facingZ - z > stepUpHeight || z - facingZ > stepDownHeight)// if(Math.abs(z-facingZ)>stepHeight)
			{
				return;
			}
		}

		x = facingX;
		y = facingY;
		z = facingZ;

		if (x < 0f) {
			x = 0f;
		}

		if (y < 0f) {
			y = 0f;
		}

		if (y > maxStepDistance) {
			y = maxStepDistance;
		}

		if (x > maxStepDistance) {
			x = maxStepDistance;
		}

	}

	public void moveDown() {
		y -= moveDistance;
		// move();
	}

	public void moveLeft() {
		/*
		 * x += getLeftX(); y += getLeftY();
		 */
		// move();

		float facingX = x + getLeftX();
		float facingY = y + getLeftY();
		float facingZ = z;
		if (lManage != null) {
			facingZ = lManage.getZ(facingX, facingY);
			if (facingZ - z > stepUpHeight || z - facingZ > stepDownHeight)// if(Math.abs(z-facingZ)>stepHeight)
			{
				return;
			}
		}

		x = facingX;
		y = facingY;
		z = facingZ;

		if (x < 0f) {
			x = 0f;
		}

		if (y < 0f) {
			y = 0f;
		}

		if (y > maxStepDistance) {
			y = maxStepDistance;
		}

		if (x > maxStepDistance) {
			x = maxStepDistance;
		}
		printStateInfo();
	}

	public void moveLieDown() {
		if (!isAttacking && h2bsuNew != h2bsuLayDown && h2bsuOld != h2bsuLayDown) {// System.out.println("is
																					// not
																					// attacking");
			isBodyStateUpperChangeable = false;

			// isAttacking = true;

			// h2bsuAttackEnd = h2bsuNew;
			h2bsuOld = h2bsuNew;
			h2bsuNew = h2bsuLayDown;// myWeapon.getAttack().getHuman2BodyStateUpper();

			h2bslPrevious = h2bslCurrent;
			h2bslCurrent = h2bslLayDown;
			actionLowerStart = gt.getTime();
			actionUpperStart = gt.getTime();

		}

	}

	public void moveRight() {
		/*
		 * x += getRightX(); y += getRightY();
		 */
		// move();

		float facingX = x + getRightX();
		float facingY = y + getRightY();
		float facingZ = z;
		if (lManage != null) {
			facingZ = lManage.getZ(facingX, facingY);
			if (facingZ - z > stepUpHeight || z - facingZ > stepDownHeight)// if(Math.abs(z-facingZ)>stepHeight)
			{
				return;
			}
		}

		x = facingX;
		y = facingY;
		z = facingZ;

		if (x < 0f) {
			x = 0f;
		}

		if (y < 0f) {
			y = 0f;
		}

		if (y > maxStepDistance) {
			y = maxStepDistance;
		}

		if (x > maxStepDistance) {
			x = maxStepDistance;
		}
		printStateInfo();
	}

	public void moveSquat() {
		if (isBodyStateLowerChangeable) {
			isBodyStateLowerChangeable = false;
			// if (/* h2bslNew.getEnumVal() */h2bslCurrent.getEnumVal() !=
			// bodyStatesLower.squating.ordinal())
			if (!h2bslCurrent.getState().contains("squat")) {
				// h2bslOld = h2bslNew;
				// h2bslNew = h2bslSquat;
				actionLowerStart = gt.getTime();
				h2bslPrevious = h2bslCurrent;

				h2bslCurrent = h2bslSquat;
				// interruptWithNewLowerBodyAction=true;
				// actionLowerStart =
				// gt.getTime()+h2bslCurrent.getTimeToComplete();

				printStateInfo();

				// if hands free, then set to hands free walking l leg forward
				if (isBodyStateUpperChangeable && !isAttacking) {
					if (myBodyStateUpperWeaponStatus == bodyStatesUpperWeaponStatus.handsFree.ordinal()) {
						h2bsuNew = h2bsuStandingHandsFree;
						actionUpperStart = actionLowerStart;
						isBodyStateUpperChangeable = false;
					}
				}
			}
		}
	}

	public void moveToStanding() { // System.out.println("human2.moveToStanding
									// entered");
		if (isBodyStateLowerChangeable) {
			isBodyStateLowerChangeable = false;
			// if (/* h2bslNew.getEnumVal() */h2bslCurrent.getEnumVal() !=
			// bodyStatesLower.standing.ordinal())
			if (!h2bslCurrent.getState().contains("stand")) {
				// h2bslNew = h2bslStand;
				// h2bslPrevious = h2bslCurrent;

				h2bslCurrent = h2bslStand;
				actionLowerStart = gt.getTime();
				printStateInfo();
				// interruptWithNewLowerBodyAction=true;

				// if hands free, then set to hands free walking l leg forward
				if (isBodyStateUpperChangeable) {
					{
						h2bsuNew = h2bsuStandingHandsFree;
						h2bsuNew.setNextState(h2bsuStandingHandsFree);
						// actionUpperStart = actionLowerStart;
						isBodyStateUpperChangeable = false;
					}
				}
			}
		}
		// System.out.println("human2.moveToStanding leaving");
	}

	public void moveUp() {
		y += moveDistance;
		// move();
	}

	public void printHeight() {
		println("height:" + heightZ);
	}

	private void println(String s) {
		if (this.showConsoleOutput) {
			System.out.println(s);
		}
	}

	public void printStateInfo() {

		// println("old state lower:" + h2bslOld.getState() + "\t\tnew state
		// lower:" + h2bslNew.getState());
	}

	public void printXYZ() {
		println("X:" + x + "\t\ty:" + y + "\t\tz:" + z + "\t\tdegreeRotateZ:" + degreeRotateZ);
	}

	public void quaternionRotate(float dist, float[] joints, int index, Vector3f vv, Vector3f vvOffset) {
		v.x = 0f;
		v.y = 0f;
		v.z = dist;
		calcuateJointsQuaternions(joints[index++], joints[index++], joints[index++]);
		// float jx,jy,jz;
		// jx = joints[index++];
		// jy = joints[index++];
		// jz = joints[index++];
		// calcuateJointsQuaternions(jx,jy,jz);
		// System.out.println("jx:"+jx+"\tjy:"+jy+"\tjz:"+jz);
		if (vvOffset == null) {
			vv.x = v.x;
			vv.y = v.y;
			vv.z = v.z;
			// System.out.println("vv.x:"+vv.x+"\tvv.y:"+vv.y+"\tvv.z:"+vv.z);
		} else {
			vv.x = v.x + vvOffset.x;
			vv.y = v.y + vvOffset.y;
			vv.z = v.z + vvOffset.z;
		}
	}

	public void registerWithActorManager()
	{ //manager_actors.getInstance().add(this);
	}
	
	public void registerWithAttackableManager()
	{ //manager_attackables.getInstance().add(this);
	}
	
	public void registerWithEngageableManager()
	{ //manager_engageables.getInstance().add(this);
	}
	
	public void registerWithStepCollisionManager()
	{ //manager_step_collisions.getInstance().add(this); 
	}
	
	public void setActorName(String n)
	{ this.actorName=n;}
	
	public void setHeight(float x) {
		heightZ = x;
		moveDistance = heightZ * .1f;
		// armWidth = (float) (heightZ / 400f);

		// float head = 1f/8f;

		// headHeight = head*heightZ;
		// distHipToKnee = head*heightZ;
		// distKneeToAnkle = head*heightZ;
		this.diameterXYZ.x = heightZ/7f;
		this.diameterXYZ.y= heightZ/7f;
		this.diameterXYZ.z= heightZ/2f;
		
		
		this.engageableDiameterXYZ.x = heightZ / 2f ;
		this.engageableDiameterXYZ.y = heightZ / 2f;
		this.engageableDiameterXYZ.z = heightZ / 1.5f;

	}

	public void setLandManager(manager_land l) {
		lManage = l;
	}

	public void setMaxStepDistance(int newDist) {
		maxStepDistance = newDist;
	}

//	public void setMVP(Matrix4f mvp) {
//		MVP = mvp;
//
//		if (myWeapon != null) {
//			myWeapon.setMVP(mvp);
//		}
//
//		head.setMVP(MVP);
//	}

	public void setRGB(float r, float g, float b) {
		skinColorRed = r;
		skinColorGreen = g;
		skinColorBlue = b;
	}

	public void setVertices(int indexCounter, float cosRadian, float sinRadian, float fX, float fY, float fZ, float[] array) {

		// System.out.println("");

		float dX = fX * cosRadian - fY * sinRadian;
		float dY = fY * cosRadian + fX * sinRadian;
		float dZ = fZ;
		// System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" +
		// dY + "\t\tdZ:" + dZ);

		dX *= heightZ;
		dY *= heightZ;
		dZ *= heightZ;
		// System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" +
		// dY + "\t\tdZ:" + dZ);

		dX += x;
		dY += y;
		dZ += z;
		// System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" +
		// dY + "\t\tdZ:" + dZ);

		array[indexCounter++] = (dX);
		array[indexCounter++] = (dY);
		array[indexCounter] = (dZ);

	}

	public void setXYZ(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		// move();
		
		this.centerXYZ.x = x;
		this.centerXYZ.x = x;
		this.centerXYZ.x = z + heightZ/2f;
	}

	public void setXY(float newX, float newY) {
		x = newX;
		y = newY;
	}

	public void setZ(float newZ) {
		z = newZ;
	}

	public void takeDamage(damage d)
	{
		System.out.println("human2 : Ouch, I just took "+d.getPhysicalDamage()+" of physical damage");
	}
}
