package local.source.actors.humans.human2;

import org.joml.Vector3f;

public class human2_weapon_sword extends human2_weapon_parent {

	public human2_weapon_sword(human2_attack n) {
		a = n;
		human2UpperBodyStateWeaponStatus = human2.bodyStatesUpperWeaponStatus.sword.ordinal();

		jointsIndicesLines = new byte[] { 0, 1 // bottom to top
		};

		jointsVerticesDraw = new float[] { 0f, 0f, 0f // bottom
				, 0f, 0f, .5f // top
		};

		glConstruct();

		timeStarted = 100;
		timeToComplete = 100;
		
		//this.setAttackUpperBodyState("human2_upperBody_sword_swing1");
	}

	public float[] actionJoints() {
		// System.out.println("weapon.actionJoints()");
		// int timeToCompleteLower = 1000;
		// int timeToCompleteUpper = 1000;

		if (rotationsDraw == null) {// System.out.println("weapon.actionJoints()
									// entered if statement for null
									// rotationsDraw array");
			rotationsDraw = rotationsOld.clone();
			// System.out.println("weapon.actionJoints() leaving if statement
			// for null rotationsDraw array");
		}

		// System.out.println("weapon.actionJoints() just before float[]
		// vertices;");
		float[] vertices;
		// System.out.println("weapon.actionJoints() just after float[]
		// vertices;");
		// float percentDoneLower = 0f;
		// float percentDoneUpper = 0f;

		int now = gt.getTime();
		//System.out.println("weapon.actionJoints() now : " + now);
		float percentDone = ((float) now - (float) timeStarted) / timeToComplete;
		//System.out.println("weapon.actionJoints() timeStarted : " + timeStarted);
		//System.out.println("weapon.actionJoints() timeToComplete : " + timeToComplete);
		//System.out.println("weapon.actionJoints() percentDone : " + percentDone);
		if (percentDone > 1f) {
			percentDone = 1f;
		}
		//System.out.println("weapon.actionJoints() percentDone : " + percentDone);

		int index = 0;

		vertices = jointsVerticesDraw;

		// for(float f : vertices)
		// { System.out.println("vertices[]:"+f);}

		for (int i = 0; i < rotationsDraw.length; i++) {
			//System.out.println("weapon.actionJoints() for loop");
			float o = rotationsOld[i];
			float n = rotationsNew[i];
			rotationsDraw[i] = o + ((n - o) * percentDone);
		}

		float dist = 1.5f;// heightZ;
		index = 0;
		v.x = 0f;
		v.y = 0f;
		v.z = dist;
		//System.out.println("weapon.actionJoints() just before calculateJointsQuaternions()");
		calcuateJointsQuaternions(rotationsDraw[index++], rotationsDraw[index++], rotationsDraw[index++]);
		//System.out.println("weapon.actionJoints() after calculateJointsQuaternions()");
		index = 0;

		vertices[index++] = 0f;
		vertices[index++] = 0f;
		vertices[index++] = 0f;

		vertices[index++] = v.x;
		vertices[index++] = v.y;
		vertices[index++] = v.z;

		// ground to crotch
		/*
		 * float dist = distGroundToCrotchDraw; quaternionRotate(dist,
		 * jointsLowerBodyDraw, index, vertexCrotch, null); index += 3;
		 * 
		 * // crotch to left hip dist = distCrotchToLHip; quaternionRotate(dist,
		 * jointsLowerBodyDraw, index, vertexLHip, vertexCrotch); index += 3;
		 * 
		 * // crotch to right dist = distCrotchToRHip; quaternionRotate(dist,
		 * jointsLowerBodyDraw, index, vertexRHip, vertexCrotch); index += 3;
		 * 
		 * // left hip to left knee dist = distLHipToLKnee;
		 * quaternionRotate(dist, jointsLowerBodyDraw, index, vertexLKnee,
		 * vertexLHip); index += 3;
		 * 
		 * // right hip to right knee dist = distRHipToRKnee;
		 * quaternionRotate(dist, jointsLowerBodyDraw, index, vertexRKnee,
		 * vertexRHip); index += 3;
		 * 
		 * // left knee to left ankle dist = distLKneeToLAnkle;
		 * quaternionRotate(dist, jointsLowerBodyDraw, index, vertexLAnkle,
		 * vertexLKnee); index += 3;
		 * 
		 * // right knee to right ankle dist = distRKneeToRAnkle;
		 * quaternionRotate(dist, jointsLowerBodyDraw, index, vertexRAnkle,
		 * vertexRKnee); index += 3;
		 * 
		 * // left ankle to left foot dist = distLAnkleToLFoot;
		 * quaternionRotate(dist, jointsLowerBodyDraw, index, vertexLFoot,
		 * vertexLAnkle); index += 3;
		 * 
		 * // right ankle to right foot dist = distRAnkleToRFoot;
		 * quaternionRotate(dist, jointsLowerBodyDraw, index, vertexRFoot,
		 * vertexRAnkle); index = 0;
		 * 
		 * dist = distCrotchToNavelLine; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexBackNavelLine, vertexCrotch); index
		 * += 3;
		 * 
		 * dist = distNavelLineToNipLine; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexBackNipLine, vertexBackNavelLine);
		 * index += 3;
		 * 
		 * dist = distNipLineToNeckLine; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexNeckCenter, vertexBackNipLine);
		 * index += 3;
		 * 
		 * dist = distNeckLineToHeadTop; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexHeadTop, vertexNeckCenter); index
		 * += 3;
		 * 
		 * dist = distNipToLShoulder; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexLShoulder, vertexBackNipLine);
		 * index += 3;
		 * 
		 * dist = distNipToRShoulder; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexRShoulder, vertexBackNipLine);
		 * index += 3;
		 * 
		 * dist = distLShoulderToLElbow; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexLElbow, vertexLShoulder); index +=
		 * 3;
		 * 
		 * dist = distRShoulderToRElbow; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexRElbow, vertexRShoulder); index +=
		 * 3;
		 * 
		 * dist = distLElbowToLWrist; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexLWrist, vertexLElbow); index += 3;
		 * 
		 * dist = distRElbowToRWrist; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexRWrist, vertexRElbow); index += 3;
		 * 
		 * dist = distLWristToLHand; quaternionRotate(dist, jointsUpperBodyDraw,
		 * index, vertexLHand, vertexLWrist); index += 3;
		 * 
		 * dist = distRWristToRHand; quaternionRotate(dist, jointsUpperBodyDraw,
		 * index, vertexRHand, vertexRWrist);
		 * 
		 * int i = 0; // ground vertices[i++] = vertexGround.x; vertices[i++] =
		 * vertexGround.y; vertices[i++] = vertexGround.z; // ground to crotch
		 * vertices[i++] = vertexCrotch.x; vertices[i++] = vertexCrotch.y;
		 * vertices[i++] = vertexCrotch.z;
		 * 
		 * // crotch to left hip vertices[i++] = vertexLHip.x; vertices[i++] =
		 * vertexLHip.y; vertices[i++] = vertexLHip.z;
		 * 
		 * // crotch to right hip vertices[i++] = vertexRHip.x; vertices[i++] =
		 * vertexRHip.y; vertices[i++] = vertexRHip.z;
		 * 
		 * // left hip to left knee vertices[i++] = vertexLKnee.x; vertices[i++]
		 * = vertexLKnee.y; vertices[i++] = vertexLKnee.z;
		 * 
		 * // right hip to right knee vertices[i++] = vertexRKnee.x;
		 * vertices[i++] = vertexRKnee.y; vertices[i++] = vertexRKnee.z;
		 * 
		 * // left knee to left ankle vertices[i++] = vertexLAnkle.x;
		 * vertices[i++] = vertexLAnkle.y; vertices[i++] = vertexLAnkle.z;
		 * 
		 * // right knee to right ankle vertices[i++] = vertexRAnkle.x;
		 * vertices[i++] = vertexRAnkle.y; vertices[i++] = vertexRAnkle.z;
		 * 
		 * // left ankle to left foot vertices[i++] = vertexLFoot.x;
		 * vertices[i++] = vertexLFoot.y; vertices[i++] = vertexLFoot.z;
		 * 
		 * // right ankle to right foot vertices[i++] = vertexRFoot.x;
		 * vertices[i++] = vertexRFoot.y; vertices[i++] = vertexRFoot.z;
		 * 
		 * // crotch to navel vertices[i++] = vertexBackNavelLine.x;
		 * vertices[i++] = vertexBackNavelLine.y; vertices[i++] =
		 * vertexBackNavelLine.z;
		 * 
		 * // navel to nips vertices[i++] = vertexBackNipLine.x; vertices[i++] =
		 * vertexBackNipLine.y; vertices[i++] = vertexBackNipLine.z;
		 * 
		 * // nips to neck center vertices[i++] = vertexNeckCenter.x;
		 * vertices[i++] = vertexNeckCenter.y; vertices[i++] =
		 * vertexNeckCenter.z;
		 * 
		 * // neck center to head top vertices[i++] = vertexHeadTop.x;
		 * vertices[i++] = vertexHeadTop.y; vertices[i++] = vertexHeadTop.z;
		 * 
		 * // l shoulder vertices[i++] = vertexLShoulder.x; vertices[i++] =
		 * vertexLShoulder.y; vertices[i++] = vertexLShoulder.z;
		 * 
		 * // r shoulder vertices[i++] = vertexRShoulder.x; vertices[i++] =
		 * vertexRShoulder.y; vertices[i++] = vertexRShoulder.z;
		 * 
		 * // l elbow vertices[i++] = vertexLElbow.x; vertices[i++] =
		 * vertexLElbow.y; vertices[i++] = vertexLElbow.z;
		 * 
		 * // r elbow vertices[i++] = vertexRElbow.x; vertices[i++] =
		 * vertexRElbow.y; vertices[i++] = vertexRElbow.z;
		 * 
		 * // l wrist vertices[i++] = vertexLWrist.x; vertices[i++] =
		 * vertexLWrist.y; vertices[i++] = vertexLWrist.z;
		 * 
		 * // r wrist vertices[i++] = vertexRWrist.x; vertices[i++] =
		 * vertexRWrist.y; vertices[i++] = vertexRWrist.z;
		 * 
		 * // l hand vertices[i++] = vertexLHand.x; vertices[i++] =
		 * vertexLHand.y; vertices[i++] = vertexLHand.z;
		 * 
		 * // r hand vertices[i++] = vertexRHand.x; vertices[i++] =
		 * vertexRHand.y; vertices[i++] = vertexRHand.z;
		 */
		return vertices;
	}

}
