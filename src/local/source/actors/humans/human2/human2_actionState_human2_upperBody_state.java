package local.source.actors.humans.human2;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import local.source.game_time;

public class human2_actionState_human2_upperBody_state {

	human2_actionState_human2_upperBody_manager manager = human2_actionState_human2_upperBody_manager.getInstance();
	
	String strNextActionStateHolder = "";
	String strWeaponActionState = "weapon_handsFree_punch";
	
	
	game_time gt = game_time.getInstance();
	float[] jointsRotations = new float[]{ 0f, 0f, 0f, // crotch to navel
			0f, 0f, 0f, // navel to nips
			0f, 0f, 0f, // nips to neck
			0f, 0f, 0f, // neck to top of head
			-45f, 0f, 0f, // nip to l shoulder
			45f, 0f, 0f, // nip to r shoulder
			0f, 300f, 10f, // l shoulder to l elbow
			0f, 300f, -30f, // r shoulder to r elbow
			0f, 300f, 0f, // l elbow to l wrist
			0f, 300f, 0f, // r elbow to r wrist
			0f, 90f, 0f, // l wrist to l hand
			0f, 90f, 0f// r wrist to r hand
	};
	int timeToComplete;
	String strState="none";
	int enumVal;
	float breathOffset = 5f;
	float nipToLShoulder=-45f;
	float nipToRShoulder=45f;
	float nipLine = 0f;
	
	human2_actionState_human2_upperBody_state nextState;
	
	boolean isAttack = false;
	
	//float[] weaponRotations = new float[]{0f,0f,0f};
	human2_actionState_weapon_parent weaponActionState;
	
	public human2_actionState_human2_upperBody_state( String strFile) {
		//enumVal = val;
		readFromActionStateFile(strFile);
		strState = strFile;
		
		manager.put(strFile, this);
	}
	
	public void breath()
	{
		int time = gt.getTime();		
		boolean up = ((time/1000)%2 == 0 )?true:false;
		int now = (time%1000) ;
		
		if(!up)
		{ now = 1000-now;}
				
		
		jointsRotations[12]= nipToLShoulder+ breathOffset*((float)now/1000f);
		jointsRotations[15]= nipToRShoulder- breathOffset*((float)now/1000f);
	}
	
	public human2_actionState_weapon_parent getWeaponActionState()
	{
		//if(weaponActionState==null)
		//{System.out.println("actionState is null by the way");}
		//else			
		//{System.out.println("actionState is not null");}
		return weaponActionState;
		}
	
	public float[] getArray()
	{ breath();
		return jointsRotations; }
	
	//public int getEnumVal()
	////{return enumVal;}
	
	public human2_actionState_human2_upperBody_state getNextState() {
		//return nextState;
		return manager.get(strNextActionStateHolder);
	}
	public String getState()
	{ return strState;}
	
	public int getTimeToComplete()
	{ return timeToComplete; }
	
	//public float[] getWeaponRotations()
	//{ return weaponRotations;}
	
	public boolean isAttack()
	{return isAttack;}
	

	public void readFromActionStateFile(String fileName) {
		try {

			File pathToFile = new File("resources"  + File.separatorChar + "actors"+ File.separatorChar + "states" + File.separatorChar + "human2" + File.separatorChar + "upperBody" + File.separatorChar + fileName + ".xml");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
			Document document = documentBuilder.parse(pathToFile);
			document.getDocumentElement().normalize();
			NodeList nList = document.getElementsByTagName("actionState");

			//System.out.println("About to enter loop of actionStates");
			for (int i = 0; i < nList.getLength(); i++) {
				//System.out.println("entered array of actionStates");
				Node n = nList.item(i);
				//System.out.println("found a node");
				//System.out.println("n.getNodeName() " + n.getNodeName());
				//System.out.println("n.getLocalName() " + n.getLocalName());
				//System.out.println("n.getNodeValue() " + n.getNodeValue());
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					//System.out.println("entered if statement for n");

					Element e = (Element) n;
					//System.out.println("converted node n to element e");
					

					//if (e.getNodeType() == Node.ELEMENT_NODE && e.getNodeName() == "joints") {
						for (int j = 0; j < e.getElementsByTagName("joints").getLength(); j++) {
							//System.out.println("found an element called joints");
							Element ee = (Element) e.getElementsByTagName("joints").item(j);
							//System.out.println("converted joints to ee element");
							for(int k=0; k<ee.getElementsByTagName("joint").getLength();k++)
							{
								//System.out.println("found an element called joint");
								Element eee = (Element) ee.getElementsByTagName("joint").item(k);
								//System.out.println("converted joint to eee element");
								String eeeName = eee.getAttribute("name");
								String eeeIndex = eee.getAttribute("index");
								String eeeX = eee.getAttribute("x");
								String eeeY = eee.getAttribute("y");
								String eeeZ = eee.getAttribute("z");
															
								int index = Integer.parseInt(eeeIndex);
								float x = Float.parseFloat(eeeX);
								float y = Float.parseFloat(eeeY);
								float z = Float.parseFloat(eeeZ);
								
								jointsRotations[index*3]=x;
								jointsRotations[index*3+1]=y;
								jointsRotations[index*3+2]=z;
							}
						}
					

					Element eTTC = (Element) e.getElementsByTagName("timeToComplete").item(0);
					String strTTC = eTTC.getAttribute("value");
					int intTTC = Integer.parseInt(strTTC);
					timeToComplete=intTTC;
					
					//System.out.println("time to complete : "+this.timeToComplete);
					

					Element eIsAttack = (Element) e.getElementsByTagName("isAttack").item(0);
					String strIsAttack = eIsAttack.getAttribute("value").toLowerCase().trim();
					//System.out.println("strIsWalk : " + strIsWalk);
					isAttack = (strIsAttack.compareTo("true")==0);
					
				
					
					Element eNext = (Element) e.getElementsByTagName("nextActionState").item(0);
					String strNext = eNext.getAttribute("value");
					strNextActionStateHolder=strNext;
					
					
					Element eWAS = (Element) e.getElementsByTagName("weaponActionState").item(0);
					String strWAS = eWAS.getAttribute("value");
					strWeaponActionState=strWAS;
					//System.out.println(strWeaponActionState);
					switch(strWeaponActionState)
					{
					case "weapon_sword_hold":
						weaponActionState = new human2_actionState_weapon_sword_hold(); //System.out.println(weaponActionState.toString());
						break;
					case "weapon_sword_swing1":
						weaponActionState = new human2_actionState_weapon_sword_swing1();//System.out.println(weaponActionState.toString());
						break;
					case "weapon_sword_swing2":
						weaponActionState = new human2_actionState_weapon_sword_swing2();//System.out.println(weaponActionState.toString());
						break;
					case "weapon_handsFree_punch":
						weaponActionState = new human2_actionState_weapon_handsFree_punch();//System.out.println(weaponActionState.toString());
						break;
					default:
						weaponActionState = new human2_actionState_weapon_sword_hold();//System.out.println(weaponActionState.toString());
						break;
					};
					
					break;
				}
			}

		} catch (Exception e) {
			System.out.println("exception caught in world.figureOutHeightMap() : " + e.getMessage() + " " + e.getStackTrace());
		}

	}
	
	public void setNextState(human2_actionState_human2_upperBody_state n)
	{nextState=n;}

	public void setWeaponActionState(human2_actionState_weapon_parent wps)
	{weaponActionState=wps;// System.out.println("just set weaponActionState : " + wps.toString());
	
	}
}
