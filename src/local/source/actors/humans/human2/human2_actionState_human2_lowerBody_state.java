package local.source.actors.humans.human2;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import local.source.game_time;

public class human2_actionState_human2_lowerBody_state {

	human2_actionState_human2_lowerBody_manager manager = human2_actionState_human2_lowerBody_manager.getInstance();
	
	String strNextActionStateHolder = "";
	
	boolean isWalking = false;
	game_time gt = game_time.getInstance();
	float[] jointsRotations = new float[] { 0f, 0f, 0f, // crotch to navel
			0f, 0f, 0f, // navel to nips
			0f, 0f, 0f, // nips to neck
			0f, 0f, 0f, // neck to top of head
			-45f, 0f, 0f, // nip to l shoulder
			45f, 0f, 0f, // nip to r shoulder
			0f, 205f, 10f, // l shoulder to l elbow
			0f, 155f, -10f, // r shoulder to r elbow
			0f, 150f, 0f, // l elbow to l wrist
			0f, 90f, 0f, // r elbow to r wrist
			0f, 90f, 0f, // l wrist to l hand
			0f, 90f, 0f// r wrist to r hand
	};;
	int timeToComplete;
	float distGroundToCrotch = .5f;
	String strState = "none";
	//int enumVal;

	human2_actionState_human2_lowerBody_state nextState;

	public human2_actionState_human2_lowerBody_state( String strFile) {
		//enumVal = val;
		strState=strFile;
		readFromActionStateFile(strFile);
		
		manager.put(strFile, this);
	}

	public float[] getArray() {
		return jointsRotations;
	}

	public float getDistGroundToCrotch() {
		return distGroundToCrotch;
	}

	//public int getEnumVal() {
	//	return enumVal;
	////}

	public human2_actionState_human2_lowerBody_state getNextState() {
		//return nextState;
		return manager.get(strNextActionStateHolder);
	}

	public String getState() {
		return strState;
	}

	public int getTimeToComplete() {
		return timeToComplete;
	}

	public boolean isWalking() {
		return isWalking;
	}

	public void readFromActionStateFile(String fileName) {
		try {

			File pathToFile = new File("resources"  + File.separatorChar + "actors" + File.separatorChar + "states" + File.separatorChar + "human2" + File.separatorChar + "lowerBody" + File.separatorChar + fileName + ".xml");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
			Document document = documentBuilder.parse(pathToFile);
			document.getDocumentElement().normalize();
			NodeList nList = document.getElementsByTagName("actionState");

			//System.out.println("About to enter loop of actionStates");
			for (int i = 0; i < nList.getLength(); i++) {
				//System.out.println("entered array of actionStates");
				Node n = nList.item(i);
				//System.out.println("found a node");
				//System.out.println("n.getNodeName() " + n.getNodeName());
				//System.out.println("n.getLocalName() " + n.getLocalName());
				//System.out.println("n.getNodeValue() " + n.getNodeValue());
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					//System.out.println("entered if statement for n");

					Element e = (Element) n;
					//System.out.println("converted node n to element e");
					

					//if (e.getNodeType() == Node.ELEMENT_NODE && e.getNodeName() == "joints") {
						for (int j = 0; j < e.getElementsByTagName("joints").getLength(); j++) {
							//System.out.println("found an element called joints");
							Element ee = (Element) e.getElementsByTagName("joints").item(j);
							//System.out.println("converted joints to ee element");
							for(int k=0; k<ee.getElementsByTagName("joint").getLength();k++)
							{
								//System.out.println("found an element called joint");
								Element eee = (Element) ee.getElementsByTagName("joint").item(k);
								//System.out.println("converted joint to eee element");
								String eeeName = eee.getAttribute("name");
								String eeeIndex = eee.getAttribute("index");
								String eeeX = eee.getAttribute("x");
								String eeeY = eee.getAttribute("y");
								String eeeZ = eee.getAttribute("z");
								
								//System.out.println("eeeName : "+eeeName);
								//System.out.println("eeeIndex : "+eeeIndex);
								//System.out.println("eeeX : "+eeeX);
								//System.out.println("eeeY : "+eeeY);
								//System.out.println("eeeZ : "+eeeZ);
								
								int index = Integer.parseInt(eeeIndex);
								float x = Float.parseFloat(eeeX);
								float y = Float.parseFloat(eeeY);
								float z = Float.parseFloat(eeeZ);
								
								jointsRotations[index*3]=x;
								jointsRotations[index*3+1]=y;
								jointsRotations[index*3+2]=z;
							}
						}
					

					Element eTTC = (Element) e.getElementsByTagName("timeToComplete").item(0);
					String strTTC = eTTC.getAttribute("value");
					int intTTC = Integer.parseInt(strTTC);
					timeToComplete=intTTC;
					
					//System.out.println("time to complete : "+this.timeToComplete);
					

					Element eIsWalk = (Element) e.getElementsByTagName("isWalking").item(0);
					String strIsWalk = eIsWalk.getAttribute("value").toLowerCase().trim();
					//System.out.println("strIsWalk : " + strIsWalk);
					isWalking = (strIsWalk.compareTo("true")==0);
					
					//System.out.println("is walking : " + isWalking);
					
					Element eDistGtC = (Element) e.getElementsByTagName("distGroundToCrotch").item(0);
					String strDistGtC = eDistGtC.getAttribute("value");
					float fltDistGtC = Float.parseFloat(strDistGtC);
					distGroundToCrotch = fltDistGtC;
					
					Element eNext = (Element) e.getElementsByTagName("nextActionState").item(0);
					String strNext = eNext.getAttribute("value");
					strNextActionStateHolder=strNext;
					//System.out.println("strNext : " + strNext);
					//isWalking = (strIsWalk.compareTo("true")==0);
					
					//System.out.println("dist gtc : "+distGroundToCrotch);
					
					/*
					 * String distVal = e.getAttribute("distanceAcross"); String
					 * uomVal = e.getAttribute("unitOfMeasurement"); String
					 * maxHeightVal = e.getAttribute("maxHeight");
					 * 
					 * worldDistanceAcross = Integer.parseInt(distVal);
					 * 
					 * int arraySize = worldDistanceAcross / 1000;
					 * imgMapHeightArray = new
					 * BufferedImage[arraySize][arraySize]; imgMapColorArray =
					 * new BufferedImage[arraySize][arraySize];
					 * 
					 * if (maxHeightVal.length() > 0) { maxHeight =
					 * Integer.parseInt(maxHeightVal); }
					 * 
					 * for (int j = 0; j <
					 * e.getElementsByTagName("map").getLength(); j++) { Element
					 * ee = (Element) e.getElementsByTagName("map").item(j); if
					 * (ee.getAttribute("type").equals("height")) { String file
					 * = ee.getAttribute("file"); String arrayX =
					 * ee.getAttribute("arrayX"); String arrayY =
					 * ee.getAttribute("arrayY");
					 * 
					 * int x = Integer.parseInt(arrayX); int y =
					 * Integer.parseInt(arrayY);
					 * 
					 * String filePath = System.getProperty("user.home") +
					 * File.separatorChar + ".jadello" + File.separatorChar +
					 * gameID + File.separatorChar + file; File f = new
					 * File(filePath); BufferedImage b = ImageIO.read(f);
					 * imgMapHeightArray[x][y] = b; }
					 * 
					 * if (ee.getAttribute("type").equals("color")) {
					 * 
					 * String file = ee.getAttribute("file"); String arrayX =
					 * ee.getAttribute("arrayX"); String arrayY =
					 * ee.getAttribute("arrayY");
					 * 
					 * int x = Integer.parseInt(arrayX); int y =
					 * Integer.parseInt(arrayY);
					 * 
					 * String filePath = System.getProperty("user.home") +
					 * File.separatorChar + ".jadello" + File.separatorChar +
					 * gameID + File.separatorChar + file; File f = new
					 * File(filePath); BufferedImage b = ImageIO.read(f);
					 * imgMapColorArray[x][y] = b; }
					 */
					break;
				}
			}

		} catch (Exception e) {
			System.out.println("exception caught in world.figureOutHeightMap() : " + e.getMessage() + " " + e.getStackTrace());
		}

	}

	//public void setNextState(actionState_human2_lowerBody_parent next) {
	//	nextState = next;
	//}
}
