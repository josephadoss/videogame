package local.source.actors.humans.human2;

import local.source.game_time;

public class human2_attack {

	game_time gt = game_time.getInstance();
	human2_actionState_human2_upperBody_state h2bsu = human2_actionState_human2_upperBody_manager.getInstance().get("human2_upperBody_handsFree_punch1");
	int damage = 10;
	
	public human2_attack()
	{}
	
	public human2_actionState_human2_upperBody_state getHuman2BodyStateUpper()
	{ return h2bsu;}
	
	public void setHuman2BodyStateUpper( String stateName )
	{ h2bsu = human2_actionState_human2_upperBody_manager.getInstance().get(stateName);}
}
