package local.source.actors.humans.human2;

import java.util.HashMap;


public class human2_actionState_human2_upperBody_manager {
	private static human2_actionState_human2_upperBody_manager instance = null;
	

	HashMap<String,human2_actionState_human2_upperBody_state> hashMapStates = new HashMap<String,human2_actionState_human2_upperBody_state>();

	
	private human2_actionState_human2_upperBody_manager()
	{}
	
	public static human2_actionState_human2_upperBody_manager getInstance()
	{
		if(instance==null)
		{ instance = new human2_actionState_human2_upperBody_manager();}
		return instance;		
	}
	
	public human2_actionState_human2_upperBody_state get( String key)
	{ ////System.out.println("entered uBM.get() with key:"+key);
		human2_actionState_human2_upperBody_state result = hashMapStates.get(key);
		//System.out.println("just grabbed uBS using key");
		if(result==null)
		{////// System.out.println("apparently ubS was null so creating new state");
			result =  new human2_actionState_human2_upperBody_state(key);
			//System.out.println("just created new state");
			this.put(key, result);
			//System.out.println("just added state to hashmap");
		}
	
		//System.out.println("returning result for key :"+key);
		return result;
	}
	

	
	public void put(String key, human2_actionState_human2_upperBody_state val)
	{
		if(!hashMapStates.containsKey(key))
		{ hashMapStates.put(key, val);}
	}

}
