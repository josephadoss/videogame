package local.source.actors.humans.human2;

import static org.lwjgl.opengl.GL11.*;
//import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
//import static org.lwjgl.opengl.GL11.GL_FLOAT;
//import static org.lwjgl.opengl.GL11.GL_FRONT;
//import static org.lwjgl.opengl.GL11.GL_LINES;
//import static org.lwjgl.opengl.GL11.GL_POINTS;
//import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
//import static org.lwjgl.opengl.GL11.glCullFace;
//import static org.lwjgl.opengl.GL11.glDrawElements;
//import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VALIDATE_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glDeleteProgram;
import static org.lwjgl.opengl.GL20.glDetachShader;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;
import static org.lwjgl.opengl.GL20.glGetProgrami;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform4f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;
import static org.lwjgl.opengl.GL32.*;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import local.source.game_time;
import local.source.manager_mvp;

//import human2.bodyStatesLower;
//import human2.bodyStatesUpper;
//import human2.bodyStatesUpperWeaponStatus;

public class human2_weapon_parent {

	human2_actionState_human2_upperBody_manager uBManager = human2_actionState_human2_upperBody_manager.getInstance();
	game_time gt = game_time.getInstance();
	
	human2_attack a = new human2_attack();
	int human2UpperBodyStateWeaponStatus;

	byte[] jointsIndicesLines = { 0, 1 // bottom to top
	};

	float[] jointsVerticesDraw = { 0f, 0f, 0f // bottom
			, 0f, 0f, .5f // top
	};

	float x, y, z;
	public float degreeRotateZ = 0.0f;

	float[] rotationsOld;
	float[] rotationsNew;
	float[] rotationsDraw;
	int timeToComplete;
	int timeStarted;

	int jointsIndicesLinesCount;// = jointsIndicesLines.length;

	float jointsRed = 1f;
	float jointsBlue = 1f;
	float jointsGreen = 1f;

	int jointsVAOID = -1;
	int jointsVBOID = -1;
	int jointsVBOIID = -1;
	
	int skinIndicesLinesCount;// = jointsIndicesLines.length;

	float skinRed = 1f;
	float skinBlue = 1f;
	float skinGreen = 1f;

	int skinVAOID = -1;
	int skinVBOID = -1;
	int skinVBOIID = -1;

	public int jointsGLMVPUniformLocation;
	public int jointsGLRGBAUniformLocation;

	public int skinGLMVPUniformLocation;
	public int skinGLRGBAUniformLocation;

	public FloatBuffer jointsFB = BufferUtils.createFloatBuffer(16);
	public FloatBuffer skinFB = BufferUtils.createFloatBuffer(16);
	public float heightZ = 2.0f;

	Quaternionf dest1 = new Quaternionf();
	Quaternionf dest2 = new Quaternionf();
	Vector3f v = new Vector3f();

	public int jointsProgramId;
	public int jointsVertexShaderId;
	public int jointsFragmentShaderId;

	public int skinProgramId;
	public int skinVertexShaderId;
	public int skinGeometryShaderId;
	public int skinFragmentShaderId;

	boolean isJointsIndexBound = false;
	boolean isSkinIndexBound=false;

	//public Matrix4f MVP;

	public boolean isDrawJoints = true;
	public boolean isDrawSkin = false;

	FloatBuffer verticesBufferJoints;
	FloatBuffer verticesBufferSkin;

	public human2_weapon_parent() {
	}

	public float[] actionJoints() {
		// int timeToCompleteLower = 1000;
		// int timeToCompleteUpper = 1000;

		float[] vertices;
		// float percentDoneLower = 0f;
		// float percentDoneUpper = 0f;

		int index = 0;
		int indexDist = 0;
		vertices = jointsVerticesDraw;

	

		vertices[index++] = 0f;
		vertices[index++] = 0f;
		vertices[index++] = 0f;

		vertices[index++] = 0f;
		vertices[index++] = 0f;
		vertices[index++] = 1f;

		// ground to crotch
		/*
		 * float dist = distGroundToCrotchDraw; quaternionRotate(dist,
		 * jointsLowerBodyDraw, index, vertexCrotch, null); index += 3;
		 * 
		 * // crotch to left hip dist = distCrotchToLHip; quaternionRotate(dist,
		 * jointsLowerBodyDraw, index, vertexLHip, vertexCrotch); index += 3;
		 * 
		 * // crotch to right dist = distCrotchToRHip; quaternionRotate(dist,
		 * jointsLowerBodyDraw, index, vertexRHip, vertexCrotch); index += 3;
		 * 
		 * // left hip to left knee dist = distLHipToLKnee;
		 * quaternionRotate(dist, jointsLowerBodyDraw, index, vertexLKnee,
		 * vertexLHip); index += 3;
		 * 
		 * // right hip to right knee dist = distRHipToRKnee;
		 * quaternionRotate(dist, jointsLowerBodyDraw, index, vertexRKnee,
		 * vertexRHip); index += 3;
		 * 
		 * // left knee to left ankle dist = distLKneeToLAnkle;
		 * quaternionRotate(dist, jointsLowerBodyDraw, index, vertexLAnkle,
		 * vertexLKnee); index += 3;
		 * 
		 * // right knee to right ankle dist = distRKneeToRAnkle;
		 * quaternionRotate(dist, jointsLowerBodyDraw, index, vertexRAnkle,
		 * vertexRKnee); index += 3;
		 * 
		 * // left ankle to left foot dist = distLAnkleToLFoot;
		 * quaternionRotate(dist, jointsLowerBodyDraw, index, vertexLFoot,
		 * vertexLAnkle); index += 3;
		 * 
		 * // right ankle to right foot dist = distRAnkleToRFoot;
		 * quaternionRotate(dist, jointsLowerBodyDraw, index, vertexRFoot,
		 * vertexRAnkle); index = 0;
		 * 
		 * dist = distCrotchToNavelLine; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexBackNavelLine, vertexCrotch); index
		 * += 3;
		 * 
		 * dist = distNavelLineToNipLine; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexBackNipLine, vertexBackNavelLine);
		 * index += 3;
		 * 
		 * dist = distNipLineToNeckLine; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexNeckCenter, vertexBackNipLine);
		 * index += 3;
		 * 
		 * dist = distNeckLineToHeadTop; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexHeadTop, vertexNeckCenter); index
		 * += 3;
		 * 
		 * dist = distNipToLShoulder; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexLShoulder, vertexBackNipLine);
		 * index += 3;
		 * 
		 * dist = distNipToRShoulder; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexRShoulder, vertexBackNipLine);
		 * index += 3;
		 * 
		 * dist = distLShoulderToLElbow; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexLElbow, vertexLShoulder); index +=
		 * 3;
		 * 
		 * dist = distRShoulderToRElbow; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexRElbow, vertexRShoulder); index +=
		 * 3;
		 * 
		 * dist = distLElbowToLWrist; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexLWrist, vertexLElbow); index += 3;
		 * 
		 * dist = distRElbowToRWrist; quaternionRotate(dist,
		 * jointsUpperBodyDraw, index, vertexRWrist, vertexRElbow); index += 3;
		 * 
		 * dist = distLWristToLHand; quaternionRotate(dist, jointsUpperBodyDraw,
		 * index, vertexLHand, vertexLWrist); index += 3;
		 * 
		 * dist = distRWristToRHand; quaternionRotate(dist, jointsUpperBodyDraw,
		 * index, vertexRHand, vertexRWrist);
		 * 
		 * int i = 0; // ground vertices[i++] = vertexGround.x; vertices[i++] =
		 * vertexGround.y; vertices[i++] = vertexGround.z; // ground to crotch
		 * vertices[i++] = vertexCrotch.x; vertices[i++] = vertexCrotch.y;
		 * vertices[i++] = vertexCrotch.z;
		 * 
		 * // crotch to left hip vertices[i++] = vertexLHip.x; vertices[i++] =
		 * vertexLHip.y; vertices[i++] = vertexLHip.z;
		 * 
		 * // crotch to right hip vertices[i++] = vertexRHip.x; vertices[i++] =
		 * vertexRHip.y; vertices[i++] = vertexRHip.z;
		 * 
		 * // left hip to left knee vertices[i++] = vertexLKnee.x; vertices[i++]
		 * = vertexLKnee.y; vertices[i++] = vertexLKnee.z;
		 * 
		 * // right hip to right knee vertices[i++] = vertexRKnee.x;
		 * vertices[i++] = vertexRKnee.y; vertices[i++] = vertexRKnee.z;
		 * 
		 * // left knee to left ankle vertices[i++] = vertexLAnkle.x;
		 * vertices[i++] = vertexLAnkle.y; vertices[i++] = vertexLAnkle.z;
		 * 
		 * // right knee to right ankle vertices[i++] = vertexRAnkle.x;
		 * vertices[i++] = vertexRAnkle.y; vertices[i++] = vertexRAnkle.z;
		 * 
		 * // left ankle to left foot vertices[i++] = vertexLFoot.x;
		 * vertices[i++] = vertexLFoot.y; vertices[i++] = vertexLFoot.z;
		 * 
		 * // right ankle to right foot vertices[i++] = vertexRFoot.x;
		 * vertices[i++] = vertexRFoot.y; vertices[i++] = vertexRFoot.z;
		 * 
		 * // crotch to navel vertices[i++] = vertexBackNavelLine.x;
		 * vertices[i++] = vertexBackNavelLine.y; vertices[i++] =
		 * vertexBackNavelLine.z;
		 * 
		 * // navel to nips vertices[i++] = vertexBackNipLine.x; vertices[i++] =
		 * vertexBackNipLine.y; vertices[i++] = vertexBackNipLine.z;
		 * 
		 * // nips to neck center vertices[i++] = vertexNeckCenter.x;
		 * vertices[i++] = vertexNeckCenter.y; vertices[i++] =
		 * vertexNeckCenter.z;
		 * 
		 * // neck center to head top vertices[i++] = vertexHeadTop.x;
		 * vertices[i++] = vertexHeadTop.y; vertices[i++] = vertexHeadTop.z;
		 * 
		 * // l shoulder vertices[i++] = vertexLShoulder.x; vertices[i++] =
		 * vertexLShoulder.y; vertices[i++] = vertexLShoulder.z;
		 * 
		 * // r shoulder vertices[i++] = vertexRShoulder.x; vertices[i++] =
		 * vertexRShoulder.y; vertices[i++] = vertexRShoulder.z;
		 * 
		 * // l elbow vertices[i++] = vertexLElbow.x; vertices[i++] =
		 * vertexLElbow.y; vertices[i++] = vertexLElbow.z;
		 * 
		 * // r elbow vertices[i++] = vertexRElbow.x; vertices[i++] =
		 * vertexRElbow.y; vertices[i++] = vertexRElbow.z;
		 * 
		 * // l wrist vertices[i++] = vertexLWrist.x; vertices[i++] =
		 * vertexLWrist.y; vertices[i++] = vertexLWrist.z;
		 * 
		 * // r wrist vertices[i++] = vertexRWrist.x; vertices[i++] =
		 * vertexRWrist.y; vertices[i++] = vertexRWrist.z;
		 * 
		 * // l hand vertices[i++] = vertexLHand.x; vertices[i++] =
		 * vertexLHand.y; vertices[i++] = vertexLHand.z;
		 * 
		 * // r hand vertices[i++] = vertexRHand.x; vertices[i++] =
		 * vertexRHand.y; vertices[i++] = vertexRHand.z;
		 */
		return vertices;
	}

	public void bindJointsVertexData() {
		// if (isDrawChange)
		{

			if (isDrawJoints) {
				if (verticesBufferJoints == null) {
					verticesBufferJoints = BufferUtils.createFloatBuffer(jointsVerticesDraw.length);
				}
				verticesBufferJoints.put(jointsVerticesDraw).flip();

				if (jointsVAOID == -1) {
					jointsVAOID = glGenVertexArrays();
				}
				glBindVertexArray(jointsVAOID);

				if (jointsVBOID == -1) {
					jointsVBOID = glGenBuffers();
				}
				glBindBuffer(GL_ARRAY_BUFFER, jointsVBOID);
				glBufferData(GL_ARRAY_BUFFER, verticesBufferJoints, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);

				if (!isJointsIndexBound) {
					jointsIndicesLinesCount = jointsIndicesLines.length;
					ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(jointsIndicesLinesCount);
					indicesBuffer.put(jointsIndicesLines);
					indicesBuffer.flip();

					jointsVBOIID = glGenBuffers();
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, jointsVBOIID);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

					isJointsIndexBound = true;
				}
			}

			// isDrawChange = false;
		}
	}

	public void bindVertexData() {
		if (isDrawJoints) {
			bindJointsVertexData();
		}
	}

	public void calcuateJointsQuaternions(float rX, float rY, float rZ) {
		dest1.w = 1f;
		dest1.x = 0f;
		dest1.y = 0f;
		dest1.z = 0f;

		dest2.w = 1f;
		dest2.x = 0f;
		dest2.y = 0f;
		dest2.z = 0f;

		dest2.rotate((float) Math.toRadians(rX), (float) Math.toRadians(rY), (float) Math.toRadians(rZ), dest1);

		// dest1.normalize();
		// dest1.invert();
		dest1.transform(v);
	}

	public void calculateJointsVertices() {
		// System.out.println("human1.move(); called");

		double radian = Math.toRadians(degreeRotateZ);
		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		float fX, fY, fZ;

		int indexCount = 0;
		float[] vertices = {};// .clone();// act.getVertices();

		// jointsVerticesDraw =
		actionJoints();
		vertices = jointsVerticesDraw;

		for (int i = vertices.length; indexCount < i; indexCount += 3) {

			fX = vertices[indexCount];// * lengthX/* X */;
			fY = vertices[indexCount + 1];// * widthY/* Y */;
			fZ = vertices[indexCount + 2];// * heightZ/* Z */;
			setVertices(indexCount, cosRadian, sinRadian, fX, fY, fZ, vertices);
		}
	}

	public void draw() {

		// if(doCalc)
		{
			calculateJointsVertices();
			// doCalc=false;
		}

		if (isDrawJoints) {
			drawJoints();
		}
		
		if(isDrawSkin)
		{ drawSkin();}

	}

	public void drawJoints() {// System.out.println("human.drawJoints");
		bindJointsVertexData();
		glUseProgram(jointsProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(jointsGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(jointsFB));
		glUniform4f(jointsGLRGBAUniformLocation, jointsRed, jointsGreen, jointsBlue, 1.0f);

		glBindVertexArray(jointsVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, jointsVBOIID);
		glDrawElements(GL_LINES, jointsIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glDrawElements(GL_POINTS, jointsIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);
	}
	
	public void drawSkin() {// System.out.println("human.drawJoints");
		bindJointsVertexData();
		glUseProgram(skinProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(skinGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(skinFB));
		glUniform4f(skinGLRGBAUniformLocation, skinRed, skinGreen, skinBlue, 1.0f);

		glBindVertexArray(skinVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skinVBOIID);
		glDrawElements(GL_TRIANGLES, skinIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);
	}

	public human2_attack getAttack() {
		return a;
	}

	public int getUpperBodyStateWeaponStatus() {
		return human2UpperBodyStateWeaponStatus;
	}

	public void glCleanup() {
		glUseProgram(0);
		if (jointsProgramId != 0) {
			if (jointsVertexShaderId != 0) {
				glDetachShader(jointsProgramId, jointsVertexShaderId);
			}
			if (jointsFragmentShaderId != 0) {
				glDetachShader(jointsProgramId, jointsFragmentShaderId);
			}
			glDeleteProgram(jointsProgramId);
		}
	}

	public void glConstruct() {
		if (isDrawJoints) {
			glConstructJoints();
		}

		if (isDrawSkin) {
			glConstructSkin();
		}
	}

	public void glConstructJoints() {
		try {
			try {
				jointsProgramId = glCreateProgram();

				glCreateVertexShaderJoints(
						"#version 130\nuniform mat4 MVP; in vec3 position; void main() { gl_Position =MVP* vec4(position, 1.0);  gl_PointSize = 3.0; }");

				glCreateFragmentShaderJoints(
						"#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(jointsProgramId);

				glUseProgram(jointsProgramId);
				jointsGLMVPUniformLocation = glGetUniformLocation(jointsProgramId, "MVP");
				jointsGLRGBAUniformLocation = glGetUniformLocation(jointsProgramId, "RGBA");
				glUseProgram(0);

				glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (jointsProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glConstructSkin() {
		try {
			try {
				skinProgramId = glCreateProgram();

				glCreateVertexShaderSkin(
						"#version 130                                   \n"
						+"uniform mat4 MVP;                             \n"
						+"in vec3 position;                             \n"
						+"void main()                                   \n"
						+"{                                             \n"
						+"gl_Position =MVP* vec4(position, 1.0);        \n"
						+"gl_PointSize = 3.0;                           \n"
						+"}                                               ");

				/*	glCreateGeometryShaderSkin(
					"#version 330                                    \n"
				        + "in vec4 gxl3d_Position;                       \n"
						+ "in vec4 gxl3d_TexCoord0;                      \n"
						+ "in vec4 gxl3d_Color;                          \n"
						+ "out vec4 Vertex_UV;                           \n"
						+ "out vec4 Vertex_Color;                        \n" 
						+ "uniform mat4 MVP;      \n"
						+ "                                              \n" 
						+ "struct Transform                              \n"
						+ "{                                             \n"
						+ "  vec4 position;                              \n" 
						+ "  vec4 axis_angle;                            \n" 
						+ "};                                            \n" 
						+ "uniform Transform T;                          \n" 
						+ "                                              \n"
						+ "                                              \n"
						+ "vec4 quat_from_axis_angle(vec3 axis, float angle)\n" 
						+ "{                                             \n" 
						+ "  vec4 qr;                                    \n"
						+ "  float half_angle = (angle * 0.5) * 3.14159 / 180.0;\n" 
						+ "  qr.x = axis.x * sin(half_angle);            \n"
						+ "  qr.y = axis.y * sin(half_angle);            \n" 
						+ "  qr.z = axis.z * sin(half_angle);            \n"
						+ "  qr.w = cos(half_angle);                     \n"
						+ "  return qr;                                  \n" 
						+ "}                                             \n" 
						+ "                                              \n"
						+ "vec4 quat_conj(vec4 q)                        \n" 
						+ "{                                             \n" 
						+ "  return vec4(-q.x, -q.y, -q.z, q.w);         \n" 
						+ "}                                             \n"
						+ "                                              \n"
						+ "vec4 quat_mult(vec4 q1, vec4 q2)              \n" 
						+ "{                                             \n"
						+ "  vec4 qr;" + "  qr.x = (q1.w * q2.x) + (q1.x * q2.w) + (q1.y * q2.z) - (q1.z * q2.y);\n"
						+ "  qr.y = (q1.w * q2.y) - (q1.x * q2.z) + (q1.y * q2.w) + (q1.z * q2.x);\n"
						+ "  qr.z = (q1.w * q2.z) + (q1.x * q2.y) - (q1.y * q2.x) + (q1.z * q2.w);\n"
						+ "  qr.w = (q1.w * q2.w) - (q1.x * q2.x) - (q1.y * q2.y) - (q1.z * q2.z);\n" 
						+ "  return qr;                                  \n"
						+ "}                                             \n"
						+ "                                              \n"
						+ "vec3 rotate_vertex_position(vec3 position, vec3 axis, float angle)\n" 
						+ "{                                             \n"
						+ "  vec4 qr = quat_from_axis_angle(axis, angle);\n"
						+ "  vec4 qr_conj = quat_conj(qr);               \n"
						+ "  vec4 q_pos = vec4(position.x, position.y, position.z, 0);\n" 
						+ "                                              \n"
						+ "  vec4 q_tmp = quat_mult(qr, q_pos);          \n" 
						+ "  qr = quat_mult(q_tmp, qr_conj);             \n" 
						+ "                                              \n"
						+ "  return vec3(qr.x, qr.y, qr.z);              \n" 
						+ "}                                             \n" 
						+ "                                              \n" 
						+ "void main()                                   \n" 
						+ "{                                             \n"
						+ "  vec3 P = rotate_vertex_position(gxl3d_Position.xyz, T.axis_angle.xyz, T.axis_angle.w);\n"
						+ "  P += T.position.xyz;                        \n"
						+ "  gl_Position = MVP * vec4(P, 1);             \n"
						+ "  Vertex_UV = gxl3d_TexCoord0;                \n" 
						+ "  Vertex_Color = gxl3d_Color;                 \n" 
						+ "}                                             "
						);*/

				glCreateFragmentShaderSkin(
						"#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(skinProgramId);

				glUseProgram(skinProgramId);
				skinGLMVPUniformLocation = glGetUniformLocation(skinProgramId, "MVP");
				skinGLRGBAUniformLocation = glGetUniformLocation(skinProgramId, "RGBA");
				glUseProgram(0);

				// glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (jointsProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glCreateFragmentShaderJoints(String shaderCode) throws Exception {
		jointsFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, jointsProgramId);
	}

	public void glCreateFragmentShaderSkin(String shaderCode) throws Exception {
		skinFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, skinProgramId);
	}

	public void glCreateGeometryShaderSkin(String shaderCode) throws Exception {
		skinGeometryShaderId = glCreateThisShader(shaderCode, GL_GEOMETRY_SHADER, skinProgramId);
	}

	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShaderJoints(String shaderCode) throws Exception {
		jointsVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, jointsProgramId);
	}

	public void glCreateVertexShaderSkin(String shaderCode) throws Exception {
		skinVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, skinProgramId);
	}

	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}

	public void quaternionRotate(float dist, float[] joints, int index, Vector3f vv, Vector3f vvOffset) {
		v.x = 0f;
		v.y = 0f;
		v.z = dist;
		calcuateJointsQuaternions(joints[index++], joints[index++], joints[index++]);
		// float jx,jy,jz;
		// jx = joints[index++];
		// jy = joints[index++];
		// jz = joints[index++];
		// calcuateJointsQuaternions(jx,jy,jz);
		// System.out.println("jx:"+jx+"\tjy:"+jy+"\tjz:"+jz);
		if (vvOffset == null) {
			vv.x = v.x;
			vv.y = v.y;
			vv.z = v.z;
			// System.out.println("vv.x:"+vv.x+"\tvv.y:"+vv.y+"\tvv.z:"+vv.z);
		} else {
			vv.x = v.x + vvOffset.x;
			vv.y = v.y + vvOffset.y;
			vv.z = v.z + vvOffset.z;
		}
	}

	public void setAttackUpperBodyState(String stateName)
	{ a.setHuman2BodyStateUpper(stateName);}
	
//	public void setMVP(Matrix4f mvp) {
//		MVP = mvp;
//	}

	public void setRGB(float r, float g, float b) {
		// skinRed = r;
		// skinGreen = g;
		// skinBlue = b;
	}

	public void setRotations(float[] n, float[] o, int tc, int ts) {
		rotationsNew = n;
		rotationsOld = o;
		timeToComplete = tc;
		timeStarted = ts;
	}

	public void setVertices(int indexCounter, float cosRadian, float sinRadian, float fX, float fY, float fZ,
			float[] array) {

		// System.out.println("");

		float dX = fX * cosRadian - fY * sinRadian;
		float dY = fY * cosRadian + fX * sinRadian;
		float dZ = fZ;
		// System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" +
		// dY + "\t\tdZ:" + dZ);

		dX *= heightZ;
		dY *= heightZ;
		dZ *= heightZ;
		// System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" +
		// dY + "\t\tdZ:" + dZ);

		dX += x;
		dY += y;
		dZ += z;
		// System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" +
		// dY + "\t\tdZ:" + dZ);

		array[indexCounter++] = (dX);
		array[indexCounter++] = (dY);
		array[indexCounter] = (dZ);

	}

	public void setXYZR(float x, float y, float z, float r) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.degreeRotateZ = r;
		// move();
	}

}
