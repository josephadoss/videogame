package local.source.actors.humans.human3;

import java.util.HashMap;


public class human3_weapon_sword_simple_rotation_manager {
	private static human3_weapon_sword_simple_rotation_manager instance = null;
	

	HashMap<String,human3_weapon_sword_simple_rotation> hashMapStates = new HashMap<String,human3_weapon_sword_simple_rotation>();

	
	private human3_weapon_sword_simple_rotation_manager()
	{}
	
	public static human3_weapon_sword_simple_rotation_manager getInstance()
	{
		if(instance==null)
		{ instance = new human3_weapon_sword_simple_rotation_manager();}
		return instance;		
	}
	
	public human3_weapon_sword_simple_rotation get( String key)
	{
		human3_weapon_sword_simple_rotation result = hashMapStates.get(key);
		
		if(result==null)
		{
			result =  new human3_weapon_sword_simple_rotation(key);
			this.put(key, result);
		}
	
		return result;
	}
	

	
	public void put(String key, human3_weapon_sword_simple_rotation val)
	{
		if(!hashMapStates.containsKey(key))
		{ hashMapStates.put(key, val);}
	}

}
