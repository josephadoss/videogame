package local.source.actors.humans.human3;
import org.joml.*;

public interface interface_human3_body_part {

	float headHeight=1f/8f;
	String bodyPart ="interface";
	
	public void draw();
	
	public Vector3f[] getBottomVerticesBack();
	
	public Vector3f[] getBottomVerticesFront();
	
	public Vector3f[] getTopVerticesBack();
	
	public Vector3f[] getTopVerticesFront();
	
	
	public Vector3f[] getVerticesBack();
	
	public Vector3f[] getVerticesFront();
	
	
	public void resetTexture();

	public void setAdjustBottomVerticesBack(Vector3f[] adjustBot);
	public void setAdjustBottomVerticesFront(Vector3f[] adjustBot);
	
	public void setAdjustTopVerticesBack(Vector3f[] adjustTop);
	public void setAdjustTopVerticesFront(Vector3f[] adjustTop);
	
	public void setBodyHeight(float height);
	
	public void setBodyPart(String newbp);
	
	public void setCenterXYZ(float x, float y, float z);
	
	public void setFacingRotationZ(float z);
	
	public void setFatMuscleGenderJoints(float fat, float muscle, int gender,human3_joints joints);
	
//	public void setMVP(Matrix4f mvp);

	public void setOutfit(human3_outfit o);
	
	public void setRotationXYZ(float x, float y, float z);
		
	public void setSkinRGB(float r, float g, float b);
	
}
