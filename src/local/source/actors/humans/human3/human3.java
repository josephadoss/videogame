package local.source.actors.humans.human3;

import local.source.actors.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL32.*;
//import static org.lwjgl.opengl.GL40.*;
//import static org.lwjgl.opengl.GL45.*;

import java.nio.*;

import org.lwjgl.*;

import local.source.damage;
import local.source.game_time;
import local.source.interface_engageable;
import local.source.manager_engageables;
import local.source.manager_step_collisions;
import local.source.actors.interface_actor;
import local.source.actors.manager_actors;
import local.source.attacks.manager_attackables;
import local.source.weapons.interface_weapon;
import local.source.worlds.manager_land;

import org.joml.*;

import java.util.*;

import java.lang.*;
import java.lang.Math;

public class human3 implements interface_actor {

	int gender = 0; // 0 male   1 female   and over 4 billion other possibilities
	
	public String actorName = "human3 default";
	public String className = "human3";

	// public boolean isDrawSkinMesh = true;
	// public boolean isDrawSkin = true;

	float skinRed = 1f;
	float skinGreen = 0f;
	float skinBlue = 0f;
	
	float hairRed = 1f;
	float hairGreen = 0f;
	float hairBlue = 0f;

	game_time gt = game_time.getInstance();
	// don't use width and length, make all vertices relative to heightZ

	manager_land lManage = manager_land.getInstance();
	float stepUpHeight = 1f;
	float stepDownHeight = 2f;

	// float headHeight = 1f / 8f;

	int maxStepDistance = -1;

	//public Matrix4f MVP;

	public float x, y, z = 0.0f;

	Vector3f centerXYZ = new Vector3f();
	Vector3f diameterXYZ = new Vector3f();
	Vector3f engageableDiameterXYZ = new Vector3f();

	public float height = 2.0f;
	public float turnDegree = 5.0f;
	public float degreeRotateX = 0.0f;
	public float degreeRotateY = 0.0f;
	public float degreeRotateZ = 0.0f;
	public float moveDistance = .0003f;// * headHeight;
	// float armWidth = 327f;

	human3_health health = new human3_health();
	human3_joints joints = new human3_joints();
	interface_weapon weapon = null;// new human3_weapon_sword_simple();//
	
	float fat = .5f;
	float muscle = .5f;
									// human3_weapon_fist();
	List<interface_human3_body_part> listBodyParts = new ArrayList<interface_human3_body_part>();
	interface_human3_body_part neck = new human3_body_part_neck("neck");
	interface_human3_body_part head = new human3_body_part_head("head");
	interface_human3_body_part chest = new human3_body_part_chest("chest");
	interface_human3_body_part tummy = new human3_body_part_tummy("tummy");
	interface_human3_body_part hips = new human3_body_part_hips("hips");
	interface_human3_body_part arm_upper_left = new human3_body_part_arm_upper("arm_upper","left");
	interface_human3_body_part arm_upper_right = new human3_body_part_arm_upper("arm_upper","right");
	interface_human3_body_part arm_lower_left = new human3_body_part_arm_lower("arm_lower","left");
	interface_human3_body_part arm_lower_right = new human3_body_part_arm_lower("arm_lower","right");
	interface_human3_body_part hand_left = new human3_body_part_hand("hand","left");
	interface_human3_body_part hand_right = new human3_body_part_hand("hand","right");
	interface_human3_body_part leg_upper_left = new human3_body_part_leg_upper("leg_upper","left");
	interface_human3_body_part leg_upper_right = new human3_body_part_leg_upper("leg_upper","right");
	interface_human3_body_part leg_lower_left = new human3_body_part_leg_lower("leg_lower","left");
	interface_human3_body_part leg_lower_right = new human3_body_part_leg_lower("leg_lower","right");
	interface_human3_body_part foot_left = new human3_body_part_foot("foot","left");
	interface_human3_body_part foot_right = new human3_body_part_foot("foot","right");
	
	public human3_outfit outfit = new human3_outfit();
	
	public human3() {
		listBodyParts.add(neck);
		listBodyParts.add(head);
		listBodyParts.add(chest);
		listBodyParts.add(tummy);
		listBodyParts.add(hips);
		listBodyParts.add(arm_upper_left);
		listBodyParts.add(arm_upper_right);
		listBodyParts.add(arm_lower_left);
		listBodyParts.add(arm_lower_right);
		listBodyParts.add(hand_left);
		listBodyParts.add(hand_right);
		listBodyParts.add(leg_upper_left);
		listBodyParts.add(leg_upper_right);
		listBodyParts.add(leg_lower_left);
		listBodyParts.add(leg_lower_right);
		listBodyParts.add(foot_left);
		listBodyParts.add(foot_right);
		
		outfit.setGender(gender);
		outfit.setArrayDetail(10);
		
		
		setHeight(6f);
		registerWithActorManager();
		registerWithAttackableManager();
		registerWithEngageableManager();
		registerWithStepCollisionManager();
		joints.setDiameterXYZ(this.diameterXYZ);
		this.centerXYZ.x = x;
		this.centerXYZ.y = y;
		this.centerXYZ.z = height / 2f;

		setDefaultWeapon();
		setSkinRGB(1f, 0.894f, 0.71f);
		//setSkinRGB(1f, 0.0f, 0.0f);

		

		for(interface_human3_body_part i : listBodyParts)
		{ 
			i.setFatMuscleGenderJoints(fat,muscle,gender,joints);
			i.setOutfit(outfit);
		}
		
	}

	public void draw() {
		joints.setXYZ(x, y, z);
		joints.setFacingDegree(degreeRotateZ);
		joints.setRotationX(degreeRotateX);
		joints.setRotationY(degreeRotateY);

		joints.draw();

		weapon.setHandleXYZ(joints.getVertexOfLeftHand());
		Vector3f vX = joints.getRotationOfLeftHand();
		vX.z += degreeRotateZ;
		weapon.setRotationXYZ(vX); // System.out.println("rotation of left hand
									// : "+vX.x + " " + vX.y + " " + vX.z);
		weapon.draw();

		// Neck
		vX = joints.getRotationOfNeckLower();
		neck.setRotationXYZ(vX.x, vX.y, vX.z);
		vX = joints.getVertexOfNeckLower();
		Vector3f vY = joints.getVertexOfNeckUpper();
		Vector3f center = new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		neck.setCenterXYZ(center.x, center.y, center.z);

		//Head
		vX = joints.getRotationOfNeckUpper();
		head.setRotationXYZ(vX.x, vX.y, vX.z);
		vX = joints.getVertexOfNeckUpper();
		vY = joints.getVertexOfHeadTop();
		center = new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		head.setCenterXYZ(center.x, center.y, center.z);
		
		//Chest
		vX = joints.getRotationOfNipLine();
		chest.setRotationXYZ(vX.x, vX.y, vX.z);
		//vX = joints.getVertexOfNeckUpper();
		//vY = joints.getVertexOfHeadTop();
		center = joints.getVertexOfNipLine();// new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		chest.setCenterXYZ(center.x, center.y, center.z);

		//Tummy
		vX = joints.getRotationFromInput(joints.indexNavel);
		tummy.setRotationXYZ(vX.x, vX.y, vX.z);
		vX = joints.getVertexFromInput(joints.indexNavel);
		//vY = joints.getVertexOfHeadTop();
		center = vX;// new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		tummy.setCenterXYZ(center.x, center.y, center.z);
		
		//Hips
		vX = joints.getRotationFromInput(joints.indexCrotch);
		hips.setRotationXYZ(vX.x, vX.y, vX.z);
		vX = joints.getVertexFromInput(joints.indexCrotch);
		//vY = joints.getVertexOfHeadTop();
		center = vX;// new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		hips.setCenterXYZ(center.x, center.y, center.z);
		
		//Arm Upper Left
		vX = joints.getRotationFromInput(joints.indexLElbow);
		arm_upper_left.setRotationXYZ(vX.x, vX.y, vX.z);
		vX = joints.getVertexFromInput(joints.indexLShoulder);
		vY = joints.getVertexFromInput(joints.indexLElbow);
		center = new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		arm_upper_left.setCenterXYZ(center.x, center.y, center.z);
		
		//Arm Upper Right
		vX = joints.getRotationFromInput(joints.indexRElbow);
		arm_upper_right.setRotationXYZ(vX.x, vX.y, vX.z);
		vX = joints.getVertexFromInput(joints.indexRShoulder);
		vY = joints.getVertexFromInput(joints.indexRElbow);
		center = new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		arm_upper_right.setCenterXYZ(center.x, center.y, center.z);
		
		
		//Arm Lower Left
		vX = joints.getRotationFromInput(joints.indexLWrist);
		arm_lower_left.setRotationXYZ(vX.x, vX.y, vX.z);
		vX = joints.getVertexFromInput(joints.indexLElbow);
		vY = joints.getVertexFromInput(joints.indexLWrist);
		center = new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		arm_lower_left.setCenterXYZ(center.x, center.y, center.z);
		
		//Arm Lower Right
		vX = joints.getRotationFromInput(joints.indexRWrist);
		arm_lower_right.setRotationXYZ(vX.x, vX.y, vX.z);
		vX = joints.getVertexFromInput(joints.indexRElbow);
		vY = joints.getVertexFromInput(joints.indexRWrist);
		center = new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		arm_lower_right.setCenterXYZ(center.x, center.y, center.z);
		
		
		//Hand Left
		vX = joints.getRotationFromInput(joints.indexLHand);
		hand_left.setRotationXYZ(vX.x, vX.y, vX.z);
		vX = joints.getVertexFromInput(joints.indexLWrist);
		vY = joints.getVertexFromInput(joints.indexLHand);
		center = new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		hand_left.setCenterXYZ(center.x, center.y, center.z);
		
		//hand Right
		vX = joints.getRotationFromInput(joints.indexRHand);
		hand_right.setRotationXYZ(vX.x, vX.y, vX.z);
		vX = joints.getVertexFromInput(joints.indexRWrist);
		vY = joints.getVertexFromInput(joints.indexRHand);
		center = new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		hand_right.setCenterXYZ(center.x, center.y, center.z);
		
		
		
		
		
		
		
		
		
		//Leg Upper Left
		vX = joints.getRotationFromInput(joints.indexLKnee);
		leg_upper_left.setRotationXYZ(vX.x, vX.y, vX.z);
		vX = joints.getVertexFromInput(joints.indexLHip);
		vY = joints.getVertexFromInput(joints.indexLKnee);
		center = new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		leg_upper_left.setCenterXYZ(center.x, center.y, center.z);
		
		//Leg Upper Right
		vX = joints.getRotationFromInput(joints.indexRKnee);
		leg_upper_right.setRotationXYZ(vX.x, vX.y, vX.z);
		vX = joints.getVertexFromInput(joints.indexRHip);
		vY = joints.getVertexFromInput(joints.indexRKnee);
		center = new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		leg_upper_right.setCenterXYZ(center.x, center.y, center.z);
		
		
		//Leg Lower Left
		vX = joints.getRotationFromInput(joints.indexLAnkle);
		leg_lower_left.setRotationXYZ(vX.x, vX.y, vX.z);
		vX = joints.getVertexFromInput(joints.indexLKnee);
		vY = joints.getVertexFromInput(joints.indexLAnkle);
		center = new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		leg_lower_left.setCenterXYZ(center.x, center.y, center.z);
		
		//Leg Lower Right
		vX = joints.getRotationFromInput(joints.indexRAnkle);
		leg_lower_right.setRotationXYZ(vX.x, vX.y, vX.z);
		vX = joints.getVertexFromInput(joints.indexRKnee);
		vY = joints.getVertexFromInput(joints.indexRAnkle);
		center = new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		leg_lower_right.setCenterXYZ(center.x, center.y, center.z);
		
		
		//Foot Left
		vX = joints.getRotationFromInput(joints.indexLFoot);
		foot_left.setRotationXYZ(vX.x, vX.y, vX.z);
		vX = joints.getVertexFromInput(joints.indexLAnkle);
		vY = joints.getVertexFromInput(joints.indexLFoot);
		center = new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		foot_left.setCenterXYZ(center.x, center.y, center.z);
		
		//Foot right
		vX = joints.getRotationFromInput(joints.indexRFoot);
		foot_right.setRotationXYZ(vX.x, vX.y, vX.z);
		vX = joints.getVertexFromInput(joints.indexRAnkle);
		vY = joints.getVertexFromInput(joints.indexRFoot);
		center = new Vector3f((vX.x + vY.x) / 2f, (vX.y + vY.y) / 2f, (vX.z + vY.z) / 2f);
		foot_right.setCenterXYZ(center.x, center.y, center.z);
		
		
		for (interface_human3_body_part i : listBodyParts) {
			i.setFacingRotationZ(degreeRotateZ);
			//i.draw();
		}
		
		
		
		
		
		head.draw();
		
		neck.draw();
		
		// shirt
		chest.draw();
		tummy.draw();
		arm_upper_left.draw();
		arm_upper_right.draw();
		arm_lower_left.draw();
		arm_lower_right.draw();
		
		// pants
		hips.draw();
		leg_upper_right.draw();
		leg_upper_left.draw();
		leg_lower_right.draw();
		leg_lower_left.draw();
		
		// shoes
		foot_right.draw();
		foot_left.draw();
		
		// gloves
		hand_left.draw();
		hand_right.draw();
	}

	public void faceLeft() {
		degreeRotateZ = (degreeRotateZ + turnDegree) % 360.0f;
	}

	public void facePoint(float newX, float newY, float newZ) {
		
		if(!joints.isDead())
		{
			float dX = newX - x;
			float dY = newY - y;
			degreeRotateZ = (float) (Math.atan2(dY, dX) * 180.0f / Math.PI);
		}
	}

	public void faceRight() {
		float change = (degreeRotateZ - turnDegree);
		degreeRotateZ = (float) (change <= 0.0d ? (360.0d - turnDegree) : change % 360.0d);
		// move();
	}

	public void engaged() {
		System.out.println("human3.engaged()");
	}

	public String getActorName() {
		return this.actorName;
	}

	public Vector3f getCenterXYZ() {
		// System.out.println("human3.getCenterXYZ x : " + centerXYZ.x + " y :
		// "+ centerXYZ.y+" z : " + centerXYZ.z);
		return centerXYZ;
	}

	public String getClassName() {
		return this.className;
	}

	public Vector3f getDiameterXYZ() {
		return diameterXYZ;
	}

	public Vector3f getEngageableDiameterXYZ() {
		return engageableDiameterXYZ;
	}

	public float getFacingDegrees() {
		return degreeRotateZ;
	}

	private float getFacingX() {
		return (float) (moveDistance * Math.cos(Math.toRadians(degreeRotateZ)));
	}

	public Vector3f getFacingXYZAsIfCenterXYZ() {
		Vector3f v = new Vector3f();
		v.x = x + getFacingX();
		v.y = y + getFacingY();
		v.z = z;
		return v;
	}

	private float getFacingY() {
		return (float) (moveDistance * Math.sin(Math.toRadians(degreeRotateZ)));
	}

	public human3_health getHealth() {
		return health;
	}

	public float getHeight() {
		return height;
	}

	public float getHPPercentageAsFloatZeroToOne() {
		return health.getHPPercentageAsFloatZeroToOne();
	}

	
	private float getLeftX() {
		return (float) (moveDistance * Math.cos(Math.toRadians(degreeRotateZ + 90f)));
	}

	private float getLeftY() {
		return (float) (moveDistance * Math.sin(Math.toRadians(degreeRotateZ + 90f)));
	}

	private float getRightX() {
		return (float) (moveDistance * Math.cos(Math.toRadians(degreeRotateZ - 90f)));
	}

	private float getRightY() {
		return (float) (moveDistance * Math.sin(Math.toRadians(degreeRotateZ - 90f)));
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getZ() {
		return z;
	}

	public Vector3f getXYZ() {
		Vector3f v = new Vector3f();
		v.x = this.x;
		v.y = this.y;
		v.z = this.z;
		return v;
	}

	public void move(String position) {}
	
	public void moveAttack() {// System.out.println("human3.moveAttack
								// entered");

		if (!joints.isAttacking()) {
			if (weapon == null) {
				setDefaultWeapon();
				System.out.println("human3.moveAttack() just found null weapon, so setting to default");
			} // System.out.println(" weapon.getWeaponName() :
				// "+weapon.getWeaponName());

			// System.out.println("weapon.getAttack().getAttackName() :
			// "+weapon.getAttack().getAttackName());
			// the attack needs to store the movements, not the
			// human/joints/weapon
			joints.moveAttack(weapon.getAttack()); // human2.moveAttack()");
			weapon.startAttackAction();
			weapon.setIsAttacking(true);
			// weapon.attack();
			resetCenterXY();
		}

	}

	public void moveBackward() {
		resetCenterXY();
		weapon.setIsAttacking(false);

	}

	public void moveDying() {

		//if (!joints.isInterruptable()) {
		//	return;
		//}

		joints.moveDying();
		weapon.setIsAttacking(false);

	}
	
	public void moveEngage() {

		if (!joints.isInterruptable()) {
			return;
		}

		List<interface_engageable> list = manager_engageables.getInstance().getCollision(getCenterXYZ(), getEngageableDiameterXYZ(), this);
		for (interface_engageable ie : list) {
			ie.engaged();
		}

		joints.moveEngage();
		weapon.setIsAttacking(false);

	}

	public void moveForward() {

		if (!joints.isInterruptable()) {
			return;
		}

		float facingX = x + getFacingX();
		float facingY = y + getFacingY();
		float facingZ = z;
		if (lManage != null) {
			facingZ = lManage.getZ(facingX, facingY);
			if (facingZ - z > stepUpHeight || z - facingZ > stepDownHeight)// if(Math.abs(z-facingZ)>stepHeight)
			{
				return;
			}
		}

		if (manager_step_collisions.getInstance().getCollision(getFacingXYZAsIfCenterXYZ(), getDiameterXYZ(), this) != null) {
			return;
		}

		x = facingX;
		y = facingY;
		z = facingZ;

		if (x < 0f) {
			x = 0f;
		}

		if (y < 0f) {
			y = 0f;
		}

		if (y > maxStepDistance) {
			y = maxStepDistance;
		}

		if (x > maxStepDistance) {
			x = maxStepDistance;
		}

		joints.moveForward();
		resetCenterXY();
		weapon.setIsAttacking(false);

	}

	/*
	 * public void moveDown() { y -= moveDistance; // move(); }
	 */

	public void moveLeft() {
		if (!joints.isInterruptable()) {
			return;
		}

		float facingX = x + getLeftX();
		float facingY = y + getLeftY();
		float facingZ = z;
		if (lManage != null) {
			facingZ = lManage.getZ(facingX, facingY);
			if (facingZ - z > stepUpHeight || z - facingZ > stepDownHeight)// if(Math.abs(z-facingZ)>stepHeight)
			{
				return;
			}
		}

		x = facingX;
		y = facingY;
		z = facingZ;

		if (x < 0f) {
			x = 0f;
		}

		if (y < 0f) {
			y = 0f;
		}

		if (y > maxStepDistance) {
			y = maxStepDistance;
		}

		if (x > maxStepDistance) {
			x = maxStepDistance;
		}
		// printStateInfo();
		resetCenterXY();
		weapon.setIsAttacking(false);

	}

	public void moveLieDown() {
		if (!joints.isInterruptable()) {
			return;
		}

		weapon.setIsAttacking(false);

	}

	public void moveRight() {
		if (!joints.isInterruptable()) {
			return;
		}

		float facingX = x + getRightX();
		float facingY = y + getRightY();
		float facingZ = z;
		if (lManage != null) {
			facingZ = lManage.getZ(facingX, facingY);
			if (facingZ - z > stepUpHeight || z - facingZ > stepDownHeight)// if(Math.abs(z-facingZ)>stepHeight)
			{
				return;
			}
		}

		x = facingX;
		y = facingY;
		z = facingZ;

		if (x < 0f) {
			x = 0f;
		}

		if (y < 0f) {
			y = 0f;
		}

		if (y > maxStepDistance) {
			y = maxStepDistance;
		}

		if (x > maxStepDistance) {
			x = maxStepDistance;
		}
		// printStateInfo();
		resetCenterXY();
		weapon.setIsAttacking(false);

	}

	public void moveSquat() {
		if (!joints.isInterruptable()) {
			return;
		}

		joints.moveSquat();
		weapon.setIsAttacking(false);

	}

	public void moveStand() {
		if (!joints.isInterruptable()) {
			return;
		}

		joints.moveStand();
		weapon.setIsAttacking(false);

	}

	/*
	 * public void moveUp() { y += moveDistance; // move(); }
	 */
	public void registerWithActorManager() {
		manager_actors.getInstance().add(this);
	}

	public void registerWithAttackableManager() {
		manager_attackables.getInstance().add(this);
	}

	public void registerWithEngageableManager() {
		manager_engageables.getInstance().add(this);
	}

	public void registerWithStepCollisionManager() {
		manager_step_collisions.getInstance().add(this);
	}

	public void resetCenterXY() {
		this.centerXYZ.x = x;
		this.centerXYZ.y = y;
		// System.out.println("resetCenterXY x : " + x + " y : "+ y);
	}

	
	public void resetTexture()
	{
		for(interface_human3_body_part i : listBodyParts)
		{ 
			i.resetTexture();
		}	
	}

	public void rotateXPos() {
		degreeRotateX = (degreeRotateX + turnDegree) % 360.0f;
	}

	public void rotateXNeg() {
		degreeRotateX = (degreeRotateX - turnDegree) % 360.0f;
	}

	public void rotateYPos() {
		degreeRotateY = (degreeRotateY + turnDegree) % 360.0f;
	}

	public void rotateYNeg() {
		degreeRotateY = (degreeRotateY - turnDegree) % 360.0f;
	}

	
	public void setActorName(String n) {
		this.actorName = n;
	}


	
	public void setDefaultWeapon() {
		if (weapon == null) {
			weapon = new human3_weapon_sword_simple();// human3_weapon_fist();
			weapon.setHolder(this);
		}
	}

	public void setFacing(float x, float y, float z)
	{
		this.facePoint(x,y,z);
		
	}
	
	public void setHairRGB(float r, float g, float b) {
		hairRed = r;
		hairGreen = g;
		hairBlue = b;

		((human3_body_part_head)head).setHairRGB(hairRed,hairGreen,hairBlue);
	}
	
	public void setHeight(float x) {
		height = x;
		moveDistance = height * .1f;

		joints.setHeight(x);

		this.diameterXYZ.x = height / 7f;
		this.diameterXYZ.y = height / 7f;
		this.diameterXYZ.z = height / 2f;

		this.engageableDiameterXYZ.x = height / 2f;
		this.engageableDiameterXYZ.y = height / 2f;
		this.engageableDiameterXYZ.z = height / 1.5f;

		for (interface_human3_body_part i : listBodyParts) {
			i.setBodyHeight(x);
		}
		
	}

	
	public void setHP(int h)
	{ health.setHP(h);}
	
	public void setLandManager(manager_land l) {
		lManage = l;
	}

	public void setMaxStepDistance(int newDist) {
		maxStepDistance = newDist;
	}

//	public void setMVP(Matrix4f mvp) {// System.out.println("human3.setMVP mvp :
//										// "+mvp.toString());
//		MVP = mvp;
//		joints.setMVP(mvp);
//		weapon.setMVP(mvp);
//
//		for (interface_human3_body_part i : listBodyParts) {
//			i.setMVP(mvp);
//		}
//	}

	public void setOutfit(human3_outfit o)
	{
		this.outfit = o;
		for(interface_human3_body_part i : listBodyParts)
		{ i.setOutfit(o);}		
	}
	
	public void setSkinRGB(float r, float g, float b) {
		skinRed = r;
		skinGreen = g;
		skinBlue = b;

		for (interface_human3_body_part i : listBodyParts) {
			i.setSkinRGB(r, g, b);
		}
	}

	public void setXYZ(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;

		resetCenterXY();
	}

	public void setXY(float newX, float newY) {
		x = newX;
		y = newY;

		resetCenterXY();
	}

	public void setZ(float newZ) {
		z = newZ;
	}

	public void takeDamage(damage d) {
		health.subtractHP(d.getPhysicalDamage());
		System.out.println("human3 just took damage. HP remaining : " + health.getHP());
		
		if(health.getHP()==0)
		{ this.moveDying();}
	}

	public void testResetVertices()
	{
		for(interface_human3_body_part i : listBodyParts)
		{ 
			i.setFatMuscleGenderJoints(fat,muscle,gender,joints);
		}
	}

	
	
	
	@Override
	public Vector3f getStandingXYZ() {
		// TODO Auto-generated method stub
		return this.centerXYZ;
	}
}