package local.source.actors.humans.human3;

import java.util.*;

import org.joml.*;

import local.source.attacks.interface_attack;
import local.source.attacks.interface_attackable;
import local.source.weapons.interface_weapon;
import local.source.weapons.manager_weapons;

public class human3_weapon_fist implements interface_weapon {

	//public Matrix4f MVP;// = new Matrix4f();

	public List<interface_attack> attacks = new ArrayList<interface_attack>();
	public interface_attack attack = null;

	public int weaponLevel = -1;
	public int weaponMaxLevel = -1;
	public int weaponExperience = -1;
	public int weaponMaxExperience = -1;

	public String weaponName = "fist";
	public String className = "human3_weapon_fist";
	
	Vector3f handleXYZ = new Vector3f();
	Vector3f centerXYZ = new Vector3f();
	Vector3f rotationXYZ = new Vector3f();

	
	interface_attackable myHolder = null;

	public void human3_weapon_fist() {
		registerWithManager();
		setDefaultAttack();
	}

	public void addExperience(int e) {
		for (int i = 0; i < e; i++) {
			if (weaponExperience < weaponMaxExperience) {
				weaponExperience++;
			}
		}
		checkLevelUp();
	}
	
	public void checkLevelUp() {
	}

	public void draw() {
		
		if(attack!=null)
		{attack.draw();}
	}

	public interface_attack getAttack() { // System.out.println("human3_weapon_fist.getAttack()
											// entered");
		if (attack == null) {// System.out.println("human3_weapon_fist attack is
								// null so setting to default");
			setDefaultAttack();
			// System.out.println("default is "+attack.getAttackName());
		}
		return attack;
	}

	public String getClassName() {
		return className;
	}

	public int getLevel() {
		return weaponLevel;
	}

	public int getWeaponExperience() {
		return weaponExperience;
	}

	public String getWeaponName() {
		return weaponName;
	}

	public void registerWithManager() {
		manager_weapons.getInstance().add(this);
	}

	public void setCenterXYZ(Vector3f n)
	{ centerXYZ.x=n.x;
	centerXYZ.y=n.y;
	centerXYZ.z=n.z;}
	
	public void setCenterXYZ(float x, float y, float z)
	{centerXYZ.x=x;centerXYZ.y=y;centerXYZ.z=z;}
	
	public void setDefaultAttack() { // System.out.println("human3_weapon_fist.setDefaultAttack
										// entered");
		interface_attack a = new human3_attack_punch();
		a.setHolder(myHolder);
		// System.out.println("just created new attack, a.getAttackName() :
		// "+a.getAttackName());
		if (!attacks.contains(a)) {
			attacks.add(a);
		}
		attack = a;
	}
	
	public void setHandleXYZ(float x, float y, float z) {
		handleXYZ.x = x;
		handleXYZ.y = y;
		handleXYZ.z = z;
	}

	public void setHandleXYZ(Vector3f nxyz) {
		handleXYZ.x=nxyz.x;
		handleXYZ.y=nxyz.y;
		handleXYZ.z=nxyz.z;
		if(attack!=null)
		{attack.setHandleXYZ(nxyz);}
	}

	public void setHolder(interface_attackable a)
	{ myHolder = a; }
	
	public void setIsAttacking(boolean b)
	{
		if(attack!=null)
		{attack.setIsAttacking(b);}
	}
	
//	public void setMVP(Matrix4f mvp) {
//		this.MVP = mvp;
//
//		if (attack != null) {
//			attack.setMVP(mvp);
//		}
//	}

	public void setRotationXYZ(float x, float y, float z) {
		rotationXYZ.x = x;
		rotationXYZ.y = y;
		rotationXYZ.z = z;
	}

	public void setRotationXYZ(Vector3f nxyz) {
		rotationXYZ.x = nxyz.x;
		rotationXYZ.y = nxyz.y;
		rotationXYZ.z = nxyz.z;
		//if(attack!=null)
		//{attack.setHandleXYZ(nxyz);}
	}
	
	public void setWeaponName(String n) {
		weaponName = n;
	}
	
	public void startAttackAction(){}

}
