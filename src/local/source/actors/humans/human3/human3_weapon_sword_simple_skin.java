package local.source.actors.humans.human3;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL32.*;
//import static org.lwjgl.opengl.GL40.*;
//import static org.lwjgl.opengl.GL45.*;

import java.nio.*;

import org.lwjgl.BufferUtils;

import local.source.manager_mvp;

import java.lang.Math;

import java.util.*;

import org.joml.*;


public class human3_weapon_sword_simple_skin {


Vector3f bladeXYZ = new Vector3f(0f,0f,0f);
Vector3f handleXYZ = new Vector3f(0f,0f,0f);
Vector3f handleTopXYZ = new Vector3f(0f,0f,0f);
	
	

float bladeSideWidth = 0.04f;
float bladeFrontBackWidth= 0.002f;
float bladeLength=1f;
float bladeTopSideLength = 0.8f;

Vector3f[] verticesModelBlade = { new Vector3f(0f, 0f, bladeLength), // 0 tip
		new Vector3f(0f, -bladeSideWidth, bladeTopSideLength), // 1 right side top
		new Vector3f(bladeFrontBackWidth,0f, bladeTopSideLength), // 2 front top
		new Vector3f(0f, bladeSideWidth, bladeTopSideLength), // 3 left side top
		new Vector3f( -bladeFrontBackWidth, 0f, bladeTopSideLength), // 4 back top
		new Vector3f(0f, -bladeSideWidth, 0f), // 5 right side bot
		new Vector3f(bladeFrontBackWidth,0f, 0f), // 6 front bot
		new Vector3f(0f, bladeSideWidth, 0f), // 7 left side bot
		new Vector3f( -bladeFrontBackWidth,0f, 0f) // 8 back bot
};

	Vector3f[] verticesBlade = {new Vector3f(0f, 0f, 0f), // 0 tip
			new Vector3f(0f, 0f, -0.083f), // 1 right side top
			new Vector3f(0f, 0f, 0.083f), // 2 front top
			new Vector3f(0f, 0.083f, 0.083f), // 3 left side top
			new Vector3f(0f, -0.083f, 0.083f), // 4 back top
			new Vector3f(0f, 0f, 0.83f), // 5 right side bot
			new Vector3f(0f, 0f, -0.083f), // 6 front bot
			new Vector3f(0f, 0f, 0.083f), // 7 left side bot
			new Vector3f(0f, 0.083f, 0.083f) // 8 back bot
	};
	

	byte[] indicesBlade = { 0,2,1//
			,0,3,2//
			,0,4,3//
			,0,1,4//
			
			,1,2,5//
			,2,6,5//
			,2,3,6//
			,3,7,6//
			,3,8,7//
			,3,4,8//
			,4,5,8//
			,4,1,5//
	};
	
	float[] verticesDrawBlade = { 0f, 0f, 0f //0 
			, 0f, 0f, .5f // 1
			, 0f, 0f, .5f // 2
			, 0f, 0f, .5f // 3
			, 0f, 0f, .5f // 4
			, 0f, 0f, .5f // 5
			, 0f, 0f, .5f // 6
			, 0f, 0f, .5f // 7
			, 0f, 0f, .5f //8
			};
	
	
	
	float smallNum = 0.01f;
	float handleDX = smallNum;
	float handleDY = smallNum;
	float handleDZ = 0.083f;
	
	
	Vector3f[] verticesModelHandle = { 
			new Vector3f(handleDX, -handleDY, handleDZ), // 0 top front right 
			new Vector3f(handleDX, handleDY, handleDZ), // 1 top front left
			new Vector3f(-handleDX, handleDY, handleDZ), // 2 top back left
			new Vector3f(-handleDX, -handleDY, handleDZ), // 3 top back right
			new Vector3f(handleDX, -handleDY, -handleDZ), // 4 bot front right
			new Vector3f(handleDX, handleDY, -handleDZ), // 5 bot front left
			new Vector3f(-handleDX, handleDY, -handleDZ), // 6 bot back left
			new Vector3f(-handleDX, -handleDY, -handleDZ) // 7 bot back right
	};

		Vector3f[] verticesHandle = { 
				new Vector3f(0f, 0f, 0f), // 0 top front right 
				new Vector3f(0f, 0f, 0f), // 1 top front left
				new Vector3f(0f, 0f, 0f), // 2 top back left
				new Vector3f(0f, 0f, 0f), // 3 top back right
				new Vector3f(0f, 0f, 0f), // 4 bot front right
				new Vector3f(0f, 0f, 0f), // 5 bot front left
				new Vector3f(0f, 0f, 0f), // 6 bot back left
				new Vector3f(0f, 0f, 0f) // 7 bot back right
		};
	
	byte[] indicesHandle = { 
			0,1,4//
			,1,5,4//
			,3,0,7//
			,0,4,7//
			,2,7,6//
			,2,3,7//
			,1,2,5//
			,2,6,5//
			,3,2,0//
			,2,1,0//
			,4,5,6//
			,4,6,7//
	};
	

	
	float[] verticesDrawHandle = { 
			0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
	};



	
	
	float handleTopDX = smallNum;
	float handleTopDY = 2f*0.083f;
	float handleTopDZ = smallNum;
	
	Vector3f[] verticesModelHandleTop = { 
			//new Vector3f(handleTopDX, -handleTopDY, handleTopDZ), // 0 top front right 
			//new Vector3f(handleTopDX, handleTopDY, handleTopDZ), // 1 top front left
			//new Vector3f(-handleTopDX, handleTopDY, handleTopDZ), // 2 top back left
			//new Vector3f(-handleTopDX, -handleTopDY, handleTopDZ), // 3 top back right
			new Vector3f(handleTopDX, -handleTopDY, 0f), // 0 top front right 
			new Vector3f(handleTopDX, handleTopDY, 0f), // 1 top front left
			new Vector3f(-handleTopDX, handleTopDY, 0f), // 2 top back left
			new Vector3f(-handleTopDX, -handleTopDY, 0f), // 3 top back right
			
			new Vector3f(handleTopDX, -handleTopDY, -handleTopDZ), // 4 bot front right
			new Vector3f(handleTopDX, handleTopDY, -handleTopDZ), // 5 bot front left
			new Vector3f(-handleTopDX, handleTopDY, -handleTopDZ), // 6 bot back left
			new Vector3f(-handleTopDX, -handleTopDY, -handleTopDZ) // 7 bot back right
	};

		Vector3f[] verticesHandleTop = { 
				new Vector3f(0f, 0f, 0f), // 0 top front right 
				new Vector3f(0f, 0f, 0f), // 1 top front left
				new Vector3f(0f, 0f, 0f), // 2 top back left
				new Vector3f(0f, 0f, 0f), // 3 top back right
				new Vector3f(0f, 0f, 0f), // 4 bot front right
				new Vector3f(0f, 0f, 0f), // 5 bot front left
				new Vector3f(0f, 0f, 0f), // 6 bot back left
				new Vector3f(0f, 0f, 0f) // 7 bot back right
		};
	
	byte[] indicesHandleTop = { 
			0,1,4//
			,1,5,4//
			,3,0,7//
			,0,4,7//
			,2,7,6//
			,2,3,7//
			,1,2,5//
			,2,6,5//
			,3,2,0//
			,2,1,0//
			,4,5,6//
			,4,6,7//
	};
	

	
	float[] verticesDrawHandleTop = { 
			0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
	};



	
	
	
	
	
	

	int bladeIndicesLinesCount;
	int handleIndicesLinesCount;
	int handleTopIndicesLinesCount;

	float bladeRed = 0.863f;
	float bladeBlue = 0.863f;
	float bladeGreen = 0.863f;
	
	float handleRed = 0.824f;
	float handleBlue = 0.412f;
	float handleGreen = 0.118f;
	
	float handleTopRed = handleRed;
	float handleTopBlue = handleBlue;
	float handleTopGreen = handleGreen;



	
	
	int bladeVAOID = -1;
	int bladeVBOID = -1;
	int bladeVBOIID = -1;
	
	int handleVAOID = -1;
	int handleVBOID = -1;
	int handleVBOIID = -1;
	
	int handleTopVAOID = -1;
	int handleTopVBOID = -1;
	int handleTopVBOIID = -1;
	
	
	
	

	public int bladeGLMVPUniformLocation;
	public int bladeGLRGBAUniformLocation;
	public FloatBuffer bladeFB = BufferUtils.createFloatBuffer(16);
	public int bladeProgramId;
	public int bladeVertexShaderId;
	public int bladeFragmentShaderId;
	boolean isBladeIndexBound = false;
	public boolean isDrawBlade = true;
	FloatBuffer verticesBufferBlade;
	
	public int handleGLMVPUniformLocation;
	public int handleGLRGBAUniformLocation;
	public FloatBuffer handleFB = BufferUtils.createFloatBuffer(16);
	public int handleProgramId;
	public int handleVertexShaderId;
	public int handleFragmentShaderId;
	boolean isHandleIndexBound = false;
	public boolean isDrawHandle = true;
	FloatBuffer verticesBufferHandle;
	
	
	public int handleTopGLMVPUniformLocation;
	public int handleTopGLRGBAUniformLocation;
	public FloatBuffer handleTopFB = BufferUtils.createFloatBuffer(16);
	public int handleTopProgramId;
	public int handleTopVertexShaderId;
	public int handleTopFragmentShaderId;
	boolean isHandleTopIndexBound = false;
	public boolean isDrawHandleTop = true;
	FloatBuffer verticesBufferHandleTop;
	
	
	

	//public Matrix4f MVP;

	

	public Vector3f xyz = new Vector3f();
	public float height = -1f;

	public Vector3f rotationXYZ = new Vector3f();

	float rotationZ = 0f;
	

	Quaternionf dest1 = new Quaternionf();
	Quaternionf dest2 = new Quaternionf();
	Vector3f v = new Vector3f();



	

	public human3_weapon_sword_simple_skin() {
		// setHeight(6f);
//System.out.println("human3_weapon_sword_simple_joints.constructor entered");
		if (isDrawBlade || isDrawHandle || isDrawHandleTop) {
			this.glConstruct();
		}
		
	//	System.out.println("human3_weapon_sword_simple_joints.constructor leaving");
	}

	public void bindBladeVertexData() {

		if (isDrawBlade) {
			if (verticesBufferBlade == null) {
				verticesBufferBlade = BufferUtils.createFloatBuffer(verticesDrawBlade.length);
			}
			verticesBufferBlade.put(verticesDrawBlade).flip();

			if (bladeVAOID == -1) {
				bladeVAOID = glGenVertexArrays();
			}
			glBindVertexArray(bladeVAOID);

			if (bladeVBOID == -1) {
				bladeVBOID = glGenBuffers();
			}
			glBindBuffer(GL_ARRAY_BUFFER, bladeVBOID);
			glBufferData(GL_ARRAY_BUFFER, verticesBufferBlade, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);

			if (!isBladeIndexBound) {
				bladeIndicesLinesCount = indicesBlade.length;

				ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(bladeIndicesLinesCount);
				indicesBuffer.put(indicesBlade);
				indicesBuffer.flip();

				bladeVBOIID = glGenBuffers();
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bladeVBOIID);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

				isBladeIndexBound = true;
			}
		}
	}

	public void bindHandleVertexData() {

		if (isDrawHandle) {
			if (verticesBufferHandle == null) {
				verticesBufferHandle = BufferUtils.createFloatBuffer(verticesDrawHandle.length);
			}
			verticesBufferHandle.put(verticesDrawHandle).flip();

			if (handleVAOID == -1) {
				handleVAOID = glGenVertexArrays();
			}
			glBindVertexArray(handleVAOID);

			if (handleVBOID == -1) {
				handleVBOID = glGenBuffers();
			}
			glBindBuffer(GL_ARRAY_BUFFER, handleVBOID);
			glBufferData(GL_ARRAY_BUFFER, verticesBufferHandle, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);

			if (!isHandleIndexBound) {
				handleIndicesLinesCount = indicesHandle.length;

				ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(handleIndicesLinesCount);
				indicesBuffer.put(indicesHandle);
				indicesBuffer.flip();

				handleVBOIID = glGenBuffers();
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handleVBOIID);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

				isHandleIndexBound = true;
			}
		}
	}

	
	public void bindHandleTopVertexData() {

		if (isDrawHandleTop) {
			if (verticesBufferHandleTop == null) {
				verticesBufferHandleTop = BufferUtils.createFloatBuffer(verticesDrawHandleTop.length);
			}
			verticesBufferHandleTop.put(verticesDrawHandleTop).flip();

			if (handleTopVAOID == -1) {
				handleTopVAOID = glGenVertexArrays();
			}
			glBindVertexArray(handleTopVAOID);

			if (handleTopVBOID == -1) {
				handleTopVBOID = glGenBuffers();
			}
			glBindBuffer(GL_ARRAY_BUFFER, handleTopVBOID);
			glBufferData(GL_ARRAY_BUFFER, verticesBufferHandleTop, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);

			if (!isHandleTopIndexBound) {
				handleTopIndicesLinesCount = indicesHandleTop.length;

				ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(handleTopIndicesLinesCount);
				indicesBuffer.put(indicesHandleTop);
				indicesBuffer.flip();

				handleTopVBOIID = glGenBuffers();
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handleTopVBOIID);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

				isHandleTopIndexBound = true;
			}
		}
	}

	
	public void calculateVertices(Vector3f[] verticesModel, Vector3f[] vertices, float[] verticesDraw, Vector3f offsetXYZ) {

		double radian = Math.toRadians(rotationZ);	

		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		float fX, fY, fZ;

		int indexCount = 0;
				
		for (int i = 0; i < vertices.length; i++) {
			v.x = verticesModel[i].x;
			v.y = verticesModel[i].y;
			v.z = verticesModel[i].z;

			dest1.w = 1f;
			dest1.x = 0f;
			dest1.y = 0f;
			dest1.z = 0f;

			dest2.w = 1f;
			dest2.x = 0f;
			dest2.y = 0f;
			dest2.z = 0f;

			float x, y, z;
			x = rotationXYZ.x ;//+ rotationXYZ.x;// testX;
			y = rotationXYZ.y ;//+ rotationXYZ.y;//testY;
			z = rotationXYZ.z ;//+ rotationXYZ.z;//testZ;
		
			x = x % 360.0f;
			y = y % 360.0f;
			z = z % 360.0f;

			dest2.rotate((float) Math.toRadians(x), (float) Math.toRadians(y), (float) Math.toRadians(z), dest1);

			dest1.transform(v);

			Vector3f vv = vertices[i];

				vv.x = v.x;
				vv.y = v.y;
				vv.z = v.z;

		}
		
		for (int i = 0; i < vertices.length; i++) {

			fX = vertices[i].x;// * lengthX/* X */;
			fY = vertices[i].y;// * widthY/* Y */;
			fZ = vertices[i].z;// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= height;
			dY *= height;
			dZ *= height;

			dX += offsetXYZ.x;
			dY += offsetXYZ.y;
			dZ += offsetXYZ.z;

			int newI = i * 3;
			verticesDraw[newI] = (dX);
			verticesDraw[newI + 1] = (dY);
			verticesDraw[newI + 2] = (dZ);
			
		}
	
	}

	public void draw() { 
		
		
		if (height < 0f) {
			setHeight(3);	
		}
	
		calculateVertices(verticesModelBlade, verticesBlade, verticesDrawBlade,bladeXYZ);
		calculateVertices(verticesModelHandle, verticesHandle, verticesDrawHandle,handleXYZ);
		calculateVertices(verticesModelHandleTop, verticesHandleTop, verticesDrawHandleTop,handleTopXYZ);
		
		if(isDrawBlade)
		{drawBlade();}
		
		if(isDrawHandle)
		{drawHandle();}
		
		if(isDrawHandleTop)
		{drawHandleTop();}
	}

	public void drawBlade() {// System.out.println("human.drawJoints");
		//System.out.println("entered drawJoints");
		//System.out.println("about to run bindJointsVertexData");
		bindBladeVertexData();
		//System.out.println("done with bindJointsVertexData");
		glUseProgram(bladeProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(bladeGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(bladeFB));
		glUniform4f(bladeGLRGBAUniformLocation, bladeRed, bladeGreen, bladeBlue, 1.0f);

		glBindVertexArray(bladeVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bladeVBOIID);
		glDrawElements(GL_TRIANGLES, bladeIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		//System.out.println("leaving drawJoints");
	}
	
	public void drawHandle() {// System.out.println("human.drawJoints");
		//System.out.println("entered drawJoints");
		//System.out.println("about to run bindJointsVertexData");
		bindHandleVertexData();
		//System.out.println("done with bindJointsVertexData");
		glUseProgram(handleProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(handleGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(handleFB));
		glUniform4f(handleGLRGBAUniformLocation, handleRed, handleGreen, handleBlue, 1.0f);

		glBindVertexArray(handleVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handleVBOIID);
		glDrawElements(GL_TRIANGLES, handleIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		//System.out.println("leaving drawJoints");
	}


	public void drawHandleTop() {// System.out.println("human.drawJoints");
		//System.out.println("entered drawJoints");
		//System.out.println("about to run bindJointsVertexData");
		bindHandleTopVertexData();
		//System.out.println("done with bindJointsVertexData");
		glUseProgram(handleTopProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(handleTopGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(handleTopFB));
		glUniform4f(handleTopGLRGBAUniformLocation, handleTopRed, handleTopGreen, handleTopBlue, 1.0f);

		glBindVertexArray(handleTopVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handleTopVBOIID);
		glDrawElements(GL_TRIANGLES, handleTopIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		//System.out.println("leaving drawJoints");
	}

	
	


	public void glCleanup() {
/*		glUseProgram(0);
		if (jointsProgramId != 0) {
			if (jointsVertexShaderId != 0) {
				glDetachShader(jointsProgramId, jointsVertexShaderId);
			}
			if (jointsFragmentShaderId != 0) {
				glDetachShader(jointsProgramId, jointsFragmentShaderId);
			}
			glDeleteProgram(jointsProgramId);
		}
*/
	}

	public void glConstruct() {//System.out.println("human3_weapon_sword_simple_joints.glConstruct entered");
		if (isDrawBlade) {
			glConstructBlade();
		}
		
		if (isDrawHandle) {
			glConstructHandle();
		}
		
		if (isDrawHandleTop) {
			glConstructHandleTop();
		}
		//System.out.println("human3_weapon_sword_simple_joints.glConstruct leaving");
	}

	public void glConstructBlade() {
		try {
			try {
				bladeProgramId = glCreateProgram();

				glCreateVertexShaderBlade("#version 130\nuniform mat4 MVP; in vec3 position; void main() { gl_Position =MVP* vec4(position, 1.0); }");

				glCreateFragmentShaderBlade("#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(bladeProgramId);

				glUseProgram(bladeProgramId);
				bladeGLMVPUniformLocation = glGetUniformLocation(bladeProgramId, "MVP");
				bladeGLRGBAUniformLocation = glGetUniformLocation(bladeProgramId, "RGBA");
				glUseProgram(0);

				//glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (bladeProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}
	
	
	
	public void glConstructHandle() {
		try {
			try {
				handleProgramId = glCreateProgram();

				glCreateVertexShaderHandle("#version 130\nuniform mat4 MVP; in vec3 position; void main() { gl_Position =MVP* vec4(position, 1.0); }");

				glCreateFragmentShaderHandle("#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(handleProgramId);

				glUseProgram(handleProgramId);
				handleGLMVPUniformLocation = glGetUniformLocation(handleProgramId, "MVP");
				handleGLRGBAUniformLocation = glGetUniformLocation(handleProgramId, "RGBA");
				glUseProgram(0);

				//glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (handleProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}
	
	
	public void glConstructHandleTop() {
		try {
			try {
				handleTopProgramId = glCreateProgram();

				glCreateVertexShaderHandleTop("#version 130\nuniform mat4 MVP; in vec3 position; void main() { gl_Position =MVP* vec4(position, 1.0); }");

				glCreateFragmentShaderHandleTop("#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(handleTopProgramId);

				glUseProgram(handleTopProgramId);
				handleTopGLMVPUniformLocation = glGetUniformLocation(handleTopProgramId, "MVP");
				handleTopGLRGBAUniformLocation = glGetUniformLocation(handleTopProgramId, "RGBA");
				glUseProgram(0);

//				glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (handleTopProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}
	

	public void glCreateFragmentShaderBlade(String shaderCode) throws Exception {
		bladeFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, bladeProgramId);
	}

	public void glCreateFragmentShaderHandle(String shaderCode) throws Exception {
		handleFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, handleProgramId);
	}
	
	public void glCreateFragmentShaderHandleTop(String shaderCode) throws Exception {
		handleTopFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, handleTopProgramId);
	}
	
	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShaderBlade(String shaderCode) throws Exception {
		bladeVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, bladeProgramId);
	}


	public void glCreateVertexShaderHandle(String shaderCode) throws Exception {
		handleVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, handleProgramId);
	}


	public void glCreateVertexShaderHandleTop(String shaderCode) throws Exception {
		handleTopVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, handleTopProgramId);
	}

	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}

	public void setHeight(float x) { //x=8f;
		height = x;
	}

//	public void setMVP(Matrix4f mvp) {
//		MVP = mvp; //System.out.println("set sword MVP");
//	}



	
	public void setRotationXYZ(float x, float y, float z) {
		rotationXYZ.x = x;
		rotationXYZ.y = y;
		rotationXYZ.z = z;
	}

	public void setRotationXYZ(Vector3f nxyz) {
		rotationXYZ.x = nxyz.x;
		rotationXYZ.y = nxyz.y;
		rotationXYZ.z = nxyz.z;
		//if(attack!=null)
		//{attack.setHandleXYZ(nxyz);}
	}

	public void setRotationZ(float x) {
		//degreeRotateY = x;
		rotationZ=x;
	}

	
	public void setBladeXYZ(float x, float y, float z) {
		bladeXYZ.x = x;
		bladeXYZ.y = y;
		bladeXYZ.z = z;
		// move();
	}
	
	public void setHandleXYZ(float x, float y, float z) {
		handleXYZ.x = x;
		handleXYZ.y = y;
		handleXYZ.z = z;
		// move();
	}
	
	public void setHandleTopXYZ(float x, float y, float z) {
		handleTopXYZ.x = x;
		handleTopXYZ.y = y;
		handleTopXYZ.z = z;
		// move();
	}
	
	
	
	public void setXYZ(float x, float y, float z) {
		xyz.x = x;
		xyz.y = y;
		xyz.z = z;
		// move();
	}

	public void setXY(float newX, float newY) {
		xyz.x = newX;
		xyz.y = newY;
	}

	public void setZ(float newZ) {
		xyz.z = newZ;
	}


}