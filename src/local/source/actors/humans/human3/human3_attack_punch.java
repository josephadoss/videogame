package local.source.actors.humans.human3;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import local.source.damage;
import local.source.game_time;
import local.source.attacks.interface_attack;
import local.source.attacks.interface_attackable;
import local.source.attacks.manager_attackables;
import local.source.attacks.manager_attacks;
import local.source.weapons.interface_weapon_rotation;

import java.util.*;

public class human3_attack_punch implements interface_attack {

	Vector3f centerXYZ = new Vector3f();
	Vector3f diameterXYZ = new Vector3f();

	// an attack can be an invisible punch at one xyz location
	// or a very visible fire ball traveling along a trajectory spanning
	// multiple xyzs

	public damage damage = null;

	public Matrix4f MVP = null;

	public String attackName = "punch";
	public String className = "human3_attack_punch";

	Vector3f handleXYZ = new Vector3f();
	Vector3f facingDegrees = new Vector3f();
	float travelSpeedPerSecond = 1f; // one foot

	boolean isAttacking = false;

	interface_attackable myHolder = null;
	List<interface_attackable> listAlreadyAttacked = new ArrayList<interface_attackable>();

	int lastAttack = 0;
	int attackEveryMilliseconds = 200;
	game_time gt = game_time.getInstance();
	
	//interface_weapon_rotation weapon_joints = null;
	// add something for bounding box, so a massive fireball could damage more
	// things than a small one
	// attack range and such
	// special magical properties of attack actionLowerStart = gt.getTime();

	// the attack manager will figure out the collision detection
	public human3_attack_punch() {
		damage = new damage();
		damage.setPhysicalDamage(1);
		diameterXYZ.x=1f;diameterXYZ.y=1f;diameterXYZ.z=1f;
	}

	public void attack() { 
		
		//System.out.println("entered human3_attack_punch.attack()");
		//testcode
		centerXYZ = handleXYZ;
		//diameterXYZ.x=1f;diameterXYZ.y=1f;diameterXYZ.z=1f;
		//testcode
		
		//System.out.println("centerXYZ.x:"+centerXYZ.x+"    centerXYZ.y:"+centerXYZ.y+"   centerXYZ.z:"+centerXYZ.z);
		//System.out.println("diameterXYZ.x:"+diameterXYZ.x+"    diameterXYZ.y:"+diameterXYZ.y+"   diameterXYZ.z:"+diameterXYZ.z);
		if(myHolder==null)
		{System.out.println("myHolder is null");}
		
		if(damage==null)
		{		damage = new damage();
		damage.setPhysicalDamage(1);}
		
		//System.out.println("damage : " + damage.getPhysicalDamage() );

		
		if (isAttacking) {
			for (interface_attackable ai : manager_attackables.getInstance().getCollision(centerXYZ, diameterXYZ, myHolder)) {
				if (!listAlreadyAttacked.contains(ai)) {
					ai.takeDamage(damage);
					listAlreadyAttacked.add(ai);	
					//System.out.println("Detected attack collision, dealth damage, added to list");
				}
			}
			lastAttack = gt.getTime();
		}
		
		//System.out.println("leaving attack()");
	}

	public void draw() {
		
		if(isAttacking){
		if(gt.getTime()> (lastAttack+attackEveryMilliseconds))
		{attack();}}
	}

	public String getAttackName() {
		return attackName;
	}

	public human3_joint_rotations_lower_body getHuman3BodyStateLower() {
		return null;// new
					// human3_joint_rotations_lower_body("human3_lower_body_stand");
	}

	public human3_joint_rotations_upper_body getHuman3BodyStateUpper() {
		return new human3_joint_rotations_upper_body("human3_upper_body_punch1");
	}

	public Vector3f getCenterXYZ() {
		return centerXYZ;
	}

	public String getClassName() {
		return className;
	}

	public damage getDamage() {
		return damage;
	}

	public Vector3f getDiameterXYZ() {
		return diameterXYZ;
	}

	public Vector3f getFacingDegrees() {
		return facingDegrees;
	}

	public Vector3f getHandleXYZ() {
		return handleXYZ;
	}

	public boolean getIsAttacking() {
		return isAttacking;
	}

	public float getTravelSpeedPerSecond() {
		return travelSpeedPerSecond;
	}

	public interface_weapon_rotation getWeaponRotation()
	{return null;}
	
	public void registerWithManager() {
		manager_attacks.getInstance().add(this);
	}

	public void setAttackName(String n) {
		attackName = n;
	}

	public void setCenterXYZ(Vector3f n)
	{ centerXYZ.x=n.x;
	centerXYZ.y=n.y;
	centerXYZ.z=n.z;}
	
	public void setCenterXYZ(float x, float y, float z)
	{centerXYZ.x=x;centerXYZ.y=y;centerXYZ.z=z;}
	
	public void setDamage(damage n) {
		damage = n;
	}

	public void setDiameterXYZ(Vector3f n)
	{ diameterXYZ.x=1f;//n.x;
	diameterXYZ.y=1f;//n.y;
	diameterXYZ.z=1f;//n.z;
	}
	
	public void setDiameterXYZ(float x, float y, float z)
	{
		diameterXYZ.x=1f;
		diameterXYZ.y=1f;
		diameterXYZ.z=1f;
	}
	
	
	
	public void setHandleXYZ(float x, float y, float z) {
		handleXYZ.x = x;
		handleXYZ.y = y;
		handleXYZ.z = z;
	}

	public void setHandleXYZ(Vector3f nxyz) {
		handleXYZ.x = nxyz.x;
		handleXYZ.y = nxyz.y;
		handleXYZ.z = nxyz.z;
	}

	public void setHolder(interface_attackable a)
	{myHolder=a;}
	
	public void setFacingDegrees(float x, float y, float z) {
		facingDegrees.x = x;
		facingDegrees.y = y;
		facingDegrees.z = z;
	}

	public void setFacingDegrees(Vector3f nfacingDegrees) {
		facingDegrees = nfacingDegrees;
	}

	public void setIsAttacking(boolean b) {
		isAttacking = b;
		
		if(!isAttacking)
		{listAlreadyAttacked.clear();}
	}

//	public void setMVP(Matrix4f mvp) {
//		MVP = mvp;
//	}

	public void setTravelSpeedPerSecond(float f) {
		travelSpeedPerSecond = f;
	}

	
}
