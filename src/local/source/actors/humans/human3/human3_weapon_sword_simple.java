package local.source.actors.humans.human3;

import java.util.*;

import org.joml.*;

import local.source.attacks.interface_attack;
import local.source.attacks.interface_attackable;
import local.source.weapons.interface_weapon;
import local.source.weapons.manager_weapons;

public class human3_weapon_sword_simple implements interface_weapon {

	//public Matrix4f MVP;// = new Matrix4f();
	
	
	human3_weapon_sword_simple_joints joints = new human3_weapon_sword_simple_joints();
	human3_weapon_sword_simple_skin skin = new human3_weapon_sword_simple_skin();

	public List<interface_attack> attacks = new ArrayList<interface_attack>();
	public interface_attack attack = null;

	public int weaponLevel = -1;
	public int weaponMaxLevel = -1;
	public int weaponExperience = -1;
	public int weaponMaxExperience = -1;

	public String weaponName = "sword";
	public String className = "human3_weapon_sword_simple";
	
	Vector3f handleXYZ = new Vector3f();
	Vector3f centerXYZ = new Vector3f();
	Vector3f rotationXYZ = new Vector3f();

	
	interface_attackable myHolder = null;

	public human3_weapon_sword_simple() {
		registerWithManager();
		setDefaultAttack();
		setHeight(2.5f);
		
	}

	public void addExperience(int e) {
		for (int i = 0; i < e; i++) {
			if (weaponExperience < weaponMaxExperience) {
				weaponExperience++;
			}
		}
		checkLevelUp();
	}
	
	public void checkLevelUp() {
	}

	public void draw() { //System.out.println("weapon handle xyz : "+handleXYZ.toString());
		
	joints.setRotationXYZ(rotationXYZ);
		joints.draw();
		
		
		attack.setCenterXYZ(joints.getCenterXYZ());
		attack.setDiameterXYZ(joints.getDiameterXYZ());
		
		skin.setRotationXYZ(joints.rotationXYZCalc.x,joints.rotationXYZCalc.y,joints.rotationXYZCalc.z);
		skin.setRotationZ(rotationXYZ.z);
		
		float x,y,z;
		int index = 2*3;		
		x = joints.jointsVerticesDraw[index];
		y = joints.jointsVerticesDraw[index+1];
		z = joints.jointsVerticesDraw[index+2];
		skin.setBladeXYZ(x,y,z);
		skin.setHandleTopXYZ(x,y,z);
		
		index = 0;		
		x = joints.jointsVerticesDraw[index];
		y = joints.jointsVerticesDraw[index+1];
		z = joints.jointsVerticesDraw[index+2];
		skin.setHandleXYZ(x,y,z);

		skin.draw();
		
		if(attack!=null)
		{attack.draw();}
	}

	public interface_attack getAttack() { // System.out.println("human3_weapon_fist.getAttack()
											// entered");
		if (attack == null) {// System.out.println("human3_weapon_fist attack is
								// null so setting to default");
			setDefaultAttack();
			// System.out.println("default is "+attack.getAttackName());
		}
		return attack;
	}

	public String getClassName() {
		return className;
	}

	public int getLevel() {
		return weaponLevel;
	}

	public int getWeaponExperience() {
		return weaponExperience;
	}

	public String getWeaponName() {
		return weaponName;
	}

	public void registerWithManager() {
		manager_weapons.getInstance().add(this);
	}

	public void setCenterXYZ(Vector3f n)
	{ centerXYZ.x = n.x; //System.out.println("human3_weapon_sword_simple.setCenterXYZ() this shouldn't be called from outside");
	centerXYZ.y = n.y;
	centerXYZ.z = n.z;
	}
	
	public void setCenterXYZ(float x, float y, float z)
	{centerXYZ.x=x;centerXYZ.y=y;centerXYZ.z=z;}
	
	public void setDefaultAttack() { // System.out.println("human3_weapon_fist.setDefaultAttack
										// entered");
		interface_attack a = new human3_attack_sword_simple_swing();
		a.setHolder(myHolder);
		// System.out.println("just created new attack, a.getAttackName() :
		// "+a.getAttackName());
		if (!attacks.contains(a)) {
			attacks.add(a);
		}
		attack = a;
	}
	
	public void setHandleXYZ(float x, float y, float z) {
		handleXYZ.x = x;
		handleXYZ.y = y;
		handleXYZ.z = z; //System.out.println( "sword xyz : "+handleXYZ.toString());
		joints.setXYZ(x,y,z);
	}

	public void setHandleXYZ(Vector3f nxyz) {
		handleXYZ.x = nxyz.x;
		handleXYZ.y = nxyz.y;
		handleXYZ.z = nxyz.z;
		if(attack!=null)
		{attack.setHandleXYZ(nxyz);}
		 //System.out.println("sword xyz : "+handleXYZ.toString());
		joints.setXYZ(nxyz.x,nxyz.y,nxyz.z);
	}

	public void setHeight(float h)
	{joints.setHeight(h);
	skin.setHeight(h);}
	
	public void setHolder(interface_attackable a)
	{ myHolder = a; 
	attack.setHolder(myHolder);
	}
	
	public void setIsAttacking(boolean b)
	{
		//if(attack!=null)
		{attack.setIsAttacking(b);}
	}
	
//	public void setMVP(Matrix4f mvp) { //System.out.println("sword.setMVP entered mvp :"+mvp.toString());
//		this.MVP = mvp;
//
//		joints.setMVP(mvp);
//		skin.setMVP(mvp);
//		if (attack != null) {
//			attack.setMVP(mvp);
//		}
//	}

	public void setRotationXYZ(float x, float y, float z) {
		rotationXYZ.x = x;
		rotationXYZ.y = y;
		rotationXYZ.z = z;
		joints.setRotationXYZ(x,y,z);
	}

	public void setRotationXYZ(Vector3f nxyz) {
		rotationXYZ.x = nxyz.x;
		rotationXYZ.y = nxyz.y;
		rotationXYZ.z = nxyz.z;
		joints.setRotationXYZ(nxyz);
		//if(attack!=null)
		//{attack.setHandleXYZ(nxyz);}
	}
	
	public void setWeaponName(String n) {
		weaponName = n;
	}

	public void startAttackAction()
	{
		joints.startAttackAction(attack.getWeaponRotation());
	}
	
}