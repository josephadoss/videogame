package local.source.actors.humans.human3;

import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import local.source.game_time;
import local.source.weapons.interface_weapon_rotation;

import org.joml.Vector3f;




public class human3_weapon_sword_simple_rotation implements interface_weapon_rotation {
	
	human3_weapon_sword_simple_rotation_manager manager = human3_weapon_sword_simple_rotation_manager.getInstance();

	
String strNextActionStateHolder = "";
	
	boolean isAttack = false;
	boolean isWalking = false;
	boolean isInterruptable = false;
	
	Vector3f rotate=new Vector3f();
	
	game_time gt = game_time.getInstance();


	int timeToComplete;
	//float distGroundToCrotch = .5f;
	String strState = "none";
	//int enumVal;
	
	//float breathOffset = (new Random()).nextFloat()%5f +3f; 
	float nipToLShoulder=-45f;
	float nipToRShoulder=45f;

	human3_weapon_sword_simple_rotation nextState;

	public human3_weapon_sword_simple_rotation( String strFile) {
		//enumVal = val;
		strState=strFile;
		readFromActionStateFile(strFile);
		
		manager.put(strFile, this);
	}



	public human3_weapon_sword_simple_rotation getNextState() {
		//return nextState;
		return manager.get(strNextActionStateHolder);
	}

	public Vector3f getRotation()
	{return rotate;}
	
	public String getState() {
		return strState;
	}

	public int getTimeToComplete() {
		return timeToComplete;
	}

	public boolean isAttack() {
		return isAttack;
	}
	
	public boolean isInterruptable()
	{
		return isInterruptable;
	}
	
	public boolean isWalking()
	{ return isWalking; }

	public void readFromActionStateFile(String fileName) {
		try {
/*
			File pathToFile = new File("resources" + "/" 
					+ "actors" + "/" 
					+ "states" + "/" 
					+ "human3_weapon" + "/" 
					+ "joints" + "/" 
					+ fileName + ".xml");*/
			String path =  "/" 
					+ "actors" + "/" 
					+ "states" + "/" 
					+ "human3_weapon" + "/" 
					+ "joints" + "/" 
					+ fileName + ".xml";
			URL jointURL = getClass().getResource(path);

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
			Document document = documentBuilder.parse(jointURL.openStream());//pathToFile);
			document.getDocumentElement().normalize();
			NodeList nList = document.getElementsByTagName("actionState");

			//System.out.println("About to enter loop of actionStates");
			for (int i = 0; i < nList.getLength(); i++) {
				//System.out.println("entered array of actionStates");
				Node n = nList.item(i);
				//System.out.println("found a node");
				//System.out.println("n.getNodeName() " + n.getNodeName());
				//System.out.println("n.getLocalName() " + n.getLocalName());
				//System.out.println("n.getNodeValue() " + n.getNodeValue());
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					//System.out.println("entered if statement for n");

					Element e = (Element) n;
					//System.out.println("converted node n to element e");
					
					Element eRotate = (Element) e.getElementsByTagName("rotate").item(0);
					String strRotateX = eRotate.getAttribute("x");
					String strRotateY = eRotate.getAttribute("y");
					String strRotateZ = eRotate.getAttribute("z");

					float floatRotateX = Float.parseFloat(strRotateX);
					float floatRotateY = Float.parseFloat(strRotateY);
					float floatRotateZ = Float.parseFloat(strRotateZ);
					
					rotate.x=floatRotateX;
					rotate.y=floatRotateY;
					rotate.z=floatRotateZ;
					
					

					Element eTTC = (Element) e.getElementsByTagName("timeToComplete").item(0);
					String strTTC = eTTC.getAttribute("value");
					int intTTC = Integer.parseInt(strTTC);
					timeToComplete=intTTC;
					
					//System.out.println("time to complete : "+this.timeToComplete);
					
					Element eIsWalk = (Element) e.getElementsByTagName("isWalking").item(0);
					String strIsWalk = eIsWalk.getAttribute("value").toLowerCase().trim();
					//System.out.println("strIsWalk : " + strIsWalk);
					isWalking = (strIsWalk.compareTo("true")==0);
					

					Element eIsAttack = (Element) e.getElementsByTagName("isAttack").item(0);
					String strIsAttack = eIsAttack.getAttribute("value").toLowerCase().trim();
					//System.out.println("strIsWalk : " + strIsWalk);
					isAttack = (strIsAttack.compareTo("true")==0);
					
					Element eIsInterruptable = (Element) e.getElementsByTagName("isInterruptable").item(0);
					String strIsInterruptable = eIsInterruptable.getAttribute("value").toLowerCase().trim();
					//System.out.println("strIsWalk : " + strIsWalk);
					isInterruptable = (strIsInterruptable.compareTo("true")==0);
				
					//System.out.println("is walking : " + isWalking);
					/*
					Element eDistGtC = (Element) e.getElementsByTagName("distGroundToCrotch").item(0);
					String strDistGtC = eDistGtC.getAttribute("value");
					float fltDistGtC = Float.parseFloat(strDistGtC);
					distGroundToCrotch = fltDistGtC;*/
					
					Element eNext = (Element) e.getElementsByTagName("nextActionState").item(0);
					String strNext = eNext.getAttribute("value");
					strNextActionStateHolder=strNext;
					
					break;
				}
			}

		} catch (Exception e) {
			System.out.println("exception caught in human3_weapon_sword_simple_joint_rotations.readXMLfile: " + e.getMessage() + " " + e.getStackTrace());
		}

	}

	//public void setNextState(actionState_human2_lowerBody_parent next) {
	//	nextState = next;
	//}

}
