package local.source.actors.humans.human3;

import java.io.File;
import java.util.*;

public class human3_outfit {

	String path = "resources" + "/" + "actors" + "/" + "skins" + "/" + "human3" + "/"+ "outfits" + "/";

	String garmentType ="shirts";
	String garmentTypeShirts = "shirts";
	String garmentTypePants = "pants";
	String garmentTypeGloves = "gloves";
	String garmentTypeShoes = "shoes";
	String garmentTypeHead = "head";
	String garmentTypeHat = "hat";
	
	public String shirt = "nude";	
	//public List<String> listShirts = new ArrayList<String>();
	public String pants = "nude";
	//public List<String> listPants = new ArrayList<String>();
	public String shoes = "nude";
	//public List<String> listShoes = new ArrayList<String>();
	public String gloves = "nude";
	//public List<String> listGloves = new ArrayList<String>();
	public String head = "nude";
	public String hat = "nude";

	int arrayDetail = 10;
	String size = "quickAndEasy";
	float fat, muscular;
	int gender = 0;
	String strGender = "male";

	public human3_outfit() {
		//listShirts.add("nude");
		//listShirts.add("redShirt1");
		//listShirts.add("blueShirt1");
		
		//listPants.add("nude");
		//listShoes.add("nude");
		//listGloves.add("nude");
	}

	

	public String getTexturePath(String bodypart, String frontback) {
		String garment = getGarmentFromBodyPart(bodypart);
		return path + garmentType + "/" + garment + "/" +  bodypart + "_" + strGender + "_"+frontback+ ".png";
	}

	private String getGarmentFromBodyPart(String bodypart) {
		String garment = shirt;

		switch (bodypart) {
		case "head":
			garment = head;
			garmentType=garmentTypeHead;
			break;
		case "hat":
			garment = hat;
			garmentType=garmentTypeHat;
			break;
		case "neck":
			garment = shirt;
			garmentType=garmentTypeShirts;
			break;
		case "chest":
			garment = shirt;
			garmentType=garmentTypeShirts;
			break;
		case "tummy":
			garment = shirt;
			garmentType=garmentTypeShirts;
			break;
		case "arm_upper":
			garment = shirt;
			garmentType=garmentTypeShirts;
			break;
		case "arm_lower":
			garment = shirt;
			garmentType=garmentTypeShirts;
			break;
		case "hips":
			garment = pants;
			garmentType=garmentTypePants;
			break;
		case "leg_upper":
			garment = pants;
			garmentType=garmentTypePants;
			break;
		case "leg_lower":
			garment = pants;
			garmentType=garmentTypePants;
			break;
		case "hand":
			garment = gloves;
			garmentType=garmentTypeGloves;
			break;
		case "foot":
			garment = shoes;
			garmentType=garmentTypeShoes;
			break;
		default:
			break;
		}
		;
		return garment;
	}

	public void setArrayDetail(int newDetail) {
		arrayDetail = newDetail;
	}

	public void setHat(String s ) {this.hat=s;}
	
	public void setHead(String s) {this.head =s;}
	
	public void setGender(int g) {
		gender = g;
		strGender = gender == 0 ? "male" : "female";
	}
	
	public void setGloves(String s)
	{gloves=s;}
	
	public void setPants(String s)
	{pants=s;}
	
	public void setShirt(String s )
	{shirt = s;}
	
	public void setShoes(String s)
	{shoes = s;}
	
}
