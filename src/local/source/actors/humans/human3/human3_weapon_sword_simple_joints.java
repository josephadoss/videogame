package local.source.actors.humans.human3;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL32.*;
//import static org.lwjgl.opengl.GL40.*;
//import static org.lwjgl.opengl.GL45.*;

import java.nio.*;
import java.util.Calendar;

import org.lwjgl.BufferUtils;

import local.source.game_time;
import local.source.manager_mvp;
import local.source.weapons.interface_weapon_rotation;

import org.joml.QuaternionfInterpolator;
import org.joml.Quaternionf;
import org.joml.Matrix4f;
import org.joml.Vector3f;

public class human3_weapon_sword_simple_joints {

	// when you rework the joints ( and you really need to )
	// consider breathing and moving more than just the shoulders
	// change the center of the upper back to move a little too
	// that should be enough to move the entire upper torso when skins come
	// along
	// or maybe add a few new joints for chest center or tommy and move those
	// around

	// add more joints for lowerneck and upper neck... no real need for center
	// of neck

	//float testX = 0f;
	//float testY=0f;
	//float testZ = 0f;
	
int indexHandle=0;
int indexGripBottom=1;
int indexGripTop=2;
int indexLeftGrip=3;
int indexRightGrip=4;
int indexBladeTip=5;

Vector3f[] verticesModel = { new Vector3f(0f, 0f, 0f), // 0 handle
		new Vector3f(0f, 0f, -0.083f), // 1 grip bottom
		new Vector3f(0f, 0f, 0.083f), // 2 grip top
		new Vector3f(0f, 0.083f, 0.083f), // 3 left grip
		new Vector3f(0f, -0.083f, 0.083f), // 4 right grip
		new Vector3f(0f, 0f, 0.83f) // 5 blade tip
		
};

	Vector3f[] vertices = { new Vector3f(0f, 0f, 0f), // 0 handle
			new Vector3f(0f, 0f, 0f), // 1 grip bottom
			new Vector3f(0f, 0f, 0f), // 2 grip top
			new Vector3f(0f, 0f, 0f), // 3 left grip
			new Vector3f(0f, 0f, 0f), // 4 right grip
			new Vector3f(0f, 0f, 0f) // 5 blade tip
			
	};

/*	Vector3f[] rotations = { new Vector3f(0f, 0f, 0f), // 0 handle
			new Vector3f(0f, 0f, 0f), // 1 grip bottom
			new Vector3f(0f, 0f, 0f), // 2 grip top
			new Vector3f(0f, 0f, 0f), // 3 left grip
			new Vector3f(0f, 0f, 0f), // 4 right grip
			new Vector3f(0f, 0f, 0f) // 5 blade tip
	};*/

	game_time gt = game_time.getInstance();

	int actionStart = gt.getTime();

	byte[] jointsIndicesLines = { 0,1 // handle to bottom of grip
			,0,2 // handle to top fo grip
			,2,3 // top grip to left grip
			,2,4 // top grip to right grip
			,2,5 // top grip to top blade
	};

	float[] jointsVerticesDraw = { 0f, 0f, 0f // handle
			, 0f, 0f, .5f // grip bottom
			, 0f, 0f, .5f // grip top
			, 0f, 0f, .5f // grip left
			, 0f, 0f, .25f // grip right
			, 0f, 0f, .25f // blade tip
	};

	boolean isBodyStateChangeable = true;

	int[] parentJoint = { 0, // 0 handle
			0, // 1 grip bottom
			0, // 2 grip top
			2, // 3 grip left
			2, // 4 grip right
			2 // 5 blade tip
	};

	float[] distancesFromParent = { 0f, // 0 handle to handle
			0.083f, // 1 handle to grip bottom
			0.083f, // 2 handle to grip top
			0.083f, // 3 grip top to grip left
			0.083f, // 4 grip top to grip right
			0.83f // 5 grip top to blade tip
	};

	interface_weapon_rotation jointsNew = new human3_weapon_sword_simple_rotation("human3_weapon_sword_simple_rotation_stand");
	interface_weapon_rotation jointsOld = new human3_weapon_sword_simple_rotation("human3_weapon_sword_simple_rotation_stand");
	interface_weapon_rotation jointsDraw = new human3_weapon_sword_simple_rotation("human3_weapon_sword_simple_rotation_stand");
	interface_weapon_rotation jointsDefault = new human3_weapon_sword_simple_rotation("human3_weapon_sword_simple_rotation_stand");

	int jointsIndicesLinesCount;// = jointsIndicesLines.length;

	float jointsRed = 1f;
	float jointsBlue = 1f;
	float jointsGreen = 1f;

	int jointsVAOID = -1;
	int jointsVBOID = -1;
	int jointsVBOIID = -1;

	public int jointsGLMVPUniformLocation;
	public int jointsGLRGBAUniformLocation;
	public FloatBuffer jointsFB = BufferUtils.createFloatBuffer(16);

	public int jointsProgramId;
	public int jointsVertexShaderId;
	public int jointsFragmentShaderId;

	boolean isJointsIndexBound = false;

	//public Matrix4f MVP;

	public boolean isDrawJoints = false;

	public Vector3f xyz = new Vector3f();
	public float height = -1f;

	//public float degreeRotateX = 0.0f;
	//public float degreeRotateY = 0.0f;
	//public float degreeRotateZ = 0.0f;
	
	public Vector3f rotationXYZ = new Vector3f();
	public Vector3f rotationXYZCalc = new Vector3f();

	public float moveDistance = .0003f;// * headHeight;
	// float armWidth = 327f;

	Quaternionf dest1 = new Quaternionf();
	Quaternionf dest2 = new Quaternionf();
	Vector3f v = new Vector3f();

	FloatBuffer verticesBufferJoints;

	float percentDone = 0f;
	
	Vector3f diameterXYZ = new Vector3f(); ;
	Vector3f centerXYZ=new Vector3f();

	public human3_weapon_sword_simple_joints() {
		// setHeight(6f);
//System.out.println("human3_weapon_sword_simple_joints.constructor entered");
		if (isDrawJoints) {
			this.glConstruct();
		}
		
	//	System.out.println("human3_weapon_sword_simple_joints.constructor leaving");
	}

	public void bindJointsVertexData() {

		// for(int i = 0; i<draw.length;i++)
		// { draw[i]=jointsVerticesDraw[i]; }

		if (isDrawJoints) {
			if (verticesBufferJoints == null) {
				verticesBufferJoints = BufferUtils.createFloatBuffer(jointsVerticesDraw.length);
			}
			verticesBufferJoints.put(jointsVerticesDraw).flip();

			if (jointsVAOID == -1) {
				jointsVAOID = glGenVertexArrays();
			}
			glBindVertexArray(jointsVAOID);

			if (jointsVBOID == -1) {
				jointsVBOID = glGenBuffers();
			}
			glBindBuffer(GL_ARRAY_BUFFER, jointsVBOID);
			glBufferData(GL_ARRAY_BUFFER, verticesBufferJoints, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);

			if (!isJointsIndexBound) {
				jointsIndicesLinesCount = jointsIndicesLines.length;

				ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(jointsIndicesLinesCount);
				indicesBuffer.put(jointsIndicesLines);
				indicesBuffer.flip();

				jointsVBOIID = glGenBuffers();
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, jointsVBOIID);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

				isJointsIndexBound = true;
			}
		}
	}

	/*
	 * public void calcuateJointsQuaternions(float rX, float rY, float rZ) {
	 * dest1.w = 1f; dest1.x = 0f; dest1.y = 0f; dest1.z = 0f;
	 * 
	 * dest2.w = 1f; dest2.x = 0f; dest2.y = 0f; dest2.z = 0f;
	 * 
	 * dest2.rotate((float) Math.toRadians(rX), (float) Math.toRadians(rY),
	 * (float) Math.toRadians(rZ), dest1);
	 * 
	 * // dest1.normalize(); // dest1.invert(); dest1.transform(v); }
	 */

	// int second=0;
	// int newsecond=0;
	// boolean isNewSecond=false;


	
	public void calculateJointsVertices() {		//System.out.println("human3_weapon_sword_simple_joints.calcuateJointsVertices entered");

		float zMax= xyz.z;
		float zMin= xyz.z;
		float yMax= xyz.y;
		float yMin= xyz.y;
		float xMax= xyz.x;
		float xMin= xyz.x;

		/*
		 * Calendar calendar = Calendar.getInstance(); newsecond=
		 * calendar.get(Calendar.SECOND); //System.out.println(newsecond);
		 * if(second!=newsecond) { second=newsecond; isNewSecond=true; } else {
		 * isNewSecond=false;
		 */

		double radian = Math.toRadians(rotationXYZ.z);	//	System.out.println("just got raidan : "+radian + " value from rotationXYZ.z : "+rotationXYZ.z);

		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		float fX, fY, fZ;

		int indexCount = 0;
		// float[] vertices = {};// .clone();// act.getVertices();

		int timeToComplete = 1000;
		
		int now = gt.getTime();

		// timeToCompleteLower = h2bslNew.getTimeToComplete();
		timeToComplete = jointsNew.getTimeToComplete(); 	//	System.out.println("just got timeToComplete :"+timeToComplete);


		percentDone = (float) ((float) (now - actionStart) / (float) timeToComplete);
//		System.out.println("Just figured out percentDone : "+percentDone);


	//	System.out.println("about to figure out if percentDone is greater than 1");
		if (percentDone > 1f) {
			isBodyStateChangeable = true;
			percentDone = 0f;
			jointsOld = jointsNew;
			if (jointsNew.getNextState() != null) {
				jointsNew = jointsNew.getNextState();
			} else {
				jointsNew = jointsDefault;
			}
			actionStart = gt.getTime();
		}
		//System.out.println("done with percentDone>1f");


		

		//float o, n;
		//System.out.println("about to loop through Vector3f of jointsOld and jointsNew to figure out jointsDraw");
		//System.out.println("jointsDraw.getArray().length : "+jointsDraw.getArray().length);

		Vector3f vO, vN;
		//for (int i = 0; i < jointsDraw.getArray().length; i++) {	//	System.out.println("entered for loop i:"+i);


			vO = jointsOld.getRotation();//.getArray()[i];// h2bslOld.getArray()[i];
			vN = jointsNew.getRotation();//.getArray()[i];// h2bslNew.getArray()[i];
			jointsDraw.getRotation().x = vO.x + ((vN.x - vO.x) * percentDone);
			jointsDraw.getRotation().y = vO.y + ((vN.y - vO.y) * percentDone);
			jointsDraw.getRotation().z = vO.z + ((vN.z - vO.z) * percentDone);

			rotationXYZCalc.x = jointsDraw.getRotation().x;// + rotationXYZ.x;
			rotationXYZCalc.y = jointsDraw.getRotation().y ;// rotationXYZ.y;
			rotationXYZCalc.z = jointsDraw.getRotation().z;
			

		
		
		
		for (int i = 0; i < vertices.length; i++) {
			v.x = verticesModel[i].x;
			v.y = verticesModel[i].y;
			v.z = verticesModel[i].z;

			dest1.w = 1f;
			dest1.x = 0f;
			dest1.y = 0f;
			dest1.z = 0f;

			dest2.w = 1f;
			dest2.x = 0f;
			dest2.y = 0f;
			dest2.z = 0f;

			float x, y, z;
			x = rotationXYZCalc.x ;//+ rotationXYZ.x;// testX;
			y = rotationXYZCalc.y ;//+ rotationXYZ.y;//testY;
			z = rotationXYZCalc.z ;//+ rotationXYZ.z;//testZ;
		
			x = x % 360.0f;
			y = y % 360.0f;
			z = z % 360.0f;

			dest2.rotate((float) Math.toRadians(x), (float) Math.toRadians(y), (float) Math.toRadians(z), dest1);

			dest1.transform(v);

			Vector3f vv = vertices[i];
			Vector3f vvOffset = vertices[parentJoint[i]];

				vv.x = v.x;
				vv.y = v.y;
				vv.z = v.z;

		}
		
		
		
		
		
		

		for (int i = 0; i < vertices.length; i++) {

			fX = vertices[i].x;// * lengthX/* X */;
			fY = vertices[i].y;// * widthY/* Y */;
			fZ = vertices[i].z;// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= height;
			dY *= height;
			dZ *= height;

			dX += xyz.x;
			dY += xyz.y;
			dZ += xyz.z;

			int newI = i * 3;
			jointsVerticesDraw[newI] = (dX);
			jointsVerticesDraw[newI + 1] = (dY);
			jointsVerticesDraw[newI + 2] = (dZ);
			
			if(dX < xMin)
			{xMin=dX;}
			if(dX > xMax)
			{xMax=dX;}
			if(dY < yMin)
			{yMin=dY;}
			if(dY > yMax)
			{yMax=dY;}
			if(dZ < zMin)
			{zMin=dZ;}
			if(dZ > zMax)
			{zMax=dZ;}
		}
		
		float xVal = xMax-xMin;
		float yVal = yMax-yMin;
		float zVal = zMax-zMin;
		
		diameterXYZ.x =xVal/2f+1f;
		diameterXYZ.y =yVal/2f+1f;
		diameterXYZ.z =zVal/2f+1f;
		
		centerXYZ.x = xMin+diameterXYZ.x ;
		centerXYZ.y = yMin+diameterXYZ.y ;
		centerXYZ.z = zMin+diameterXYZ.z ;
		
		//System.out.println("weapon joints draw xMax:"+xMax+" xMin:"+xMin+" yMax:"+yMax+" yMin:"+yMin+" zMax:"+zMax+" zMin:"+zMin+"");
		//System.out.println("weapon joints draw : diameter x:"+diameterXYZ.x+" y:"+diameterXYZ.y+" z:"+diameterXYZ.z+"");
		//System.out.println("weapon joints draw : center x:"+centerXYZ.x+" y:"+centerXYZ.y+" z:"+centerXYZ.z+"");

		//System.out.println("done transforming by height and xyz");


		//int index = indexGround;
		// System.out.println("ground : x:"+jointsVerticesDraw[index]+"
		// y:"+jointsVerticesDraw[index+1]+" z:"+jointsVerticesDraw[index+2]);

		//index = indexCrotch;
		// System.out.println("crotch : x:"+jointsVerticesDraw[index]+"
		// y:"+jointsVerticesDraw[index+1]+" z:"+jointsVerticesDraw[index+2]);

		//index = indexLHip;
		// System.out.println("lhip : x:"+jointsVerticesDraw[index]+"
		// y:"+jointsVerticesDraw[index+1]+" z:"+jointsVerticesDraw[index+2]);
		//System.out.println("human3_weapon_sword_simple_joints.calculateJointsVertices leaving");


	}

	public void draw() { 
		
		
		//System.out.println("rotation x:"+rotationXYZ.x+"  y:"+rotationXYZ.y+"  z:"+rotationXYZ.z+"   rotationCalc   x:"+rotationXYZCalc.x+" y:"+rotationXYZCalc.y+" z:"+rotationXYZCalc.z+" ");
		
		if (height < 0f) {
			setHeight(3);	//System.out.println("human3_weapon_sword_simple_joints.draw just set height to 6");
		}
	
//		System.out.println("human3_weapon_sword_simple_joints.draw about to calculateJointsVertices");
		calculateJointsVertices();
//		System.out.println("human3_weapon_sword_simple_joints.draw finished calculateJointsVertices");

		
		if (isDrawJoints) {//System.out.println("human3_weapon_sword_simple_joints.draw about to drawJoints");
			drawJoints();//System.out.println("human3_weapon_sword_simple_joints.draw finsihed drawJoints");
		}
		//System.out.println("human3_weapon_sword_simple_joints.draw leaving");
	}

	public void drawJoints() {// System.out.println("human.drawJoints");
		//System.out.println("entered drawJoints");
		//System.out.println("about to run bindJointsVertexData");
		bindJointsVertexData();
		//System.out.println("done with bindJointsVertexData");
		glUseProgram(jointsProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(jointsGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(jointsFB));
		glUniform4f(jointsGLRGBAUniformLocation, jointsRed, jointsGreen, jointsBlue, 1.0f);

		glBindVertexArray(jointsVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, jointsVBOIID);
		glDrawElements(GL_LINES, jointsIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glDrawElements(GL_POINTS, jointsIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		//System.out.println("leaving drawJoints");
	}

	public Vector3f getCenterXYZ(){return centerXYZ;}
	
	public Vector3f getDiameterXYZ(){return diameterXYZ;}
	
	public Vector3f getVertexOfHandle()
	{
		Vector3f result = new Vector3f();
		int index = indexHandle *3;
		result.x = jointsVerticesDraw[index];
		result.y = jointsVerticesDraw[index+1];
		result.z = jointsVerticesDraw[index+2];
		//System.out.println("getting location of left hand x:"+result.x+" y:"+result.y+" z:"+result.z);
		return result;
	}
	
	/*
	 * public void faceLeft() { degreeRotateZ = (degreeRotateZ + turnDegree) %
	 * 360.0f; // move(); }
	 * 
	 * public void facePoint(float newX, float newY, float newZ) { float dX =
	 * newX - xyz.x; float dY = newY - xyz.y; degreeRotateZ = (float)
	 * (Math.atan2(dY, dX) * 180.0f / Math.PI); }
	 * 
	 * public void faceRight() { float change = (degreeRotateZ - turnDegree);
	 * degreeRotateZ = (float) (change <= 0.0d ? (360.0d - turnDegree) : change
	 * % 360.0d); // move(); }
	 * 
	 * public float getFacingDegrees() { return degreeRotateZ; }
	 * 
	 * public float getHeight() { return height; }
	 */
	public void glCleanup() {
		glUseProgram(0);
		if (jointsProgramId != 0) {
			if (jointsVertexShaderId != 0) {
				glDetachShader(jointsProgramId, jointsVertexShaderId);
			}
			if (jointsFragmentShaderId != 0) {
				glDetachShader(jointsProgramId, jointsFragmentShaderId);
			}
			glDeleteProgram(jointsProgramId);
		}

	}

	public void glConstruct() {//System.out.println("human3_weapon_sword_simple_joints.glConstruct entered");
		if (isDrawJoints) {
			glConstructJoints();
		}
		//System.out.println("human3_weapon_sword_simple_joints.glConstruct leaving");
	}

	public void glConstructJoints() {
		try {
			try {
				jointsProgramId = glCreateProgram();

				glCreateVertexShaderJoints("#version 130\nuniform mat4 MVP; in vec3 position; void main() { gl_Position =MVP* vec4(position, 1.0);  gl_PointSize = 3.0; }");

				glCreateFragmentShaderJoints("#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(jointsProgramId);

				glUseProgram(jointsProgramId);
				jointsGLMVPUniformLocation = glGetUniformLocation(jointsProgramId, "MVP");
				jointsGLRGBAUniformLocation = glGetUniformLocation(jointsProgramId, "RGBA");
				glUseProgram(0);

				glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (jointsProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glCreateFragmentShaderJoints(String shaderCode) throws Exception {
		jointsFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, jointsProgramId);
	}

	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShaderJoints(String shaderCode) throws Exception {
		jointsVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, jointsProgramId);
	}

	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}

/*	public boolean isAttacking()
	{ 
		return (jointsNew.isAttack() );//|| jointsLowerBodyNew.isAttack() );
	}
	
	public boolean isInterruptable()
	{
		if(jointsNew.isInterruptable() && jointsNew.isInterruptable())
		{return true;}
		return false;
	}*/
	
/*	public void moveAttack() {

		if(!jointsUpperBodyNew.isInterruptable() || !jointsLowerBodyNew.isInterruptable())
		{return;}
		
		if (!jointsUpperBodyNew.isAttack()) {
			// System.out.println("upper body new isn't stand, so entered stand
			// code");
			actionUpperStart = gt.getTime();
			jointsUpperBodyOld = jointsUpperBodyDraw;

			jointsUpperBodyNew = new human3_joint_rotations_upper_body("human3_upper_body_punch1");

		}
	}*/
	/*
	public void moveAttack(interface_attack a)
	{
		if(!jointsUpperBodyNew.isInterruptable() || !jointsLowerBodyNew.isInterruptable())
		{return;}
		
		if(!jointsUpperBodyNew.isAttack())
		{
			actionUpperStart = gt.getTime();

			jointsUpperBodyOld = jointsUpperBodyNew;
			
			human3_joint_rotations_lower_body lnext = a.getHuman3BodyStateLower();
			//human3_joint_rotations_upper_body unext = a.getHuman3BodyStateUpper();
			
			jointsUpperBodyNew = a.getHuman3BodyStateUpper();

			if(lnext!=null)
			{
				actionLowerStart = gt.getTime();
				jointsLowerBodyOld = jointsLowerBodyNew;
				jointsLowerBodyNew = a.getHuman3BodyStateLower();
			}
		}
		
	}

	public void moveEngage() {
		
		if(!jointsUpperBodyNew.isInterruptable() || !jointsLowerBodyNew.isInterruptable())
		{return;}

		if (!jointsLowerBodyNew.getState().contains("engage")) {
			jointsLowerBodyOld = jointsLowerBodyDraw;
			jointsLowerBodyNew = new human3_joint_rotations_lower_body("human3_lower_body_engage");

			actionLowerStart = gt.getTime();			
		}

			if (!jointsUpperBodyNew.getState().contains("engage")) {
				jointsUpperBodyOld = jointsUpperBodyDraw;
				jointsUpperBodyNew = new human3_joint_rotations_upper_body("human3_upper_body_engage");
				

				actionUpperStart = actionLowerStart;// gt.getTime();
			}

	}
	
	public void moveForward() {
		
		if(!jointsUpperBodyNew.isInterruptable() || !jointsLowerBodyNew.isInterruptable())
		{return;}

		if (!jointsLowerBodyNew.isWalking()) {
			jointsLowerBodyOld = jointsLowerBodyNew;
			jointsLowerBodyNew = new human3_joint_rotations_lower_body("human3_lower_body_first_step");

			actionLowerStart = gt.getTime();
		}

		if (jointsLowerBodyNew.isWalking() && !jointsUpperBodyNew.isWalking()) {
			if (!jointsUpperBodyNew.isAttack()) {
				jointsUpperBodyOld = jointsUpperBodyNew;

				if (jointsLowerBodyNew.getState().contains("left") || jointsLowerBodyNew.getState().contains("first")) {
					jointsUpperBodyNew = new human3_joint_rotations_upper_body("human3_upper_body_right_arm_forward");
				} else {
					jointsUpperBodyNew = new human3_joint_rotations_upper_body("human3_upper_body_left_arm_forward");
				}

				actionUpperStart = actionLowerStart;// gt.getTime();
			}
		}
	}

	public void moveSquat() {
		
		if(!jointsUpperBodyNew.isInterruptable() || !jointsLowerBodyNew.isInterruptable())
		{return;}

		if (!jointsLowerBodyNew.getState().contains("squat")) {
			actionLowerStart = gt.getTime();
			jointsLowerBodyOld = jointsLowerBodyNew;

			jointsLowerBodyNew = new human3_joint_rotations_lower_body("human3_lower_body_squat");

		}

		if (!jointsUpperBodyNew.getState().contains("squat") && !jointsUpperBodyNew.isAttack()) {
			// System.out.println("upper body new isn't squat, so entered
			// squat code");
			// h2bslOld = h2bslNew;
			// h2bslNew = h2bslSquat;
			actionUpperStart = gt.getTime();
			jointsUpperBodyOld = jointsUpperBodyNew;

			jointsUpperBodyNew = new human3_joint_rotations_upper_body("human3_upper_body_squat");
			// interruptWithNewLowerBodyAction=true;
			// actionLowerStart =
			// gt.getTime()+h2bslCurrent.getTimeToComplete();
		}

		// }
	}

	public void moveStand() {
		
		if(!jointsUpperBodyNew.isInterruptable() || !jointsLowerBodyNew.isInterruptable())
		{return;}
		
		if (!jointsLowerBodyNew.getState().contains("stand")) {

			actionLowerStart = gt.getTime();
			jointsLowerBodyOld = jointsLowerBodyDraw;

			jointsLowerBodyNew = new human3_joint_rotations_lower_body("human3_lower_body_stand");

		}

		if (!jointsUpperBodyNew.getState().contains("stand") && !jointsUpperBodyNew.isAttack()) {
			// System.out.println("upper body new isn't stand, so entered stand
			// code");
			actionUpperStart = gt.getTime();
			jointsUpperBodyOld = jointsUpperBodyDraw;

			jointsUpperBodyNew = new human3_joint_rotations_upper_body("human3_upper_body_stand");

		}

	}
*/
	/*
	 * public void quaternionRotate(float dist, float[] joints, int index,
	 * Vector3f vv, Vector3f vvOffset) { v.x = 0f; v.y = 0f; v.z = dist;
	 * calcuateJointsQuaternions(joints[index++], joints[index++],
	 * joints[index++]); // float jx,jy,jz; // jx = joints[index++]; // jy =
	 * joints[index++]; // jz = joints[index++]; //
	 * calcuateJointsQuaternions(jx,jy,jz); //
	 * System.out.println("jx:"+jx+"\tjy:"+jy+"\tjz:"+jz); if (vvOffset == null)
	 * { vv.x = v.x; vv.y = v.y; vv.z = v.z; //
	 * System.out.println("vv.x:"+vv.x+"\tvv.y:"+vv.y+"\tvv.z:"+vv.z); } else {
	 * vv.x = v.x + vvOffset.x; vv.y = v.y + vvOffset.y; vv.z = v.z +
	 * vvOffset.z; } }
	 */

/*	public void setDiameterXYZ(Vector3f p)
	{ this.diameterXYZ = p;}*/
	
/*	public void setFacingDegree(float x) {
		degreeRotateZ = x;
	}*/

	public void setHeight(float x) { //x=8f;
		height = x;
		//moveDistance = height * .1f;

		float baseMeasure1 = 6f/36f;
		float baseMeasure2 = 30f/36f;

		distancesFromParent[indexHandle] = 0f;
		distancesFromParent[indexGripBottom] = baseMeasure1;
		distancesFromParent[indexGripTop] = baseMeasure1;
		distancesFromParent[indexLeftGrip] = baseMeasure1;
		distancesFromParent[indexRightGrip] = baseMeasure1;
		distancesFromParent[indexBladeTip] = baseMeasure2;

	}

//	public void setMVP(Matrix4f mvp) {
//		MVP = mvp; //System.out.println("set sword MVP");
//	}
	/*
	 * public void setVertices(int indexCounter, float cosRadian, float
	 * sinRadian, float fX, float fY, float fZ, float[] array) {
	 * 
	 * // System.out.println("");
	 * 
	 * float dX = fX * cosRadian - fY * sinRadian; float dY = fY * cosRadian +
	 * fX * sinRadian; float dZ = fZ; //
	 * System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" + // dY
	 * + "\t\tdZ:" + dZ);
	 * 
	 * dX *= height; dY *= height; dZ *= height; //
	 * System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" + // dY
	 * + "\t\tdZ:" + dZ);
	 * 
	 * dX += xyz.x; dY += xyz.y; dZ += xyz.z; //
	 * System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" + // dY
	 * + "\t\tdZ:" + dZ);
	 * 
	 * array[indexCounter++] = (dX); array[indexCounter++] = (dY);
	 * array[indexCounter] = (dZ);
	 * 
	 * }
	 */

	public void setRotationX(float x) {
		//degreeRotateX = x;
		rotationXYZ.x=x;
	}
	
	public void setRotationXYZ(float x, float y, float z) {
		rotationXYZ.x = x;
		rotationXYZ.y = y;
		rotationXYZ.z = z;
	}

	public void setRotationXYZ(Vector3f nxyz) {
		rotationXYZ.x = nxyz.x;
		rotationXYZ.y = nxyz.y;
		rotationXYZ.z = nxyz.z;
		//if(attack!=null)
		//{attack.setHandleXYZ(nxyz);}
	}

	public void setRotationY(float x) {
		//degreeRotateY = x;
		rotationXYZ.y=x;
	}

	public void setXYZ(float x, float y, float z) {
		xyz.x = x;
		xyz.y = y;
		xyz.z = z;
		// move();
	}

	public void setXY(float newX, float newY) {
		xyz.x = newX;
		xyz.y = newY;
	}

	public void setZ(float newZ) {
		xyz.z = newZ;
	}

	public void startAttackAction(interface_weapon_rotation iwrNew)
	{
		actionStart = gt.getTime();

		jointsOld = jointsNew;
		//jointsNew = new human3_weapon_sword_simple_rotation("human3_weapon_sword_simple_default");

		jointsNew = iwrNew;
	}
}