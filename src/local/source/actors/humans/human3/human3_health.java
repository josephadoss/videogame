package local.source.actors.humans.human3;

public class human3_health {

	boolean shouldUpdateHealthBar = false;
	int maxHealthPoints = 1000;
	int healthPoints = 50;

	public human3_health() {
	}

	public void addHP(int add) {
		this.healthPoints += add;
		if (this.healthPoints > maxHealthPoints) {
			healthPoints = maxHealthPoints;
		}
		shouldUpdateHealthBar = true;
	}

	public int getHP() {
		return healthPoints;
	}

	public int getHPPercentage() {
		return (int) (healthPoints / maxHealthPoints * 100);
	}

	public float getHPPercentageAsFloatZeroToOne() {
		return (float) healthPoints / (float) maxHealthPoints;
	}

	public int getMaxHP() {
		return maxHealthPoints;
	}

	public boolean getShouldUpdateHealthBar() {
		boolean result = shouldUpdateHealthBar;

		if (shouldUpdateHealthBar) {
			shouldUpdateHealthBar = false;
		}

		return result;
	}

	public void regen() {
		if (healthPoints > 0) {
			addHP(1);
		}
	}

	public void setHP(int h) {
		healthPoints = h;
		if (healthPoints > maxHealthPoints) {
			healthPoints = maxHealthPoints;
		}
	}

	public void subtractHP(int sub) {
		healthPoints -= sub;
		if (healthPoints < 0) {
			healthPoints = 0;
		}
		shouldUpdateHealthBar = true;
	}
}
