package local.source.actors.humans.human3;

import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import local.source.game_time;
import local.source.actors.humans.human2.human2_actionState_human2_lowerBody_state;

import org.joml.Vector3f;




public class human3_joint_rotations_upper_body {
	
	human3_joint_rotations_upper_body_manager manager = human3_joint_rotations_upper_body_manager.getInstance();

	
String strNextActionStateHolder = "";
	
	boolean isAttack = false;
	boolean isWalking = false;
	boolean isInterruptable = false;
	boolean isDying = false;
	boolean isDead=false;
	game_time gt = game_time.getInstance();
	Vector3f[] rotations = new Vector3f[] { new Vector3f(), //  navel
			new Vector3f(), // nips
			new Vector3f(),// neck lower
			new Vector3f(),//  neck upper
			new Vector3f(),//  top of head
			new Vector3f(),// l shoulder
			new Vector3f(),//  r shoulder
			new Vector3f(),//  l elbow
			new Vector3f(), //  r elbow
			new Vector3f(),//   l wrist
			new Vector3f(),//   r wrist
			new Vector3f(),//   l hand
			new Vector3f()//  r hand
	};;
	int timeToComplete;
	float distGroundToCrotch = .5f;
	String strState = "none";
	//int enumVal;
	
	float breathOffset = (new Random()).nextFloat()%5f +3f; 
	float nipToLShoulder=-45f;
	float nipToRShoulder=45f;

	human2_actionState_human2_lowerBody_state nextState;

	public human3_joint_rotations_upper_body( String strFile) {
		//enumVal = val;
		strState=strFile;
		readFromActionStateFile(strFile);
		
		manager.put(strFile, this);
	}

	public void breath()
	{
		int time = gt.getTime();		
		boolean up = ((time/1000)%2 == 0 )?true:false;
		int now = (time%1000) ;
		
		if(!up)
		{ now = 1000-now;}
				
		//System.out.println("breathOffset : "+ breathOffset);
		
		rotations[5].x= nipToLShoulder+ breathOffset*((float)now/1000f);
		rotations[6].x= nipToRShoulder- breathOffset*((float)now/1000f);
	}
	
	public Vector3f[] getArray() {
		breath();
		return rotations;
	}

	public float getDistGroundToCrotch() {
		return distGroundToCrotch;
	}

	//public int getEnumVal() {
	//	return enumVal;
	////}

	public human3_joint_rotations_upper_body getNextState() {
		//return nextState;
		return manager.get(strNextActionStateHolder);
	}

	public String getState() {
		return strState;
	}

	public int getTimeToComplete() {
		return timeToComplete;
	}

	public boolean isAttack() {
		return isAttack;
	}
	
	public boolean isDead()
	{return isDead;}
	
	public boolean isDying()
	{ return isDying;}
	
	public boolean isInterruptable()
	{
		return isInterruptable;
	}
	
	public boolean isWalking()
	{ return isWalking; }

	public void readFromActionStateFile(String fileName) {
		try {
/*
			File pathToFile = new File("resources" + "/" 
					+ "actors" + "/" 
					+ "states" + "/" 
					+ "human3" + "/" 
					+ "joints" + "/" 
					+ fileName + ".xml");*/
	
			String pathToFile =  "/" 
					+ "actors" + "/" 
					+ "states" + "/" 
					+ "human3" + "/" 
					+ "joints" + "/" 
					+ fileName + ".xml";
			
			
			URL jointURL = getClass().getResource(pathToFile);
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
			Document document = documentBuilder.parse(jointURL.openStream());//pathToFile);
			document.getDocumentElement().normalize();
			NodeList nList = document.getElementsByTagName("actionState");

			//System.out.println("About to enter loop of actionStates");
			for (int i = 0; i < nList.getLength(); i++) {
				//System.out.println("entered array of actionStates");
				Node n = nList.item(i);
				//System.out.println("found a node");
				//System.out.println("n.getNodeName() " + n.getNodeName());
				//System.out.println("n.getLocalName() " + n.getLocalName());
				//System.out.println("n.getNodeValue() " + n.getNodeValue());
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					//System.out.println("entered if statement for n");

					Element e = (Element) n;
					//System.out.println("converted node n to element e");
					

					//if (e.getNodeType() == Node.ELEMENT_NODE && e.getNodeName() == "joints") {
						for (int j = 0; j < e.getElementsByTagName("joints").getLength(); j++) {
							//System.out.println("found an element called joints");
							Element ee = (Element) e.getElementsByTagName("joints").item(j);
							//System.out.println("converted joints to ee element");
							for(int k=0; k<ee.getElementsByTagName("joint").getLength();k++)
							{
								//System.out.println("found an element called joint");
								Element eee = (Element) ee.getElementsByTagName("joint").item(k);
								//System.out.println("converted joint to eee element");
								String eeeName = eee.getAttribute("name");
								String eeeIndex = eee.getAttribute("index");
								String eeeX = eee.getAttribute("x");
								String eeeY = eee.getAttribute("y");
								String eeeZ = eee.getAttribute("z");
								
								//System.out.println("eeeName : "+eeeName);
								//System.out.println("eeeIndex : "+eeeIndex);
								//System.out.println("eeeX : "+eeeX);
								//System.out.println("eeeY : "+eeeY);
								//System.out.println("eeeZ : "+eeeZ);
								
								int index = Integer.parseInt(eeeIndex);
								float x = Float.parseFloat(eeeX);
								float y = Float.parseFloat(eeeY);
								float z = Float.parseFloat(eeeZ);
								
								rotations[index].x=x;
								rotations[index].y=y;
								rotations[index].z=z;
							}
						}
					

					Element eTTC = (Element) e.getElementsByTagName("timeToComplete").item(0);
					String strTTC = eTTC.getAttribute("value");
					int intTTC = Integer.parseInt(strTTC);
					timeToComplete=intTTC;
					
					//System.out.println("time to complete : "+this.timeToComplete);
					
					Element eIsWalk = (Element) e.getElementsByTagName("isWalking").item(0);
					String strIsWalk = eIsWalk.getAttribute("value").toLowerCase().trim();
					//System.out.println("strIsWalk : " + strIsWalk);
					isWalking = (strIsWalk.compareTo("true")==0);
					

					Element eIsAttack = (Element) e.getElementsByTagName("isAttack").item(0);
					String strIsAttack = eIsAttack.getAttribute("value").toLowerCase().trim();
					//System.out.println("strIsWalk : " + strIsWalk);
					isAttack = (strIsAttack.compareTo("true")==0);
					
					Element eIsInterruptable = (Element) e.getElementsByTagName("isInterruptable").item(0);
					String strIsInterruptable = eIsInterruptable.getAttribute("value").toLowerCase().trim();
					//System.out.println("strIsWalk : " + strIsWalk);
					isInterruptable = (strIsInterruptable.compareTo("true")==0);
				
					
					Element eIsDying = (Element) e.getElementsByTagName("isDying").item(0);
					String strIsDying = eIsDying.getAttribute("value").toLowerCase().trim();
					//System.out.println("strIsWalk : " + strIsWalk);
					isDying = (strIsDying.compareTo("true")==0);
					
					Element eIsDead = (Element) e.getElementsByTagName("isDead").item(0);
					String strIsDead = eIsDead.getAttribute("value").toLowerCase().trim();
					//System.out.println("strIsWalk : " + strIsWalk);
					isDead = (strIsDead.compareTo("true")==0);
					
					//System.out.println("is walking : " + isWalking);
					/*
					Element eDistGtC = (Element) e.getElementsByTagName("distGroundToCrotch").item(0);
					String strDistGtC = eDistGtC.getAttribute("value");
					float fltDistGtC = Float.parseFloat(strDistGtC);
					distGroundToCrotch = fltDistGtC;*/
					
					Element eNext = (Element) e.getElementsByTagName("nextActionState").item(0);
					String strNext = eNext.getAttribute("value");
					strNextActionStateHolder=strNext;
					
					break;
				}
			}

		} catch (Exception e) {
			System.out.println("exception caught in human3_joint_rotations_upper_body.readXMLfile: " + e.getMessage() + " " + e.getStackTrace());
		}

	}

	//public void setNextState(actionState_human2_lowerBody_parent next) {
	//	nextState = next;
	//}

}
