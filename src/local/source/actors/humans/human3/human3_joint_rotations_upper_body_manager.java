package local.source.actors.humans.human3;

import java.util.HashMap;


public class human3_joint_rotations_upper_body_manager {
	private static human3_joint_rotations_upper_body_manager instance = null;
	

	HashMap<String,human3_joint_rotations_upper_body> hashMapStates = new HashMap<String,human3_joint_rotations_upper_body>();

	
	private human3_joint_rotations_upper_body_manager()
	{}
	
	public static human3_joint_rotations_upper_body_manager getInstance()
	{
		if(instance==null)
		{ instance = new human3_joint_rotations_upper_body_manager();}
		return instance;		
	}
	
	public human3_joint_rotations_upper_body get( String key)
	{
		human3_joint_rotations_upper_body result = hashMapStates.get(key);
		
		if(result==null)
		{
			result =  new human3_joint_rotations_upper_body(key);
			this.put(key, result);
		}
	
		return result;
	}
	

	
	public void put(String key, human3_joint_rotations_upper_body val)
	{
		if(!hashMapStates.containsKey(key))
		{ hashMapStates.put(key, val);}
	}

}
