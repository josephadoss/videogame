package local.source.actors.humans.human3;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import local.source.cuboid;

public class human3_body_part_neck implements interface_human3_body_part {

	cuboid c = new cuboid(1f,1f,1f);
	
	float fat,muscle;
	int gender;
	String bodyPart = "";
	human3_outfit outfit;
	
	public human3_body_part_neck(String bodyPart)
	{
		this.bodyPart=bodyPart;		
	}
	

	
	public void draw()
	{ c.draw();}
	
	
	public Vector3f[] getBottomVerticesBack(){return new Vector3f[1];}
	
	public Vector3f[] getBottomVerticesFront(){return new Vector3f[1];}
	
	public Vector3f[] getTopVerticesBack(){return new Vector3f[1];}
	
	public Vector3f[] getTopVerticesFront(){return new Vector3f[1];}
	
	
	public Vector3f[] getVerticesBack() {
		return new Vector3f[1];
	}

	public Vector3f[] getVerticesFront() {
		return new Vector3f[1];
	}

	
	public void resetTexture(){}
	
	Vector3f[] adjustBottomVerticesBack =null;
	Vector3f[] adjustBottomVerticesFront =null;
	Vector3f[] adjustTopVerticesBack =null;
	Vector3f[] adjustTopVerticesFront =null;
	
	public void setAdjustBottomVerticesBack(Vector3f[] adjustBot){adjustBottomVerticesBack=adjustBot;}
	public void setAdjustBottomVerticesFront(Vector3f[] adjustBot){adjustBottomVerticesFront=adjustBot;}
	
	public void setAdjustTopVerticesBack(Vector3f[] adjustTop){adjustTopVerticesBack=adjustTop;}
	public void setAdjustTopVerticesFront(Vector3f[] adjustTop){adjustTopVerticesFront=adjustTop;}
	
	public void setBodyHeight(float height)
	{
		float headHeight = 1f / 8f;
		float neck = headHeight * .2f;
		
		float l = neck*height;
		float w = neck*height ;
		float h = neck*height*2f;
		c = new cuboid(l,w,h);
	}
	
	public void setBodyPart(String newbp)
	{ this.bodyPart = newbp;}
	
	public void setCenterXYZ(float x, float y, float z)
	{ c.setCenterXYZ(x,y,z);}
	
	public void setFacingRotationZ(float z)
	{ c.setRotationZ(z);}
	
	public void setFatMuscleGenderJoints(float fat, float muscle, int gender,human3_joints j)
	{
		this.fat = fat;
		this.muscle=muscle;
		this.gender=gender;
	}
	
//	public void setMVP(Matrix4f mvp)
//	{ c.setMVP(mvp);}


	public void setOutfit(human3_outfit o)
	{outfit = o;}
	
	public void setRotationXYZ(float x, float y, float z)
	{ c.setRotationXYZ(x,y,z);}
	
	
	public void setSkinRGB(float r, float g, float b)
	{ c.setRGB( r, g, b );}
	
}
