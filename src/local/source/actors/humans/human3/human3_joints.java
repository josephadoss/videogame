package local.source.actors.humans.human3;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL32.*;
//import static org.lwjgl.opengl.GL40.*;
//import static org.lwjgl.opengl.GL45.*;

import java.nio.*;
import java.util.Calendar;

import org.lwjgl.BufferUtils;

import local.source.game_time;
import local.source.manager_mvp;
import local.source.attacks.interface_attack;

import org.joml.QuaternionfInterpolator;
import org.joml.Quaternionf;
import org.joml.Matrix4f;
import org.joml.Vector3f;

public class human3_joints {

	// when you rework the joints ( and you really need to )
	// consider breathing and moving more than just the shoulders
	// change the center of the upper back to move a little too
	// that should be enough to move the entire upper torso when skins come
	// along
	// or maybe add a few new joints for chest center or tommy and move those
	// around

	// add more joints for lowerneck and upper neck... no real need for center
	// of neck

	float headHeight = 1f / 8f;
	float neckHeight = headHeight * .2f;

	int indexGround = 0;
	int indexCrotch = 1;
	int indexLHip = 2;
	int indexRHip = 3;
	int indexLKnee = 4;
	int indexRKnee = 5;
	int indexLAnkle = 6;
	int indexRAnkle = 7;
	int indexLFoot = 8;
	int indexRFoot = 9;
	int indexNavel = 10;
	int indexNipple = 11;
	int indexNeckLower = 12;
	int indexNeckUpper = 13;
	int indexTopOfHead = 14;
	int indexLShoulder = 15;
	int indexRShoulder = 16;
	int indexLElbow = 17;
	int indexRElbow = 18;
	int indexLWrist = 19;
	int indexRWrist = 20;
	int indexLHand = 21;
	int indexRHand = 22;

	Vector3f[] vertices = { new Vector3f(0f, 0f, 0f), // 0 ground
			new Vector3f(0f, 0f, 0f), // 1 crotch
			new Vector3f(0f, 0f, 0f), // 2 left hip
			new Vector3f(0f, 0f, 0f), // 3 right hip
			new Vector3f(0f, 0f, 0f), // 4 left knee
			new Vector3f(0f, 0f, 0f), // 5 right knee
			new Vector3f(0f, 0f, 0f), // 6 left ankle
			new Vector3f(0f, 0f, 0f), // 7 right ankle
			new Vector3f(0f, 0f, 0f), // 8 left foot
			new Vector3f(0f, 0f, 0f), // 9 right foot
			new Vector3f(0f, 0f, 0f), // 10 navel
			new Vector3f(0f, 0f, 0f), // 11 nipple
			new Vector3f(0f, 0f, 0f), // 12 neck lower
			new Vector3f(0f, 0f, 0f), // 13 neck upper
			new Vector3f(0f, 0f, 0f), // 14 top of head
			new Vector3f(0f, 0f, 0f), // 15 left shoulder
			new Vector3f(0f, 0f, 0f), // 16 right shoulder
			new Vector3f(0f, 0f, 0f), // 17 left elbow
			new Vector3f(0f, 0f, 0f), // 18 right elbow
			new Vector3f(0f, 0f, 0f), // 19 left wrist
			new Vector3f(0f, 0f, 0f), // 20 right wrist
			new Vector3f(0f, 0f, 0f), // 21 left hand
			new Vector3f(0f, 0f, 0f) // 22 right hand
	};

	Vector3f[] rotations = { new Vector3f(0f, 0f, 0f), // 0 ground
			new Vector3f(0f, 0f, 0f), // 1 crotch
			new Vector3f(0f, 0f, 0f), // 2 left hip
			new Vector3f(0f, 0f, 0f), // 3 right hip
			new Vector3f(0f, 0f, 0f), // 4 left knee
			new Vector3f(0f, 0f, 0f), // 5 right knee
			new Vector3f(0f, 0f, 0f), // 6 left ankle
			new Vector3f(0f, 0f, 0f), // 7 right ankle
			new Vector3f(0f, 0f, 0f), // 8 left foot
			new Vector3f(0f, 0f, 0f), // 9 right foot
			new Vector3f(0f, 0f, 0f), // 10 navel
			new Vector3f(0f, 0f, 0f), // 11 nipple
			new Vector3f(0f, 0f, 0f), // 12 neck lower
			new Vector3f(0f, 0f, 0f), // 13 neck upper
			new Vector3f(0f, 0f, 0f), // 14 top of head
			new Vector3f(0f, 0f, 0f), // 15 left shoulder
			new Vector3f(0f, 0f, 0f), // 16 right shoulder
			new Vector3f(0f, 0f, 0f), // 17 left elbow
			new Vector3f(0f, 0f, 0f), // 18 right elbow
			new Vector3f(0f, 0f, 0f), // 19 left wrist
			new Vector3f(0f, 0f, 0f), // 20 right wrist
			new Vector3f(0f, 0f, 0f), // 21 left hand
			new Vector3f(0f, 0f, 0f) // 22 right hand
	};

	game_time gt = game_time.getInstance();

	int actionLowerStart = gt.getTime();
	int actionUpperStart = gt.getTime();

	byte[] jointsIndicesLines = { 0, 1 // ground to crotch
			, 1, 2 // crotch to l hip
			, 1, 3 // crotch to r hip
			, 2, 4 // l hip to l knee
			, 3, 5 // r hip to r knee
			, 4, 6 // l knee to l ankle
			, 5, 7 // r knee to r ankle
			, 6, 8 // l ankle to l foot
			, 7, 9 // r ankle to r foot
			, 1, 10 // crotch to navel
			, 10, 11 // navel to nips
			, 11, 12 // nips to neck bottom
			, 12, 13 // neck bottom to neck top
			, 13, 14 // neck top to top of head
			, 11, 15 // nip line to l shoulder
			, 11, 16 // nip line to r shoulder
			, 15, 17 // l shoulder to l elbow
			, 16, 18 // r shoulder to r elbow
			, 17, 19 // l elbow to l wrist
			, 18, 20 // r elbow to r wrist
			, 19, 21 // l wrist to l hand
			, 20, 22 // r wrist to r hand */
	};

	float[] jointsVerticesDraw = { 0f, 0f, 0f // ground 0
			, 0f, 0f, .5f // crotch 1
			, 0f, 0f, .5f // l hip 2
			, 0f, 0f, .5f // r hip 3
			, 0f, 0f, .25f // l knee 4
			, 0f, 0f, .25f // r knee 5
			, 0f, 0f, 0f // l ankle 6
			, 0f, 0f, 0f // r ankle 7
			, 0f, 0f, 0f // l foot 8
			, 0f, 0f, 0f // r foot 9
			, 0f, 0f, 0f // backNavelLine 10
			, 0f, 0f, 0f // backNipLine 11
			, 0f, 0f, 0f // back Neck Bottom 12
			, 0f, 0f, 0f // back Neck Top 13
			, 0f, 0f, 0f // headTop 14
			, 0f, 0f, 0f // l shoulder 15
			, 0f, 0f, 0f // r shoulder 16
			, 0f, 0f, 0f // l elbow 17
			, 0f, 0f, 0f // r elbow 18
			, 0f, 0f, 0f // l wrist 19
			, 0f, 0f, 0f // r wrist 20
			, 0f, 0f, 0f // l hand 21
			, 0f, 0f, 0f // r hand 22

	};

	boolean isBodyStateLowerChangeable = true;
	boolean isBodyStateUpperChangeable = true;

	int[] parentJoint = { 0, // 0 ground
			0, // 1 crotch
			1, // 2 left hip
			1, // 3 right hip
			2, // 4 left knee
			3, // 5 right knee
			4, // 6 left ankle
			5, // 7 right ankle
			6, // 8 left foot
			7, // 9 right foot
			1, // 10 navel
			10, // 11 nipple
			11, // 12 neck lower
			12, // 13 neck upper
			13, // 14 top of head
			11, // 15 left shoulder
			11, // 16 right shoulder
			15, // 17 left elbow
			16, // 18 right elbow
			17, // 19 left wrist
			18, // 20 right wrist
			19, // 21 left hand
			20 // 22 right hand
	};

	float[] distancesFromParent = { 0f, // 0 ground to ground
			0f, // 1 ground to crotch
			0f, // 2 crotch to left hip
			0f, // 3 crotch to right hip
			0f, // 4 left hip to left knee
			0f, // 5 right hip to right knee
			0f, // 6 left knee to left ankle
			0f, // 7 right knee to right ankle
			0f, // 8 left ankel to left foot
			0f, // 9 right ankel to right foot
			0f, // 10 crotch to navel
			0f, // 11 navel to nipple
			0f, // 12 nipple to neck lower
			0f, // 13 neck lower to neck upper
			0f, // 14 neck upper to top of head
			0f, // 15 nipple to left shoulder
			0f, // 16 nipple to right shoulder
			0f, // 17 left shoulder to left elbow
			0f, // 18 right shoulder to right elbow
			0f, // 19 left elbow to left wrist
			0f, // 20 right elbow to right wrist
			0f, // 21 left wrist to left hand
			0f // 22 right wrist to right hand
	};

	human3_joint_rotations_lower_body jointsLowerBodyNew = new human3_joint_rotations_lower_body("human3_lower_body_stand");
	human3_joint_rotations_lower_body jointsLowerBodyOld = new human3_joint_rotations_lower_body("human3_lower_body_stand");
	human3_joint_rotations_lower_body jointsLowerBodyDraw = new human3_joint_rotations_lower_body("human3_lower_body_stand");
	human3_joint_rotations_lower_body jointsLowerBodyDefault = new human3_joint_rotations_lower_body("human3_lower_body_stand");

	human3_joint_rotations_upper_body jointsUpperBodyNew = new human3_joint_rotations_upper_body("human3_upper_body_stand");
	human3_joint_rotations_upper_body jointsUpperBodyOld = new human3_joint_rotations_upper_body("human3_upper_body_stand");
	human3_joint_rotations_upper_body jointsUpperBodyDraw = new human3_joint_rotations_upper_body("human3_upper_body_stand");
	human3_joint_rotations_upper_body jointsUpperBodyDefault = new human3_joint_rotations_upper_body("human3_upper_body_stand");

	int jointsIndicesLinesCount;// = jointsIndicesLines.length;

	float jointsRed = 1f;
	float jointsBlue = 1f;
	float jointsGreen = 1f;

	int jointsVAOID = -1;
	int jointsVBOID = -1;
	int jointsVBOIID = -1;

	public int jointsGLMVPUniformLocation;
	public int jointsGLRGBAUniformLocation;
	public FloatBuffer jointsFB = BufferUtils.createFloatBuffer(16);

	public int jointsProgramId;
	public int jointsVertexShaderId;
	public int jointsFragmentShaderId;

	boolean isJointsIndexBound = false;

	//public Matrix4f MVP;

	public boolean isDrawJoints = false;

	public Vector3f xyz = new Vector3f();
	public float height = -1f;

	public float degreeRotateX = 0.0f;
	public float degreeRotateY = 0.0f;
	public float degreeRotateZ = 0.0f;

	public float moveDistance = .0003f;// * headHeight;
	// float armWidth = 327f;

	Quaternionf dest1 = new Quaternionf();
	Quaternionf dest2 = new Quaternionf();
	Vector3f v = new Vector3f();

	FloatBuffer verticesBufferJoints;

	float percentDoneLower = 0f;
	float percentDoneUpper = 0f;

	Vector3f diameterXYZ = new Vector3f();;

	public human3_joints() {
		// setHeight(6f);

		if (isDrawJoints) {
			this.glConstruct();
		}
	}

	public void bindJointsVertexData() {

		// for(int i = 0; i<draw.length;i++)
		// { draw[i]=jointsVerticesDraw[i]; }

		if (isDrawJoints) {
			if (verticesBufferJoints == null) {
				verticesBufferJoints = BufferUtils.createFloatBuffer(jointsVerticesDraw.length);
			}
			verticesBufferJoints.put(jointsVerticesDraw).flip();

			if (jointsVAOID == -1) {
				jointsVAOID = glGenVertexArrays();
			}
			glBindVertexArray(jointsVAOID);

			if (jointsVBOID == -1) {
				jointsVBOID = glGenBuffers();
			}
			glBindBuffer(GL_ARRAY_BUFFER, jointsVBOID);
			glBufferData(GL_ARRAY_BUFFER, verticesBufferJoints, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);

			if (!isJointsIndexBound) {
				jointsIndicesLinesCount = jointsIndicesLines.length;

				ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(jointsIndicesLinesCount);
				indicesBuffer.put(jointsIndicesLines);
				indicesBuffer.flip();

				jointsVBOIID = glGenBuffers();
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, jointsVBOIID);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

				isJointsIndexBound = true;
			}
		}
	}

	/*
	 * public void calcuateJointsQuaternions(float rX, float rY, float rZ) {
	 * dest1.w = 1f; dest1.x = 0f; dest1.y = 0f; dest1.z = 0f;
	 * 
	 * dest2.w = 1f; dest2.x = 0f; dest2.y = 0f; dest2.z = 0f;
	 * 
	 * dest2.rotate((float) Math.toRadians(rX), (float) Math.toRadians(rY),
	 * (float) Math.toRadians(rZ), dest1);
	 * 
	 * // dest1.normalize(); // dest1.invert(); dest1.transform(v); }
	 */

	// int second=0;
	// int newsecond=0;
	// boolean isNewSecond=false;

	public void calculateJointsVertices() {

		/*
		 * Calendar calendar = Calendar.getInstance(); newsecond=
		 * calendar.get(Calendar.SECOND); //System.out.println(newsecond);
		 * if(second!=newsecond) { second=newsecond; isNewSecond=true; } else {
		 * isNewSecond=false;
		 */

		double radian = Math.toRadians(degreeRotateZ);
		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		float fX, fY, fZ;

		int indexCount = 0;
		// float[] vertices = {};// .clone();// act.getVertices();

		int timeToCompleteLower = 1000;
		int timeToCompleteUpper = 1000;

		int now = gt.getTime();

		// timeToCompleteLower = h2bslNew.getTimeToComplete();
		timeToCompleteLower = jointsLowerBodyNew.getTimeToComplete();
		timeToCompleteUpper = jointsUpperBodyNew.getTimeToComplete();

		percentDoneLower = (float) ((float) (now - actionLowerStart) / (float) timeToCompleteLower);
		percentDoneUpper = (float) ((float) (now - actionUpperStart) / (float) timeToCompleteUpper);

		if (percentDoneLower > 1f) {
			isBodyStateLowerChangeable = true;
			percentDoneLower = 0f;
			jointsLowerBodyOld = jointsLowerBodyNew;
			if (jointsLowerBodyNew.getNextState() != null) {
				jointsLowerBodyNew = jointsLowerBodyNew.getNextState();
			} else {
				jointsLowerBodyNew = jointsLowerBodyDefault;
			}
			actionLowerStart = gt.getTime();
		}

		if (percentDoneUpper > 1f) {
			percentDoneUpper = 0f;

			isBodyStateUpperChangeable = true;
			jointsUpperBodyOld = jointsUpperBodyNew;
			if (jointsUpperBodyNew.getNextState() != null) {
				jointsUpperBodyNew = jointsUpperBodyNew.getNextState();
			} else {
				jointsUpperBodyNew = jointsUpperBodyDefault;
			}
			actionUpperStart = gt.getTime();

		}

		float o, n;
		Vector3f vO, vN;
		for (int i = 0; i < jointsLowerBodyDraw.getArray().length; i++) {

			vO = jointsLowerBodyOld.getArray()[i];// h2bslOld.getArray()[i];
			vN = jointsLowerBodyNew.getArray()[i];// h2bslNew.getArray()[i];
			jointsLowerBodyDraw.getArray()[i].x = vO.x + ((vN.x - vO.x) * percentDoneLower);
			jointsLowerBodyDraw.getArray()[i].y = vO.y + ((vN.y - vO.y) * percentDoneLower);
			jointsLowerBodyDraw.getArray()[i].z = vO.z + ((vN.z - vO.z) * percentDoneLower);

			rotations[i].x = jointsLowerBodyDraw.getArray()[i].x;
			rotations[i].y = jointsLowerBodyDraw.getArray()[i].y;
			rotations[i].z = jointsLowerBodyDraw.getArray()[i].z;
		}

		// rIndex = 0;
		for (int i = 0; i < jointsUpperBodyDraw.getArray().length; i++) {
			vO = jointsUpperBodyOld.getArray()[i];// h2bslOld.getArray()[i];
			vN = jointsUpperBodyNew.getArray()[i];// h2bslNew.getArray()[i];
			jointsUpperBodyDraw.getArray()[i].x = vO.x + ((vN.x - vO.x) * percentDoneUpper);
			jointsUpperBodyDraw.getArray()[i].y = vO.y + ((vN.y - vO.y) * percentDoneUpper);
			jointsUpperBodyDraw.getArray()[i].z = vO.z + ((vN.z - vO.z) * percentDoneUpper);

			rotations[i + 10].x = jointsUpperBodyDraw.getArray()[i].x;
			rotations[i + 10].y = jointsUpperBodyDraw.getArray()[i].y;
			rotations[i + 10].z = jointsUpperBodyDraw.getArray()[i].z;
		}

		n = jointsLowerBodyNew.getDistGroundToCrotch();//
		// h2bslNew.getDistGroundToCrotch();
		o = jointsLowerBodyOld.getDistGroundToCrotch();//
		// h2bslOld.getDistGroundToCrotch()
		float dist = o + ((n - o) * percentDoneLower);
		distancesFromParent[indexCrotch] = dist; // System.out.println("dist :
													// "+dist);
		jointsLowerBodyDraw.setDistGroundToCrotch(dist);
		this.diameterXYZ.z = (height / 2f + dist * (height)) / 2f;

		for (int i = 0; i < vertices.length; i++) {
			v.x = 0f;
			v.y = 0f;
			v.z = distancesFromParent[i];

			dest1.w = 1f;
			dest1.x = 0f;
			dest1.y = 0f;
			dest1.z = 0f;

			dest2.w = 1f;
			dest2.x = 0f;
			dest2.y = 0f;
			dest2.z = 0f;

			float x, y, z;
			x = rotations[i].x;
			y = rotations[i].y;
			z = rotations[i].z;

			// if(i==1)
			// { z = degreeRotateZ;}
			/*
			 * if(i<4 || i>9) { x+=degreeRotateX; y+=degreeRotateY;
			 * //z+=degreeRotateZ; } else { x+=degreeRotateX+180f;
			 * y+=degreeRotateY+180f; }
			 */

			x = x % 360.0f;
			y = y % 360.0f;
			z = z % 360.0f;

			dest2.rotate((float) Math.toRadians(x), (float) Math.toRadians(y), (float) Math.toRadians(z), dest1);

			dest1.transform(v);

			// if(isNewSecond&&i==2)
			// {
			// System.out.println("i:"+i+" \tx:"+x+" \ty:"+y+" \tz:"+z+"
			// \tdist:"+distancesFromParent[i]+" \tv.x:"+v.x+" \tv.y:"+v.y+"
			// \tv.z:"+v.z);
			// System.out.println("drX:"+degreeRotateX+" drY:"+degreeRotateY+"
			// drZ:"+degreeRotateZ);
			// }

			Vector3f vv = vertices[i];
			Vector3f vvOffset = vertices[parentJoint[i]];

			if (vvOffset == null) {
				vv.x = v.x;
				vv.y = v.y;
				vv.z = v.z;
				// System.out.println("vv.x:"+vv.x+"\tvv.y:"+vv.y+"\tvv.z:"+vv.z);
			} else {
				vv.x = v.x + vvOffset.x;
				vv.y = v.y + vvOffset.y;
				vv.z = v.z + vvOffset.z;
			}
		}

		for (int i = 0; i < vertices.length; i++) {

			fX = vertices[i].x;// * lengthX/* X */;
			fY = vertices[i].y;// * widthY/* Y */;
			fZ = vertices[i].z;// * heightZ/* Z */;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= height;
			dY *= height;
			dZ *= height;

			dX += xyz.x;
			dY += xyz.y;
			dZ += xyz.z;

			int newI = i * 3;
			jointsVerticesDraw[newI] = (dX);
			jointsVerticesDraw[newI + 1] = (dY);
			jointsVerticesDraw[newI + 2] = (dZ);
		}

		int index = indexGround;
		// System.out.println("ground : x:"+jointsVerticesDraw[index]+"
		// y:"+jointsVerticesDraw[index+1]+" z:"+jointsVerticesDraw[index+2]);

		index = indexCrotch;
		// System.out.println("crotch : x:"+jointsVerticesDraw[index]+"
		// y:"+jointsVerticesDraw[index+1]+" z:"+jointsVerticesDraw[index+2]);

		index = indexLHip;
		// System.out.println("lhip : x:"+jointsVerticesDraw[index]+"
		// y:"+jointsVerticesDraw[index+1]+" z:"+jointsVerticesDraw[index+2]);

	}

	public void draw() { // System.out.println("human2.draw() just entered" );

		if (height <= 0f) {
			setHeight(6);
		}

		calculateJointsVertices();

		if (isDrawJoints) {
			drawJoints();
		}

	}

	public void drawJoints() {// System.out.println("human.drawJoints");
		bindJointsVertexData();
		glUseProgram(jointsProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(jointsGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(jointsFB));
		glUniform4f(jointsGLRGBAUniformLocation, jointsRed, jointsGreen, jointsBlue, 1.0f);

		glBindVertexArray(jointsVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, jointsVBOIID);
		glDrawElements(GL_LINES, jointsIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glDrawElements(GL_POINTS, jointsIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
	}

	public Vector3f getRotationFromInput(int index) {
		// index = indexNipple;
		Vector3f result = new Vector3f();
		result.x = rotations[index].x;
		result.y = rotations[index].y;
		result.z = rotations[index].z;

		// int index = indexLHand *3;
		// result.x = jointsVerticesDraw[index];
		// result.y = jointsVerticesDraw[index+1];
		// result.z = jointsVerticesDraw[index+2];
		// System.out.println("getting location of left hand x:"+result.x+"
		// y:"+result.y+" z:"+result.z);
		return result;
	}

	public Vector3f getRotationOfLeftHand() {
		Vector3f result = new Vector3f();
		result.x = rotations[indexLHand].x;
		result.y = rotations[indexLHand].y;
		result.z = rotations[indexLHand].z;

		// int index = indexLHand *3;
		// result.x = jointsVerticesDraw[index];
		// result.y = jointsVerticesDraw[index+1];
		// result.z = jointsVerticesDraw[index+2];
		// System.out.println("getting location of left hand x:"+result.x+"
		// y:"+result.y+" z:"+result.z);
		return result;
	}

	public Vector3f getRotationOfNeckLower() {
		Vector3f result = new Vector3f();
		result.x = rotations[indexNeckLower].x;
		result.y = rotations[indexNeckLower].y;
		result.z = rotations[indexNeckLower].z;

		// int index = indexLHand *3;
		// result.x = jointsVerticesDraw[index];
		// result.y = jointsVerticesDraw[index+1];
		// result.z = jointsVerticesDraw[index+2];
		// System.out.println("getting location of left hand x:"+result.x+"
		// y:"+result.y+" z:"+result.z);
		return result;
	}

	public Vector3f getRotationOfNeckUpper() {
		int index = indexNeckUpper;
		Vector3f result = new Vector3f();
		result.x = rotations[index].x;
		result.y = rotations[index].y;
		result.z = rotations[index].z;

		// int index = indexLHand *3;
		// result.x = jointsVerticesDraw[index];
		// result.y = jointsVerticesDraw[index+1];
		// result.z = jointsVerticesDraw[index+2];
		// System.out.println("getting location of left hand x:"+result.x+"
		// y:"+result.y+" z:"+result.z);
		return result;
	}

	public Vector3f getRotationOfNipLine() {
		int index = indexNipple;
		Vector3f result = new Vector3f();
		result.x = rotations[index].x;
		result.y = rotations[index].y;
		result.z = rotations[index].z;

		// int index = indexLHand *3;
		// result.x = jointsVerticesDraw[index];
		// result.y = jointsVerticesDraw[index+1];
		// result.z = jointsVerticesDraw[index+2];
		// System.out.println("getting location of left hand x:"+result.x+"
		// y:"+result.y+" z:"+result.z);
		return result;
	}

	public Vector3f[] getVerticesNotAdjustedForHeight() {
		return vertices;
	}

	public Vector3f getVertexFromInput(int index) {
		Vector3f result = new Vector3f();
		index = index * 3;
		result.x = jointsVerticesDraw[index];
		result.y = jointsVerticesDraw[index + 1];
		result.z = jointsVerticesDraw[index + 2];
		// System.out.println("getting location of left hand x:"+result.x+"
		// y:"+result.y+" z:"+result.z);
		return result;
	}

	public Vector3f getVertexNotAdjustedForHeight(int index) {
		return vertices[index];
	}

	public Vector3f getVertexOfCrotch() {
		Vector3f result = new Vector3f();
		int index = indexCrotch * 3;
		result.x = jointsVerticesDraw[index];
		result.y = jointsVerticesDraw[index + 1];
		result.z = jointsVerticesDraw[index + 2];
		// System.out.println("getting location of left hand x:"+result.x+"
		// y:"+result.y+" z:"+result.z);
		return result;
	}

	public Vector3f getVertexOfHeadTop() {
		Vector3f result = new Vector3f();
		int index = indexTopOfHead * 3;
		result.x = jointsVerticesDraw[index];
		result.y = jointsVerticesDraw[index + 1];
		result.z = jointsVerticesDraw[index + 2];
		// System.out.println("getting location of left hand x:"+result.x+"
		// y:"+result.y+" z:"+result.z);
		return result;
	}

	public Vector3f getVertexOfLeftHand() {
		Vector3f result = new Vector3f();
		int index = indexLHand * 3;
		result.x = jointsVerticesDraw[index];
		result.y = jointsVerticesDraw[index + 1];
		result.z = jointsVerticesDraw[index + 2];
		// System.out.println("getting location of left hand x:"+result.x+"
		// y:"+result.y+" z:"+result.z);
		return result;
	}

	public Vector3f getVertexOfNavel() {
		Vector3f result = new Vector3f();
		int index = indexNavel * 3;
		result.x = jointsVerticesDraw[index];
		result.y = jointsVerticesDraw[index + 1];
		result.z = jointsVerticesDraw[index + 2];
		// System.out.println("getting location of left hand x:"+result.x+"
		// y:"+result.y+" z:"+result.z);
		return result;
	}

	public Vector3f getVertexOfNeckLower() {
		Vector3f result = new Vector3f();
		int index = indexNeckLower * 3;
		result.x = jointsVerticesDraw[index];
		result.y = jointsVerticesDraw[index + 1];
		result.z = jointsVerticesDraw[index + 2];
		// System.out.println("getting location of left hand x:"+result.x+"
		// y:"+result.y+" z:"+result.z);
		return result;
	}

	public Vector3f getVertexOfNeckUpper() {
		Vector3f result = new Vector3f();
		int index = indexNeckUpper * 3;
		result.x = jointsVerticesDraw[index];
		result.y = jointsVerticesDraw[index + 1];
		result.z = jointsVerticesDraw[index + 2];
		// System.out.println("getting location of left hand x:"+result.x+"
		// y:"+result.y+" z:"+result.z);
		return result;
	}

	public Vector3f getVertexOfNipLine() {
		Vector3f result = new Vector3f();
		int index = indexNipple * 3;
		result.x = jointsVerticesDraw[index];
		result.y = jointsVerticesDraw[index + 1];
		result.z = jointsVerticesDraw[index + 2];
		// System.out.println("getting location of left hand x:"+result.x+"
		// y:"+result.y+" z:"+result.z);
		return result;
	}

	/*
	 * public void faceLeft() { degreeRotateZ = (degreeRotateZ + turnDegree) %
	 * 360.0f; // move(); }
	 * 
	 * public void facePoint(float newX, float newY, float newZ) { float dX =
	 * newX - xyz.x; float dY = newY - xyz.y; degreeRotateZ = (float)
	 * (Math.atan2(dY, dX) * 180.0f / Math.PI); }
	 * 
	 * public void faceRight() { float change = (degreeRotateZ - turnDegree);
	 * degreeRotateZ = (float) (change <= 0.0d ? (360.0d - turnDegree) : change
	 * % 360.0d); // move(); }
	 * 
	 * public float getFacingDegrees() { return degreeRotateZ; }
	 * 
	 * public float getHeight() { return height; }
	 */
	public void glCleanup() {
		glUseProgram(0);
		if (jointsProgramId != 0) {
			if (jointsVertexShaderId != 0) {
				glDetachShader(jointsProgramId, jointsVertexShaderId);
			}
			if (jointsFragmentShaderId != 0) {
				glDetachShader(jointsProgramId, jointsFragmentShaderId);
			}
			glDeleteProgram(jointsProgramId);
		}

	}

	public void glConstruct() {
		if (isDrawJoints) {
			glConstructJoints();
		}

	}

	public void glConstructJoints() {
		try {
			try {
				jointsProgramId = glCreateProgram();

				glCreateVertexShaderJoints("#version 130\nuniform mat4 MVP; in vec3 position; void main() { gl_Position =MVP* vec4(position, 1.0);  gl_PointSize = 3.0; }");

				glCreateFragmentShaderJoints("#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(jointsProgramId);

				glUseProgram(jointsProgramId);
				jointsGLMVPUniformLocation = glGetUniformLocation(jointsProgramId, "MVP");
				jointsGLRGBAUniformLocation = glGetUniformLocation(jointsProgramId, "RGBA");
				glUseProgram(0);

				glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (jointsProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glCreateFragmentShaderJoints(String shaderCode) throws Exception {
		jointsFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, jointsProgramId);
	}

	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShaderJoints(String shaderCode) throws Exception {
		jointsVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, jointsProgramId);
	}

	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}

	public boolean isAttacking() {
		return (jointsUpperBodyNew.isAttack());// ||
												// jointsLowerBodyNew.isAttack()
												// );
	}

	public boolean isDead() {
		//System.out.println("isDead : "+jointsUpperBodyNew.isDead());
		return jointsUpperBodyNew.isDead(); 
	}

	public boolean isDying() {
		return jointsUpperBodyNew.isDying();
	}

	public boolean isInterruptable() {
		if(jointsUpperBodyNew.isDead()) {return false;}
		
		if (jointsUpperBodyNew.isInterruptable() && jointsLowerBodyNew.isInterruptable()) {
			return true;
		}
		return false;
	}

	/*
	 * public void moveAttack() {
	 * 
	 * if(!jointsUpperBodyNew.isInterruptable() ||
	 * !jointsLowerBodyNew.isInterruptable()) {return;}
	 * 
	 * if (!jointsUpperBodyNew.isAttack()) { // System.out.println("upper body
	 * new isn't stand, so entered stand // code"); actionUpperStart =
	 * gt.getTime(); jointsUpperBodyOld = jointsUpperBodyDraw;
	 * 
	 * jointsUpperBodyNew = new
	 * human3_joint_rotations_upper_body("human3_upper_body_punch1");
	 * 
	 * } }
	 */

	public void moveAttack(interface_attack a) {
		if (!jointsUpperBodyNew.isInterruptable() || !jointsLowerBodyNew.isInterruptable()) {
			return;
		}

		if (!jointsUpperBodyNew.isAttack()) {
			actionUpperStart = gt.getTime();

			jointsUpperBodyOld = jointsUpperBodyNew;

			human3_joint_rotations_lower_body lnext = a.getHuman3BodyStateLower();
			// human3_joint_rotations_upper_body unext =
			// a.getHuman3BodyStateUpper();

			jointsUpperBodyNew = a.getHuman3BodyStateUpper();

			if (lnext != null) {
				actionLowerStart = gt.getTime();
				jointsLowerBodyOld = jointsLowerBodyNew;
				jointsLowerBodyNew = a.getHuman3BodyStateLower();
			}
		}

	}

	public void moveDying() {

		if (jointsUpperBodyNew.isDead() ) {
			return;
		}

		if (!jointsLowerBodyNew.getState().contains("engage")) {
			jointsLowerBodyOld = jointsLowerBodyDraw;
			jointsLowerBodyNew = new human3_joint_rotations_lower_body("human3_lower_body_dying_step1");

			actionLowerStart = gt.getTime();
		}

		if (!jointsUpperBodyNew.getState().contains("engage")) {
			jointsUpperBodyOld = jointsUpperBodyDraw;
			jointsUpperBodyNew = new human3_joint_rotations_upper_body("human3_upper_body_dying_step1");

			actionUpperStart = actionLowerStart;// gt.getTime();
		}

	}

	public void moveEngage() {

		if (!jointsUpperBodyNew.isInterruptable() || !jointsLowerBodyNew.isInterruptable()) {
			return;
		}

		if (!jointsLowerBodyNew.getState().contains("engage")) {
			jointsLowerBodyOld = jointsLowerBodyDraw;
			jointsLowerBodyNew = new human3_joint_rotations_lower_body("human3_lower_body_engage");

			actionLowerStart = gt.getTime();
		}

		if (!jointsUpperBodyNew.getState().contains("engage")) {
			jointsUpperBodyOld = jointsUpperBodyDraw;
			jointsUpperBodyNew = new human3_joint_rotations_upper_body("human3_upper_body_engage");

			actionUpperStart = actionLowerStart;// gt.getTime();
		}

	}

	public void moveForward() {

		if (!jointsUpperBodyNew.isInterruptable() || !jointsLowerBodyNew.isInterruptable()) {
			return;
		}

		if (!jointsLowerBodyNew.isWalking()) {
			jointsLowerBodyOld = jointsLowerBodyNew;
			jointsLowerBodyNew = new human3_joint_rotations_lower_body("human3_lower_body_first_step");

			actionLowerStart = gt.getTime();
		}

		if (jointsLowerBodyNew.isWalking() && !jointsUpperBodyNew.isWalking()) {
			if (!jointsUpperBodyNew.isAttack()) {
				jointsUpperBodyOld = jointsUpperBodyNew;

				if (jointsLowerBodyNew.getState().contains("left") || jointsLowerBodyNew.getState().contains("first")) {
					jointsUpperBodyNew = new human3_joint_rotations_upper_body("human3_upper_body_right_arm_forward");
				} else {
					jointsUpperBodyNew = new human3_joint_rotations_upper_body("human3_upper_body_left_arm_forward");
				}

				actionUpperStart = actionLowerStart;// gt.getTime();
			}
		}
	}

	public void moveSquat() {

		if (!jointsUpperBodyNew.isInterruptable() || !jointsLowerBodyNew.isInterruptable()) {
			return;
		}

		if (!jointsLowerBodyNew.getState().contains("squat")) {
			actionLowerStart = gt.getTime();
			jointsLowerBodyOld = jointsLowerBodyNew;

			jointsLowerBodyNew = new human3_joint_rotations_lower_body("human3_lower_body_squat");

		}

		if (!jointsUpperBodyNew.getState().contains("squat") && !jointsUpperBodyNew.isAttack()) {
			// System.out.println("upper body new isn't squat, so entered
			// squat code");
			// h2bslOld = h2bslNew;
			// h2bslNew = h2bslSquat;
			actionUpperStart = gt.getTime();
			jointsUpperBodyOld = jointsUpperBodyNew;

			jointsUpperBodyNew = new human3_joint_rotations_upper_body("human3_upper_body_squat");
			// interruptWithNewLowerBodyAction=true;
			// actionLowerStart =
			// gt.getTime()+h2bslCurrent.getTimeToComplete();
		}

		// }
	}

	public void moveStand() {

		if (!jointsUpperBodyNew.isInterruptable() || !jointsLowerBodyNew.isInterruptable()) {
			return;
		}

		if (!jointsLowerBodyNew.getState().contains("stand")) {

			actionLowerStart = gt.getTime();
			jointsLowerBodyOld = jointsLowerBodyDraw;

			jointsLowerBodyNew = new human3_joint_rotations_lower_body("human3_lower_body_stand");

		}

		if (!jointsUpperBodyNew.getState().contains("stand") && !jointsUpperBodyNew.isAttack()) {
			// System.out.println("upper body new isn't stand, so entered stand
			// code");
			actionUpperStart = gt.getTime();
			jointsUpperBodyOld = jointsUpperBodyDraw;

			jointsUpperBodyNew = new human3_joint_rotations_upper_body("human3_upper_body_stand");

		}

	}

	/*
	 * public void quaternionRotate(float dist, float[] joints, int index,
	 * Vector3f vv, Vector3f vvOffset) { v.x = 0f; v.y = 0f; v.z = dist;
	 * calcuateJointsQuaternions(joints[index++], joints[index++],
	 * joints[index++]); // float jx,jy,jz; // jx = joints[index++]; // jy =
	 * joints[index++]; // jz = joints[index++]; //
	 * calcuateJointsQuaternions(jx,jy,jz); //
	 * System.out.println("jx:"+jx+"\tjy:"+jy+"\tjz:"+jz); if (vvOffset == null)
	 * { vv.x = v.x; vv.y = v.y; vv.z = v.z; //
	 * System.out.println("vv.x:"+vv.x+"\tvv.y:"+vv.y+"\tvv.z:"+vv.z); } else {
	 * vv.x = v.x + vvOffset.x; vv.y = v.y + vvOffset.y; vv.z = v.z +
	 * vvOffset.z; } }
	 */

	public void setDiameterXYZ(Vector3f p) {
		this.diameterXYZ = p;
	}

	public void setFacingDegree(float x) {
		degreeRotateZ = x;
	}

	public void setHeight(float x) {
		height = x;
		moveDistance = height * .1f;

		distancesFromParent[indexGround] = 0f;
		distancesFromParent[indexCrotch] = .5f;
		distancesFromParent[indexLHip] = headHeight / 2.8f;
		distancesFromParent[indexRHip] = headHeight / 2.8f;
		distancesFromParent[indexLKnee] = headHeight * 2f;
		distancesFromParent[indexRKnee] = headHeight * 2f;
		distancesFromParent[indexLAnkle] = headHeight * 2f;
		distancesFromParent[indexRAnkle] = headHeight * 2f;
		distancesFromParent[indexLFoot] = headHeight;
		distancesFromParent[indexRFoot] = headHeight;
		distancesFromParent[indexNavel] = headHeight;
		distancesFromParent[indexNipple] = headHeight;
		distancesFromParent[indexNeckLower] = headHeight;
		distancesFromParent[indexNeckUpper] = neckHeight;
		distancesFromParent[indexTopOfHead] = headHeight;
		distancesFromParent[indexLShoulder] = headHeight * 1.5f;
		distancesFromParent[indexRShoulder] = headHeight * 1.5f;
		distancesFromParent[indexLElbow] = headHeight * 1.5f;
		distancesFromParent[indexRElbow] = headHeight * 1.5f;
		distancesFromParent[indexLWrist] = headHeight * 1.5f;
		distancesFromParent[indexRWrist] = headHeight * 1.5f;
		distancesFromParent[indexLHand] = headHeight * .25f;
		distancesFromParent[indexRHand] = headHeight * .25f;

	}

//	public void setMVP(Matrix4f mvp) {
//		MVP = mvp;
//	}
	/*
	 * public void setVertices(int indexCounter, float cosRadian, float
	 * sinRadian, float fX, float fY, float fZ, float[] array) {
	 * 
	 * // System.out.println("");
	 * 
	 * float dX = fX * cosRadian - fY * sinRadian; float dY = fY * cosRadian +
	 * fX * sinRadian; float dZ = fZ; //
	 * System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" + // dY
	 * + "\t\tdZ:" + dZ);
	 * 
	 * dX *= height; dY *= height; dZ *= height; //
	 * System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" + // dY
	 * + "\t\tdZ:" + dZ);
	 * 
	 * dX += xyz.x; dY += xyz.y; dZ += xyz.z; //
	 * System.out.println("human.setVertices():\t\tdX:" + dX + "\t\tdY:" + // dY
	 * + "\t\tdZ:" + dZ);
	 * 
	 * array[indexCounter++] = (dX); array[indexCounter++] = (dY);
	 * array[indexCounter] = (dZ);
	 * 
	 * }
	 */

	public void setRotationX(float x) {
		degreeRotateX = x;
	}

	public void setRotationY(float x) {
		degreeRotateY = x;
	}

	public void setXYZ(float x, float y, float z) {
		xyz.x = x;
		xyz.y = y;
		xyz.z = z;
		// move();
	}

	public void setXY(float newX, float newY) {
		xyz.x = newX;
		xyz.y = newY;
	}

	public void setZ(float newZ) {
		xyz.z = newZ;
	}

}