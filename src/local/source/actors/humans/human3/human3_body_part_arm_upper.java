package local.source.actors.humans.human3;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_CLAMP_TO_BORDER;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VALIDATE_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glGetAttribLocation;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;
import static org.lwjgl.opengl.GL20.glGetProgrami;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform4f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;
import static org.lwjgl.stb.STBImage.*;

import java.nio.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;
import org.joml.Vector2f;
import org.lwjgl.BufferUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import local.source.globalStatic;
import local.source.manager_mvp;
public class human3_body_part_arm_upper implements interface_human3_body_part {

	String className = "human3_body_part_arm_upper";

	float fat, muscle;
	int gender;
	String bodyPart = "arm_upper";
	human3_outfit outfit;


	Vector3f centerXYZ = new Vector3f(0f, 0f, 0f);
	human3_joints joints;

	float widthY, heightZ, lengthX;

	//public Matrix4f MVP;
	public Vector3f rotationXYZ = new Vector3f();
	float rotationZ = 0f;
	Quaternionf dest1 = new Quaternionf();
	Quaternionf dest2 = new Quaternionf();
	Vector3f v = new Vector3f();

	int arrayDetail = 10; // this is how many columns of vertices, not how many
							// squares. 2 is one square. 3 is four squares
	// when doing rows and columns, row 0 is top, column 0 is left

	Vector3f[] verticesModelFront = new Vector3f[arrayDetail * arrayDetail];
	Vector3f[] verticesModelBack = new Vector3f[arrayDetail * arrayDetail];

	Vector2f[] whModelFront = new Vector2f[arrayDetail * arrayDetail];
	Vector2f[] whModelBack = new Vector2f[arrayDetail * arrayDetail];

	HashMap<Integer, HashMap<String, HashMap<Integer, Float>>> weightedJoints = new HashMap<Integer, HashMap<String, HashMap<Integer, Float>>>();

	Vector3f[] verticesFront = new Vector3f[arrayDetail * arrayDetail];
	Vector3f[] verticesBack = new Vector3f[arrayDetail * arrayDetail];

	int[] indicesFront = new int[arrayDetail * arrayDetail * 6];

	int[] indicesBack = new int[arrayDetail * arrayDetail * 6];

	int vertexNumbers = 5; // 3 or 5. 3 is regular without textures 5 is with textures
	float[] verticesDrawFront = new float[arrayDetail * arrayDetail * vertexNumbers];
	float[] verticesDrawBack = new float[arrayDetail * arrayDetail * vertexNumbers];

	int indicesLinesCountFront;
	int indicesLinesCountBack;

	float skinRed = 1f;// 0.863f;
	float skinBlue = 0f;// 0.863f;
	float skinGreen = 0f;// 0.863f;

	int VAOIDFront = -1;
	int VBOIDFront = -1;
	int VBOIIDFront = -1;

	int VAOIDBack = -1;
	int VBOIDBack = -1;
	int VBOIIDBack = -1;

	public int GLMVPUniformLocationFront;
	public int GLMVPUniformLocationBack;
	public int GLRGBAUniformLocationFront;
	public int GLRGBAUniformLocationBack;
	public FloatBuffer FBFront = BufferUtils.createFloatBuffer(20);
	public FloatBuffer FBBack = BufferUtils.createFloatBuffer(20);
	public int programIdFront;
	public int programIdBack;
	public int vertexShaderIdFront;
	public int vertexShaderIdBack;
	public int fragmentShaderIdFront;
	public int fragmentShaderIdBack;
	boolean isIndexBoundFront = false;
	boolean isIndexBoundBack = false;
	public boolean isDrawFront = true;
	public boolean isDrawBack = true;
	FloatBuffer verticesBufferFront;
	FloatBuffer verticesBufferBack;

	IntBuffer textureWidthFront = BufferUtils.createIntBuffer(1);
	IntBuffer textureHeightFront = BufferUtils.createIntBuffer(1);
	IntBuffer textureComponentsFront = BufferUtils.createIntBuffer(1);
	ByteBuffer textureDataFront;
	String textureSourceFront = "resources" + "/" + "actors" + "/" + "skins" + "/" + "human3" + "/" + "outfits" + "/" + "shirts" + "/" + "nude" + "/" + "arm_upper_male_front.png";
	int textureIDFront = -1;
	public int GLTextureCoordLocationFront;
	public int GLTextureImageUniformFront;

	IntBuffer textureWidthBack = BufferUtils.createIntBuffer(1);
	IntBuffer textureHeightBack = BufferUtils.createIntBuffer(1);
	IntBuffer textureComponentsBack = BufferUtils.createIntBuffer(1);
	ByteBuffer textureDataBack;
	String textureSourceBack = "resources" + "/" + "actors" + "/" + "skins" + "/" + "human3" + "/" + "outfits" + "/" + "shirts" + "/" + "nude" + "/" + "arm_upper_male_back.png";
	int textureIDBack = -1;
	public int GLTextureCoordLocationBack;
	public int GLTextureImageUniformBack;

	
	Vector3f[] adjustBottomVerticesBack =null;
	Vector3f[] adjustBottomVerticesFront =null;
	Vector3f[] adjustTopVerticesBack =null;
	Vector3f[] adjustTopVerticesFront =null;
	
	String orientation = "left";

	
	public human3_body_part_arm_upper(String bodyPart, String orientation)
	{
		this.bodyPart=bodyPart;
		this.orientation = orientation;
		
		lengthX = 1f;
		widthY = 1f;
		heightZ = 1f;

		float height = heightZ / 2f;
		float width = widthY / 2f;
		float length = lengthX / 2f;

		if (isDrawFront || isDrawBack) {

			try {
				textureDataFront = stbi_load_from_memory(globalStatic.ioResourceToByteBuffer(textureSourceFront, 20 * 1024), textureWidthFront, textureHeightFront, textureComponentsFront, 4);
			} catch (Exception e) {
				System.out.println(className + " constructor : error thrown making the texture message:" + e.getMessage() + " stacktrace:" + e.getStackTrace());
			}

			try {
				textureDataBack = stbi_load_from_memory(globalStatic.ioResourceToByteBuffer(textureSourceBack, 20 * 1024), textureWidthBack, textureHeightBack, textureComponentsBack, 4);
			} catch (Exception e) {
				System.out.println(className + " constructor : error thrown making the texture message:" + e.getMessage() + " stacktrace:" + e.getStackTrace());
			}

			this.glConstruct();
		}

		for (int i = 0; i < verticesModelFront.length; i++) {
			verticesModelFront[i] = new Vector3f(0f, 0f, 0f);
			verticesFront[i] = new Vector3f(0f, 0f, 0f);
			whModelFront[i] = new Vector2f(0f, 0f);
		}

		for (int i = 0; i < verticesModelBack.length; i++) {
			verticesModelBack[i] = new Vector3f(0f, 0f, 0f);
			verticesBack[i] = new Vector3f(0f, 0f, 0f);
			whModelBack[i] = new Vector2f(0f, 0f);
		}

		int indexFront = 0;
		int indexBack = 0;
		for (int row = 1; row < (arrayDetail); row++) {
			for (int col = 1; col < (arrayDetail); col++) {
				// System.out.println("row:"+row+" col:"+col+"
				// arrayDetail:"+arrayDetail);
				int a = col - 1 + (row - 1) * arrayDetail; // System.out.println("a:"+a);
				int b = col + (row - 1) * arrayDetail; // System.out.println("b:"+b);
				int c = col + (row) * arrayDetail; // System.out.println("c:"+c);
				int d = col - 1 + (row) * arrayDetail; // System.out.println("d:"+d);

				indicesFront[indexFront++] = a;
				indicesFront[indexFront++] = b;
				indicesFront[indexFront++] = c;

				indicesFront[indexFront++] = a;
				indicesFront[indexFront++] = c;
				indicesFront[indexFront++] = d;

				indicesBack[indexBack++] = a;
				indicesBack[indexBack++] = c;
				indicesBack[indexBack++] = b;

				indicesBack[indexBack++] = a;
				indicesBack[indexBack++] = d;
				indicesBack[indexBack++] = c;
			}
		}

	}
	

	public void bindVertexDataBack() {

		if (isDrawBack) {
			if (verticesBufferBack == null) {
				verticesBufferBack = BufferUtils.createFloatBuffer(verticesDrawBack.length);
			}
			verticesBufferBack.put(verticesDrawBack).flip();

			if (textureIDBack == -1) {
				textureIDBack = glGenTextures();

				glBindTexture(GL_TEXTURE_2D, textureIDBack);
				// glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
				// glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

				// System.out.println("about to do glTexImage2D");
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureWidthBack.get(), textureHeightBack.get(), 0, GL_RGBA, GL_UNSIGNED_BYTE, textureDataBack);
				stbi_image_free(textureDataBack);
				// System.out.println("done with glTexImage2D");

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
				// GL_LINEAR);
				// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
				// GL_LINEAR);

				// System.out.println("texture width and height :
				// "+textureWidth.get() + " " +textureHeight.get());
				if (textureDataBack == null) {
					System.out.println("about to make texture with null textureData");
				}

			}

			if (VAOIDBack == -1) {
				VAOIDBack = glGenVertexArrays();
			}
			glBindVertexArray(VAOIDBack);

			if (VBOIDBack == -1) {
				VBOIDBack = glGenBuffers();
			}
			glBindBuffer(GL_ARRAY_BUFFER, VBOIDBack);
			glBufferData(GL_ARRAY_BUFFER, verticesBufferBack, GL_DYNAMIC_DRAW);
			// glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 20, 0);
			glVertexAttribPointer(1, 2, GL_FLOAT, false, 20, 12);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);

			if (!isIndexBoundBack) {
				indicesLinesCountBack = indicesBack.length;

				IntBuffer indicesBuffer = BufferUtils.createIntBuffer(indicesLinesCountBack);
				indicesBuffer.put(indicesBack);
				indicesBuffer.flip();

				VBOIIDBack = glGenBuffers();
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIIDBack);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

				isIndexBoundBack = true;
			}
		}
	}

	public void bindVertexDataFront() {

		if (isDrawFront) {
			if (verticesBufferFront == null) {
				verticesBufferFront = BufferUtils.createFloatBuffer(verticesDrawFront.length);
			}
			verticesBufferFront.put(verticesDrawFront).flip();

			if (textureIDFront == -1) {
				textureIDFront = glGenTextures();

				glBindTexture(GL_TEXTURE_2D, textureIDFront);
				// glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
				// glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

				// System.out.println("about to do glTexImage2D");
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureWidthFront.get(), textureHeightFront.get(), 0, GL_RGBA, GL_UNSIGNED_BYTE, textureDataFront);
				stbi_image_free(textureDataFront);
				// System.out.println("done with glTexImage2D");

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
				// GL_LINEAR);
				// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
				// GL_LINEAR);

				// System.out.println("texture width and height :
				// "+textureWidth.get() + " " +textureHeight.get());
				if (textureDataFront == null) {
					System.out.println("about to make texture with null textureData");
				}

			}

			if (VAOIDFront == -1) {
				VAOIDFront = glGenVertexArrays();
			}
			glBindVertexArray(VAOIDFront);

			if (VBOIDFront == -1) {
				VBOIDFront = glGenBuffers();
			}
			glBindBuffer(GL_ARRAY_BUFFER, VBOIDFront);
			glBufferData(GL_ARRAY_BUFFER, verticesBufferFront, GL_DYNAMIC_DRAW);
			// glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 20, 0);
			glVertexAttribPointer(1, 2, GL_FLOAT, false, 20, 12);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);

			if (!isIndexBoundFront) {
				indicesLinesCountFront = indicesFront.length;

				IntBuffer indicesBuffer = BufferUtils.createIntBuffer(indicesLinesCountFront);
				indicesBuffer.put(indicesFront);
				indicesBuffer.flip();

				VBOIIDFront = glGenBuffers();
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIIDFront);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

				isIndexBoundFront = true;
			}
		}
	}

	public void calculateVertices(Vector3f[] verticesModel, Vector2f[] whModel, Vector3f[] vertices, float[] verticesDraw, Vector3f offsetXYZ ) {

		double radian = Math.toRadians(rotationZ);

		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		float fX, fY, fZ;

		int indexCount = 0;

		for (int i = 0; i < vertices.length; i++) {
			v.x = verticesModel[i].x;
			v.y = verticesModel[i].y;
			v.z = verticesModel[i].z;

			if (joints != null) {
				if (weightedJoints != null) {
					if (weightedJoints.containsKey(i)) {
						if (weightedJoints.get(i).containsKey("x")) {
							for (int j = 0; j < weightedJoints.get(i).get("x").size(); j++) {
								Integer jointID = (Integer) weightedJoints.get(i).get("x").keySet().toArray()[j];
								Float weight = (Float) weightedJoints.get(i).get("x").get(jointID);

								// System.out.println("key:"+key+" val:"+val);

								float val = joints.getVertexNotAdjustedForHeight(jointID).x;

								float adjust = val + weight * val ;

								v.x += adjust; // maybe do this transform before
												// the quaternion step

							}

							// if y

							// if z
						}
					}
				}
			}

			dest1.w = 1f;
			dest1.x = 0f;
			dest1.y = 0f;
			dest1.z = 0f;

			dest2.w = 1f;
			dest2.x = 0f;
			dest2.y = 0f;
			dest2.z = 0f;

			float x, y, z;
			x = rotationXYZ.x;// + rotationXYZ.x;// testX;
			y = rotationXYZ.y;// + rotationXYZ.y;//testY;
			z = rotationXYZ.z;// + rotationXYZ.z;//testZ;

			x = x % 360.0f;
			y = y % 360.0f;
			z = z % 360.0f;

			dest2.rotate((float) Math.toRadians(x), (float) Math.toRadians(y), (float) Math.toRadians(z), dest1);

			dest1.transform(v);

			Vector3f vv = vertices[i];

			vv.x = v.x;
			vv.y = v.y;
			vv.z = v.z;

		}

		float height = heightZ / 8f;
		for (int i = 0; i < vertices.length; i++) {

			fX = vertices[i].x;
			fY = vertices[i].y;
			fZ = vertices[i].z;

		
			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= height;
			dY *= height;
			dZ *= height;

			dX += offsetXYZ.x;
			dY += offsetXYZ.y;
			dZ += offsetXYZ.z;
	
			int newI = i * vertexNumbers;

			verticesDraw[newI] = (dX);
			verticesDraw[newI + 1] = (dY);
			verticesDraw[newI + 2] = (dZ);

			if (vertexNumbers == 5) { // this could be optimized to only run once
				verticesDraw[newI + 3] = whModel[i].x;// frow;
				verticesDraw[newI + 4] = whModel[i].y;// fcol;
			}
		}

	}

	public void draw() {

		if (isDrawFront) {
			calculateVertices(verticesModelFront, whModelFront, verticesFront, verticesDrawFront, centerXYZ);
			drawFront();
		}

		if (isDrawBack) {
			calculateVertices(verticesModelBack, whModelBack, verticesBack, verticesDrawBack, centerXYZ);
			drawBack();
		}

	}

	public void drawBack() {

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_ALPHA_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND); 
		glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

		glUseProgram(programIdBack);
		bindVertexDataBack();

		glUniformMatrix4fv(GLMVPUniformLocationBack, false, manager_mvp.getInstance().get().get(FBBack));
		glUniform4f(GLRGBAUniformLocationBack, skinRed, skinGreen, skinBlue, 1.0f);

		glBindVertexArray(VAOIDBack);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIIDBack);
		glBindTexture(GL_TEXTURE_2D, textureIDBack);
		
		glDrawElements(GL_TRIANGLES, indicesLinesCountBack, GL_UNSIGNED_INT, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glBindVertexArray(0);

		glUseProgram(0);

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_ALPHA_TEST);
		glDisable(GL_BLEND);
	}

	public void drawFront() {
		
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_ALPHA_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND); 
		glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

		glUseProgram(programIdFront);
		bindVertexDataFront();

		glUniformMatrix4fv(GLMVPUniformLocationFront, false, manager_mvp.getInstance().get().get(FBFront));
		glUniform4f(GLRGBAUniformLocationFront, skinRed, skinGreen, skinBlue, 1.0f);

		glBindVertexArray(VAOIDFront);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIIDFront);
		glBindTexture(GL_TEXTURE_2D, textureIDFront);

		glDrawElements(GL_TRIANGLES, indicesLinesCountFront, GL_UNSIGNED_INT, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glBindVertexArray(0);

		glUseProgram(0);

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_ALPHA_TEST);
		glDisable(GL_BLEND);

	}

	public Vector3f[] getBottomVerticesBack() {
		Vector3f[] vertices = verticesBack;
		
		Vector3f[] result = new Vector3f[arrayDetail];
		for (int i = 0; i < arrayDetail; i++) // how to parse just the top row?
		{
			Vector3f newV = new Vector3f();
			result[i] = newV;
			//int newI = i;// verticesBack.length - arrayDetail +i -1 ;// i *
							// vertexNumbers + (arrayDetail-1);
			//newI *= vertexNumbers;
			//newV.x = verticesDraw[newI];
			//newV.y = verticesDraw[newI + 1];
			//newV.z = verticesDraw[newI + 2];
			newV.x = vertices[i].x;
			newV.y = vertices[i].y;
			newV.z = vertices[i].z;
			
		}
		return result;
	}

	public Vector3f[] getBottomVerticesFront() {
		float[] verticesDraw = verticesDrawFront;
		Vector3f[] result = new Vector3f[arrayDetail];
		for (int i = 0; i < arrayDetail; i++) // how to parse just the top row?
		{
			Vector3f newV = new Vector3f();
			result[i] = newV;
			int newI = i;// verticesFront.length - arrayDetail +i -1 ;// i *
							// vertexNumbers + (arrayDetail-1);
			newI *= vertexNumbers;
			newV.x = verticesDraw[newI];
			newV.y = verticesDraw[newI + 1];
			newV.z = verticesDraw[newI + 2];
		}
		return result;
	}

	public Vector3f[] getTopVerticesBack() {
		float[] verticesDraw = verticesDrawBack;
		Vector3f[] result = new Vector3f[arrayDetail];
		for (int i = 0; i < arrayDetail; i++) // how to parse just the top row?
		{
			Vector3f newV = new Vector3f();
			result[i] = newV;
			int newI = verticesBack.length - arrayDetail + i - 1;// i *
																	// vertexNumbers
																	// +
																	// (arrayDetail-1);
			newI *= vertexNumbers;
			newV.x = verticesDraw[newI];
			newV.y = verticesDraw[newI + 1];
			newV.z = verticesDraw[newI + 2];

		}
		return result;
	}

	public Vector3f[] getTopVerticesFront() {
		float[] verticesDraw = verticesDrawFront;
		Vector3f[] result = new Vector3f[arrayDetail];
		for (int i = 0; i < arrayDetail; i++) // how to parse just the top row?
		{
			Vector3f newV = new Vector3f();
			result[i] = newV;
			int newI = verticesFront.length - arrayDetail + i - 1;// i *
																	// vertexNumbers
																	// +
																	// (arrayDetail-1);
			newI *= vertexNumbers;
			newV.x = verticesDraw[newI];
			newV.y = verticesDraw[newI + 1];
			newV.z = verticesDraw[newI + 2];

		}
		return result;
	}

	
	public Vector3f[] getVerticesBack() {
		return verticesBack;
	}

	public Vector3f[] getVerticesFront() {
		return verticesFront;
	}

	
	public void glCleanup() {
		/*
		 * glUseProgram(0); if (jointsProgramId != 0) { if (jointsVertexShaderId
		 * != 0) { glDetachShader(jointsProgramId, jointsVertexShaderId); } if
		 * (jointsFragmentShaderId != 0) { glDetachShader(jointsProgramId,
		 * jointsFragmentShaderId); } glDeleteProgram(jointsProgramId); }
		 */
	}

	public void glConstruct() {

		if (isDrawFront) {
			glConstructFront();
		}

		if (isDrawBack) {
			glConstructBack();
		}
	}

	public void glConstructBack() {
		try {
			try {
				programIdBack = glCreateProgram();

				// glCreateVertexShaderFront("#version 130 \nuniform mat4 MVP;
				// in vec3 position; void main() { gl_Position =MVP*
				// vec4(position, 1.0); }");

				// glCreateFragmentShaderFront("#version 130\nuniform vec4 RGBA;
				// out vec4 fragColor; void main() { fragColor = RGBA; }");

				glCreateVertexShaderBack("#version 130         \n"//
						+ "uniform mat4 MVP;                       \n"//
						+ "in vec3 position;                       \n"//
						+ "in vec2 texcoord;                       \n"//						
						+ "out vec2 textureCoord;              \n"//
						+ "void main() {                           \n"//
						+ "     gl_Position =MVP* vec4(position, 1.0); \n"//						
						+ "       textureCoord = texcoord;           \n"//
						+ "}");

				glCreateFragmentShaderBack("#version 130       \n"//
						+ "uniform vec4 RGBA; \n"//
						+ "in vec2 textureCoord;              \n"//
						+ "uniform sampler2D texImage;             \n"//
						+ "out vec4 out_color;                     \n"//
						+ "void main() {                           \n"//						
						+ "   out_color= texture(texImage,textureCoord);\n"//
						+ " if(out_color.a<0.5f) \n" + " { out_color = RGBA ;}" + "}   ");

				glLink(programIdBack);

				glUseProgram(programIdBack);
				GLMVPUniformLocationBack = glGetUniformLocation(programIdBack, "MVP");
				GLRGBAUniformLocationBack = glGetUniformLocation(programIdBack, "RGBA");
				GLTextureCoordLocationBack = glGetAttribLocation(programIdBack, "texcoord");
				GLTextureImageUniformBack = glGetUniformLocation(programIdBack, "texImage");

				glUseProgram(0);

				// glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught in " + className + ":" + e.getMessage() + " " + e.getStackTrace());
			}

			if (programIdBack == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in " + className + " " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glConstructFront() {
		try {
			try {
				programIdFront = glCreateProgram();

				// glCreateVertexShaderFront("#version 130 \nuniform mat4 MVP;
				// in vec3 position; void main() { gl_Position =MVP*
				// vec4(position, 1.0); }");

				// glCreateFragmentShaderFront("#version 130\nuniform vec4 RGBA;
				// out vec4 fragColor; void main() { fragColor = RGBA; }");

				glCreateVertexShaderFront("#version 130         \n"//
						+ "uniform mat4 MVP;                       \n"//
						+ "in vec3 position;                       \n"//
						+ "in vec2 texcoord;                       \n"//
						// +"in vec4 in_color; \n"//
						// +"out vec4 pass_color; \n"//
						+ "out vec2 textureCoord;              \n"//
						+ "void main() {                           \n"//
						+ "     gl_Position =MVP* vec4(position, 1.0); \n"//
						// +" pass_color=in_color; \n"//
						// +" pass_color=vec4(1.0,1.0,1.0,1.0); \n"//
						+ "       textureCoord = texcoord;           \n"//
						+ "}");

				glCreateFragmentShaderFront("#version 130       \n"//
						+ "uniform vec4 RGBA; \n"//
						+ "in vec2 textureCoord;              \n"//
						+ "uniform sampler2D texImage;             \n"//
						+ "out vec4 out_color;                     \n"//
						+ "void main() {                           \n"//
						// +" vec4 textureColor =
						// texture(texImage,textureCoord);\n"//
						// +" out_color=pass_color*textureColor; \n"//
						// +" out_color=vec4(1f,1f,1f,1f); \n"//
						// + " out_color=
						// vec4(1.0,1.0,1.0,1.0);\n"//texture(texImage,textureCoord);\n"//
						// + " out_color=
						// texture(texImage,textureCoord)*RGBA;\n"//
						+ "   out_color= texture(texImage,textureCoord);\n"//
						+ " if(out_color.a<0.5f) \n" + " { out_color = RGBA ;}" + "}   ");

				glLink(programIdFront);

				glUseProgram(programIdFront);
				GLMVPUniformLocationFront = glGetUniformLocation(programIdFront, "MVP");
				GLRGBAUniformLocationFront = glGetUniformLocation(programIdFront, "RGBA");
				GLTextureCoordLocationFront = glGetAttribLocation(programIdFront, "texcoord");
				GLTextureImageUniformFront = glGetUniformLocation(programIdFront, "texImage");

				glUseProgram(0);

				// glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught in " + className + ":" + e.getMessage() + " " + e.getStackTrace());
			}

			if (programIdFront == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in " + className + " : " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glCreateFragmentShaderBack(String shaderCode) throws Exception {
		fragmentShaderIdBack = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, programIdBack);
	}

	public void glCreateFragmentShaderFront(String shaderCode) throws Exception {
		fragmentShaderIdFront = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, programIdFront);
	}

	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error in " + className + " creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error in " + className + " compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShaderBack(String shaderCode) throws Exception {
		vertexShaderIdBack = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, programIdBack);
	}

	public void glCreateVertexShaderFront(String shaderCode) throws Exception {
		vertexShaderIdFront = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, programIdFront);
	}

	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error in " + className + " linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning in " + className + " validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}


	public void resetTexture() {
		textureIDFront = -1;
		textureIDBack = -1;

		textureWidthFront = BufferUtils.createIntBuffer(1);
		textureHeightFront = BufferUtils.createIntBuffer(1);
		textureComponentsFront = BufferUtils.createIntBuffer(1);

		textureWidthBack = BufferUtils.createIntBuffer(1);
		textureHeightBack = BufferUtils.createIntBuffer(1);
		textureComponentsBack = BufferUtils.createIntBuffer(1);

		textureSourceBack = outfit.getTexturePath(bodyPart, "back");
		textureSourceFront = outfit.getTexturePath(bodyPart, "front");

		// System.out.println("human3_body_part_chest.resetTexture()
		// textureSourceBack:"+textureSourceBack);
		// System.out.println("human3_body_part_chest.resetTexture()
		// textureSourceFront:"+textureSourceFront);

		try {
			textureDataFront = stbi_load_from_memory(globalStatic.ioResourceToByteBuffer(textureSourceFront, 20 * 1024), textureWidthFront, textureHeightFront, textureComponentsFront, 4);
		} catch (Exception e) {
			System.out.println(className + " resetTexture() : error thrown making the texture message:" + e.getMessage() + " stacktrace:" + e.getStackTrace());
		}

		try {
			textureDataBack = stbi_load_from_memory(globalStatic.ioResourceToByteBuffer(textureSourceBack, 20 * 1024), textureWidthBack, textureHeightBack, textureComponentsBack, 4);
		} catch (Exception e) {
			System.out.println(className + " resetTexture() : error thrown making the texture message:" + e.getMessage() + " stacktrace:" + e.getStackTrace());
		}

	}


	
	public void setAdjustBottomVerticesBack(Vector3f[] adjustBot){adjustBottomVerticesBack=adjustBot;}
	public void setAdjustBottomVerticesFront(Vector3f[] adjustBot){adjustBottomVerticesFront=adjustBot;}
	
	public void setAdjustTopVerticesBack(Vector3f[] adjustTop){adjustTopVerticesBack=adjustTop;}
	public void setAdjustTopVerticesFront(Vector3f[] adjustTop){adjustTopVerticesFront=adjustTop;}
	
	public void setBodyHeight(float height) {
		this.heightZ = height;
		float headHeight = 1f / 8f;
		// float neck = headHeight * .2f;

		float l = headHeight * height;
		float w = headHeight * height;
		float h = headHeight * height * 1.5f;
	}

	public void setBodyPart(String newbp) {
		this.bodyPart = newbp;
	}

	public void setCenterXYZ(float x, float y, float z) {
		centerXYZ.x = x;
		centerXYZ.y = y;
		centerXYZ.z = z;
	}

	public void setFacingRotationZ(float x) {
		// degreeRotateY = x;
		rotationZ = x;
	}

	public void setFatMuscleGenderJoints(float fat, float muscle, int gender, human3_joints j) {
		this.joints = j;
		this.fat = fat;
		this.muscle = muscle;
		this.gender = gender;
		String maleFemale = gender == 0 ? "male" : "female";
		String size = "thin";
		String outfit = "nude";

		String path = "resources" + "/" + "actors" + "/" + "skins" + "/" + "human3" + "/" + "skinMesh" + "/" + this.bodyPart + "/";

		// String skinMeshFrontThin = path + "human3_skinMesh_" + this.bodyPart
		// + "_front_thin_" + maleFemale + "_" + arrayDetail + "x" + arrayDetail
		// + ".xml";
		// String skinMeshFrontFat = path + "human3_skinMesh_" + this.bodyPart +
		// "_front_fat_" + maleFemale + "_" + arrayDetail + "x" + arrayDetail +
		// ".xml";
		// String skinMeshFrontMuscular = path + "human3_skinMesh_" +
		// this.bodyPart + "_front_muscular_" + maleFemale + "_" + arrayDetail +
		// "x" + arrayDetail + ".xml";

		String skinMeshFrontWH = path + "front_wh_" + arrayDetail + "x" + arrayDetail + ".xml";
		String skinMeshBackWH = path + "back_wh_" + arrayDetail + "x" + arrayDetail + ".xml";

		String skinMeshFrontQuickAndEasy = path + "front_quickAndEasy_" + maleFemale + "_" + arrayDetail + "x" + arrayDetail + ".xml";
		String skinMeshBackQuickAndEasy = path + "back_quickAndEasy_" + maleFemale + "_" + arrayDetail + "x" + arrayDetail + ".xml";

		String skinMeshWeightedJoints = path + "jointsAndWeights_" + arrayDetail + "x" + arrayDetail + ".xml";

		//System.out.println("armUpper:skinMeshFrontQuickAndEasy : "+skinMeshFrontQuickAndEasy);
		//System.out.println("armUpper:skinMeshBackQuickAndEasy : "+skinMeshBackQuickAndEasy);
		
		/*
		 * Vector3f[] verticesFrontModelThin = new
		 * Vector3f[verticesModelFront.length]; Vector3f[] verticesFrontModelFat
		 * = new Vector3f[verticesModelFront.length]; Vector3f[]
		 * verticesFrontModelMuscular = new Vector3f[verticesModelFront.length];
		 * Vector3f[] verticesFrontModelGarment = new
		 * Vector3f[verticesModelFront.length];
		 * 
		 * 
		 * Vector3f[] verticesFrontMyFat = new
		 * Vector3f[verticesModelFront.length]; Vector3f[]
		 * verticesFrontMyMuscular = new Vector3f[verticesModelFront.length];
		 * 
		 * 
		 * Vector3f[] verticesFrontMyQuickAndEasy = new
		 * Vector3f[verticesModelFront.length]; Vector3f[]
		 * verticesBackMyQuickAndEasy = new Vector3f[verticesModelBack.length];
		 */

		// loop through the first three and store in the next two

		// loop through those two and store in verticesModelFront

		// loop through garment and adjust the verticesModelFront

		// figureOutSkinMeshXYZ(skinMeshFrontThin, verticesModelFront);
		// figureOutWH(skinMeshFrontWH, whModelFront);

		globalStatic.human3BodyPartFigureOutSkinMeshXYZ(skinMeshFrontQuickAndEasy, verticesModelFront, arrayDetail);
		globalStatic.human3BodyPartFigureOutWH(skinMeshFrontWH, whModelFront, arrayDetail);

		globalStatic.human3BodyPartFigureOutSkinMeshXYZ(skinMeshBackQuickAndEasy, verticesModelBack, arrayDetail);
		globalStatic.human3BodyPartFigureOutWH(skinMeshBackWH, whModelBack, arrayDetail);

		globalStatic.human3BodyPartFigureOutWeightedJoints(skinMeshWeightedJoints, weightedJoints, arrayDetail);

	}

//	public void setMVP(Matrix4f mvp) {
//		MVP = mvp; // System.out.println("set sword MVP");
//	}

	public void setOutfit(human3_outfit o) {
		outfit = o;
		resetTexture();
	}

	public void setSkinRGB(float r, float g, float b) {
		skinRed = r;
		skinBlue = b;
		skinGreen = g;
	}

	public void setRotationXYZ(float x, float y, float z) {
		rotationXYZ.x = x;
		rotationXYZ.y = y;
		rotationXYZ.z = z;
	}

	public void setRotationXYZ(Vector3f nxyz) {
		rotationXYZ.x = nxyz.x;
		rotationXYZ.y = nxyz.y;
		rotationXYZ.z = nxyz.z;
		// if(attack!=null)
		// {attack.setHandleXYZ(nxyz);}
	}

	public void setRotationZ(float x) {
		// degreeRotateY = x;
		rotationZ = x;
	}

}