package local.source.actors.humans.human4;

import java.util.HashMap;


public class human4_joint_state_manager {
	private static human4_joint_state_manager instance = null;
	

	HashMap<String,human4_joint_state> hashMapStates = new HashMap<String,human4_joint_state>();

	
	private human4_joint_state_manager()
	{}
	
	public static human4_joint_state_manager getInstance()
	{
		if(instance==null)
		{ instance = new human4_joint_state_manager();}
		return instance;		
	}
	
	public human4_joint_state get( String key)
	{
		human4_joint_state result = hashMapStates.get(key);
		
		if(result==null)
		{
			result =  new human4_joint_state(key);
			this.put(key, result);
		}
	
		return result;
	}
	

	
	public void put(String key, human4_joint_state val)
	{
		if(!hashMapStates.containsKey(key))
		{ hashMapStates.put(key, val);}
	}

}
