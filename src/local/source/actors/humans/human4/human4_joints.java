package local.source.actors.humans.human4;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL32.*;
//import static org.lwjgl.opengl.GL40.*;
//import static org.lwjgl.opengl.GL45.*;

import java.nio.*;
import java.util.Calendar;
import java.util.Random;

import org.lwjgl.BufferUtils;

import local.source.game_time;
import local.source.manager_mvp;
import local.source.actors.humans.human3.human3_joint_rotations_lower_body;
import local.source.actors.humans.human3.human3_joint_rotations_upper_body;
import local.source.attacks.interface_attack;

import org.joml.QuaternionfInterpolator;
import org.joml.Quaternionf;
import org.joml.Matrix4f;
import org.joml.Vector3f;

public class human4_joints {

	// when you rework the joints ( and you really need to )
	// consider breathing and moving more than just the shoulders
	// change the center of the upper back to move a little too
	// that should be enough to move the entire upper torso when skins come
	// along
	// or maybe add a few new joints for chest center or tommy and move those
	// around

	float speed = 1.0f;

	public enum indexJoints {
		ground(0), crotch(1), lHip(2), rHip(3), lKnee(4), rKnee(5), lAnkle(6), rAnkle(7), lFoot(8), rFoot(9), navel(
				10), nipple(11), neckLower(12), neckUpper(13), topOfHead(14), lShoulder(
						15), rShoulder(16), lElbow(17), rElbow(18), lWrist(19), rWrist(20), lHand(21), rHand(2);

		private final int index;

		private indexJoints(int value) {
			index = value;
		}

		public int getIndex() {
			return index;
		}
	}

	human4_joint[] joints = { new human4_joint(0, 0f), // 0 ground
			new human4_joint(0, .5f), // 1 crotch
			new human4_joint(1, 0.044f), // 2 left hip
			new human4_joint(1, 0.044f), // 3 right hip
			new human4_joint(2, 0.25f), // 4 left knee
			new human4_joint(3, 0.25f), // 5 right knee
			new human4_joint(4, 0.25f), // 6 left ankle
			new human4_joint(5, 0.25f), // 7 right ankle
			new human4_joint(6, 0.125f), // 8 left foot
			new human4_joint(7, 0.125f), // 9 right foot
			new human4_joint(1, 0.125f), // 10 navel
			new human4_joint(10, 0.125f), // 11 nipple
			new human4_joint(11, 0.125f), // 12 neck lower
			new human4_joint(12, 0.025f), // 13 neck upper
			new human4_joint(13, 0.125f), // 14 top of head
			new human4_joint(11, 0.1875f), // 15 left shoulder
			new human4_joint(11, 0.1875f), // 16 right shoulder
			new human4_joint(15, 0.1875f), // 17 left elbow
			new human4_joint(16, 0.1875f), // 18 right elbow
			new human4_joint(17, 0.1875f), // 19 left wrist
			new human4_joint(18, 0.1875f), // 20 right wrist
			new human4_joint(19, 0.03125f), // 21 left hand
			new human4_joint(20, 0.03125f) // 22 right hand
	};

	game_time gt = game_time.getInstance();

	int actionStart = gt.getTime();

	byte[] jointsIndicesLines = { (byte) joints[1].parent, 1 // ground to crotch
			, (byte) joints[2].parent, 2 // crotch to l hip
			, (byte) joints[3].parent, 3 // crotch to r hip
			, (byte) joints[4].parent, 4 // l hip to l knee
			, (byte) joints[5].parent, 5 // r hip to r knee
			, (byte) joints[6].parent, 6 // l knee to l ankle
			, (byte) joints[7].parent, 7 // r knee to r ankle
			, (byte) joints[8].parent, 8 // l ankle to l foot
			, (byte) joints[9].parent, 9 // r ankle to r foot
			, (byte) joints[10].parent, 10 // crotch to navel
			, (byte) joints[11].parent, 11 // navel to nips
			, (byte) joints[12].parent, 12 // nips to neck bottom
			, (byte) joints[13].parent, 13 // neck bottom to neck top
			, (byte) joints[14].parent, 14 // neck top to top of head
			, (byte) joints[15].parent, 15 // neck bottom to l shoulder
			, (byte) joints[16].parent, 16 // neck bottom to r shoulder
			, (byte) joints[17].parent, 17 // l shoulder to l elbow
			, (byte) joints[18].parent, 18 // r shoulder to r elbow
			, (byte) joints[19].parent, 19 // l elbow to l wrist
			, (byte) joints[20].parent, 20 // r elbow to r wrist
			, (byte) joints[21].parent, 21 // l wrist to l hand
			, (byte) joints[22].parent, 22 // r wrist to r hand */
	};

	float[] jointsVerticesDraw = { 0f, 0f, 0f // ground 0
			, 0f, 0f, .5f // crotch 1
			, 0f, 0f, .5f // l hip 2
			, 0f, 0f, .5f // r hip 3
			, 0f, 0f, .25f // l knee 4
			, 0f, 0f, .25f // r knee 5
			, 0f, 0f, 0f // l ankle 6
			, 0f, 0f, 0f // r ankle 7
			, 0f, 0f, 0f // l foot 8
			, 0f, 0f, 0f // r foot 9
			, 0f, 0f, 0f // backNavelLine 10
			, 0f, 0f, 0f // backNipLine 11
			, 0f, 0f, 0f // back Neck Bottom 12
			, 0f, 0f, 0f // back Neck Top 13
			, 0f, 0f, 0f // headTop 14
			, 0f, 0f, 0f // l shoulder 15
			, 0f, 0f, 0f // r shoulder 16
			, 0f, 0f, 0f // l elbow 17
			, 0f, 0f, 0f // r elbow 18
			, 0f, 0f, 0f // l wrist 19
			, 0f, 0f, 0f // r wrist 20
			, 0f, 0f, 0f // l hand 21
			, 0f, 0f, 0f // r hand 22
	};

	human4_joint_state jointsNew = new human4_joint_state("stand");
	human4_joint_state jointsOld = new human4_joint_state("stand");
	// human4_joint_state jointsDraw = new human4_joint_state("default");
	human4_joint_state jointsDefault = new human4_joint_state("stand");

	int jointsIndicesLinesCount;// = jointsIndicesLines.length;

	float jointsRed = 1f;
	float jointsBlue = 1f;
	float jointsGreen = 1f;

	int jointsVAOID = -1;
	int jointsVBOID = -1;
	int jointsVBOIID = -1;

	public int jointsGLMVPUniformLocation;
	public int jointsGLRGBAUniformLocation;
	public FloatBuffer jointsFB = BufferUtils.createFloatBuffer(16);

	public int jointsProgramId;
	public int jointsVertexShaderId;
	public int jointsFragmentShaderId;

	boolean isJointsIndexBound = false;

	//public boolean skinDegreesUpdated =false;
	//public Matrix4f MVP;

	public boolean isDrawJoints = true;

	public Vector3f standingXYZ = new Vector3f();
	public float height = -1f;

	public float degreeRotateX = 0.0f;
	public float degreeRotateY = 0.0f;
	public float degreeRotateZ = 0.0f;

	public float moveDistance = .0003f;// * headHeight;
	// float armWidth = 327f;

	Quaternionf dest1 = new Quaternionf();
	Quaternionf dest2 = new Quaternionf();
	Vector3f v = new Vector3f();

	FloatBuffer verticesBufferJoints;

	float percentDone = 0f;

	Vector3f diameterXYZ = new Vector3f();;

	float breathOffset = (new Random()).nextFloat() % 5f + 3f;
	float distNipToLShoulder = -45f;
	float distNipToRShoulder = 45f;

	public human4_joints() {
		// setHeight(6f);

		if (isDrawJoints) {
			this.glConstruct();
		}
	}

	public void bindJointsVertexData() {

		// for(int i = 0; i<draw.length;i++)
		// { draw[i]=jointsVerticesDraw[i]; }

		if (isDrawJoints) {
			if (verticesBufferJoints == null) {
				verticesBufferJoints = BufferUtils.createFloatBuffer(jointsVerticesDraw.length);
			}
			verticesBufferJoints.put(jointsVerticesDraw).flip();

			if (jointsVAOID == -1) {
				jointsVAOID = glGenVertexArrays();
			}
			glBindVertexArray(jointsVAOID);

			if (jointsVBOID == -1) {
				jointsVBOID = glGenBuffers();
			}
			glBindBuffer(GL_ARRAY_BUFFER, jointsVBOID);
			glBufferData(GL_ARRAY_BUFFER, verticesBufferJoints, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);

			if (!isJointsIndexBound) {
				jointsIndicesLinesCount = jointsIndicesLines.length;

				ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(jointsIndicesLinesCount);
				indicesBuffer.put(jointsIndicesLines);
				indicesBuffer.flip();

				jointsVBOIID = glGenBuffers();
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, jointsVBOIID);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

				isJointsIndexBound = true;
			}
		}
	}

	public void breath() {
		int time = gt.getTime();
		boolean up = ((time / 1000) % 2 == 0) ? true : false;
		int now = (time % 1000);

		if (!up) {
			now = 1000 - now;
		}

		// System.out.println("breathOffset : " + breathOffset);

		joints[indexJoints.lShoulder.getIndex()].rotationFromParent.x = distNipToLShoulder
				+ breathOffset * ((float) now / 1000f);
		joints[indexJoints.rShoulder.getIndex()].rotationFromParent.x = distNipToRShoulder
				- breathOffset * ((float) now / 1000f);

		// System.out.println(joints[indexJoints.lShoulder.getIndex()].rotationFromParent.x);
		// System.out.println(joints[indexJoints.rShoulder.getIndex()].rotationFromParent.x);
	}

	public void calculateJointsVertices() {

		double radian = Math.toRadians(degreeRotateZ);
		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		float fX, fY, fZ;

		int timeToComplete = 1000;

		int now = gt.getTime();

		timeToComplete = jointsNew.getTimeToComplete(); //System.out.println("ttc:"+timeToComplete);
		// as speed goes up, timeToComplete goes down 1.0 is normal speed 2.0 is double
		// time speed 3.0 triple...

		percentDone = (float) ((float) (now - actionStart) / ((float) timeToComplete / speed));

		if (percentDone > 1f) {
			percentDone = 0f;
			jointsOld = jointsNew;
			if (jointsNew.getNextState() != null) {
				jointsNew = jointsNew.getNextState();
			} else {
				jointsNew = jointsDefault;
			}
			actionStart = gt.getTime();
		}

		float o, n;
		Vector3f vO, vN;
		int sdrOld, sdrNew;
		for (int i = 0; i < joints.length; i++) {
			vO = jointsOld.getRotations()[i];
			vN = jointsNew.getRotations()[i];
			joints[i].rotationFromParent.x = vO.x + ((vN.x - vO.x) * percentDone);
			joints[i].rotationFromParent.y = vO.y + ((vN.y - vO.y) * percentDone);
			joints[i].rotationFromParent.z = vO.z + ((vN.z - vO.z) * percentDone);

			vO = jointsOld.skin[i];
			vN = jointsNew.skin[i];
			joints[i].rotationSkin.x = vO.x + ((vN.x - vO.x) * percentDone);
			joints[i].rotationSkin.y = vO.y + ((vN.y - vO.y) * percentDone);
			joints[i].rotationSkin.z = vO.z + ((vN.z - vO.z) * percentDone);
			
			
			sdrOld = jointsOld.skinDegrees[i];
			sdrNew = jointsNew.skinDegrees[i];
			int newVal = sdrOld + (int)((float)(sdrNew-sdrOld)*percentDone);
			joints[i].skinRotateDegrees=newVal;
			/*if(newVal!=joints[i].skinRotateDegrees)
			{ joints[i].skinRotateDegrees=newVal; skinDegreesUpdated=true;}
			*/
		}

		breath();

		n = jointsNew.getDistGroundToCrotch();//
		// h2bslNew.getDistGroundToCrotch();
		o = jointsOld.getDistGroundToCrotch();//
		// h2bslOld.getDistGroundToCrotch()
		float dist = o + ((n - o) * percentDone);
		joints[1].distanceFromParent = dist;
		// jointsDraw.setDistGroundToCrotch(dist);
		this.diameterXYZ.z = (height / 2f + dist * (height)) / 2f;

		for (int i = 0; i < joints.length; i++) {
			v.x = 0f;
			v.y = 0f;
			v.z = joints[i].distanceFromParent;

			dest1.w = 1f;
			dest1.x = 0f;
			dest1.y = 0f;
			dest1.z = 0f;

			dest2.w = 1f;
			dest2.x = 0f;
			dest2.y = 0f;
			dest2.z = 0f;

			float x, y, z;
			x = joints[i].rotationFromParent.x;// rotations[i].x;
			y = joints[i].rotationFromParent.y;// rotations[i].y;
			z = joints[i].rotationFromParent.z;// rotations[i].z;

			x = x % 360.0f;
			y = y % 360.0f;
			z = z % 360.0f;

			dest2.rotate((float) Math.toRadians(x), (float) Math.toRadians(y), (float) Math.toRadians(z), dest1);

			dest1.transform(v);

			Vector3f vv = joints[i].modelVertex;// vertices[i];
			Vector3f vvOffset = joints[joints[i].parent].modelVertex;// vertices[parentJoint[i]];

			if (vvOffset == null) {
				vv.x = v.x;
				vv.y = v.y;
				vv.z = v.z;
			} else {
				vv.x = v.x + vvOffset.x;
				vv.y = v.y + vvOffset.y;
				vv.z = v.z + vvOffset.z;
			}
		}

		for (int i = 0; i < joints.length; i++) {

			fX = joints[i].modelVertex.x;
			fY = joints[i].modelVertex.y;
			fZ = joints[i].modelVertex.z;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= height;
			dY *= height;
			dZ *= height;

			dX += standingXYZ.x;
			dY += standingXYZ.y;
			dZ += standingXYZ.z;

			int newI = i * 3;
			jointsVerticesDraw[newI] = (dX);
			jointsVerticesDraw[newI + 1] = (dY);
			jointsVerticesDraw[newI + 2] = (dZ);

			joints[i].literalDrawnVertex.x = dX;
			joints[i].literalDrawnVertex.y = dY;
			joints[i].literalDrawnVertex.z = dZ;
		}
	}

	public void draw() {

		if (height <= 0f) {
			setHeight(6);
		}

		calculateJointsVertices();

		if (isDrawJoints) {
			drawJoints();

			// this is going to go away at some point
			for (int i = 0; i < joints.length; i++) {
				joints[i].draw(manager_mvp.getInstance().get(), height, standingXYZ, degreeRotateZ);
			}
		}
	}

	public void drawJoints() {
		bindJointsVertexData();
		glUseProgram(jointsProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(jointsGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(jointsFB));
		glUniform4f(jointsGLRGBAUniformLocation, jointsRed, jointsGreen, jointsBlue, 1.0f);

		glBindVertexArray(jointsVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, jointsVBOIID);
		glDrawElements(GL_LINES, jointsIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glDrawElements(GL_POINTS, jointsIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
	}

	public human4_joint getJoint(int index)
	{ return joints[index];}
	
	public void glCleanup() {
		glUseProgram(0);
		if (jointsProgramId != 0) {
			if (jointsVertexShaderId != 0) {
				glDetachShader(jointsProgramId, jointsVertexShaderId);
			}
			if (jointsFragmentShaderId != 0) {
				glDetachShader(jointsProgramId, jointsFragmentShaderId);
			}
			glDeleteProgram(jointsProgramId);
		}

	}

	public void glConstruct() {
		if (isDrawJoints) {
			glConstructJoints();
		}

	}

	public void glConstructJoints() {
		try {
			try {
				jointsProgramId = glCreateProgram();

				glCreateVertexShaderJoints(
						"#version 130\nuniform mat4 MVP; in vec3 position; void main() { gl_Position =MVP* vec4(position, 1.0);  gl_PointSize = 3.0; }");

				glCreateFragmentShaderJoints(
						"#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(jointsProgramId);

				glUseProgram(jointsProgramId);
				jointsGLMVPUniformLocation = glGetUniformLocation(jointsProgramId, "MVP");
				jointsGLRGBAUniformLocation = glGetUniformLocation(jointsProgramId, "RGBA");
				glUseProgram(0);

				glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (jointsProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glCreateFragmentShaderJoints(String shaderCode) throws Exception {
		jointsFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, jointsProgramId);
	}

	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShaderJoints(String shaderCode) throws Exception {
		jointsVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, jointsProgramId);
	}

	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}

	public boolean isInterruptable()
	{ return jointsNew.isInterruptable;}
	
	public void move(String position) {

		if (jointsNew.getActionType().compareTo(position) != 0 ) {
			int now = gt.getTime();
			int timeToComplete = jointsNew.getTimeToComplete();
			if (now > (actionStart + timeToComplete)) // if uninterrupted movement
			{				
				jointsOld = jointsNew;
			} else // else interruption
			{
				// old needs to equal current setup
				jointsOld = new human4_joint_state(joints);
			}
			actionStart = now;
			jointsNew = new human4_joint_state(position);
			// System.out.println("Changed joints position "+position+"
			// actionType:"+jointsNew.getActionType());
			/*System.out.println("jointsNew:"+jointsNew.getActionType());
			System.out.println("now:"+now);
			System.out.println("timeToComplete:"+timeToComplete);
			System.out.println("actionStart:"+actionStart);*/
		}

	}

	public void setDiameterXYZ(Vector3f p) {
		this.diameterXYZ = p;
	}

	public void setFacingDegree(float x) {
		degreeRotateZ = x;
	}

	public void setHeight(float x) {
		height = x;
		moveDistance = height * .1f;
	}

//	public void setMVP(Matrix4f mvp) {
//		MVP = mvp;
//	}

	public void setRotationX(float x) {
		degreeRotateX = x;
	}

	public void setRotationY(float x) {
		degreeRotateY = x;
	}

	public void setSpeed(float s) {
		this.speed = s;
	}

	public void setStandingXYZ(float x, float y, float z) {
		standingXYZ.x = x;
		standingXYZ.y = y;
		standingXYZ.z = z;
		// move();
	}

	public void setStandingXY(float newX, float newY) {
		standingXYZ.x = newX;
		standingXYZ.y = newY;
	}

	public void setStandingZ(float newZ) {
		standingXYZ.z = newZ;
	}

}