package local.source.actors.humans.human4;

import org.joml.Matrix4f;
import org.joml.Vector3f;

public class human4_joint {

	public Vector3f modelVertex;
	public Vector3f literalDrawnVertex;
	public Vector3f rotationSkin;
	public Vector3f rotationFromParent; // rotations based off parent to determine vertex
	public int parent;
	public float distanceFromParent; // this is a ratio that's multiplied by height before being drawn
	public int skinRotateDegrees=0;
	
	public human4_joint( int par, float distFromPar)
	{
		literalDrawnVertex = new Vector3f(0f,0f,0f); // I think this isn't needed
		modelVertex = new Vector3f(0f,0f,0f);
		rotationFromParent = new Vector3f(0f,0f,0f);
		rotationSkin =new Vector3f(0f,0f,0f);
		parent = par;
		distanceFromParent=distFromPar;
	}
	
	
	
	human4_joint_drawable drawable = new human4_joint_drawable();
	
	public void draw(Matrix4f mvp, float height, Vector3f standing,float degreeRotateZ)
	{
		drawable.setFacingDegree(degreeRotateZ);
//		drawable.setMVP(mvp);
		drawable.setStandingXYZ(standing.x,standing.y,standing.z);
		//drawable.setJointCenterXYZ(modelVertex.x, modelVertex.y, modelVertex.z);
		drawable.setJointCenterXYZ(literalDrawnVertex.x, literalDrawnVertex.y, literalDrawnVertex.z);
		drawable.setSkinRotationXYZ(rotationSkin.x, rotationSkin.y, rotationSkin.z);
		drawable.setHeight(height);
		drawable.draw();
	}
}
