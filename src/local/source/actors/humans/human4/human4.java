package local.source.actors.humans.human4;

import java.util.ArrayList;
import java.util.List;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import local.source.damage;
import local.source.game_time;
import local.source.interface_engageable;
import local.source.manager_engageables;
import local.source.manager_step_collisions;
import local.source.actors.interface_actor;
import local.source.actors.manager_actors;
import local.source.actors.humans.human4.human4_joints;
import local.source.attacks.manager_attackables;
import local.source.weapons.interface_weapon;
import local.source.worlds.manager_land;

public class human4 implements interface_actor {
	
	human4_body_part_arm_lower_left leftLowerArm = new human4_body_part_arm_lower_left(6);
	human4_body_part_arm_lower_right rightLowerArm = new human4_body_part_arm_lower_right(6);
	human4_body_part_arm_upper_left leftUpperArm = new human4_body_part_arm_upper_left(6);
	human4_body_part_arm_upper_right rightUpperArm = new human4_body_part_arm_upper_right(6);
	human4_body_part_arm_hand_left leftArmHand = new human4_body_part_arm_hand_left(6);
	human4_body_part_arm_hand_right rightArmHand = new human4_body_part_arm_hand_right(6);
	
	List<human4_body_part> listBodyParts = new ArrayList<human4_body_part>();


	float speed = 10.0f;

	float skinRed = 1f;
	float skinGreen = 0f;
	float skinBlue = 0f;

	float hairRed = 1f;
	float hairGreen = 0f;
	float hairBlue = 0f;

	game_time gt = game_time.getInstance();

	manager_land lManage = manager_land.getInstance();
	float stepUpHeight = 1f;
	float stepDownHeight = 2f;
	int maxStepDistance = -1;

	//public Matrix4f MVP;

	Vector3f standingXYZ = new Vector3f();
	Vector3f diameterXYZ = new Vector3f();
	Vector3f engageableDiameterXYZ = new Vector3f();

	public float height = 2.0f;
	public float turnDegree = 2.0f;
	public float degreeRotateX = 0.0f;
	public float degreeRotateY = 0.0f;
	public float degreeRotateZ = 0.0f;
	public float moveDistance = .0003f;// * headHeight;

	human4_joints joints = new human4_joints();

	boolean isMoveable = true; // this has nothing to do with being interruptable

	float fat = .5f;
	float muscle = .5f;

	public human4() {
		
		listBodyParts.add(leftLowerArm);
		listBodyParts.add(rightLowerArm);
		listBodyParts.add(leftUpperArm);
		listBodyParts.add(rightUpperArm);
		listBodyParts.add(leftArmHand);
		listBodyParts.add(rightArmHand);
		
		setHeight(6f);
		registerWithActorManager();
		registerWithAttackableManager();
		registerWithEngageableManager();
		registerWithStepCollisionManager();
		joints.setDiameterXYZ(this.diameterXYZ);
		setStandingXYZ(0f, 0f, 0f);
		setSkinRGB(1f, 0.894f, 0.71f);
	}

	public void draw() {
		
		//this.isMoveable = joints.isInterruptable();
		
		joints.setSpeed(speed); // this should only be passed once and whenever the speed is altered
		joints.setStandingXYZ(standingXYZ.x, standingXYZ.y, standingXYZ.z);
		joints.setFacingDegree(degreeRotateZ);
		joints.setRotationX(degreeRotateX);
		joints.setRotationY(degreeRotateY);

		joints.draw();
		
		int tempJointID = 19;
		leftLowerArm.setRotationZ(degreeRotateZ);
		leftLowerArm.setFromParentRotationXYZ(joints.getJoint(tempJointID).rotationFromParent.x, joints.getJoint(tempJointID).rotationFromParent.y, joints.getJoint(tempJointID).rotationFromParent.z);
		leftLowerArm.setJoint(joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.x, joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.y, joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.z);
		leftLowerArm.setSkinDegreesRotate(joints.getJoint(tempJointID).skinRotateDegrees);
		
		tempJointID=20;
		rightLowerArm.setRotationZ(degreeRotateZ);
		rightLowerArm.setFromParentRotationXYZ(joints.getJoint(tempJointID).rotationFromParent.x, joints.getJoint(tempJointID).rotationFromParent.y, joints.getJoint(tempJointID).rotationFromParent.z);
		rightLowerArm.setJoint(joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.x, joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.y, joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.z);
		rightLowerArm.setSkinDegreesRotate(joints.getJoint(tempJointID).skinRotateDegrees);
		
		tempJointID = 17;
		leftUpperArm.setRotationZ(degreeRotateZ);
		leftUpperArm.setFromParentRotationXYZ(joints.getJoint(tempJointID).rotationFromParent.x, joints.getJoint(tempJointID).rotationFromParent.y, joints.getJoint(tempJointID).rotationFromParent.z);
		leftUpperArm.setJoint(joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.x, joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.y, joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.z);
		leftUpperArm.setSkinDegreesRotate(joints.getJoint(tempJointID).skinRotateDegrees);
		
		tempJointID=18;
		rightUpperArm.setRotationZ(degreeRotateZ);
		rightUpperArm.setFromParentRotationXYZ(joints.getJoint(tempJointID).rotationFromParent.x, joints.getJoint(tempJointID).rotationFromParent.y, joints.getJoint(tempJointID).rotationFromParent.z);
		rightUpperArm.setJoint(joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.x, joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.y, joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.z);
		rightUpperArm.setSkinDegreesRotate(joints.getJoint(tempJointID).skinRotateDegrees);
		
		tempJointID = 21;
		leftArmHand.setRotationZ(degreeRotateZ);
		leftArmHand.setFromParentRotationXYZ(joints.getJoint(tempJointID).rotationFromParent.x, joints.getJoint(tempJointID).rotationFromParent.y, joints.getJoint(tempJointID).rotationFromParent.z);
		leftArmHand.setJoint(joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.x, joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.y, joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.z);
		leftArmHand.setSkinDegreesRotate(joints.getJoint(tempJointID).skinRotateDegrees);
		
		tempJointID=22;
		rightArmHand.setRotationZ(degreeRotateZ);
		rightArmHand.setFromParentRotationXYZ(joints.getJoint(tempJointID).rotationFromParent.x, joints.getJoint(tempJointID).rotationFromParent.y, joints.getJoint(tempJointID).rotationFromParent.z);
		rightArmHand.setJoint(joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.x, joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.y, joints.getJoint(joints.getJoint(tempJointID).parent).literalDrawnVertex.z);
		rightArmHand.setSkinDegreesRotate(joints.getJoint(tempJointID).skinRotateDegrees);
		
		//leftForArm.draw();
		for( human4_body_part hbp : listBodyParts)
		{ hbp.draw(); }
	}

	public void faceLeft() {
		degreeRotateZ = (degreeRotateZ + turnDegree) % 360.0f;
	}

	public void facePoint(float newX, float newY, float newZ) {

		// if(!joints.isDead())
		{
			float dX = newX - standingXYZ.x;
			float dY = newY - standingXYZ.y;
			degreeRotateZ = (float) (Math.atan2(dY, dX) * 180.0f / Math.PI);
		}
	}

	public void faceRight() {
		float change = (degreeRotateZ - turnDegree);
		degreeRotateZ = (float) (change <= 0.0d ? (360.0d - turnDegree) : change % 360.0d);
		// move();
	}

	public String getActorName() {
		return "";// this.actorName;
	}

	public Vector3f getCenterXYZ() {
		// System.out.println("human3.getCenterXYZ x : " + centerXYZ.x + " y :
		// "+ centerXYZ.y+" z : " + centerXYZ.z);
		return standingXYZ;
	}

	public String getClassName() {
		return "";// this.className;
	}

	public Vector3f getDiameterXYZ() {
		return diameterXYZ;
	}

	public Vector3f getEngageableDiameterXYZ() {
		return engageableDiameterXYZ;
	}

	public float getFacingDegrees() {
		return degreeRotateZ;
	}

	private float getFacingX() {
		return (float) (moveDistance * Math.cos(Math.toRadians(degreeRotateZ)));
	}

	public Vector3f getFacingXYZAsIfCenterXYZ() {
		Vector3f v = new Vector3f();
		v.x = this.standingXYZ.x + getFacingX();
		v.y = this.standingXYZ.y + getFacingY();
		v.z = this.standingXYZ.z;
		return v;
	}

	private float getFacingY() {
		return (float) (moveDistance * Math.sin(Math.toRadians(degreeRotateZ)));
	}

	public float getHeight() {
		return height;
	}

	public float getHPPercentageAsFloatZeroToOne() {
		return 1f;// health.getHPPercentageAsFloatZeroToOne();
	}

	private float getLeftX() {
		return (float) (moveDistance * Math.cos(Math.toRadians(degreeRotateZ + 90f)));
	}

	private float getLeftY() {
		return (float) (moveDistance * Math.sin(Math.toRadians(degreeRotateZ + 90f)));
	}

	private float getRightX() {
		return (float) (moveDistance * Math.cos(Math.toRadians(degreeRotateZ - 90f)));
	}

	private float getRightY() {
		return (float) (moveDistance * Math.sin(Math.toRadians(degreeRotateZ - 90f)));
	}

	public float getStandingX() {
		return this.standingXYZ.x;
	}

	public float getStandingY() {
		return this.standingXYZ.y;
	}

	public float getStandingZ() {
		return this.standingXYZ.z;
	}

	public Vector3f getXYZ() {
		Vector3f v = new Vector3f();
		v.x = this.standingXYZ.x;
		v.y = this.standingXYZ.y;
		v.z = this.standingXYZ.z;
		return v;
	}

	public void move(String position) {
		
		//System.out.println("new position:"+position);
	
		if (isMoveable) {
			float dirX = 0f;
			float dirY = 0f;

			if (position.contains("walk")) {
				if (position.contains("forward")) {
					dirX = getFacingX();
					dirY = getFacingY();
				} else if (position.contains("left")) {
					dirX = getLeftX();
					dirY = getLeftY();
				} else if (position.contains("right")) {
					dirX = getRightX();
					dirY = getRightY();
				}
				// else if(position.contains("backward"))
				// {dirX=getBackX(); dirY=getBackY();}
			}
			
			
			
			
			float facingX = standingXYZ.x + dirX;
			float facingY = standingXYZ.y + dirY;
			float facingZ = standingXYZ.z;
			if (lManage != null) {
				facingZ = lManage.getZ(facingX, facingY);
				if (facingZ - standingXYZ.z > stepUpHeight || standingXYZ.z - facingZ > stepDownHeight)// if(Math.abs(z-facingZ)>stepHeight)
				{return;}
			}

			if (manager_step_collisions.getInstance().getCollision(getFacingXYZAsIfCenterXYZ(), getDiameterXYZ(),this) != null) 
			{return;}

			standingXYZ.x = facingX;
			standingXYZ.y = facingY;
			standingXYZ.z = facingZ;

			if (standingXYZ.x < 0f) {standingXYZ.x = 0f;}
			if (standingXYZ.y < 0f) {standingXYZ.y = 0f;}
			if (standingXYZ.y > maxStepDistance) {standingXYZ.y = maxStepDistance;}
			if (standingXYZ.x > maxStepDistance) {standingXYZ.x = maxStepDistance;}			
		}		
		
		joints.move(position);
		
	}
	

	private void moveForward() {
		if (isMoveable) {
			float facingX = standingXYZ.x + getFacingX();
			float facingY = standingXYZ.y + getFacingY();
			float facingZ = standingXYZ.z;
			if (lManage != null) {
				facingZ = lManage.getZ(facingX, facingY);
				if (facingZ - standingXYZ.z > stepUpHeight || standingXYZ.z - facingZ > stepDownHeight)// if(Math.abs(z-facingZ)>stepHeight)
				{
					return;
				}
			}

			if (manager_step_collisions.getInstance().getCollision(getFacingXYZAsIfCenterXYZ(), getDiameterXYZ(),
					this) != null) {
				return;
			}

			standingXYZ.x = facingX;
			standingXYZ.y = facingY;
			standingXYZ.z = facingZ;

			if (standingXYZ.x < 0f) {
				standingXYZ.x = 0f;
			}

			if (standingXYZ.y < 0f) {
				standingXYZ.y = 0f;
			}

			if (standingXYZ.y > maxStepDistance) {
				standingXYZ.y = maxStepDistance;
			}

			if (standingXYZ.x > maxStepDistance) {
				standingXYZ.x = maxStepDistance;
			}
		}

		joints.move("walk-forward");
	}

	private void moveLeft() {

		if (isMoveable) {
			float facingX = standingXYZ.x + getLeftX();
			float facingY = standingXYZ.y + getLeftY();
			float facingZ = standingXYZ.z;
			if (lManage != null) {
				facingZ = lManage.getZ(facingX, facingY);
				if (facingZ - standingXYZ.z > stepUpHeight || standingXYZ.z - facingZ > stepDownHeight)// if(Math.abs(z-facingZ)>stepHeight)
				{
					return;
				}
			}

			standingXYZ.x = facingX;
			standingXYZ.y = facingY;
			standingXYZ.z = facingZ;

			if (standingXYZ.x < 0f) {
				standingXYZ.x = 0f;
			}

			if (standingXYZ.y < 0f) {
				standingXYZ.y = 0f;
			}

			if (standingXYZ.y > maxStepDistance) {
				standingXYZ.y = maxStepDistance;
			}

			if (standingXYZ.x > maxStepDistance) {
				standingXYZ.x = maxStepDistance;
			}
		}

	}

	public void movementDisable() {
		this.isMoveable = false;
	}

	public void movementEnable() {
		this.isMoveable = true;
	}

	private void moveRight() {

		if (isMoveable) {
			float facingX = standingXYZ.x + getRightX();
			float facingY = standingXYZ.y + getRightY();
			float facingZ = standingXYZ.z;
			if (lManage != null) {
				facingZ = lManage.getZ(facingX, facingY);
				if (facingZ - standingXYZ.z > stepUpHeight || standingXYZ.z - facingZ > stepDownHeight)// if(Math.abs(z-facingZ)>stepHeight)
				{
					return;
				}
			}

			standingXYZ.x = facingX;
			standingXYZ.y = facingY;
			standingXYZ.z = facingZ;

			if (standingXYZ.x < 0f) {
				standingXYZ.x = 0f;
			}

			if (standingXYZ.y < 0f) {
				standingXYZ.y = 0f;
			}

			if (standingXYZ.y > maxStepDistance) {
				standingXYZ.y = maxStepDistance;
			}

			if (standingXYZ.x > maxStepDistance) {
				standingXYZ.x = maxStepDistance;
			}
		}
	}

	private void moveStand() {
		joints.move("stand");
	}

	public void registerWithActorManager() {
		manager_actors.getInstance().add(this);
	}

	public void registerWithAttackableManager() {
		manager_attackables.getInstance().add(this);
	}

	public void registerWithEngageableManager() {
		manager_engageables.getInstance().add(this);
	}

	public void registerWithStepCollisionManager() {
		manager_step_collisions.getInstance().add(this);
	}

	public void rotateXPos() {
		degreeRotateX = (degreeRotateX + turnDegree) % 360.0f;
	}

	public void rotateXNeg() {
		degreeRotateX = (degreeRotateX - turnDegree) % 360.0f;
	}

	public void rotateYPos() {
		degreeRotateY = (degreeRotateY + turnDegree) % 360.0f;
	}

	public void rotateYNeg() {
		degreeRotateY = (degreeRotateY - turnDegree) % 360.0f;
	}

	public void setActorName(String n) {
		// this.actorName = n;
	}

	public void setFacing(float x, float y, float z) {
		this.facePoint(x, y, z);
	}

	public void setHairRGB(float r, float g, float b) {
		hairRed = r;
		hairGreen = g;
		hairBlue = b;

		// ((human3_body_part_head)head).setHairRGB(hairRed,hairGreen,hairBlue);
	}

	public void setHeight(float x) {
		height = x;
		moveDistance = height * .1f;

		joints.setHeight(x);

		this.diameterXYZ.x = height / 7f;
		this.diameterXYZ.y = height / 7f;
		this.diameterXYZ.z = height / 2f;

		this.engageableDiameterXYZ.x = height / 2f;
		this.engageableDiameterXYZ.y = height / 2f;
		this.engageableDiameterXYZ.z = height / 1.5f;
		
		//leftForArm.setHeight(x);
		for(human4_body_part p : listBodyParts)
		{ p.setHeight(x); }
	}

	public void setHP(int h) { // health.setHP(h);
	}

	public void setLandManager(manager_land l) {
		lManage = l;
	}

	public void setMaxStepDistance(int newDist) {
		maxStepDistance = newDist;
	}

//	public void setMVP(Matrix4f mvp) {
//		MVP = mvp;
//		joints.setMVP(mvp);
//	}

	public void setSkinRGB(float r, float g, float b) {
		skinRed = r;
		skinGreen = g;
		skinBlue = b;
		
		//leftLowerArm.setRGB(r, g, b);
		for( human4_body_part hbp : listBodyParts)
		{ hbp.setRGB(r,g,b); }
	}

	public void setStandingXYZ(float x, float y, float z) {
		this.standingXYZ.x = x;
		this.standingXYZ.y = y;
		this.standingXYZ.z = z;
	}

	public void testResetVertices() {

	}

	@Override
	public void takeDamage(damage d) {
		// TODO Auto-generated method stub

	}

	@Override
	public void engaged() {
		// TODO Auto-generated method stub

	}

	@Override
	public Vector3f getStandingXYZ() {
		// TODO Auto-generated method stub
		return null;
	}
}