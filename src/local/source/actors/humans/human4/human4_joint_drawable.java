package local.source.actors.humans.human4;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL32.*;
//import static org.lwjgl.opengl.GL40.*;
//import static org.lwjgl.opengl.GL45.*;

import java.nio.*;
import java.util.Calendar;

import org.lwjgl.BufferUtils;

import local.source.game_time;
import local.source.manager_mvp;
import local.source.attacks.interface_attack;

import org.joml.QuaternionfInterpolator;
import org.joml.Quaternionf;
import org.joml.Matrix4f;
import org.joml.Vector3f;

public class human4_joint_drawable {

	// when you rework the joints ( and you really need to )
	// consider breathing and moving more than just the shoulders
	// change the center of the upper back to move a little too
	// that should be enough to move the entire upper torso when skins come
	// along
	// or maybe add a few new joints for chest center or tommy and move those
	// around

	Vector3f centerXYZ = new Vector3f();
	Vector3f skinXYZ = new Vector3f();
	Vector3f standingXYZ = new Vector3f();

	float height = 6f;

	Vector3f[] verticesModel = { new Vector3f(0.1f, 0f, 0f), new Vector3f(-0.05f, 0.1f, 0f),
			new Vector3f(-0.05f, -0.1f, 0f) };

	Vector3f[] verticesDrawing = { new Vector3f(0.1f, 0f, 0f), new Vector3f(-0.05f, 0.1f, 0f),
			new Vector3f(-0.05f, -0.1f, 0f) };

	byte[] indicesLines = { 0, 1 //
			, 1, 2 //
			, 2, 0 //
	};

	float[] verticesDraw = { 0.1f, 0f, 0f //
			, -0.05f, 0.1f, 0f //
			, -0.05f, -0.1f, 0f //
	};

	int indicesLinesCount;// = jointsIndicesLines.length;


	int jointsVAOID = -1;
	int jointsVBOID = -1;
	int jointsVBOIID = -1;

	public int jointsGLMVPUniformLocation;
	public int jointsGLRGBAUniformLocation;
	public FloatBuffer jointsFB = BufferUtils.createFloatBuffer(16);

	public int jointsProgramId;
	public int jointsVertexShaderId;
	public int jointsFragmentShaderId;

	boolean isJointsIndexBound = false;

	//public Matrix4f MVP;

	public boolean isDrawJoints = true;

	public float degreeRotateX = 0.0f;
	public float degreeRotateY = 0.0f;
	public float degreeRotateZ = 0.0f;

	Quaternionf dest1 = new Quaternionf();
	Quaternionf dest2 = new Quaternionf();
	Vector3f v = new Vector3f();

	FloatBuffer verticesBufferJoints;
	FloatBuffer colorsBuffer;
	public int VBColors = -1;
	float[] verticesColor = new float[3*4];



	public human4_joint_drawable() {
		// setHeight(6f);

		verticesColor[0]=1f;
		verticesColor[1]=0f;
		verticesColor[2]=0f;		
		verticesColor[3]=0f;
		
		verticesColor[4]=0f;
		verticesColor[5]=1f;
		verticesColor[6]=0f;
		verticesColor[7]=0f;
		
		verticesColor[8]=0f;
		verticesColor[9]=0f;
		verticesColor[10]=1f;
		verticesColor[11]=0f;

		if (isDrawJoints) {
			this.glConstruct();
		}
	}

	public void bindJointsVertexData() {

		// for(int i = 0; i<draw.length;i++)
		// { draw[i]=jointsVerticesDraw[i]; }

		if (isDrawJoints) {
			if (verticesBufferJoints == null) {
				verticesBufferJoints = BufferUtils.createFloatBuffer(verticesDraw.length);
			}
			verticesBufferJoints.put(verticesDraw).flip();

			if (colorsBuffer == null) {
				colorsBuffer = BufferUtils.createFloatBuffer(verticesColor.length);
			}
			colorsBuffer.put(verticesColor).flip();
			
			if (jointsVAOID == -1) {
				jointsVAOID = glGenVertexArrays();
			}
			glBindVertexArray(jointsVAOID);

			if (jointsVBOID == -1) {
				jointsVBOID = glGenBuffers();
			}
			glBindBuffer(GL_ARRAY_BUFFER, jointsVBOID);
			glBufferData(GL_ARRAY_BUFFER, verticesBufferJoints, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			//glBindVertexArray(0);
			
			if (VBColors == -1) {
				VBColors = glGenBuffers();
			}
			glBindBuffer(GL_ARRAY_BUFFER, VBColors);
			glBufferData(GL_ARRAY_BUFFER, colorsBuffer, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(1, 4, GL_FLOAT, false, 0, 0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);

			if (!isJointsIndexBound) {
				indicesLinesCount = indicesLines.length;

				ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(indicesLinesCount);
				indicesBuffer.put(indicesLines);
				indicesBuffer.flip();

				jointsVBOIID = glGenBuffers();
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, jointsVBOIID);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

				isJointsIndexBound = true;
			}
		}
	}

	// int second=0;
	// int newsecond=0;
	// boolean isNewSecond=false;

	public void calculateJointsVertices() {

		double radian = Math.toRadians(degreeRotateZ);
		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		float fX, fY, fZ;
		float magnify = 2f;

		for (int i = 0; i < verticesModel.length; i++) {
			v.x = verticesModel[i].x*magnify;// 0f;
			v.y = verticesModel[i].y*magnify;// 0f;
			v.z = verticesModel[i].z*magnify;// 0f;

			dest1.w = 1f;
			dest1.x = 0f;
			dest1.y = 0f;
			dest1.z = 0f;

			dest2.w = 1f;
			dest2.x = 0f;
			dest2.y = 0f;
			dest2.z = 0f;

			float x, y, z;
			x = skinXYZ.x;// rotations[i].x;
			y = skinXYZ.y;// rotations[i].y;
			z = skinXYZ.z;// rotations[i].z;
			//System.out.println("skinX:"+skinXYZ.x+"    skinY:"+skinXYZ.y+"    skinZ:"+skinXYZ.z);

			x = x % 360.0f;
			y = y % 360.0f;
			z = z % 360.0f;

			dest2.rotate((float) Math.toRadians(x), (float) Math.toRadians(y), (float) Math.toRadians(z), dest1);

			dest1.transform(v);

			Vector3f vv = verticesDrawing[i];// vertices[i];

			vv.x = v.x;
			vv.y = v.y;
			vv.z = v.z;

		}

		for (int i = 0; i < verticesModel.length; i++) {

			fX = verticesDrawing[i].x;
			fY = verticesDrawing[i].y;
			fZ = verticesDrawing[i].z;
			//System.out.println("fX:" + fX + "     fY" + fY + "    fZ:" + fZ);

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			// dX *= height;
			// dY *= height;
			// dZ *= height;

			//dX += centerXYZ.x * height;
			//dY += centerXYZ.y * height;
			//dZ += centerXYZ.z * height;

			//dX += standingXYZ.x;
			//dY += standingXYZ.y;
			//dZ += standingXYZ.z;
			
			dX += centerXYZ.x;
			dY += centerXYZ.y;
			dZ += centerXYZ.z;

			int newI = i * 3;
			verticesDraw[newI] = (dX);
			verticesDraw[newI + 1] = (dY);
			verticesDraw[newI + 2] = (dZ);

			//System.out.println("dX:" + dX + "    dY:" + dY + "     dZ:" + dZ + "    height:" + height);
			//System.out.println("cX:" + centerXYZ.x + "     cY:" + centerXYZ.y + "     cZ:" + centerXYZ.z);
		}
	}

	public void draw() {

		calculateJointsVertices();

		if (isDrawJoints) {
			drawJoints();
		}

	}

	public void drawJoints() {
		bindJointsVertexData();
		glUseProgram(jointsProgramId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(jointsGLMVPUniformLocation, false, manager_mvp.getInstance().get().get(jointsFB));
		//glUniform4f(jointsGLRGBAUniformLocation, jointsRed, jointsGreen, jointsBlue, 1.0f);

		glBindVertexArray(jointsVAOID);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, jointsVBOIID);
		glDrawElements(GL_LINES, indicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glDrawElements(GL_POINTS, indicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glBindVertexArray(0);

		glUseProgram(0);

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
	}

	public void glCleanup() {
		glUseProgram(0);
		if (jointsProgramId != 0) {
			if (jointsVertexShaderId != 0) {
				glDetachShader(jointsProgramId, jointsVertexShaderId);
			}
			if (jointsFragmentShaderId != 0) {
				glDetachShader(jointsProgramId, jointsFragmentShaderId);
			}
			glDeleteProgram(jointsProgramId);
		}

	}

	public void glConstruct() {
		if (isDrawJoints) {
			glConstructJoints();
		}

	}

	public void glConstructJoints() {
		try {
			try {
				jointsProgramId = glCreateProgram();

				glCreateVertexShaderJoints(
						"#version 130\nuniform mat4 MVP; in vec3 position; in vec4 in_color; out vec4 pass_color; void main() { pass_color=in_color; gl_Position =MVP* vec4(position, 1.0);  gl_PointSize = 3.0; }");

				glCreateFragmentShaderJoints(
						"#version 130\nin vec4 pass_color; out vec4 fragColor;  void main() { fragColor = pass_color; }");

				glLink(jointsProgramId);

				glUseProgram(jointsProgramId);
				jointsGLMVPUniformLocation = glGetUniformLocation(jointsProgramId, "MVP");
				jointsGLRGBAUniformLocation = glGetUniformLocation(jointsProgramId, "RGBA");
				glUseProgram(0);

				glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (jointsProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glCreateFragmentShaderJoints(String shaderCode) throws Exception {
		jointsFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, jointsProgramId);
	}

	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShaderJoints(String shaderCode) throws Exception {
		jointsVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, jointsProgramId);
	}

	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}
	
	public void setFacingDegree(float x) {
		degreeRotateZ = x;
	}

	public void setHeight(float h) {
		this.height = h;
	}

	public void setJointCenterXYZ(float x, float y, float z) {
		centerXYZ.x = x;
		centerXYZ.y = y;
		centerXYZ.z = z;
	}

//	public void setMVP(Matrix4f mvp) {
//		MVP = mvp;
//	}

	public void setSkinRotationXYZ(float x, float y, float z) {
		skinXYZ.x = x;
		skinXYZ.y = y;
		skinXYZ.z = z;
		//System.out.println("human4_joint_drawable setSkinRotationXYZ skinX:"+skinXYZ.x+"    skinY:"+skinXYZ.y+"    skinZ:"+skinXYZ.z);

	}

	public void setStandingXYZ(float x, float y, float z) {
		standingXYZ.x = x;
		standingXYZ.y = y;
		standingXYZ.z = z;
	}

}