package local.source.actors.humans.human4;

import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import local.source.game_time;

import org.joml.Vector3f;

public class human4_joint_state {

	human4_joint_state_manager manager = human4_joint_state_manager.getInstance();

	String strNextActionStateHolder = "";

	game_time gt = game_time.getInstance();
	Vector3f[] rotations = new Vector3f[] { 
			new Vector3f(0f, 0f, 0f), // 0 ground
			new Vector3f(0f, 0f, 0f), // 1 crotch
			new Vector3f(0f, 0f, 0f), // 2 left hip
			new Vector3f(0f, 0f, 0f), // 3 right hip
			new Vector3f(0f, 0f, 0f), // 4 left knee
			new Vector3f(0f, 0f, 0f), // 5 right knee
			new Vector3f(0f, 0f, 0f), // 6 left ankle
			new Vector3f(0f, 0f, 0f), // 7 right ankle
			new Vector3f(0f, 0f, 0f), // 8 left foot
			new Vector3f(0f, 0f, 0f), // 9 right foot
			new Vector3f(0f, 0f, 0f), // 10 navel
			new Vector3f(0f, 0f, 0f), // 11 nipple
			new Vector3f(0f, 0f, 0f), // 12 neck lower
			new Vector3f(0f, 0f, 0f), // 13 neck upper
			new Vector3f(0f, 0f, 0f), // 14 top of head
			new Vector3f(0f, 0f, 0f), // 15 left shoulder
			new Vector3f(0f, 0f, 0f), // 16 right shoulder
			new Vector3f(0f, 0f, 0f), // 17 left elbow
			new Vector3f(0f, 0f, 0f), // 18 right elbow
			new Vector3f(0f, 0f, 0f), // 19 left wrist
			new Vector3f(0f, 0f, 0f), // 20 right wrist
			new Vector3f(0f, 0f, 0f), // 21 left hand
			new Vector3f(0f, 0f, 0f)  // 22 right hand
	};
	
	Vector3f[] skin = new Vector3f[] { 
			new Vector3f(0f, 0f, 0f), // 0 ground
			new Vector3f(0f, 0f, 0f), // 1 crotch
			new Vector3f(0f, 0f, 0f), // 2 left hip
			new Vector3f(0f, 0f, 0f), // 3 right hip
			new Vector3f(0f, 0f, 0f), // 4 left knee
			new Vector3f(0f, 0f, 0f), // 5 right knee
			new Vector3f(0f, 0f, 0f), // 6 left ankle
			new Vector3f(0f, 0f, 0f), // 7 right ankle
			new Vector3f(0f, 0f, 0f), // 8 left foot
			new Vector3f(0f, 0f, 0f), // 9 right foot
			new Vector3f(0f, 0f, 0f), // 10 navel
			new Vector3f(0f, 0f, 0f), // 11 nipple
			new Vector3f(0f, 0f, 0f), // 12 neck lower
			new Vector3f(0f, 0f, 0f), // 13 neck upper
			new Vector3f(0f, 0f, 0f), // 14 top of head
			new Vector3f(0f, 0f, 0f), // 15 left shoulder
			new Vector3f(0f, 0f, 0f), // 16 right shoulder
			new Vector3f(0f, 0f, 0f), // 17 left elbow
			new Vector3f(0f, 0f, 0f), // 18 right elbow
			new Vector3f(0f, 0f, 0f), // 19 left wrist
			new Vector3f(0f, 0f, 0f), // 20 right wrist
			new Vector3f(0f, 0f, 0f), // 21 left hand
			new Vector3f(0f, 0f, 0f)  // 22 right hand
	};
	
	int[] skinDegrees = new int[]
			{
					0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
			};
	
	int timeToComplete=30000;
	float distGroundToCrotch = .5f;
	String strState = "none";
	// int enumVal;
	
	String actionType = "default";
	public boolean isInterruptable = true;

	human4_joint_state nextState;

	public human4_joint_state(String strFile) {
		// enumVal = val;
		strState = strFile;
		readFromActionStateFile(strFile);

		manager.put(strFile, this);
	}
	
	public human4_joint_state(human4_joint[] joints) {
	
		for(int i = 0 ; i < joints.length;i++)
		{
			rotations[i].x=joints[i].rotationFromParent.x;
			rotations[i].y=joints[i].rotationFromParent.y;
			rotations[i].z=joints[i].rotationFromParent.z;

			skin[i].x=joints[i].rotationSkin.x;
			skin[i].y=joints[i].rotationSkin.y;
			skin[i].z=joints[i].rotationSkin.z;
			
			skinDegrees[i]=joints[i].skinRotateDegrees;
		}		
	}

	public String getActionType() {return actionType;}
	
	public float getDistGroundToCrotch() {
		return distGroundToCrotch;
	}

	public human4_joint_state getNextState() {
		// return nextState;
		return manager.get(strNextActionStateHolder);
	}

	public Vector3f[] getRotations() {
		return rotations;
	}
	
	public Vector3f[] getSkins() {
		return skin;
	}
	
	public String getState() {
		return strState;
	}

	public int getTimeToComplete() {
		return timeToComplete;
	}


	public void readFromActionStateFile(String fileName) {
		try {	
			String pathToFile =  "/" 
					+ "actors" + "/" 				
					+ "human4" + "/" 
					+ "joints" + "/" 
					+ fileName + ".xml";
			
			
			URL jointURL = getClass().getResource(pathToFile);
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
			Document document = documentBuilder.parse(jointURL.openStream());//pathToFile);
			document.getDocumentElement().normalize();
			NodeList nList = document.getElementsByTagName("actionState");

			for (int i = 0; i < nList.getLength(); i++) {
				Node n = nList.item(i);	
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					Element e = (Element) n;
					for (int j = 0; j < e.getElementsByTagName("joints").getLength(); j++) {
						Element ee = (Element) e.getElementsByTagName("joints").item(j);
						for (int k = 0; k < ee.getElementsByTagName("joint").getLength(); k++) {

							Element eee = (Element) ee.getElementsByTagName("joint").item(k);
							//String eeeName = eee.getAttribute("name");
							String pIndex = eee.getAttribute("index");
							String pX = eee.getAttribute("fromParentX");
							String pY = eee.getAttribute("fromParentY");
							String pZ = eee.getAttribute("fromParentZ");
							String sX = eee.getAttribute("skinX");
							String sY = eee.getAttribute("skinY");
							String sZ = eee.getAttribute("skinZ");
							String sDegrees = eee.getAttribute("skinDegrees");

							int index = Integer.parseInt(pIndex);
							float x = Float.parseFloat(pX);
							float y = Float.parseFloat(pY);
							float z = Float.parseFloat(pZ);
							rotations[index].x = x;
							rotations[index].y = y;
							rotations[index].z = z;
							
							 x = Float.parseFloat(sX);
							 y = Float.parseFloat(sY);
							 z = Float.parseFloat(sZ);
							skin[index].x = x;
							skin[index].y = y;
							skin[index].z = z;
							
							skinDegrees[index] = Integer.parseInt(sDegrees);
							
							//System.out.println("human4_joint_state  readFromActionStateFile   skinX:"+x+"    skinY:"+y+"     skinZ:"+z);
						}
					}

					Element eTTC = (Element) e.getElementsByTagName("timeToComplete").item(0);
					String strTTC = eTTC.getAttribute("value");
					int intTTC = Integer.parseInt(strTTC);
					timeToComplete = intTTC;
										
					Element eDistGtC = (Element) e.getElementsByTagName("distGroundToCrotch").item(0);
					String strDistGtC = eDistGtC.getAttribute("value");
					float fltDistGtC = Float.parseFloat(strDistGtC);
					distGroundToCrotch = fltDistGtC;

					Element eNext = (Element) e.getElementsByTagName("nextActionState").item(0);
					String strNext = eNext.getAttribute("value");
					strNextActionStateHolder = strNext;
					
					Element eActionType = (Element) e.getElementsByTagName("actionType").item(0);
					String strActionType = eActionType.getAttribute("value");
					actionType = strActionType;
					
					Element eIsInterruptable = (Element) e.getElementsByTagName("interruptable").item(0);
					String strIsInterruptable = eIsInterruptable.getAttribute("value");
					this.isInterruptable = (strIsInterruptable.compareTo("false")==0)?false:true;

					break;
				}
			}

		} catch (Exception e) {
			System.out.println("exception caught in human4_joint_state.readFromActionStateFile : " + e.getMessage() + " " + e.getStackTrace());
		}

	}


	public void setDistGroundToCrotch(float f) {
		distGroundToCrotch = f;
	}

}
