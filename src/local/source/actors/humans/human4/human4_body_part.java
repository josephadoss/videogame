package local.source.actors.humans.human4;

public interface human4_body_part {
	
	public void draw();
	
	public void setHeight(float height);
	
	public void setRGB(float r, float g, float b);

}
