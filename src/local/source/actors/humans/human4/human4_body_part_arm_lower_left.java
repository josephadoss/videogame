package local.source.actors.humans.human4;
import static org.lwjgl.opengl.GL11.GL_ALPHA_TEST;
import static org.lwjgl.opengl.GL11.GL_BACK;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_FRONT;
import static org.lwjgl.opengl.GL11.GL_NEAREST;
import static org.lwjgl.opengl.GL11.GL_ONE;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_ENV;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_ENV_MODE;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;

import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glCullFace;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glTexEnvf;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import static org.lwjgl.opengl.GL13.GL_CLAMP_TO_BORDER;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VALIDATE_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glGetAttribLocation;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;
import static org.lwjgl.opengl.GL20.glGetProgrami;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform4f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;
import static org.lwjgl.stb.STBImage.stbi_image_free;
import static org.lwjgl.stb.STBImage.stbi_load_from_memory;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import local.source.globalStatic;
import local.source.manager_mvp;

public class human4_body_part_arm_lower_left implements human4_body_part {


Vector3f jointXYZ = new Vector3f(0f,0f,0f);

int skinDegreesRotate = 0;

float widthY, heightZ, lengthX;	

//public Matrix4f MVP;
public Vector3f fromParentRotationXYZ = new Vector3f(0f,0f,0f);
float rotationZ = 0f;
Quaternionf dest1 = new Quaternionf();
Quaternionf dest2 = new Quaternionf();
Vector3f v = new Vector3f();	
	
	
	Vector3f[] verticesModel ;/*= { 
			new Vector3f(0f,0f, 0f), // 0 top front right 
			new Vector3f(0f,0f, 0f), // 1 top front left
			new Vector3f(0f,0f, 0f), // 2 top back left
			new Vector3f(0f,0f, 0f), // 3 top back right
			new Vector3f(0f,0f, 0f), // 4 bot front right
			new Vector3f(0f,0f, 0f), // 5 bot front left
			new Vector3f(0f,0f, 0f), // 6 bot back left
			new Vector3f(0f,0f, 0f) // 7 bot back right
	};*/

	Vector3f[] vertices = { 
				new Vector3f(0f, 0f, 0f), // 0 top front right 
				new Vector3f(0f, 0f, 0f), // 1 top front left
				new Vector3f(0f, 0f, 0f), // 2 top back left
				new Vector3f(0f, 0f, 0f), // 3 top back right
				new Vector3f(0f, 0f, 0f), // 4 bot front right
				new Vector3f(0f, 0f, 0f), // 5 bot front left
				new Vector3f(0f, 0f, 0f), // 6 bot back left
				new Vector3f(0f, 0f, 0f), // 7 bot back right
				new Vector3f(0f, 0f, 0f), // 8 top front right, again
				new Vector3f(0f, 0f, 0f) // 9 bot front right, again
		};
	
	int[] indices = { 
			0,1,4// front
			,1,5,4// front
			,3,8,7// right - change
			,8,9,7// right - change
			,2,7,6// back
			,2,3,7// back
			,1,2,5// left
			,2,6,5// left
			,3,2,0// top
			,2,1,0// top
			,4,5,6// bottom
			,4,6,7// bottom
			
	};
	

	int vertexNumbers=5;
	float[] verticesDraw = { 
			0f,0f,0f,1f,1f// 0
			,0f,0f,0f,0.75f,1f// 1
			,0f,0f,0f,0.5f,1f// 2
			,0f,0f,0f,0.25f,1f// 3
			,0f,0f,0f,1f,0f// 4
			,0f,0f,0f,0.75f,0f// 5
			,0f,0f,0f,0.5f,0f// 6
			,0f,0f,0f,0.25f,0f// 7 
			,0f,0f,0f,0f,1f// 8
			,0f,0f,0f,0f,0f// 9 
	};



	int indicesLinesCount;
	
	float skinRed = 0.863f;
	float skinBlue = 0.863f;
	float skinGreen = 0.863f;
		
	
	int VAOID = -1;
	int VBOID = -1;
	int VBOIID = -1;
		

	public int GLMVPUniformLocation;
	public int GLRGBAUniformLocation;
	public FloatBuffer FB = BufferUtils.createFloatBuffer(20);
	public int programId;
	public int vertexShaderId;
	public int fragmentShaderId;
	boolean isIndexBound = false;
	public boolean isDraw = true;
	FloatBuffer verticesBuffer;
	
	IntBuffer textureWidth = BufferUtils.createIntBuffer(1);
	IntBuffer textureHeight = BufferUtils.createIntBuffer(1);
	IntBuffer textureComponents = BufferUtils.createIntBuffer(1);
	ByteBuffer textureData;
	String textureSource = "resources" + "/" + "actors"  + "/" + "human4" + "/" + "textures" + "/" + "outfits" + "/" + "shirts" + "/" + "nude" + "/" + "arm_lower.png";
	int textureID = -1;
	public int GLTextureCoordLocation;
	public int GLTextureImageUniform;



	

	public human4_body_part_arm_lower_left( float height)  {
		
		setHeight(height);
		
		
		if (isDraw) {
			
			try {
				textureData = stbi_load_from_memory(globalStatic.ioResourceToByteBuffer(textureSource, 20 * 1024), textureWidth, textureHeight, textureComponents, 4);
			} catch (Exception e) {
				System.out.println("human4_body_part_arm_lower_left constructor : error thrown making the texture message:" + e.getMessage() + " stacktrace:" + e.getStackTrace());
			}
			
			this.glConstruct();
		}
		
	//	System.out.println("human3_weapon_sword_simple_joints.constructor leaving");
	}

	public void bindVertexData() {

		if (isDraw) {
			if (verticesBuffer == null) {
				verticesBuffer = BufferUtils.createFloatBuffer(verticesDraw.length);
			}
			verticesBuffer.put(verticesDraw).flip();
			
			if (textureID == -1) {
				textureID = glGenTextures();

				glBindTexture(GL_TEXTURE_2D, textureID);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureWidth.get(), textureHeight.get(), 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData);
				stbi_image_free(textureData);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

				if (textureData == null) {
					System.out.println("about to make texture with null textureData");
				}

			}

			if (VAOID == -1) {
				VAOID = glGenVertexArrays();
			}
			glBindVertexArray(VAOID);

			if (VBOID == -1) {
				VBOID = glGenBuffers();
			}
			glBindBuffer(GL_ARRAY_BUFFER, VBOID);
			glBufferData(GL_ARRAY_BUFFER, verticesBuffer, GL_DYNAMIC_DRAW);
			//glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 20, 0);
			glVertexAttribPointer(1, 2, GL_FLOAT, false, 20, 12);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);

			if (!isIndexBound) {
				indicesLinesCount = indices.length;

				IntBuffer indicesBuffer = BufferUtils.createIntBuffer(indicesLinesCount);
				indicesBuffer.put(indices);
				indicesBuffer.flip();

				VBOIID = glGenBuffers();
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIID);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

				isIndexBound = true;
			}
		}
	}

	private void calcuateModelVertices()
	{
		float height = heightZ;
		height *= 0.1875f;	

		float lengthTop = height/7f;
		float widthTop = height/7f;
		
		float lengthBot = height/10f;
		float widthBot = height/10f;
		
		
		
		
		double skinRadian = Math.toRadians(this.skinDegreesRotate);	
		float skinCosRadian = (float) Math.cos(skinRadian);
		float skinSinRadian = (float) Math.sin(skinRadian);
		
		//rotate around joint alignment, skinRotationDegrees
		/*float sX = fX * skinCosRadian - fY * skinSinRadian;
		float sY = fY * skinCosRadian + fX * skinSinRadian;

		fX=sX;
		fY=sY;*/
		
		float tfrX = lengthTop *skinCosRadian - (-widthTop) *skinSinRadian;
		float tfrY = (-widthTop)*skinCosRadian + lengthTop*skinSinRadian;
		
		float tflX = lengthTop *skinCosRadian - (widthTop) *skinSinRadian;
		float tflY = (widthTop)*skinCosRadian + lengthTop*skinSinRadian;
		
		float tblX = (-lengthTop) *skinCosRadian - (widthTop) *skinSinRadian;
		float tblY = (widthTop)*skinCosRadian + (-lengthTop)*skinSinRadian;
		
		float tbrX = (-lengthTop) *skinCosRadian - (-widthTop) *skinSinRadian;
		float tbrY = (-widthTop)*skinCosRadian + (-lengthTop)*skinSinRadian;
		
		
		float bfrX = lengthBot *skinCosRadian - (-widthBot) *skinSinRadian;
		float bfrY = (-widthBot)*skinCosRadian + lengthBot*skinSinRadian;
		
		float bflX = lengthBot *skinCosRadian - (widthBot) *skinSinRadian;
		float bflY = (widthBot)*skinCosRadian + lengthBot*skinSinRadian;
		
		float bblX = (-lengthBot) *skinCosRadian - (widthBot) *skinSinRadian;
		float bblY = (widthBot)*skinCosRadian + (-lengthBot)*skinSinRadian;
		
		float bbrX = (-lengthBot) *skinCosRadian - (-widthBot) *skinSinRadian;
		float bbrY = (-widthBot)*skinCosRadian + (-lengthBot)*skinSinRadian;
		
		verticesModel = new Vector3f[] { 
				new Vector3f( tfrX, tfrY,  height), // 0 top front right 
				new Vector3f( tflX, tflY,  height), // 1 top front left
				new Vector3f( tblX, tblY,  height), // 2 top back left
				new Vector3f( tbrX, tbrY,  height), // 3 top back right
				new Vector3f( bfrX, bfrY, 0f), // 4 bot front right
				new Vector3f( bflX, bflY, 0f), // 5 bot front left
				new Vector3f( bblX, bblY, 0f), // 6 bot back left
				new Vector3f( bbrX, bbrY, 0f), // 7 bot back right
				new Vector3f( tfrX, tfrY,  height), // 8 top front right, again
				new Vector3f( bfrX, bfrY, 0f)  // 9 bot front right, again
				/*new Vector3f(  lengthTop, -widthTop,  height), // 0 top front right 
				new Vector3f(  lengthTop,  widthTop,  height), // 1 top front left
				new Vector3f( -lengthTop,  widthTop,  height), // 2 top back left
				new Vector3f( -lengthTop, -widthTop,  height), // 3 top back right
				
				new Vector3f(  lengthBot, -widthBot, 0f), // 4 bot front right
				new Vector3f(  lengthBot,  widthBot, 0f), // 5 bot front left
				new Vector3f( -lengthBot,  widthBot, 0f), // 6 bot back left
				new Vector3f( -lengthBot, -widthBot, 0f), // 7 bot back right
				new Vector3f( lengthTop, -widthTop,  height), // 8 top front right, again
				new Vector3f(   lengthBot, -widthBot, 0f)  // 9 bot front right, again
				*/
		};
	}
	
	public void calculateVertices(Vector3f[] verticesModel, Vector3f[] vertices, float[] verticesDraw, Vector3f offsetXYZ) {

		double radian = Math.toRadians(rotationZ);	
		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);
		float fX, fY, fZ;
				
		for (int i = 0; i < vertices.length; i++) {
			v.x = verticesModel[i].x;
			v.y = verticesModel[i].y;
			v.z = verticesModel[i].z;

			dest1.w = 1f;
			dest1.x = 0f;
			dest1.y = 0f;
			dest1.z = 0f;

			dest2.w = 1f;
			dest2.x = 0f;
			dest2.y = 0f;
			dest2.z = 0f;

			float x, y, z;
			x = fromParentRotationXYZ.x ;//+ rotationXYZ.x;// testX;
			y = fromParentRotationXYZ.y ;//+ rotationXYZ.y;//testY;
			z = fromParentRotationXYZ.z ;//+ rotationXYZ.z;//testZ;
		
			x = x % 360.0f;
			y = y % 360.0f;
			z = z % 360.0f;

			dest2.rotate((float) Math.toRadians(x), (float) Math.toRadians(y), (float) Math.toRadians(z), dest1);
			dest1.transform(v);

			Vector3f vv = vertices[i];

			vv.x = v.x;
			vv.y = v.y;
			vv.z = v.z;
		}

		for (int i = 0; i < vertices.length; i++) {

			fX = vertices[i].x;// * lengthX/* X */;
			fY = vertices[i].y;// * widthY/* Y */;
			fZ = vertices[i].z;// * heightZ/* Z */;
			


			// rotate to facing alignment
			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX += offsetXYZ.x;
			dY += offsetXYZ.y;
			dZ += offsetXYZ.z;

			int newI = i * vertexNumbers;
			verticesDraw[newI] = (dX);
			verticesDraw[newI + 1] = (dY);
			verticesDraw[newI + 2] = (dZ);	
			
			//if (vertexNumbers == 5) { // this could be optimized to only run once
			//	verticesDraw[newI + 3] = whModel[i].x;// frow;
			//	verticesDraw[newI + 4] = whModel[i].y;// fcol;
			//}
		}	
	}

	public void draw() { 		
		
		this.calcuateModelVertices();
		calculateVertices(verticesModel, vertices, verticesDraw,jointXYZ);
		
		if(isDraw)
		{
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);
			glCullFace(GL_FRONT);
			//glCullFace(GL_BACK);

			glEnable(GL_TEXTURE_2D);
			glEnable(GL_ALPHA_TEST);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);
			glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

			glUseProgram(programId);
			bindVertexData();


			glUniformMatrix4fv(GLMVPUniformLocation, false, manager_mvp.getInstance().get().get(FB));
			glUniform4f(GLRGBAUniformLocation, skinRed, skinGreen, skinBlue, 1.0f);

			glBindVertexArray(VAOID);
			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);


			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIID);
			glBindTexture(GL_TEXTURE_2D, textureID);

			glDrawElements(GL_TRIANGLES, indicesLinesCount, GL_UNSIGNED_INT, 0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			glDisableVertexAttribArray(0);
			glDisableVertexAttribArray(1);

			glBindVertexArray(0);

			glUseProgram(0);

			glDisable(GL_DEPTH_TEST);
			glDisable(GL_CULL_FACE);
			glDisable(GL_TEXTURE_2D);
			glDisable(GL_ALPHA_TEST);
			glDisable(GL_BLEND);
		}
		
	}

	public void glCleanup() {
/*		glUseProgram(0);
		if (jointsProgramId != 0) {
			if (jointsVertexShaderId != 0) {
				glDetachShader(jointsProgramId, jointsVertexShaderId);
			}
			if (jointsFragmentShaderId != 0) {
				glDetachShader(jointsProgramId, jointsFragmentShaderId);
			}
			glDeleteProgram(jointsProgramId);
		}
*/
	}


	public void glConstruct() {
		try {
			try {/*
				programId = glCreateProgram();

				glCreateVertexShader("#version 130\n"//
						+"uniform mat4 MVP; "//
						+"in vec3 position; "//
						+"void main() "//
						+"{ gl_Position =MVP* vec4(position, 1.0); }");

				glCreateFragmentShader("#version 130\n"//
						+"uniform vec4 RGBA; "//
						+"out vec4 out_color;  "//
						+"void main() "//
						+"{ out_color = RGBA; }");

				glLink(programId);

				glUseProgram(programId);
				GLMVPUniformLocation = glGetUniformLocation(programId, "MVP");
				GLRGBAUniformLocation = glGetUniformLocation(programId, "RGBA");
				glUseProgram(0);*/
				
				programId = glCreateProgram();

				glCreateVertexShader("#version 130         \n"//
						+ "uniform mat4 MVP;                       \n"//
						+ "in vec3 position;                       \n"//
						+ "in vec2 texcoord;                       \n"//
						+ "out vec2 textureCoord;              \n"//
						+ "void main() {                           \n"//
						+ "     gl_Position =MVP* vec4(position, 1.0); \n"//
						+ "       textureCoord = texcoord;           \n"//
						+ "}");

				glCreateFragmentShader("#version 130       \n"//
						+ "uniform vec4 RGBA; \n"//
						+ "in vec2 textureCoord;              \n"//
						+ "uniform sampler2D texImage;             \n"//
						+ "out vec4 out_color;                     \n"//
						+ "void main() {                           \n"//
						+ "   out_color= texture(texImage,textureCoord);\n"//
						+ " if(out_color.a<0.5f) \n" + " { out_color = RGBA ;}" + "}   ");

				glLink(programId);

				glUseProgram(programId);
				GLMVPUniformLocation = glGetUniformLocation(programId, "MVP");
				GLRGBAUniformLocation = glGetUniformLocation(programId, "RGBA");
				GLTextureCoordLocation = glGetAttribLocation(programId, "texcoord");
				GLTextureImageUniform = glGetUniformLocation(programId, "texImage");

				glUseProgram(0);

				//glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (programId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}
	

	public void glCreateFragmentShader(String shaderCode) throws Exception {
		fragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, programId);
	}
	
	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShader(String shaderCode) throws Exception {
		vertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, programId);
	}

	
	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}

	
	
	public void setFromParentRotationXYZ(float x, float y, float z) {
		fromParentRotationXYZ.x = x;
		fromParentRotationXYZ.y = y;
		fromParentRotationXYZ.z = z;
	}
	
	
	public void setHeight(float height)
	{		
		heightZ=height;		
		//this.calcuateModelVertices();
	}
	
	public void setJoint(float x, float y, float z)
	{
		jointXYZ.x=x;
		jointXYZ.y=y;
		jointXYZ.z=z;		
	}
	

//	public void setCenterXYZ(float x, float y, float z) {
//		centerXYZ.x = x;
//		centerXYZ.y = y;
//		centerXYZ.z = z;
//	}

//	public void setMVP(Matrix4f mvp) {
//		MVP = mvp; //System.out.println("set sword MVP");
//	}

	public void setRGB(float r, float g, float b)
	{ skinRed = r; skinBlue = b; skinGreen = g;}


	public void setRotationZ(float x) {
		//degreeRotateY = x;
		rotationZ=x;
	}
	
	public void setSkinDegreesRotate(int newRotate)
	{ 
		this.skinDegreesRotate=newRotate;
		//this.calcuateModelVertices();
	}
}
