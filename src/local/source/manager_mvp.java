package local.source;

import java.util.*;

import org.joml.Matrix4f;
import org.joml.Vector3f;

public class manager_mvp {
	private Matrix4f MVP = new Matrix4f();

	private static manager_mvp instance = null;
	
	private manager_mvp()
	{}
			
	public static manager_mvp getInstance()
	{
		if(instance==null)
		{instance = new manager_mvp();}
		return instance;
	}
	
	public Matrix4f get() { return MVP;}
	

	
}
