package local.source.attacks;

import java.util.ArrayList;
import java.util.List;

import org.joml.Matrix4f;

import local.source.game_time;
public class manager_attacks {

	private static manager_attacks instance = null;
	game_time gt =game_time.getInstance();
	List<interface_attack> list = new ArrayList<interface_attack>(); // these are going to be short lived... rethink storage
	
	//land_landManager lManage = land_landManager.getInstance();
	
	private manager_attacks()
	{}
	
	public void add(interface_attack a)
	{ list.add(a);  }
	
	public void draw()
	{ 
		for(interface_attack a : list)
		{ a.draw();}
		
	}
	
	public static manager_attacks getInstance()
	{
		if(instance==null)
		{instance = new manager_attacks();}
		return instance;
	}
	
	
	
	//public void setLandManager(land_landManager l)
	//{
	//	for(actor a : actors)
	//	{ a.setLandManager(l);}
	//}
	
//	public void setMVP(Matrix4f MVP)
//	{
//		for(interface_attack a : list)
//		{a.setMVP(MVP);}
//	}
		
	
	
	public void remove(interface_attack b)
	{
		for(interface_attack a : list)
		{
			if(a==b)
			{
				list.remove(a);
				return;
			}
		}
	}
}