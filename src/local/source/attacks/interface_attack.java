package local.source.attacks;


import org.joml.*;

import local.source.damage;
import local.source.game_time;
import local.source.actors.humans.human3.human3_joint_rotations_lower_body;
import local.source.actors.humans.human3.human3_joint_rotations_upper_body;
import local.source.weapons.interface_weapon_rotation;

import java.util.*;


public interface interface_attack {
	
	Vector3f centerXYZ = new Vector3f();
	Vector3f diameterXYZ = new Vector3f();
	
	// an attack can be an invisible punch at one xyz location
	// or a very visible fire ball traveling along a trajectory spanning multiple xyzs

	public damage damage = null;
	
	public Matrix4f MVP=null;

	
	public String attackName = "interface_attack";
	public String className = "interface_attack";
	
	Vector3f handleXYZ = new Vector3f();
	Vector3f facingDegrees = new Vector3f();
	float travelSpeedPerSecond = 1f; // one foot
	
	boolean isAttacking = false;
	
	interface_attackable myHolder = null;
	List<interface_attackable> listAlreadyAttacked = null;

	int lastAttack = 0;
	int attackEveryMilliseconds = 200;
	game_time gt = game_time.getInstance();
	
	//interface_weapon_rotation weapon_joints = null;
	
	// add something for bounding box, so a massive fireball could damage more things than a small one
	// attack range and such
	// special magical properties of attack
	
	// the attack manager will figure out the collision detection
	
	public void attack();
	
	public void draw();
	
	public String getAttackName();

	public human3_joint_rotations_lower_body getHuman3BodyStateLower();

	public human3_joint_rotations_upper_body getHuman3BodyStateUpper();
	
	public Vector3f getCenterXYZ();
	
	public String getClassName();
		
	public damage getDamage();
	
	public Vector3f getDiameterXYZ();
	
	public Vector3f getFacingDegrees();
	
	public Vector3f getHandleXYZ();
	
	public boolean getIsAttacking();

	
	public float getTravelSpeedPerSecond();
	
	public interface_weapon_rotation getWeaponRotation();
	
	public void registerWithManager();
		
	public void setAttackName(String n);
	
	public void setCenterXYZ(Vector3f n);
	
	public void setCenterXYZ(float x, float y, float z);
	
	public void setDamage(damage n);
	
	public void setDiameterXYZ(Vector3f n);
	
	public void setDiameterXYZ(float x, float y, float z);
	
	public void setFacingDegrees(float x, float y, float z);
	
	public void setFacingDegrees(Vector3f nfacingDegrees);
	
	public void setHandleXYZ(float x, float y, float z);
	
	public void setHandleXYZ(Vector3f nxyz);
	
	public void setHolder(interface_attackable a);
	
	public void setIsAttacking(boolean b);
	
//	public void setMVP(Matrix4f mvp);
	
	public void setTravelSpeedPerSecond(float f);
	


}



