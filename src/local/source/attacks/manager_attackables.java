package local.source.attacks;

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector3f;

import local.source.game_time;



public class manager_attackables {
	// if running this gets expensive, consider breaking it out into a thread and just running it like five times a second

	private static manager_attackables instance = null;
	game_time gt =game_time.getInstance();
	List<interface_attackable> list = new ArrayList<interface_attackable>(); // these won't exist after being attacked to the point of death, so long term storage is bad idea
	
	//land_landManager lManage = land_landManager.getInstance();
	
	private manager_attackables()
	{}
	
	public void add(interface_attackable a)
	{ list.add(a);  }
	

	
	public static manager_attackables getInstance()
	{
		if(instance==null)
		{instance = new manager_attackables();}
		return instance;
	}
	

	
	public List<interface_attackable> getCollision(interface_attack b) {
		List<interface_attackable> result = new ArrayList<interface_attackable>();
		
		for (interface_attackable a : list) {
			Vector3f aC = a.getCenterXYZ();
			Vector3f bC = b.getCenterXYZ();

			Vector3f aD = a.getDiameterXYZ();
			Vector3f bD = b.getDiameterXYZ();

			if (a != b) {
				float axr = aC.x + aD.x;
				float axl = aC.x - aD.x;

				float bxr = bC.x + bD.x;
				float bxl = bC.x - bD.x;

				if ((axl >= bxl && axl <= bxr) || (axr >= bxl && axr <= bxr) || (bxl >= axl && bxl <= axr) || (bxr >= axl && bxr <= axr)) {
					float ayr = aC.y + aD.y;
					float ayl = aC.y - aD.y;

					float byr = bC.y + bD.y;
					float byl = bC.y - bD.y;
					if ((ayl >= byl && ayl <= byr) || (ayr >= byl && ayr <= byr) || (byl >= ayl && byl <= ayr) || (byr >= ayl && byr <= ayr)) {
						float azr = aC.z + aD.z;
						float azl = aC.z - aD.z;

						float bzr = bC.z + bD.z;
						float bzl = bC.z - bD.z;
						if ((azl >= bzl && azl <= bzr) || (azr >= bzl && azr <= bzr) || (bzl >= azl && bzl <= azr) || (bzr >= azl && bzr <= azr)) {
							result.add(a);
						}
					}
				}
			}
		}

		return result;
	}
	

	public List<interface_attackable> getCollision(Vector3f center, Vector3f diameter) {
		List<interface_attackable> result = new ArrayList<interface_attackable>();

		for (interface_attackable a : list) {
			Vector3f aC = a.getCenterXYZ();
			Vector3f bC = center;// b.getCenterXYZ();

			Vector3f aD = a.getDiameterXYZ();
			Vector3f bD = diameter;// b.getDiameterXYZ();

			//if (a != b) 
			{
				float axr = aC.x + aD.x;
				float axl = aC.x - aD.x;

				float bxr = bC.x + bD.x;
				float bxl = bC.x - bD.x;

				if ((axl >= bxl && axl <= bxr) || (axr >= bxl && axr <= bxr) || (bxl >= axl && bxl <= axr) || (bxr >= axl && bxr <= axr)) {
					float ayr = aC.y + aD.y;
					float ayl = aC.y - aD.y;

					float byr = bC.y + bD.y;
					float byl = bC.y - bD.y;
					if ((ayl >= byl && ayl <= byr) || (ayr >= byl && ayr <= byr) || (byl >= ayl && byl <= ayr) || (byr >= ayl && byr <= ayr)) {
						float azr = aC.z + aD.z;
						float azl = aC.z - aD.z;

						float bzr = bC.z + bD.z;
						float bzl = bC.z - bD.z;
						if ((azl >= bzl && azl <= bzr) || (azr >= bzl && azr <= bzr) || (bzl >= azl && bzl <= azr) || (bzr >= azl && bzr <= azr)) {
							result.add(a);
						}
					}
				}
			}
		}

		return result;
	}
	
	

	public List<interface_attackable> getCollision(Vector3f center, Vector3f diameter,interface_attackable b) {
		List<interface_attackable> result = new ArrayList<interface_attackable>();

		for (interface_attackable a : list) {
			Vector3f aC = a.getCenterXYZ();
			Vector3f bC = center;// b.getCenterXYZ();

			Vector3f aD = a.getDiameterXYZ();
			Vector3f bD = diameter;// b.getDiameterXYZ();

			if (a != b) 
			{
				float axr = aC.x + aD.x;
				float axl = aC.x - aD.x;

				float bxr = bC.x + bD.x;
				float bxl = bC.x - bD.x;

				if ((axl >= bxl && axl <= bxr) || (axr >= bxl && axr <= bxr) || (bxl >= axl && bxl <= axr) || (bxr >= axl && bxr <= axr)) {
					float ayr = aC.y + aD.y;
					float ayl = aC.y - aD.y;

					float byr = bC.y + bD.y;
					float byl = bC.y - bD.y;
					if ((ayl >= byl && ayl <= byr) || (ayr >= byl && ayr <= byr) || (byl >= ayl && byl <= ayr) || (byr >= ayl && byr <= ayr)) {
						float azr = aC.z + aD.z;
						float azl = aC.z - aD.z;

						float bzr = bC.z + bD.z;
						float bzl = bC.z - bD.z;
						if ((azl >= bzl && azl <= bzr) || (azr >= bzl && azr <= bzr) || (bzl >= azl && bzl <= azr) || (bzr >= azl && bzr <= azr)) {
							result.add(a);
						}
					}
				}
			}
		}

		return result;
	}
	
	
	public void remove(interface_attackable b)
	{
		for(interface_attackable a : list)
		{
			if(a==b)
			{
				list.remove(a);
				return;
			}
		}
	}
		
	
}
