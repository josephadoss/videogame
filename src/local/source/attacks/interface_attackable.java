package local.source.attacks;

import org.joml.*;

import local.source.damage;

public interface interface_attackable {	
	
	Vector3f centerXYZ = new Vector3f();
	Vector3f diameterXYZ = new Vector3f();
	
	public Vector3f getCenterXYZ();
	
	public Vector3f getDiameterXYZ();
	
		
	public void registerWithAttackableManager();
	
	public void takeDamage(damage d);
}
