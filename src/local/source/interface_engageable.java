package local.source;

import org.joml.*;

public interface interface_engageable {

	Vector3f centerXYZ = new Vector3f();
	Vector3f engageableDiameterXYZ = new Vector3f();
	
	public void engaged();
	
	public Vector3f getCenterXYZ();
	
	public Vector3f getEngageableDiameterXYZ();
	
	public void registerWithEngageableManager();
	
	
}


