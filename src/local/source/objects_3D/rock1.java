package local.source.objects_3D;
import static org.lwjgl.opengl.GL11.GL_ALPHA_TEST;
import static org.lwjgl.opengl.GL11.GL_BACK;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_FRONT;
import static org.lwjgl.opengl.GL11.GL_NEAREST;
import static org.lwjgl.opengl.GL11.GL_ONE;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_ENV;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_ENV_MODE;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;

import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glCullFace;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glTexEnvf;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import static org.lwjgl.opengl.GL13.GL_CLAMP_TO_BORDER;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VALIDATE_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glGetAttribLocation;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;
import static org.lwjgl.opengl.GL20.glGetProgrami;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform4f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;
import static org.lwjgl.stb.STBImage.stbi_image_free;
import static org.lwjgl.stb.STBImage.stbi_load_from_memory;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import local.source.globalStatic;
import local.source.interface_step_collisionable;
import local.source.manager_mvp;
import local.source.manager_step_collisions;
import local.source.flat_plants.manager_flat_plants;

public class rock1 implements interface_objects_3D , interface_step_collisionable {




float widthY, heightZ, lengthX;	

float rotationZ = 0f;
Quaternionf dest1 = new Quaternionf();
Quaternionf dest2 = new Quaternionf();
Vector3f v = new Vector3f();	
	
	
	Vector3f[] verticesModel ;/*= { 
			new Vector3f(0f,0f, 0f), // 0 top front right 
			new Vector3f(0f,0f, 0f), // 1 top front left
			new Vector3f(0f,0f, 0f), // 2 top back left
			new Vector3f(0f,0f, 0f), // 3 top back right
			new Vector3f(0f,0f, 0f), // 4 bot front right
			new Vector3f(0f,0f, 0f), // 5 bot front left
			new Vector3f(0f,0f, 0f), // 6 bot back left
			new Vector3f(0f,0f, 0f) // 7 bot back right
	};*/

	Vector3f[] vertices = { 
				new Vector3f(0f, 0f, 0f), // 0 top front right 
				new Vector3f(0f, 0f, 0f), // 1 top front left
				new Vector3f(0f, 0f, 0f), // 2 top back left
				new Vector3f(0f, 0f, 0f), // 3 top back right
				new Vector3f(0f, 0f, 0f), // 4 bot front right
				new Vector3f(0f, 0f, 0f), // 5 bot front left
				new Vector3f(0f, 0f, 0f), // 6 bot back left
				new Vector3f(0f, 0f, 0f), // 7 bot back right
				new Vector3f(0f, 0f, 0f), // 8 top front right, again
				new Vector3f(0f, 0f, 0f), // 9 bot front right, again
				new Vector3f(0f, 0f, 0f), // 10 bot front right, again
				new Vector3f(0f, 0f, 0f), // 11 bot front right, again
				new Vector3f(0f, 0f, 0f) // 12 bot front right, again
		};
	
	int[] indices = { 
			0,6,5//
			,5,11,4//
			,4,10,3//
			,3,9,2//
			,2,8,1//
			,1,7,0//
			,0,7,6//
			,5,6,11//
			,4,11,10//
			,3,10,9//
			,2,9,8//
			,1,8,7//
			,7,12,6//
			,6,12,11//
			,11,12,10//
			,10,12,9//
			,9,12,8//
			,8,12,7//
	};
	

	int vertexNumbers=5;
	float[] verticesDraw = { 
			0f,0f,0f,.5f,1f// 0
			,0f,0f,0f,1f,.75f// 1
			,0f,0f,0f,1f,.25f// 2
			,0f,0f,0f,0.5f,0f// 3
			,0f,0f,0f,0f,0.25f// 4
			,0f,0f,0f,0f,0.75f// 5
			,0f,0f,0f,0.33f,0.75f// 6
			,0f,0f,0f,0.66f,0.75f// 7 
			,0f,0f,0f,0.83f,.5f// 8
			,0f,0f,0f,0.66f,0.25f// 9 
			,0f,0f,0f,0.33f,0.25f// 10 
			,0f,0f,0f,0.16f,0.5f// 11 
			,0f,0f,0f,0.5f,0.5f// 12 
	};



	int indicesLinesCount;
	
	float skinRed = 0.863f;
	float skinBlue = 0.863f;
	float skinGreen = 0.863f;
		
	
	int VAOID = -1;
	int VBOID = -1;
	int VBOIID = -1;
		

	public int GLMVPUniformLocation;
	public int GLRGBAUniformLocation;
	public FloatBuffer FB = BufferUtils.createFloatBuffer(20);
	public int programId;
	public int vertexShaderId;
	public int fragmentShaderId;
	boolean isIndexBound = false;
	public boolean isDraw = true;
	FloatBuffer verticesBuffer;
	
	IntBuffer textureWidth = BufferUtils.createIntBuffer(1);
	IntBuffer textureHeight = BufferUtils.createIntBuffer(1);
	IntBuffer textureComponents = BufferUtils.createIntBuffer(1);
	ByteBuffer textureData;
	//String textureSource = "resources" + "/" + "objects_3D" + "/" + "rock1.png";
	String textureSourcePath = "resources/objects_3D/";
	String textureSourceFullPath= textureSourcePath+ "rock1.png";
	int textureID = -1;
	public int GLTextureCoordLocation;
	public int GLTextureImageUniform;


	Vector3f position = new Vector3f(0f,0f,0f);
	Vector3f diameterXYZ = new Vector3f(0f,0f,0f);


	

	public rock1( )  {
		
		setHeight(10);
		
		
		if (isDraw) {
			
			try {
				textureData = stbi_load_from_memory(globalStatic.ioResourceToByteBuffer(textureSourceFullPath, 20 * 1024), textureWidth, textureHeight, textureComponents, 4);
			} catch (Exception e) {
				System.out.println("rock1 constructor : error thrown making the texture message:" + e.getMessage() + " stacktrace:" + e.getStackTrace());
			}
			
			this.glConstruct();
		}
		
		this.registerWithFlatPlantManager();
		this.registerWithStepCollisionManager();
		
	//	System.out.println("human3_weapon_sword_simple_joints.constructor leaving");
	}

	public void bindVertexData() {

		if (isDraw) {
			if (verticesBuffer == null) {
				verticesBuffer = BufferUtils.createFloatBuffer(verticesDraw.length);
			}
			verticesBuffer.put(verticesDraw).flip();
			
			if (textureID == -1) {
				textureID = glGenTextures();

				glBindTexture(GL_TEXTURE_2D, textureID);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureWidth.get(), textureHeight.get(), 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData);
				stbi_image_free(textureData);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

				if (textureData == null) {
					System.out.println("about to make texture with null textureData");
				}

			}

			if (VAOID == -1) {
				VAOID = glGenVertexArrays();
			}
			glBindVertexArray(VAOID);

			if (VBOID == -1) {
				VBOID = glGenBuffers();
			}
			glBindBuffer(GL_ARRAY_BUFFER, VBOID);
			glBufferData(GL_ARRAY_BUFFER, verticesBuffer, GL_DYNAMIC_DRAW);
			//glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 20, 0);
			glVertexAttribPointer(1, 2, GL_FLOAT, false, 20, 12);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);

			if (!isIndexBound) {
				indicesLinesCount = indices.length;

				IntBuffer indicesBuffer = BufferUtils.createIntBuffer(indicesLinesCount);
				indicesBuffer.put(indices);
				indicesBuffer.flip();

				VBOIID = glGenBuffers();
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIID);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

				isIndexBound = true;
			}
		}
	}

	private void calcuateModelVertices()
	{
		float height = heightZ;

		
		verticesModel = new Vector3f[] { 
				new Vector3f(  0.5f,1f,0f), // 0 top front right 
				new Vector3f(  1f,0.75f,0f), // 1 top front right 
				new Vector3f(  1f,0.25f,0f), // 2 top front right 
				new Vector3f(  0.5f,0f,0f), // 3 top front right 
				new Vector3f(  0f,0.25f,0f), // 4 top front right 
				new Vector3f(  0f,0.75f,0f), // 5 top front right 
				new Vector3f(  0.33f,0.75f,0.75f), // 6 top front right 
				new Vector3f(  0.66f,0.75f,0.75f), // 7 top front right 
				new Vector3f(  0.83f,0.5f,0.75f), // 8 top front right 
				new Vector3f(  0.66f,0.25f,0.75f), // 9 top front right 
				new Vector3f(  0.33f,0.25f,0.75f), // 10 top front right 
				new Vector3f(  0.16f,0.5f,0.75f), // 11 top front right 
				new Vector3f(  0.5f,0.5f,1f), // 12 top front right 				
		};
	}
	
	public void calculateVertices(Vector3f[] verticesModel, Vector3f[] vertices, float[] verticesDraw, Vector3f offsetXYZ) {

		double radian = Math.toRadians(rotationZ);	
		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);
		float fX, fY, fZ;
			/*	
		for (int i = 0; i < vertices.length; i++) {
			v.x = verticesModel[i].x;
			v.y = verticesModel[i].y;
			v.z = verticesModel[i].z;

			dest1.w = 1f;
			dest1.x = 0f;
			dest1.y = 0f;
			dest1.z = 0f;

			dest2.w = 1f;
			dest2.x = 0f;
			dest2.y = 0f;
			dest2.z = 0f;

			float x, y, z;
			x = fromParentRotationXYZ.x ;//+ rotationXYZ.x;// testX;
			y = fromParentRotationXYZ.y ;//+ rotationXYZ.y;//testY;
			z = fromParentRotationXYZ.z ;//+ rotationXYZ.z;//testZ;
		
			x = x % 360.0f;
			y = y % 360.0f;
			z = z % 360.0f;

			dest2.rotate((float) Math.toRadians(x), (float) Math.toRadians(y), (float) Math.toRadians(z), dest1);
			dest1.transform(v);

			Vector3f vv = vertices[i];

			vv.x = v.x;
			vv.y = v.y;
			vv.z = v.z;
		}*/

		for (int i = 0; i < vertices.length; i++) {

			fX = verticesModel[i].x;// * lengthX/* X */;
			fY = verticesModel[i].y;// * widthY/* Y */;
			fZ = verticesModel[i].z;// * heightZ/* Z */;
			


			// rotate to facing alignment
			float dX = fX*heightZ ;//* cosRadian - fY * sinRadian;
			float dY = fY*heightZ;// * cosRadian + fX * sinRadian;
			float dZ = fZ*heightZ;

			dX += offsetXYZ.x;
			dY += offsetXYZ.y;
			dZ += offsetXYZ.z;

			int newI = i * vertexNumbers;
			verticesDraw[newI] = (dX);
			verticesDraw[newI + 1] = (dY);
			verticesDraw[newI + 2] = (dZ);	
			
			//if (vertexNumbers == 5) { // this could be optimized to only run once
			//	verticesDraw[newI + 3] = whModel[i].x;// frow;
			//	verticesDraw[newI + 4] = whModel[i].y;// fcol;
			//}
		}	
	}

	public void draw() { 		
		

		if(isDraw)
		{
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);
			glCullFace(GL_FRONT);
			//glCullFace(GL_BACK);

			glEnable(GL_TEXTURE_2D);
			glEnable(GL_ALPHA_TEST);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);
			glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

			glUseProgram(programId);
			bindVertexData();


			glUniformMatrix4fv(GLMVPUniformLocation, false, manager_mvp.getInstance().get().get(FB));
			glUniform4f(GLRGBAUniformLocation, skinRed, skinGreen, skinBlue, 1.0f);

			glBindVertexArray(VAOID);
			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);


			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIID);
			glBindTexture(GL_TEXTURE_2D, textureID);

			glDrawElements(GL_TRIANGLES, indicesLinesCount, GL_UNSIGNED_INT, 0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			glDisableVertexAttribArray(0);
			glDisableVertexAttribArray(1);

			glBindVertexArray(0);

			glUseProgram(0);

			glDisable(GL_DEPTH_TEST);
			glDisable(GL_CULL_FACE);
			glDisable(GL_TEXTURE_2D);
			glDisable(GL_ALPHA_TEST);
			glDisable(GL_BLEND);
		}
		
	}

	public Vector3f getCenterXYZ() {
		return position;
	}

	public Vector3f getDiameterXYZ() {
		return diameterXYZ;
	}
	
	public void glCleanup() {
/*		glUseProgram(0);
		if (jointsProgramId != 0) {
			if (jointsVertexShaderId != 0) {
				glDetachShader(jointsProgramId, jointsVertexShaderId);
			}
			if (jointsFragmentShaderId != 0) {
				glDetachShader(jointsProgramId, jointsFragmentShaderId);
			}
			glDeleteProgram(jointsProgramId);
		}
*/
	}


	public void glConstruct() {
		try {
			try {				
				programId = glCreateProgram();

				glCreateVertexShader("#version 130         \n"//
						+ "uniform mat4 MVP;                       \n"//
						+ "in vec3 position;                       \n"//
						+ "in vec2 texcoord;                       \n"//
						+ "out vec2 textureCoord;              \n"//
						+ "void main() {                           \n"//
						+ "     gl_Position =MVP* vec4(position, 1.0); \n"//
						+ "       textureCoord = texcoord;           \n"//
						+ "}");

				glCreateFragmentShader("#version 130       \n"//
						+ "uniform vec4 RGBA; \n"//
						+ "in vec2 textureCoord;              \n"//
						+ "uniform sampler2D texImage;             \n"//
						+ "out vec4 out_color;                     \n"//
						+ "void main() {                           \n"//
						+ "   out_color= texture(texImage,textureCoord);\n"//
						+ " if(out_color.a<0.5f) \n" + " { out_color = RGBA ;}" + "}   ");

				glLink(programId);

				glUseProgram(programId);
				GLMVPUniformLocation = glGetUniformLocation(programId, "MVP");
				GLRGBAUniformLocation = glGetUniformLocation(programId, "RGBA");
				GLTextureCoordLocation = glGetAttribLocation(programId, "texcoord");
				GLTextureImageUniform = glGetUniformLocation(programId, "texImage");

				glUseProgram(0);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (programId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}
	

	public void glCreateFragmentShader(String shaderCode) throws Exception {
		fragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, programId);
	}
	
	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShader(String shaderCode) throws Exception {
		vertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, programId);
	}

	
	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}

	public void registerWithFlatPlantManager() {
		manager_objects_3D.getInstance().add(this);	
		
	}
	
	public void registerWithStepCollisionManager() { manager_step_collisions.getInstance().add(this); }
	
	public void setHeight(float height)
	{		
		heightZ=height;		
		//this.calcuateModelVertices();
		this.diameterXYZ.x = height/1.5f;
		this.diameterXYZ.y = height/1.5f;
		this.diameterXYZ.z = height;
		
		this.calcuateModelVertices();
		calculateVertices(verticesModel, vertices, verticesDraw,position);
		
	}
	
	public void setImage(String image)
	{
		textureSourceFullPath= textureSourcePath+ image+".png";
		
		try {
			textureData = stbi_load_from_memory(globalStatic.ioResourceToByteBuffer(textureSourceFullPath, 20 * 1024), textureWidth, textureHeight, textureComponents, 4);
		} catch (Exception e) {
			System.out.println(className + " constructor : error thrown making the texture message:" + e.getMessage() + " stacktrace:" + e.getStackTrace());
		}
	}


	public void setRGB(float r, float g, float b)
	{ skinRed = r; skinBlue = b; skinGreen = g;}


	public void setRotationZ(float x) {
		//degreeRotateY = x;
		rotationZ=x;
		
		this.calcuateModelVertices();
		calculateVertices(verticesModel, vertices, verticesDraw,position);
		
	}
	
	public void setXYZ(float x, float y, float z)
	{
		this.position.x=x;
		this.position.y=y;
		this.position.z=z;
		
		this.calcuateModelVertices();
		calculateVertices(verticesModel, vertices, verticesDraw,position);
		
	}




	@Override
	public void setFacing(float x, float y, float z) {
		// TODO Auto-generated method stub
		
	}
	
}
