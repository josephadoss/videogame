package local.source.objects_3D;

import java.util.ArrayList;
import java.util.List;
import org.joml.*;

import local.source.game_time;

public class manager_objects_3D {

	private static manager_objects_3D instance = null;
	game_time gt = game_time.getInstance();
	List<interface_objects_3D> objects = new ArrayList<interface_objects_3D>();

	float x = 0;
	float y = 0;
	float z = 0;
	float collisionDetection = 40f;

	private manager_objects_3D() {
	}

	public void add(interface_objects_3D a) {
		objects.add(a); //System.out.println("rock added");
	}

	public void draw() {
		for (interface_objects_3D a : objects) {
			Vector3f c = a.getCenterXYZ();
			if (c.x > (x - collisionDetection) && c.x < (x + collisionDetection)) {
				if (c.y > (y - collisionDetection) && c.y < (y + collisionDetection)) {
					if (c.z > (z - collisionDetection) && c.z < (z + collisionDetection)) {
						a.draw(); //System.out.println("drew rock1");
					}
				}
			}
		}
	}

	public static manager_objects_3D getInstance() {
		if (instance == null) {
			instance = new manager_objects_3D();
		}
		return instance;
	}

	public void remove(interface_objects_3D b) {
		for (interface_objects_3D a : objects) {
			if (a == b) {
				objects.remove(a);
				return;
			}
		}
	}

	public void removeAll() {
		for (interface_objects_3D a : objects) {
			objects.remove(a);
		}
	}

	public void setCenter(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public void setFacing(float x, float y, float z) {
		for (interface_objects_3D a : objects) {
			a.setFacing(x, y, z);
		}
	}

//	public void setMVP(Matrix4f MVP) {
//		for (interface_flat_plant a : plants) {
//			a.setMVP(MVP);
//		}
//	}

}
