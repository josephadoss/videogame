package local.source.hud;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VALIDATE_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glDeleteProgram;
import static org.lwjgl.opengl.GL20.glDetachShader;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;
import static org.lwjgl.opengl.GL20.glGetProgrami;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform4f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;
//import static org.lwjgl.opengl.GL32.GL_GEOMETRY_SHADER;
import static org.lwjgl.opengl.GL32.GL_PROGRAM_POINT_SIZE;
import static org.lwjgl.stb.STBImage.*;

import static org.lwjgl.opengl.GL11.*;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.opengl.GLUtil;
import org.lwjgl.system.Callback;




public class hud_testing {
	
	byte[] hudIndicesLines = { //0,1,2
			2,1,0
	};

	float[] hudVerticesDraw = { 0f, 0f, 0f//
			, 1f, 0f, 0f //
			, 0.5f, 1f, 0f //			
			};

	
	int hudIndicesLinesCount;// = jointsIndicesLines.length;

	float hudRed = 1f;
	float hudBlue = 1f;
	float hudGreen = 1f;

	int hudVAOID = -1;
	int hudVBOID = -1;
	int hudVBOIID = -1;
	

	public int hudGLRGBAUniformLocation;

	public FloatBuffer hudFB = BufferUtils.createFloatBuffer(16);
	
	public int hudProgramId;
	public int hudVertexShaderId;
	public int hudFragmentShaderId;	

	boolean isHUDIndexBound = false;
	
	

	public boolean isDrawHUD = true;

	FloatBuffer verticesHUD;


	public hud_testing() {
		
		this.glConstruct();
				
	}

	public float[] actionJoints() {

		float[] vertices = hudVerticesDraw;
	

		return vertices;
	}

	public void bindHUDVertexData() {
		// if (isDrawChange)
		{

			if (isDrawHUD) {
				if (verticesHUD == null) {
					verticesHUD = BufferUtils.createFloatBuffer(hudVerticesDraw.length);
				}
				verticesHUD.put(hudVerticesDraw).flip();

				if (hudVAOID == -1) {
					hudVAOID = glGenVertexArrays();
				}
				glBindVertexArray(hudVAOID);

				if (hudVBOID == -1) {
					hudVBOID = glGenBuffers();
				}
				glBindBuffer(GL_ARRAY_BUFFER, hudVBOID);
				glBufferData(GL_ARRAY_BUFFER, verticesHUD, GL_DYNAMIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);

				if (!isHUDIndexBound) {
					hudIndicesLinesCount = hudIndicesLines.length;
					ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(hudIndicesLinesCount);
					indicesBuffer.put(hudIndicesLines);
					indicesBuffer.flip();

					hudVBOIID = glGenBuffers();
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, hudVBOIID);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

					isHUDIndexBound = true;
				}
			}

			// isDrawChange = false;
		}
	}

	public void draw() {

		if (isDrawHUD) {
			drawHUD();
		}

	}

	public void drawHUD() {// System.out.println("human.drawJoints");
		bindHUDVertexData();
		glUseProgram(hudProgramId);

		//glDisable(GL_DEPTH_TEST);
		//glDisable(GL_CULL_FACE);

		glUniform4f(hudGLRGBAUniformLocation, hudRed, hudGreen, hudBlue, 1.0f);

		glBindVertexArray(hudVAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, hudVBOIID);
		//glDrawElements(GL_LINES, hudIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		//glDrawElements(GL_POINTS, hudIndicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glDrawElements(GL_TRIANGLES,hudIndicesLinesCount,GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		// System.out.println("human.drawJoints
		// jointsIndicesCount:"+jointsIndicesLinesCount);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);
		glUseProgram(0);
		
	}


	
	

	public void glCleanup() {
		glUseProgram(0);
		if (hudProgramId != 0) {
			if (hudVertexShaderId != 0) {
				glDetachShader(hudProgramId, hudVertexShaderId);
			}
			if (hudFragmentShaderId != 0) {
				glDetachShader(hudProgramId, hudFragmentShaderId);
			}
			glDeleteProgram(hudProgramId);
		}
	}

	public void glConstruct() {
		if (isDrawHUD) {
			glConstructHUD();
		}
	}

	public void glConstructHUD() {
		try {
			try {
				hudProgramId = glCreateProgram();

				glCreateVertexShaderHUD("#version 130   \n"
						//+ "uniform mat4 MVP; \n"
						+ "in vec3 position; \n"
						+ "void main() \n"
						+ "{ \n"
						+ "gl_Position = vec4(position, 1.0); \n"						
						+ "}");

				glCreateFragmentShaderHUD("#version 130 \n"
						+ "uniform vec4 RGBA; \n"
						+ "out vec4 fragColor; \n"
						+ " void main() \n"
						+ "{ \n"
						+ "fragColor = RGBA; \n"
						+ " }");

				glLink(hudProgramId);

				glUseProgram(hudProgramId);				
				hudGLRGBAUniformLocation = glGetUniformLocation(hudProgramId, "RGBA");
				glUseProgram(0);

				

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (hudProgramId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}

	public void glCreateFragmentShaderHUD(String shaderCode) throws Exception {
		hudFragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, hudProgramId);
	}



	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShaderHUD(String shaderCode) throws Exception {
		hudVertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, hudProgramId);
	}

	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}
	

}