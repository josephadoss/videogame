package local.source;
//import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.HashMap;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class globalStatic {

	public static void human3BodyPartFigureOutSkinMeshXYZ(String file, Vector3f[] model, int arrayDetail) {
		try {
			URL url = globalStatic.class.getResource(file.replace("resources",""));

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
			Document document = documentBuilder.parse(url.openStream());
			document.getDocumentElement().normalize();
			NodeList skinMeshList = document.getElementsByTagName("skinMesh");

			for (int i = 0; i < skinMeshList.getLength(); i++) {
				Node skinMesh = skinMeshList.item(i);
				if (skinMesh.getNodeType() == Node.ELEMENT_NODE) {

					Element eSkinMesh = (Element) skinMesh;

					NodeList meshList = eSkinMesh.getElementsByTagName("mesh");

					for (int l = 0; l < meshList.getLength(); l++) {
						Node mesh = meshList.item(l);
						if (mesh.getNodeType() == Node.ELEMENT_NODE) {
							Element eMesh = (Element) mesh;

							NodeList rowList = eMesh.getElementsByTagName("row");

							for (int j = 0; j < rowList.getLength(); j++) {
								try {
									Node row = rowList.item(j);

									if (row.getNodeType() == Node.ELEMENT_NODE) {
										Element eRow = (Element) row;
										int indexRow = Integer.parseInt(((Element) row).getAttribute("id"));
										
										float floatDefault = -1000000;
										float rowZ =floatDefault;
										float rowY = floatDefault;
										float rowX= floatDefault;
										String strRowZ, strRowY, strRowX;
										
										strRowZ = eRow.getAttribute("z");
										strRowY = eRow.getAttribute("y");
										strRowX = eRow.getAttribute("x");
										
										if(strRowZ!=null&& strRowZ.length()>0)
										{ rowZ = Float.parseFloat(strRowZ);}
										
										if(strRowY!=null&& strRowY.length()>0)
										{ rowY = Float.parseFloat(strRowY);}
										
										if(strRowX!=null&& strRowX.length()>0)
										{ rowX = Float.parseFloat(strRowX);}
										

										NodeList colList = eRow.getElementsByTagName("col");
										for (int k = 0; k < colList.getLength(); k++) {
											try {
												Node col = colList.item(k);
												if (col.getNodeType() == Node.ELEMENT_NODE) {
													Element eCol = (Element)col;
													
													int indexCol = Integer.parseInt(eCol.getAttribute("id"));
													
													float x = Float.parseFloat(eCol.getAttribute("x"));
													float y = Float.parseFloat(eCol.getAttribute("y"));
													float z = Float.parseFloat(eCol.getAttribute("z"));
												
													int index = (arrayDetail * indexRow) + indexCol;

													model[index].x = rowX==floatDefault ? x : rowX;
													model[index].y = rowY==floatDefault?y:rowY;
													model[index].z = rowZ==floatDefault?z:rowZ;												
												}
											} catch (Exception e) {
												System.out.println("exception caught in globalStatic.figureOutSkinMesh indexRow:" + indexRow + " e.Message : " +e.getMessage() );
											}
										}
									}
								} catch (Exception e) {
									System.out.println("exception caught in globalStatic.figureOutSkinMesh in j for loop where j:" + j);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("exception caught in globalStatic.figureOutSkinMesh() : " + e.getMessage() + " " + e.getStackTrace());
		}
	}

	public static void human3BodyPartFigureOutWeightedJoints(String file, HashMap<Integer,HashMap<String,HashMap<Integer,Float>>> sally, int arrayDetail) {
		try {
			URL url = globalStatic.class.getResource(file.replace("resources",""));

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
			Document document = documentBuilder.parse(url.openStream());
			document.getDocumentElement().normalize();
			NodeList skinMeshList = document.getElementsByTagName("skinMesh");

			for (int i = 0; i < skinMeshList.getLength(); i++) {
				Node skinMesh = skinMeshList.item(i);
				if (skinMesh.getNodeType() == Node.ELEMENT_NODE) {

					Element eSkinMesh = (Element) skinMesh;

					NodeList meshList = eSkinMesh.getElementsByTagName("mesh");

					for (int l = 0; l < meshList.getLength(); l++) {
						Node mesh = meshList.item(l);
						if (mesh.getNodeType() == Node.ELEMENT_NODE) {
							Element eMesh = (Element) mesh;

							NodeList rowList = eMesh.getElementsByTagName("row");

							for (int j = 0; j < rowList.getLength(); j++) {
								try {
									Node row = rowList.item(j);

									if (row.getNodeType() == Node.ELEMENT_NODE) {
										Element eRow = (Element) row;
										int indexRow = Integer.parseInt(((Element) row).getAttribute("id"));

										NodeList colList = eRow.getElementsByTagName("col");
										for (int k = 0; k < colList.getLength(); k++) {
											try {
												Node col = colList.item(k);
												if (col.getNodeType() == Node.ELEMENT_NODE) {
													Element eCol = (Element)col;
													
													int indexCol = Integer.parseInt(eCol.getAttribute("id"));
													
													String xyz = (eCol.getAttribute("xyz"));
													
													if(xyz=="")
													{continue;}
													
													int jointID = Integer.parseInt(eCol.getAttribute("joint"));
													float weight = Float.parseFloat(eCol.getAttribute("weight"));

													int index = (arrayDetail * indexRow) + indexCol;											
													
													if(!sally.containsKey(index))
													{ sally.put(index,new HashMap<String,HashMap<Integer,Float>>());}
													
													if(!sally.get(index).containsKey(xyz))
													{sally.get(index).put(xyz,new HashMap<Integer,Float>());}
													
													if(!sally.get(index).get(xyz).containsKey(jointID))
													{ sally.get(index).get(xyz).put(jointID,weight);}
													
												}
											} catch (Exception e) {
												System.out.println("exception caught in globalStatic.figureOutWeightedJoints indexRow:" + indexRow + " e.Message : " +e.getMessage() );
											}
										}
									}
								} catch (Exception e) {
									System.out.println("exception caught in globalStatic.figureOutWeightedJoints in j for loop where j:" + j);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("exception caught in globalStatic.figureOutWeightedJoints() : " + e.getMessage() + " " + e.getStackTrace());
		}
	}
	
	
	public static void human3BodyPartFigureOutWH(String file, Vector2f[] wh, int arrayDetail) {
		try {
			URL url = globalStatic.class.getResource(file.replace("resources",""));

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
			Document document = documentBuilder.parse(url.openStream());
			document.getDocumentElement().normalize();
			NodeList skinMeshList = document.getElementsByTagName("skinMesh");

			for (int i = 0; i < skinMeshList.getLength(); i++) {
				Node skinMesh = skinMeshList.item(i);
				if (skinMesh.getNodeType() == Node.ELEMENT_NODE) {

					Element eSkinMesh = (Element) skinMesh;

					NodeList meshList = eSkinMesh.getElementsByTagName("mesh");

					for (int l = 0; l < meshList.getLength(); l++) {
						Node mesh = meshList.item(l);
						if (mesh.getNodeType() == Node.ELEMENT_NODE) {
							Element eMesh = (Element) mesh;

							NodeList rowList = eMesh.getElementsByTagName("row");

							for (int j = 0; j < rowList.getLength(); j++) {
								try {
									Node row = rowList.item(j);

									if (row.getNodeType() == Node.ELEMENT_NODE) {
										Element eRow = (Element) row;
										int indexRow = Integer.parseInt(((Element) row).getAttribute("id"));

										NodeList colList = eRow.getElementsByTagName("col");
										for (int k = 0; k < colList.getLength(); k++) {
											try {
												Node col = colList.item(k);
												if (col.getNodeType() == Node.ELEMENT_NODE) {
													Element eCol = (Element)col;
													
													int indexCol = Integer.parseInt(eCol.getAttribute("id"));
													
													float w = Float.parseFloat(eCol.getAttribute("w"));
													float h = Float.parseFloat(eCol.getAttribute("h"));

													int index = (arrayDetail * indexRow) + indexCol;
												
													wh[index].x = w;
													wh[index].y = h;
												}
											} catch (Exception e) {
												System.out.println("exception caught in globalStatic.figureOutWH indexRow:" + indexRow + " e.Message : " +e.getMessage() );
											}
										}
									}
								} catch (Exception e) {
									System.out.println("exception caught in globalStatic.figureOutWH in j for loop where j:" + j);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("exception caught in globalStatic.figureOutWH() : " + e.getMessage() + " " + e.getStackTrace());
		}
	}
	
		
	public static ByteBuffer ioResourceToByteBuffer(String resource, int bufferSize) throws IOException {
		ByteBuffer buffer;
		File file = new File(resource);
		
		URL url = globalStatic.class.getResource(resource.replace("resources",""));

		if (url == null) {
			System.out.println("ioResourceToByteBuffer url is null");
		}


		if (file.isFile())
		{				
			
			FileInputStream fis = new FileInputStream(file);
			FileChannel fc = fis.getChannel();	
			buffer = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
			fc.close();
			fis.close();	
		} else 
		{	buffer = BufferUtils.createByteBuffer(bufferSize);
			InputStream source = url.openStream();
			if (source == null)
				throw new FileNotFoundException(resource);
			try {
				ReadableByteChannel rbc = Channels.newChannel(source);
				try {
					while (true) {
						int bytes = rbc.read(buffer);
						if (bytes == -1)
							break;
						if (buffer.remaining() == 0)
							buffer = resizeBuffer(buffer, buffer.capacity() * 2);
					}
					buffer.flip();
				} finally {
					rbc.close();
				}
			} finally {
				source.close();
			}
		}
		return buffer;
	}

	private static ByteBuffer resizeBuffer(ByteBuffer buffer, int newCapacity) {
		ByteBuffer newBuffer = BufferUtils.createByteBuffer(newCapacity);
		buffer.flip();
		newBuffer.put(buffer);
		return newBuffer;
	}
	
}
