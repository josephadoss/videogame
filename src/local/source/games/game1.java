package local.source.games;

import org.lwjgl.*;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.joml.*;

import local.source.actors.actor_ai_demo_positions;
import local.source.actors.manager_actors;
import local.source.actors.humans.human1.human1;
import local.source.actors.humans.human2.human2;
import local.source.actors.humans.human3.human3;
import local.source.actors.humans.human3.human3_outfit;
import local.source.actors.humans.human4.human4;
import local.source.attacks.manager_attackables;
import local.source.flat_plants.flower1;
import local.source.flat_plants.manager_flat_plants;
import local.source.hud.hud_attackable_collision_notice;
import local.source.hud.hud_engageable_collision_notice;
import local.source.hud.hud_health_bar;
import local.source.hud.hud_step_collision_notice;
import local.source.hud.hud_testing;
import local.source.objects_3D.manager_objects_3D;
import local.source.worlds.manager_land;
import local.source.worlds.world1;
import local.source.*;

import static org.lwjgl.opengl.GL11.*;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;

import static org.lwjgl.system.MemoryUtil.*;

import java.awt.image.BufferedImage;
import java.io.File;
import java.lang.Math;
import java.net.URL;
import java.nio.*;
import java.util.*;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class game1 {
	
	// all this needs to be modified to pull from an xml file
	// anything that can be pulled out, should be pulled out
	

	// 0 is testing material
	int gameID = 0;
	game_time gt = game_time.getInstance();
	actor_ai_demo_positions aiDemo=actor_ai_demo_positions.getInstance();

	private long window;

	enum drawModes { // at some point you'll need to make separate views
		playerWorld, playerDungeon, map
	};

	drawModes drawMode = drawModes.playerWorld;

	hud_testing hud;
	hud_health_bar hudHealthBar;
	hud_step_collision_notice hudStepCollisionNotice;
	boolean isDrawStepCollisionHUD=false;
	hud_engageable_collision_notice hudEngageableCollisionNotice;
	boolean isDrawEngageableCollisionHUD=false;
	hud_attackable_collision_notice hudAttackableCollisionNotice;
	boolean isDrawAttackableCollisionHUD=false;

	//private Matrix4f MVP = new Matrix4f();
	manager_mvp mvpManage = manager_mvp.getInstance();
	int WIDTH = 1200;
	int HEIGHT = 800;

	int mouseCenterX = WIDTH / 2;
	int mouseCenterY = HEIGHT / 2;

	world1 w = world1.getInstance();
	manager_actors aManage = manager_actors.getInstance();
	manager_land lManage;

	manager_flat_plants fpManage = manager_flat_plants.getInstance();
	manager_objects_3D o3DManage = manager_objects_3D.getInstance();
	
	String path =  "/games/game1/" ;
	
	URL gameURL ;
	String gameWindowTitle="Joseph's Amazing Video Game";
	
	
	public game1(String gameFile)
	{				
		gameURL = getClass().getResource(path + gameFile);
		run();
	}
	
	
	private void drawHUD(human3 p)
	{		
		if (p.getHealth().getShouldUpdateHealthBar()) 
		{ hudHealthBar.updateHealth(); }
		hudHealthBar.draw();

		if( isDrawStepCollisionHUD)
		{hudStepCollisionNotice.draw();}
		
		if( isDrawEngageableCollisionHUD)
		{hudEngageableCollisionNotice.draw();}
		
		if( isDrawAttackableCollisionHUD)
		{hudAttackableCollisionNotice.draw();}
				
	}

	private void init() {
		if (!glfwInit()) {
			throw new IllegalStateException("Unable to initialize GLFW");
		}
		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

		// Create the window
		window = glfwCreateWindow(WIDTH, HEIGHT, gameWindowTitle, NULL, NULL);
		if (window == NULL) {
			throw new RuntimeException("Error creating GLFW window");
		}

		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		glfwSetWindowPos(window, (vidmode.width() - WIDTH) / 2, (vidmode.height() - HEIGHT) / 2);
		glfwMakeContextCurrent(window);
		glfwSwapInterval(1); // set to one for 50 fps
		glfwShowWindow(window);
		GL.createCapabilities();

		// because this constructor calls the plots objects ( which call a bunch
		// of opengl stuff ) you have to wait until after the opengl context is
		// created to create this
		lManage = manager_land.getInstance();

		hud = new hud_testing();

		hudHealthBar = new hud_health_bar();
		
		hudStepCollisionNotice = new hud_step_collision_notice();
		hudEngageableCollisionNotice = new hud_engageable_collision_notice();
		hudAttackableCollisionNotice = new hud_attackable_collision_notice();
		
		figureOutGameFile();

		lManage.setWorld();
		lManage.setCenter(aManage.getPlayer().getXYZ().x,aManage.getPlayer().getXYZ().y);
		
		
		human4 h1 = new human4();
		h1.setHeight(6);
		h1.setStandingXYZ(20, 10, 0);
		h1.movementDisable();
		
		human4 h2 = new human4();
		h2.setHeight(7);
		h2.setStandingXYZ(20, 20, 0);
		h2.movementDisable();
		
		human4 h3 = new human4();
		h3.setHeight(8);
		h3.setStandingXYZ(20, 30, 0);
		h3.movementDisable();
		
		human4 h4 = new human4();
		h4.setHeight(9);
		h4.setStandingXYZ(20, 40, 0);
		h4.movementDisable();

		aiDemo.add(h1);
		aiDemo.add(h2);
		aiDemo.add(h3);
		aiDemo.add(h4);
	}

	private void loop() {
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		
		human4 p = (human4)aManage.getPlayer();

		int second = -1;
		int newSecond = -1;
		int frames = 0;

		int lastInputMillisecond = -1;
		int newInputMillisecond = -1;
		int grabInputEveryMilliseconds = 30;
		int nextGrab = -1;
		int updatesPerSecond = 0;
		long nextMapChangeAllow = -1;

		Date d = new Date();
		Calendar calendar = Calendar.getInstance();

		float facingRadians;
		float cameraDistanceBehind = -30.0f;
		float heightAbovePlayer = 8.0f;
		float eyeX;
		float eyeY;
		float eyeZ;
		Vector3f eye = new Vector3f(0f, 0f, 0f);
		Vector3f center = new Vector3f(0f, 0f, 0f);
		Vector3f up = new Vector3f(0f, 0f, 0f);

		DoubleBuffer mouseX = BufferUtils.createDoubleBuffer(1);
		DoubleBuffer mouseY = BufferUtils.createDoubleBuffer(1);
		int newMouseX = -1;
		int newMouseY = -1;

		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

		boolean stateChange = false;
		float x, y, z = 0f;
		boolean isNewSecond = false;

		boolean paused = false;
		int whenPaused =Integer.MIN_VALUE;

		int loopBegin = 0;
		int loopEnd = 0;
		while (!glfwWindowShouldClose(window)) {

			loopBegin = (int) System.currentTimeMillis();
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);

			glCullFace(GL_FRONT);
			d = (new Date());
			calendar.setTime(d);
			newSecond = calendar.get(Calendar.SECOND);
			newInputMillisecond = calendar.get(Calendar.MILLISECOND);
			if (second != newSecond) {
				System.out.println("second:" + second + "\t\tframes:" + frames + "\t\tupdatedInputPerSecond:" + updatesPerSecond);
				updatesPerSecond = 0;
				frames = 0;
				second = newSecond;
				isNewSecond = true;
			}

			// check for input
			if (nextGrab < newInputMillisecond || nextGrab > (newInputMillisecond + grabInputEveryMilliseconds)) {
				glfwPollEvents();

				stateChange = false;

				if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) { //System.out.println("P pressed"); System.out.println("whenPaused:"+whenPaused); System.out.println("loopBegin:"+loopBegin);
					if (loopBegin > (whenPaused + 1000)) {//System.out.println("one second passed since last paused");
						whenPaused = loopBegin;
						paused = !paused;
						System.out.println("paused pressed       paused:" + paused);
					}
				}

				if (!paused) {
					glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

					
					
					// YANK OUT ALL OF THIS INTO SOME KIND OF INPUT HANDLER
					// input handler needs to work differently for different screen modes
					// mode: game view
					// mode: menu system(s)
					// mode: map
					// mode: paused
					
					if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS) {
						//p.moveAttack(); //h.moveForward();
						//h.moveEngage();
						stateChange = true;		
					}

					if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
						p.move("walk-forward");
						stateChange = true;
					}
					// System.out.println("entered loop()3");
					if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
						//p.moveBackward();
						stateChange = true;
					}
					// System.out.println("entered loop()4");
					if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
						// h.moveLeft();
						p.faceLeft();
						stateChange = true;
					}

					// System.out.println("entered loop()5");
					if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
						// h.moveRight();
						p.faceRight();
						stateChange = true;
					}

					// System.out.println("entered loop()6");
					if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
						p.move("engage");
						stateChange = true;
					}
					// System.out.println("entered loop()7");
					//if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
						// h.changeWeapon();
					//	stateChange = true;
					//}
					// System.out.println("entered loop()8");
					if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) { //System.out.println("1 pressed");
						//p.moveSquat();
						stateChange = true;
					}
					// System.out.println("entered loop()9");
					if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
					{ // h.movePunch();
						//p.moveLieDown();
						stateChange = true;
					}
					// System.out.println("entered loop()10");
					if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS) {
						// h.moveKick();
						stateChange = true;
					}
					// System.out.println("entered loop()11");
					if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
						// h.moveKick();
						// stateChange = true;
						glfwSetWindowShouldClose(window, true);
					}
					// System.out.println("entered loop()12");
					if (glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS) {
						heightAbovePlayer = 20f;
					}
					// System.out.println("entered loop()13");
					if (glfwGetKey(window, GLFW_KEY_M) == GLFW_PRESS) {
						long now = System.currentTimeMillis();
						if (now > nextMapChangeAllow) {
							if (drawMode != drawModes.map) {
								drawMode = drawModes.map;
								lManage.setDrawModeMap();
							} else {
								drawMode = drawModes.playerWorld;
								lManage.setDrawModePlayerWorld();
							}

							nextMapChangeAllow = now + 100;
							System.out.println("nextMapChangeAllow : " + nextMapChangeAllow);
						}
					}
					// System.out.println("entered loop()14");
					if (glfwGetKey(window, GLFW_KEY_G) == GLFW_PRESS) {
						heightAbovePlayer += 1;
					}
					// System.out.println("entered loop()15");
					if (glfwGetKey(window, GLFW_KEY_H) == GLFW_PRESS) {
						heightAbovePlayer -= 1;
					}

					if (glfwGetKey(window, GLFW_KEY_KP_ADD) == GLFW_PRESS) {
						//p.getHealth().addHP(1);
					}

					if (glfwGetKey(window, GLFW_KEY_KP_SUBTRACT) == GLFW_PRESS) {
						//p.getHealth().subtractHP(1);
					}

					if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
						// h.moveLeft();
						// h.faceLeft();
						p.rotateXNeg();
						stateChange = true;
					}
					if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
						// h.moveLeft();
						// h.faceRight();
						p.rotateXPos();
						stateChange = true;
					}

					if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
						// h.moveLeft();
						// h.faceLeft();
						p.rotateYPos();
						stateChange = true;
					}
					if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
						// h.moveLeft();
						// h.faceRight();
						p.rotateYNeg();
						stateChange = true;
					}

					
					
					
					
					// test feature
					if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS ) {
						aManage.testResetVertices();
					}

			
					
					
					
					
					
					if (!stateChange) {	p.move("stand");}

					// System.out.println("entered loop()17");
					glfwGetCursorPos(window, mouseX, mouseY);
					mouseX.rewind();
					mouseY.rewind();

					newMouseX = (int) mouseX.get(0);
					newMouseY = (int) mouseY.get(0);

					if (newMouseX < mouseCenterX - 10) {
						p.faceLeft();
						// h.rotateXPos();
						newMouseX = mouseCenterX;
					}

					if (newMouseX > mouseCenterX + 10) {
						p.faceRight();
						// h.rotateXNeg();
						newMouseX = mouseCenterX;
					}

					if (newMouseY < mouseCenterY - 10) {
						// h.faceLeft();
						// h.rotateYPos();
						newMouseY = mouseCenterY;
					}

					if (newMouseY > mouseCenterY + 10) {
						// h.faceRight();
						// h.rotateYNeg();
						newMouseY = mouseCenterY;
					}

					// keep this around
					/*
					 * if (newMouseY > mouseCenterY + 10) { // if
					 * (heightAbovePlayer < z) { heightAbovePlayer += 1; }
					 * newMouseY = mouseCenterY; }
					 * 
					 * if (newMouseY < mouseCenterY - 10) { if
					 * (heightAbovePlayer > 0) { heightAbovePlayer -= 1; }
					 * newMouseY = mouseCenterY; }
					 */

					/*
					 * final int val = 0; GLFWScrollCallback scrollCallback;
					 * glfwSetScrollCallback(window, scrollCallback =
					 * GLFWScrollCallback.create((window, xoffset, yoffset) -> {
					 * System.out.println("yoffset:"+yoffset); if (yoffset < 0)
					 * { val =-1;//cameraDistanceBehind -= 1f; } else if
					 * (yoffset > 0) { val=1;// cameraDistanceBehind += 1f; }
					 * })); System.out.println("val:"+val);
					 */
					/*
					 * glfwGetScrollPos(window,); int dWheel =
					 * Mouse.getDWheel(); if(dWheel<0) { cameraDistanceBehind
					 * -=1;} else if(dWheel>0) { cameraDistanceBehind +=1;}
					 */
					// System.out.println("entered loop()18");
					updatesPerSecond++;
					lastInputMillisecond = newInputMillisecond;
					nextGrab = (lastInputMillisecond + grabInputEveryMilliseconds) % 1000;

					glfwSetCursorPos(window, mouseCenterX, mouseCenterY);
				} else {
					glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
				}
			}

			if (paused) 
			{ continue; }

			if (!paused && isNewSecond) 
			{ 	//p.getHealth().regen();
				}

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			switch (drawMode) {
			case playerWorld:
				facingRadians = (float) Math.toRadians(p.getFacingDegrees() - 180f);
				cameraDistanceBehind =10;// 20;
				
				
				// test code to get close up of character 
				//facingRadians = (float) Math.toRadians(90f);
				//cameraDistanceBehind=10; // test code
				//heightAbovePlayer= 2f;
				
				eyeX = (float) (p.getStandingX() + cameraDistanceBehind * Math.cos(facingRadians));
				eyeY = (float) (p.getStandingY() + cameraDistanceBehind * Math.sin(facingRadians));
				eyeZ = (float) (p.getStandingZ() + heightAbovePlayer);

				eye.x = eyeX;
				eye.y = eyeY;
				eye.z = eyeZ;

				center.x = p.getStandingX();
				center.y = p.getStandingY();
				center.z = p.getStandingZ() + p.getHeight();

				// test code
				// center.x = h.getX();
				// center.y = h.getY();
				// center.z = h.getZ()+h.getHeight()/2f;

				up.x = 0.0f;
				up.y = 0.0f;
				up.z = center.z + 10.0f;

				mvpManage.get().identity();
				mvpManage.get().perspective((float) Math.toRadians(45), 640.0f / 480.0f, 0.0001f, 100000.0f).lookAt(eye, center, up, mvpManage.get());
				
				aiDemo.update();
				
				//aManage.setMVP(MVP); 
				aManage.draw(); // add some logic to only draw if nearby player
				x = p.getStandingX();
				y = p.getStandingY();
				z = p.getStandingZ();
				aManage.setFacing(x,y,z);

					
				
				if (isNewSecond) {
					
					if(manager_step_collisions.getInstance().getCollision(p.getFacingXYZAsIfCenterXYZ(),p.getDiameterXYZ(),p)!=null)
					{ isDrawStepCollisionHUD=true; }
					else
					{ isDrawStepCollisionHUD=false;}
					
					if(manager_engageables.getInstance().getCollision(p).size()>0)
					{isDrawEngageableCollisionHUD=true;}
					else
					{isDrawEngageableCollisionHUD=false;}
					
					if(manager_attackables.getInstance().getCollision(p.getCenterXYZ(),p.getDiameterXYZ()).size()>0)
					{isDrawAttackableCollisionHUD=true;}
					else
					{isDrawAttackableCollisionHUD=false;}
				
					int secondModVal = 5;
					switch (second % secondModVal) { // every five seconds do a special action
					case 0:
						lManage.setCenter(x, y);
						break;
					case 1:
						System.gc();
						break;
					case 2:
						
						break;
					default:
						break;
					}
				}


		
				
				
				lManage.setDrawModePlayerWorld();
				lManage.recalculatePlotVertices();
				//lManage.setMVP(MVP);
				lManage.draw();
		
				fpManage.setCenter(x, y, z);
				fpManage.setFacing(eyeX, eyeY, p.getStandingZ());
				//fpManage.setMVP(MVP);
				fpManage.draw();
				
				
				
				o3DManage.setCenter(x, y, z);
				o3DManage.draw();

				// after drawing 3D scene, switch to orthagonal view and draw 2d
				// menu.
				// 0,0 is top left
				// may need to disable depth testing too

				/*
				 * You can render the normal game objects using perspective
				 * view, and then when the render func is done with that, switch
				 * to orthographic view and render your GUI or whatever it is,
				 * this way.
				 * 
				 * Using glOrtho you can define your screen going exactly from
				 * left 0 to right 1 and the same for bottom and up. Since there
				 * is no perspective distortion anymore, those coordinates
				 * directly map to screen coordinates then.
				 */

				//this.drawHUD(p);
				
				frames++;
				break;
			case map:
				break;
			}

			glfwSwapBuffers(window);

			isNewSecond = false;

			loopEnd = (int) System.currentTimeMillis();
			gt.updateTime(loopEnd - loopBegin);
		}

		lManage.stopPlotThreads();
	}

	
	public void run() {

		try {
			try {
				init();
			} catch (Exception ex) {
				System.out.println("error game1.run() init() message:" + ex.getMessage() + " stackTrace:" + ex.getStackTrace());
			}
			try {
				loop();
			} catch (Exception ex) {
				System.out.println("error game1.run() loop() message:" + ex.getMessage() + " stackTrace:" + ex.getStackTrace());
			}

			glfwFreeCallbacks(window);
			glfwDestroyWindow(window);
		} catch (Exception e) {
			System.out.println("exception caught in game1.run() message:" + e.getMessage() + " stackTrace:" + e.getStackTrace());
		} finally {
			glfwTerminate();
			glfwSetErrorCallback(null).free();
		}
	}
	
	
	public void figureOutGameFile() {		
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
			Document document = documentBuilder.parse(gameURL.openStream());//pathToFile);			
			document.getDocumentElement().normalize();
			NodeList nList = document.getElementsByTagName("game1");

			for (int i = 0; i < nList.getLength(); i++) {
				Node n = nList.item(i);				
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					
					Element game = (Element) n;	
					
					Element player = (Element) game.getElementsByTagName("player").item(0);	
					Element worlds = (Element) game.getElementsByTagName("worlds").item(0);	

					String currentWorldID = player.getAttribute("currentWorldID");
					String currentWorldType = player.getAttribute("currentWorldType");
					
					NodeList worldList = worlds.getElementsByTagName(currentWorldType);
					for(int j = 0 ; j<worldList.getLength(); j++)
					{
						Element world = (Element)  worldList.item(j);
						
						
						if(world.getAttribute("id").compareTo(currentWorldID)==0)
						{
							
							NodeList characterList = world.getElementsByTagName("character");
							for(int k=0;k<characterList.getLength();k++)
							{
								Element character = (Element) characterList.item(k);
								
								human3 h = getHuman3FromCharacter(character);

							}
							
							break;
						}						
					}
					
					human4 p = getHuman4FromCharacter(player);
					aManage.setPlayer(p);
					hudHealthBar.setPlayer(p);
					//p.setXYZ(Float.parseFloat(player.getAttribute("x")),Float.parseFloat(player.getAttribute("y")),Float.parseFloat(player.getAttribute("z")));
					//p.setHeight(Float.parseFloat(player.getAttribute("height")));
					//p.setHP(Integer.parseInt(player.getAttribute("hp")));
				}
			}			
		} catch (Exception e) {
			System.out.println(
					"exception caught in world1.figureOutHeightMap() : " + e.getMessage() + " " + e.getStackTrace());
		}
	}
	
	
	
	private human3 getHuman3FromCharacter(Element c)
	{
		human3 h = new human3();
									
		h.setXYZ(Float.parseFloat(c.getAttribute("x")),Float.parseFloat(c.getAttribute("y")),Float.parseFloat(c.getAttribute("z")));
		h.setHeight(Float.parseFloat(c.getAttribute("height")));
		h.setHP(Integer.parseInt(c.getAttribute("hp")));
	
		NodeList human3List = c.getElementsByTagName("human3");
		Element skinColor = (Element)((Element)human3List.item(0)).getElementsByTagName("skinColor").item(0);
		Element hairColor = (Element)((Element)human3List.item(0)).getElementsByTagName("hairColor").item(0);
		Element garment = (Element)((Element)human3List.item(0)).getElementsByTagName("garment").item(0);
	
		float r, g, b;
		r=Float.parseFloat(skinColor.getAttribute("red"));
		g=Float.parseFloat(skinColor.getAttribute("green"));
		b=Float.parseFloat(skinColor.getAttribute("blue"));
		h.setSkinRGB(r, g, b);
		
		r=Float.parseFloat(hairColor.getAttribute("red"));
		g=Float.parseFloat(hairColor.getAttribute("green"));
		b=Float.parseFloat(hairColor.getAttribute("blue"));
		h.setHairRGB(r, g, b);
		
		human3_outfit outfit = new human3_outfit();
		outfit.setGloves(garment.getAttribute("gloves"));
		outfit.setPants(garment.getAttribute("pants"));
		outfit.setShirt(garment.getAttribute("shirt"));
		outfit.setShoes(garment.getAttribute("shoes"));
		outfit.setHead(garment.getAttribute("head"));
		outfit.setHat(garment.getAttribute("hat"));
		h.setOutfit(outfit);
		
		return h;
		
	}
	

	private human4 getHuman4FromCharacter(Element c)
	{
		human4 h = new human4();
									
		h.setStandingXYZ(Float.parseFloat(c.getAttribute("x")),Float.parseFloat(c.getAttribute("y")),Float.parseFloat(c.getAttribute("z")));
		h.setHeight(Float.parseFloat(c.getAttribute("height")));
		h.setHP(Integer.parseInt(c.getAttribute("hp")));
	
		//NodeList human4List = c.getElementsByTagName("human4");
		//Element skinColor = (Element)((Element)human4List.item(0)).getElementsByTagName("skinColor").item(0);
		//Element hairColor = (Element)((Element)human4List.item(0)).getElementsByTagName("hairColor").item(0);
		//Element garment = (Element)((Element)human4List.item(0)).getElementsByTagName("garment").item(0);
	
		//float r, g, b;
		//r=Float.parseFloat(skinColor.getAttribute("red"));
		//g=Float.parseFloat(skinColor.getAttribute("green"));
		//b=Float.parseFloat(skinColor.getAttribute("blue"));
		//h.setSkinRGB(r, g, b);
		
		//r=Float.parseFloat(hairColor.getAttribute("red"));
		//g=Float.parseFloat(hairColor.getAttribute("green"));
		//b=Float.parseFloat(hairColor.getAttribute("blue"));
		//h.setHairRGB(r, g, b);
		
		//human3_outfit outfit = new human3_outfit();
		//outfit.setGloves(garment.getAttribute("gloves"));
		//outfit.setPants(garment.getAttribute("pants"));
		//outfit.setShirt(garment.getAttribute("shirt"));
		//outfit.setShoes(garment.getAttribute("shoes"));
		//outfit.setHead(garment.getAttribute("head"));
		//outfit.setHat(garment.getAttribute("hat"));
		//h.setOutfit(outfit);
		
		return h;
		
	}
	
}
