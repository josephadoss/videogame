package local.source.worlds;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
//import static org.lwjgl.opengl.GL32.*;

import java.io.File;
import javax.xml.parsers.*;
//import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.*;

import local.source.game_time;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

import java.nio.*;

import java.util.*;

import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;
import java.net.URL;
import local.source.flat_plants.*;
import local.source.objects_3D.interface_objects_3D;
import local.source.objects_3D.rock1;

public class world1 {

	private static world1 instance = new world1();
	game_time gt = game_time.getInstance();
	int worldID = -1;
	boolean showConsoleOutput = false;

	int worldDistanceAcross = 0;
	int worldUnitOfMeasurement = -1;
	float maxHeight = 3000000f;

	manager_flat_plants fpManager = manager_flat_plants.getInstance();
	// File pathToWorldFile;

	// File worldFile;
	URL worldURL;
	BufferedImage[][] imgMapHeightArray;
	BufferedImage[][] imgMapColorArray;

	String path = "/worlds/world1/";

	private world1() {
		// gameID = gID;
		// worldID = wID;
		// figureOutWorldFile();
	}

	public int getDistanceAcross() {
		return worldDistanceAcross;
	}

	public static world1 getInstance() {
		if (instance == null) {
			instance = new world1();
		}

		return instance;
	}

	BufferedImage imgMapColor;

	public int getRGB(int x, int y) {
		int max = worldDistanceAcross;
		if (x >= max || y >= max || x < 1 || y < 1) {
			return 0;
		}

		int arrayX = (int) x / 1000;
		int arrayY = (int) y / 1000;

		int indexX = (int) x % 1000;
		int indexY = (int) y % 1000;
		int newY = 999 - indexY;

		imgMapColor = imgMapColorArray[arrayX][arrayY];

		int rgb = imgMapColor.getRGB(indexX, newY);
		return rgb;
	}

	BufferedImage imgMapHeight;

	public float getZ(float x, float y) {

		int max = worldDistanceAcross;

		if (x >= max || y >= max || x < 1 || y < 1) {
			return 0f;
		}

		int arrayX = (int) x / 1000;
		int arrayY = (int) y / 1000;

		int indexX = (int) x % 1000;
		int indexY = (int) y % 1000;

		imgMapHeight = imgMapHeightArray[arrayX][arrayY];

		int newY = 999 - indexY;

		int p = imgMapHeight.getRGB(indexX, newY);

		int r = (p >> 16) & 0xff;
		int g = (p >> 8) & 0xff;
		int b = p & 0xff;

		float newB = (float) b;
		float newG = (float) g / 256f * 65280f;
		float newR = (float) r / 256f * 16711680f;
		float newP = newB + newG + newR;

		float height = (float) (newP / 16777216f * maxHeight);

		return height;
	}

	public void figureOutHeightMap() {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
			Document document = documentBuilder.parse(worldURL.openStream());
			document.getDocumentElement().normalize();
			NodeList nList = document.getElementsByTagName("world");

			for (int i = 0; i < nList.getLength(); i++) {
				Node n = nList.item(i);
				if (n.getNodeType() == Node.ELEMENT_NODE) {

					Element e = (Element) n;

					String wID = Integer.toString(worldID);

					String wVal = e.getAttribute("id");

					if (wVal.equals(wID)) {

						String distVal = e.getAttribute("distanceAcross");
						String uomVal = e.getAttribute("unitOfMeasurement");
						String maxHeightVal = e.getAttribute("maxHeight");

						worldDistanceAcross = Integer.parseInt(distVal);

						int arraySize = worldDistanceAcross / 1000;
						imgMapHeightArray = new BufferedImage[arraySize][arraySize];
						imgMapColorArray = new BufferedImage[arraySize][arraySize];

						if (maxHeightVal.length() > 0) {
							maxHeight = Integer.parseInt(maxHeightVal);
						}
						for (int j = 0; j < e.getElementsByTagName("map").getLength(); j++) {
							Element ee = (Element) e.getElementsByTagName("map").item(j);
							if (ee.getAttribute("type").equals("height")) {
								String file = ee.getAttribute("file");
								String arrayX = ee.getAttribute("arrayX");
								String arrayY = ee.getAttribute("arrayY");

								int x = Integer.parseInt(arrayX);
								int y = Integer.parseInt(arrayY);

								String filePath = path + worldID + "/" + file;
								URL url = getClass().getResource(filePath);
								BufferedImage b = ImageIO.read(url.openStream());
								imgMapHeightArray[x][y] = b;
							}

							if (ee.getAttribute("type").equals("color")) {

								String file = ee.getAttribute("file");
								String arrayX = ee.getAttribute("arrayX");
								String arrayY = ee.getAttribute("arrayY");

								int x = Integer.parseInt(arrayX);
								int y = Integer.parseInt(arrayY);

								String filePath = path + worldID + "/" + file;
								URL url = getClass().getResource(filePath);
								BufferedImage b = ImageIO.read(url.openStream());
								imgMapColorArray[x][y] = b;
							}
							
							if (ee.getAttribute("type").equals("flatPlant")) {

								String file = ee.getAttribute("file");
								String arrayX = ee.getAttribute("arrayX");
								String arrayY = ee.getAttribute("arrayY");
								
								String valHeight = ee.getAttribute("height");
								String valClass = ee.getAttribute("class");
								String valImage = ee.getAttribute("image");

								int x = Integer.parseInt(arrayX) *1000;
								int y = Integer.parseInt(arrayY)*1000;
								float height = Float.parseFloat(valHeight);

								String filePath = path + worldID + "/" + file;
								URL url = getClass().getResource(filePath);
								BufferedImage b = ImageIO.read(url.openStream());
							
								for(int xx = 0; xx<1000;xx++)
								{
									for(int yy=0;yy<1000;yy++)
									{			
										int nx = x+xx;
										int ny = y+yy;
										int p = b.getRGB(nx,999-ny);
										int red = (p >> 16) & 0xff; 
										int green = (p >> 8) & 0xff; ;
										int blue = p & 0xff; 
										if(red>100&&blue>100&&green>100)
										{   
											interface_flat_plant plant;
											
											if (valClass.equals("flower1") ) {
												plant = new flower1();
												plant.setXYZ(nx,ny, getZ(nx,ny));
												plant.setHeight(height);
												plant.setImage(valImage);
											}
										}
									}
								}	
							}									
						}
						for (int j = 0; j < e.getElementsByTagName("flatPlant").getLength(); j++) {
							Element ee = (Element) e.getElementsByTagName("flatPlant").item(j);

							String valX = ee.getAttribute("x");
							String valY = ee.getAttribute("y");
							String valZ = ee.getAttribute("z");
							String valHeight = ee.getAttribute("height");
							String valClass = ee.getAttribute("class");
							String valImage = ee.getAttribute("image");

							float x = Float.parseFloat(valX);
							float y = Float.parseFloat(valY);
							float z = Float.parseFloat(valZ);
							float height = Float.parseFloat(valHeight);

							interface_flat_plant plant;

							if (valClass.equals("flower1") ) {
								plant = new flower1();
								plant.setXYZ(x, y, getZ(x,y));
								plant.setHeight(height);
								plant.setImage(valImage);
							}
						}
						
						
						for (int j = 0; j < e.getElementsByTagName("object_3D").getLength(); j++) {
							Element ee = (Element) e.getElementsByTagName("object_3D").item(j);

							String valX = ee.getAttribute("x");
							String valY = ee.getAttribute("y");
							String valZ = ee.getAttribute("z");
							String valHeight = ee.getAttribute("height");
							String valClass = ee.getAttribute("class");
							String valImage = ee.getAttribute("image");

							float x = Float.parseFloat(valX);
							float y = Float.parseFloat(valY);
							float z = Float.parseFloat(valZ);
							float height = Float.parseFloat(valHeight);

							interface_objects_3D obj;

							if (valClass.equals("rock1") ) {
								obj = new rock1();
								obj.setXYZ(x, y, getZ(x,y));
								obj.setHeight(height);
								obj.setImage(valImage);
							}
						}
						break;
					}
				}
			}
		} catch (Exception e) {
			System.out.println(
					"exception caught in world1.figureOutHeightMap() : " + e.getMessage() + " " + e.getStackTrace());
		}
	}

	public void figureOutWorldFile() {
		String newPath = path + worldID + "/worlds.xml";
		worldURL = getClass().getResource(newPath);

		figureOutHeightMap();
	}

	private void println(String s) {
		if (this.showConsoleOutput) {
			System.out.println(s);
		}
	}



	public void setWorldID(int newWorldID) {
		worldID = newWorldID;
		figureOutWorldFile();
	}

}
