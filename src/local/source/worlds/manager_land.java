package local.source.worlds;
import java.util.ArrayList;
import java.util.List;

import org.joml.Matrix4f;

import local.source.game_time;
import local.source.actors.manager_actors;

public class manager_land {
	world1 w = world1.getInstance();
	public static manager_land instance= null;
	game_time gt=game_time.getInstance();
	manager_actors aManage = manager_actors.getInstance();
	
	enum drawModes {
		playerWorld, playerDungeon, map
	};

	drawModes drawMode = drawModes.playerWorld;

	land_plot plotLNW = new land_plot(100);
	land_plot plotMNW = new land_plot(100);
	//plot plotHNW = new plot(100);

	land_plot plotLSW = new land_plot(100);
	land_plot plotMSW = new land_plot(100);
	//plot plotHSW = new plot(100);

	land_plot plotLNE = new land_plot(100);
	land_plot plotMNE = new land_plot(100);
	//plot plotHNE = new plot(100);

	land_plot plotLSE = new land_plot(100);
	land_plot plotMSE = new land_plot(100);
//	plot plotHSE = new plot(100);

	land_plot plotLN = new land_plot(100);
	land_plot plotMN = new land_plot(100);
//	plot plotHN = new plot(100);

	land_plot plotLS = new land_plot(100);
	land_plot plotMS = new land_plot(100);
//	plot plotHS = new plot(100);

	land_plot plotLW = new land_plot(100);
	land_plot plotMW = new land_plot(100);
	//plot plotHW = new plot(100);

	land_plot plotLE = new land_plot(100);
	land_plot plotME = new land_plot(100);
//	plot plotHE = new plot(100);

	land_plot plotHC = new land_plot(300);

	int dacH = plotHC.getDistanceAcrossHigh();
	int dacM = plotHC.getDistanceAcrossMedium();
	int dacL = plotHC.getDistanceAcrossLow();
	List<land_plot> plots = new ArrayList<land_plot>();

	
	
	
	private manager_land() {
		
		w.setWorldID(0);
		w.figureOutWorldFile();

		plots.add(plotLNW);
		plots.add(plotMNW);
		//plots.add(plotHNW);

		plots.add(plotLSW);
		plots.add(plotMSW);
		//plots.add(plotHSW);

		plots.add(plotLNE);
		plots.add(plotMNE);
		//plots.add(plotHNE);

		plots.add(plotLSE);
		plots.add(plotMSE);
		//plots.add(plotHSE);

		plots.add(plotLN);
		plots.add(plotMN);
		//plots.add(plotHN);

		plots.add(plotLS);
		plots.add(plotMS);
		//plots.add(plotHS);

		plots.add(plotLW);
		plots.add(plotMW);
		//plots.add(plotHW);

		plots.add(plotLE);
		plots.add(plotME);
		//plots.add(plotHE);

		plots.add(plotHC);

		plotLNW.setLevelOfDetailLow();
		plotMNW.setLevelOfDetailMedium();
	//	plotHNW.setLevelOfDetailHigh();

		plotLSW.setLevelOfDetailLow();
		plotMSW.setLevelOfDetailMedium();
		//plotHSW.setLevelOfDetailHigh();

		plotLNE.setLevelOfDetailLow();
		plotMNE.setLevelOfDetailMedium();
	//	plotHNE.setLevelOfDetailHigh();

		plotLSE.setLevelOfDetailLow();
		plotMSE.setLevelOfDetailMedium();
	//	plotHSE.setLevelOfDetailHigh();

		plotLN.setLevelOfDetailLow();
		plotMN.setLevelOfDetailMedium();
	//	plotHN.setLevelOfDetailHigh();

		plotLS.setLevelOfDetailLow();
		plotMS.setLevelOfDetailMedium();
	//	plotHS.setLevelOfDetailHigh();

		plotLW.setLevelOfDetailLow();
		plotMW.setLevelOfDetailMedium();
	//	plotHW.setLevelOfDetailHigh();

		plotLE.setLevelOfDetailLow();
		plotME.setLevelOfDetailMedium();
	//	plotHE.setLevelOfDetailHigh();

		plotHC.setLevelOfDetailHigh();

		plotHC.setAsMainPlot();

		// setWorld();
	}

	public void draw() {
		switch (drawMode) {
		case playerWorld:
			for (land_plot p : plots) {
				p.draw();
			}		
			
			break;
		case map:
	
			break;
		}
	}

	
	public static manager_land getInstance()
	{
		if(instance==null)
		{instance = new manager_land();}
		return instance;
	}

	public int getMapDistanceAcrossInFeet() {
		return w.getDistanceAcross();
	}

	public float getZ(float x, float y) {
		return w.getZ(x, y);
	}

	public void recalculatePlotVertices() {
		for (land_plot p : plots) {
			p.recalculateVertices();
		}		
	}


	float dist = 99f;// w.getDistanceAcross();

	float whx;// = x - dist;
	float wmx;// = whx - dist * 2f;
	float wlx;// = wmx - dist*6f;

	float ehx;// = x + dist;
	float emx;// = ehx + dist* 2f;
	float elx;// = emx + dist*6f;

	float nhy;// = y + dist;
	float nmy;// = nhy + dist* 2f;
	float nly;// = nmy + dist*6f;

	float shy;// = y - dist;
	float smy;// = shy - dist* 2f;
	float sly;// = smy - dist*6f;

	public void setCenter(float x, float y) {
	
		// keep this turned off until you can figure out how to work level of
		// detail into the plots. eg. make the low level one's cover a huge area
		// dn the high level ones cover a small area
		// float dist = 97f;// w.getDistanceAcross();

		x=(int)x;
		y=(int)y;
		
		whx = x - dist;
		wmx = whx - dist * 2f;
		wlx = wmx - dist * 6f;

		ehx = x + dist;
		emx = ehx + dist * 2f;
		elx = emx + dist * 6f;

		nhy = y + dist;
		nmy = nhy + dist * 2f;
		nly = nmy + dist * 6f;

		shy = y - dist;
		smy = shy - dist * 2f;
		sly = smy - dist * 6f;
		
		
		float mod = 0f;
		int modAmount = 9;
		mod = whx%modAmount;
		whx += mod ;
		mod = wmx%modAmount;
		wmx += mod;
		mod = wlx%modAmount;
		wlx += mod;

		mod = ehx%modAmount;
		ehx += mod;
		mod = emx%modAmount;
		emx += mod;
		mod = elx%modAmount;
		elx += mod;

		mod = nhy%modAmount;
		nhy += mod;;
		mod = nmy%modAmount;
		nmy +=mod;
		mod = nly%modAmount;
		nly += mod;

		mod = shy%modAmount;
		shy += mod;
		mod = smy%modAmount;
		smy += mod;
		mod = sly%modAmount;
		sly += mod;
		
		mod = x%modAmount;
		x += mod;
		
		mod = y%modAmount;
		y+= mod;

		plotHC.setCenter(x, y);

		plotLNW.setCenter(wlx, nly);
		plotMNW.setCenter(wmx, nmy);
	//	plotHNW.setCenter(whx, nhy);

		plotLSW.setCenter(wlx, sly);
		plotMSW.setCenter(wmx, smy);
	//	plotHSW.setCenter(whx, shy);

		plotLNE.setCenter(elx, nly);
		plotMNE.setCenter(emx, nmy);
	//	plotHNE.setCenter(ehx, nhy);

		plotLSE.setCenter(elx, sly);
		plotMSE.setCenter(emx, smy);
	//	plotHSE.setCenter(ehx, shy);

		plotLN.setCenter(x, nly);
		plotMN.setCenter(x, nmy);
	//	plotHN.setCenter(x, nhy);

		plotLS.setCenter(x, sly);
		plotMS.setCenter(x, smy);
	//	plotHS.setCenter(x, shy);

		plotLW.setCenter(wlx, y);
		plotMW.setCenter(wmx, y);
	//	plotHW.setCenter(whx, y);

		plotLE.setCenter(elx, y);
		plotME.setCenter(emx, y);
	//	plotHE.setCenter(ehx, y);

		
		
	}

	public void setDrawModeMap() {
		drawMode = drawModes.map;
	}

	public void setDrawModePlayerDungeon() {
		drawMode = drawModes.playerDungeon;
	}

	public void setDrawModePlayerWorld() {
		drawMode = drawModes.playerWorld;
	}

	public void setWorld() {		
		int dist = w.getDistanceAcross();
		aManage.setMaxStepDistance(dist);
	}

	public void stopPlotThreads() {
		for (land_plot p : plots) {
			p.stopThread();
		}}
}
