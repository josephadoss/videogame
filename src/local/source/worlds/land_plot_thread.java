package local.source.worlds;

import local.source.game_time;

public class land_plot_thread implements Runnable {
	game_time gt=game_time.getInstance();
	private Thread t;
	private world1 w= world1.getInstance();
	private int sleepTime;
	float[] verticesDraw;
	float[] verticesColor;
	float centerX, centerY;
	int detail;

	public int arraySize = 0;
	public int distanceAcross;

	boolean continueUpdating = true;
	boolean isMainThread = false;

	public land_plot p;

	land_plot_thread(land_plot newP, float[] newDraw, float[] newColor, int newSize, int newDetail, int newDistance,float newX, float newY) {
		p = newP;
		verticesDraw = newDraw;
		verticesColor = newColor;
		arraySize=newSize;
		detail=newDetail;
		distanceAcross=newDistance;
		centerX=newX;
		centerY=newY;
	}

	public void run() {
		float cX, cY;
		int index = 0;
		int colorIndex = 0;

		float aS = (float) arraySize;
		float increment = 1f;
		int sleep = 1;
		float xx, yy, x, y;
		int rgb, a, r, g, b;
		float rr, gg, bb;
		// int counter=0;

		while (continueUpdating) 
		{
			cX = centerX;
			cY = centerY;
			if (w != null) {
				index = 0;
				colorIndex = 0;

				aS = (float) arraySize;
				increment = 1f;
				sleep = 200;
				if (detail > 2) {
					increment = 3f;
					sleep = 1000;
				}
				if (detail > 20) {
					increment = 9f;
					sleep = 5000;
				}

				for (x = 0; x < aS; x++) {
					xx = cX - aS * increment / 2f + x * increment;
					for (y = 0; y < aS; y++) {
						yy = cY - aS * increment / 2f + y * increment;

						verticesDraw[index++] = (int) xx;
						verticesDraw[index++] = (int) yy;
						verticesDraw[index++] = w.getZ(xx, yy);

						rgb = w.getRGB((int) xx, (int) yy);
						r = (rgb >> 16) & 0xff;
						g = (rgb >> 8) & 0xff;
						b = rgb & 0xff;

						rr = (float) (r % 255) / 255f;
						gg = (float) (g % 255) / 255f;
						bb = (float) (b % 255) / 255f;

						verticesColor[colorIndex++] = rr;
						verticesColor[colorIndex++] = gg;
						verticesColor[colorIndex++] = bb;
						verticesColor[colorIndex++] = 0f;
					}
				}

				try {
					p.setVertexBoundToFalse();
				 if(!isMainThread)
					{
						Thread.sleep(sleep);
					}
				} catch (Exception e) {
					System.out.println("exception caught in updatePlot.run " + e.getMessage());
				}
			} else {
				try {
					Thread.sleep(1000);
				} catch (Exception e) {
				}
			}
		}
	}

	public void setAsMainThread() {
		isMainThread = true;
	}

	public void setCenter(float x, float y) {
		centerX = x;
		centerY = y;
	}

	public void start() {
		if (t == null) {
			t = new Thread(this, "");
			t.start();
		}
	}

	public void stopUpdating() {
		continueUpdating = false;
	}
}
