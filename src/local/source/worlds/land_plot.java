package local.source.worlds;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL32.*;
import static org.lwjgl.opengl.GL40.*;

import java.nio.*;

import java.util.Random;

import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;

import local.source.game_time;
import local.source.manager_mvp;

public class land_plot {

	game_time gt = game_time.getInstance();
	
	enum levelOfDetails {
		high, medium, low
	};

	levelOfDetails levelOfDetail = levelOfDetails.low;
	float centerX, centerY;
	int measureOfDetailHigh = 1;
	int measureOfDetailMedium = 10;
	int measureOfDetailLow = 100;
	int measureOfDetail = measureOfDetailLow;
	int distanceAcrossHigh = 1;
	int distanceAcrossMedium = 10;
	int distanceAcrossLow = 100;
	int distanceAcross = distanceAcrossLow;

	world1 w = world1.getInstance();

	int seed = 0;
	Random rand = new Random();
	int arraySize = 100;

	public int programId;
	public int vertexShaderId;
	public int fragmentShaderId;
	public int GLMVPUniformLocation;
	public int GLRGBAUniformLocation;

	boolean showConsoleOutput = false;

	public int VBOID = -1;
	public int VAOID = -1;
	public int VBOIID = -1;
	public int VBColors = -1;

	float[] verticesDraw;
	float[] verticesColor;
	int[][] verticesDrawIndices;
	int[] indices;
	int indicesCount;
	boolean isVertexBound = false;
	boolean isIndexBound = false;
	public FloatBuffer FB = BufferUtils.createFloatBuffer(16);

	boolean verticesCalculated = false;

	land_plot_thread t;// = new threadPlot(); // don't initialize here. instead
					// process the array the one time, then pass to the thread
					// in the constructor, then start. In the setCenter method,
					// check if thread is null to get around that issue

	public land_plot(int sizeOfArray) {
		arraySize = sizeOfArray;

		verticesDraw = new float[arraySize * arraySize * 3];
		verticesColor = new float[arraySize * arraySize * 4];
		verticesDrawIndices = new int[arraySize][arraySize];
		indices = new int[arraySize * arraySize * 6];

		centerX = 0.0f;
		centerY = 0.0f;
//this.setWorld(w);
		this.glConstruct();
		
		
	}

	FloatBuffer verticesBuffer;
	FloatBuffer colorsBuffer;
	IntBuffer indicesBuffer;

	public void bindVertexData() {
		if (!isVertexBound) {
			if (verticesBuffer == null) {
				verticesBuffer = BufferUtils.createFloatBuffer(verticesDraw.length);
			}
			verticesBuffer.put(verticesDraw).flip();

			if (colorsBuffer == null) {
				colorsBuffer = BufferUtils.createFloatBuffer(verticesColor.length);
			}
			colorsBuffer.put(verticesColor).flip();

			if (VAOID == -1) {
				VAOID = glGenVertexArrays();
			}
			glBindVertexArray(VAOID);

			if (VBOID == -1) {
				VBOID = glGenBuffers();
			}
			glBindBuffer(GL_ARRAY_BUFFER, VBOID);
			glBufferData(GL_ARRAY_BUFFER, verticesBuffer, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

			if (VBColors == -1) {
				VBColors = glGenBuffers();
			}
			glBindBuffer(GL_ARRAY_BUFFER, VBColors);
			glBufferData(GL_ARRAY_BUFFER, colorsBuffer, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(1, 4, GL_FLOAT, false, 0, 0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);

			isVertexBound = true;

			if (!isIndexBound) {
				indicesCount = indices.length;
				if (indicesBuffer == null) {
					indicesBuffer = BufferUtils.createIntBuffer(indicesCount);
				}
				indicesBuffer.put(indices);
				indicesBuffer.flip();

				VBOIID = glGenBuffers();
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIID);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

				isIndexBound = true;
			}
		}
	}

	public void calculateVertices() {
		this.println("plot.calculateVertices()");
		if (!verticesCalculated) {
			int index = 0;
			int colorIndex = 0;
			int vertexIndex = 0;
			for (int x = 0; x < arraySize; x++) {
				for (int y = 0; y < arraySize; y++) {
					verticesDraw[index] = 0f;// xx;
					verticesColor[colorIndex] = 0f;// (float) (index % 255) /
													// 255f;
					index++;
					colorIndex++;
					verticesDraw[index] = 0f;// yy;
					verticesColor[colorIndex] = 0f;
					index++;
					colorIndex++;
					verticesDraw[index] = 0f;// w.getZ(xx, yy);
					verticesColor[colorIndex] = 0f;// 1f;
					index++;
					colorIndex++;
					verticesColor[colorIndex] = 0f;
					colorIndex++;
					verticesDrawIndices[x][y] = vertexIndex++;
				}
			}

			index = 0;
			for (int x = 1; x < arraySize; x++) {
				for (int y = 1; y < arraySize; y++) {
					int thisIndices = verticesDrawIndices[x][y];
					int leftIndices = verticesDrawIndices[x - 1][y];
					int upIndices = verticesDrawIndices[x][y - 1];
					int upleftIndices = verticesDrawIndices[x - 1][y - 1];

					this.indices[index++] = (thisIndices);
					this.indices[index++] = (upIndices);
					this.indices[index++] = (upleftIndices);

					this.indices[index++] = (thisIndices);
					this.indices[index++] = (upleftIndices);
					this.indices[index++] = (leftIndices);
				}
			}

			verticesCalculated = true;
		}

	}

	public void draw() {
		calculateVertices();

		bindVertexData();
		glUseProgram(programId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		glUniformMatrix4fv(GLMVPUniformLocation, false, manager_mvp.getInstance().get().get(FB));
		glBindVertexArray(VAOID);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIID);
		glDrawElements(GL_TRIANGLES, indicesCount, GL_UNSIGNED_INT, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glBindVertexArray(0);

		glUseProgram(0);

	}

	public int getDistanceAcross() {
		return distanceAcross;
	}

	public int getDistanceAcrossHigh() {
		return distanceAcrossHigh;
	}

	public int getDistanceAcrossLow() {
		return distanceAcrossLow;
	}

	public int getDistanceAcrossMedium() {
		return distanceAcrossMedium;
	}

	public int getMeasureOfDetailHigh() {
		return measureOfDetailHigh;
	}

	public int getMeasureOfDetailLow() {
		return measureOfDetailLow;
	}

	public int getMeasureOfDetailMedium() {
		return measureOfDetailMedium;
	}

	public float getZ(float x, float y) {
		// you need to redo this to take into account Z values at some float
		// point
		// get the four int xy points around this float xy, then figure out how
		// the decimal places change things
		// return arrayHeight[(int) x][(int) y];
		return w.getZ(x, y);
	}

	public void glConstruct() {
		try {
			try {
				programId = glCreateProgram();

				glCreateVertexShader("#version 130\nuniform mat4 MVP; in vec3 position; in vec4 in_color; out vec4 pass_color; void main() { gl_Position =MVP* vec4(position, 1.0); pass_color=in_color; }");

				glCreateFragmentShader(	"#version 130\n in vec4 pass_color; out vec4 out_color;   void main() { out_color=pass_color; }");		

				glLink(programId);

				glUseProgram(programId);
				GLMVPUniformLocation = glGetUniformLocation(programId, "MVP");
				glUseProgram(0);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (programId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}	
	}

	public void glCreateFragmentShader(String shaderCode) throws Exception {
		fragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, programId);
	}

	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShader(String shaderCode) throws Exception {
		vertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, programId);
	}

	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}

	private void makeThread() {
		t = new land_plot_thread(this, verticesDraw, verticesColor, arraySize, measureOfDetail, distanceAcross, centerX,centerY);
		t.start();

	}

	private void println(String s) {
		if (this.showConsoleOutput) {
			System.out.println(s);
		}
	}

	public void recalculateVertices() {
		//// verticesCalculated=false;
		//// calculateVertices();
		//// isVertexBound=false;
		//// bindVertexData();
		//// t.start();
	}

	public void setAsMainPlot() {
		t.setAsMainThread();
	}

	public void setCenter(float x, float y) {
		centerX = x;
		centerY = y;

		if (t != null) {
			t.setCenter(centerX, centerY);
		}
	}

	public void setLevelOfDetailHigh() {
		levelOfDetail = levelOfDetails.high;
		measureOfDetail = measureOfDetailHigh;
		distanceAcross = 100 * measureOfDetail;
		makeThread();
	}

	public void setLevelOfDetailLow() {
		levelOfDetail = levelOfDetails.low;
		measureOfDetail = measureOfDetailLow;
		distanceAcross = 100 * measureOfDetail;
		makeThread();
	}

	public void setLevelOfDetailMedium() {
		levelOfDetail = levelOfDetails.medium;
		measureOfDetail = measureOfDetailMedium;
		distanceAcross = 100 * measureOfDetail;
		makeThread();
	}

	public void setVertexBoundToFalse() {
		isVertexBound = false;
	}


	public void stopThread() {
		t.stopUpdating();
	}

}
