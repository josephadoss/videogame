package local.source;

public class damage {
	
	public int physicalDamage = 0;
	
	// do percentages too
	// so that an attack does 10% damage to the HP of the person being hit
	// this will be great in boss fights
	
	// and things like fireDamage and other magic types
	
	public void damage(){}
	
	public int getPhysicalDamage()
	{ return physicalDamage;}
	
	public void setPhysicalDamage(int n)
	{physicalDamage=n;}

}
