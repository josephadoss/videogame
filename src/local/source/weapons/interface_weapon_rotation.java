package local.source.weapons;
import org.joml.Vector3f;

public interface interface_weapon_rotation {
	
	public interface_weapon_rotation getNextState();

	public Vector3f getRotation();
	
	public int getTimeToComplete();

}
