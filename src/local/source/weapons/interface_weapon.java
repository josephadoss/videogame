package local.source.weapons;

import org.joml.*;

import local.source.attacks.interface_attack;
import local.source.attacks.interface_attackable;

import java.util.ArrayList;
import java.util.List;
import org.joml.*;


public interface interface_weapon {
	
	public Matrix4f MVP=null;

	public List<interface_attack> attacks = null;
	public interface_attack attack=null;
	
	public int weaponLevel = -1;
	public int weaponMaxLevel = -1;
	public int weaponExperience = -1;
	public int weaponMaxExperience =-1;
	
	public String weaponName = "interface_weapon";
	public String className = "interface_weapon";
	
	Vector3f handleXYZ = null;
	Vector3f centerXYZ = null;
	Vector3f rotationXYZ = null;

	
	interface_attackable myHolder = null;
	
	public void addExperience(int e);
	
	public void checkLevelUp();
	
	public void draw();

	public interface_attack getAttack();
	
	public String getClassName();
	
	public int getLevel();
	
	public int getWeaponExperience();
	
	public String getWeaponName();
	
	public void registerWithManager();
	
	public void setCenterXYZ(Vector3f n);
	
	public void setCenterXYZ(float x, float y, float z);
	
	public void setDefaultAttack();
	
	public void setHandleXYZ(float x, float y, float z) ;
	
	public void setHandleXYZ(Vector3f nxyz) ;
	
	public void setHolder(interface_attackable a);
	
	public void setIsAttacking(boolean b);

	public void setRotationXYZ(float x, float y, float z) ;
	
	public void setRotationXYZ(Vector3f nxyz) ;
	
	public void setWeaponName(String n);
	
	public void startAttackAction();
	
}
