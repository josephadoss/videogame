package local.source.weapons;

import java.util.ArrayList;
import java.util.List;

import org.joml.*;

import local.source.game_time;
public class manager_weapons {

	private static manager_weapons instance = null;
	game_time gt =game_time.getInstance();
	List<interface_weapon> list = new ArrayList<interface_weapon>(); // these are going to be short lived... rethink storage
		
	private manager_weapons()
	{}
	
	public void add(interface_weapon a)
	{ list.add(a);  }

	public static manager_weapons getInstance()
	{
		if(instance==null)
		{instance = new manager_weapons();}
		return instance;
	}
		
	public void remove(interface_weapon b)
	{
		for(interface_weapon a : list)
		{
			if(a==b)
			{
				list.remove(a);
				return;
			}
		}
	}
}