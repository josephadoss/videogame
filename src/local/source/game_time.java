package local.source;

public class game_time {
	private static game_time instance = null;
	
	int gameTime = 0; // this is how many milliseconds of playtime ( or unpaused time ) has passed since game started
		
	private game_time()
	{}
	
	public static game_time getInstance()
	{
		if(instance==null)
		{ instance = new game_time();}
		return instance;		
	}
	
	public int getTime()
	{return gameTime;}
	

	public void setTime(int newTime)
	{ gameTime=newTime;}
	
	public void updateTime(int moreTime)
	{
		gameTime+=moreTime;
	}

}
