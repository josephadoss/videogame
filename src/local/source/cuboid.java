package local.source;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_FRONT;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glCullFace;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VALIDATE_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;
import static org.lwjgl.opengl.GL20.glGetProgrami;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform4f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

public class cuboid {


Vector3f centerXYZ = new Vector3f(0f,0f,0f);

float widthY, heightZ, lengthX;	

//public Matrix4f MVP;
public Vector3f rotationXYZ = new Vector3f();
float rotationZ = 0f;
Quaternionf dest1 = new Quaternionf();
Quaternionf dest2 = new Quaternionf();
Vector3f v = new Vector3f();	
	
	
	Vector3f[] verticesModel ;/*= { 
			new Vector3f(0f,0f, 0f), // 0 top front right 
			new Vector3f(0f,0f, 0f), // 1 top front left
			new Vector3f(0f,0f, 0f), // 2 top back left
			new Vector3f(0f,0f, 0f), // 3 top back right
			new Vector3f(0f,0f, 0f), // 4 bot front right
			new Vector3f(0f,0f, 0f), // 5 bot front left
			new Vector3f(0f,0f, 0f), // 6 bot back left
			new Vector3f(0f,0f, 0f) // 7 bot back right
	};*/

	Vector3f[] vertices = { 
				new Vector3f(0f, 0f, 0f), // 0 top front right 
				new Vector3f(0f, 0f, 0f), // 1 top front left
				new Vector3f(0f, 0f, 0f), // 2 top back left
				new Vector3f(0f, 0f, 0f), // 3 top back right
				new Vector3f(0f, 0f, 0f), // 4 bot front right
				new Vector3f(0f, 0f, 0f), // 5 bot front left
				new Vector3f(0f, 0f, 0f), // 6 bot back left
				new Vector3f(0f, 0f, 0f) // 7 bot back right
		};
	
	byte[] indices = { 
			0,1,4//
			,1,5,4//
			,3,0,7//
			,0,4,7//
			,2,7,6//
			,2,3,7//
			,1,2,5//
			,2,6,5//
			,3,2,0//
			,2,1,0//
			,4,5,6//
			,4,6,7//
	};
	
	float[] verticesDraw = { 
			0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
			,0f,0f,0f//
	};

	int indicesLinesCount;
	
	float skinRed = 0.863f;
	float skinBlue = 0.863f;
	float skinGreen = 0.863f;
		
	int VAOID = -1;
	int VBOID = -1;
	int VBOIID = -1;

	public int GLMVPUniformLocation;
	public int GLRGBAUniformLocation;
	public FloatBuffer FB = BufferUtils.createFloatBuffer(16);
	public int programId;
	public int vertexShaderId;
	public int fragmentShaderId;
	boolean isIndexBound = false;
	public boolean isDraw = true;
	FloatBuffer verticesBuffer;
	
	public cuboid(float length, float width, float height) {

		lengthX=length;
		widthY=width;
		heightZ=height;
		
		height /=2f;
		width/=2f;
		length/=2f;
		
		verticesModel = new Vector3f[] { 
				new Vector3f(  length, -width,  height), // 0 top front right 
				new Vector3f(  length,  width,  height), // 1 top front left
				new Vector3f( -length,  width,  height), // 2 top back left
				new Vector3f( -length, -width,  height), // 3 top back right
				new Vector3f(  length, -width, -height), // 4 bot front right
				new Vector3f(  length,  width, -height), // 5 bot front left
				new Vector3f( -length,  width, -height), // 6 bot back left
				new Vector3f( -length, -width, -height) // 7 bot back right
		};
		
		if (isDraw) {
			this.glConstruct();
		}
	}

	public void bindVertexData() {

		if (isDraw) {
			if (verticesBuffer == null) {
				verticesBuffer = BufferUtils.createFloatBuffer(verticesDraw.length);
			}
			verticesBuffer.put(verticesDraw).flip();

			if (VAOID == -1) {
				VAOID = glGenVertexArrays();
			}
			glBindVertexArray(VAOID);

			if (VBOID == -1) {
				VBOID = glGenBuffers();
			}
			glBindBuffer(GL_ARRAY_BUFFER, VBOID);
			glBufferData(GL_ARRAY_BUFFER, verticesBuffer, GL_DYNAMIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);

			if (!isIndexBound) {
				indicesLinesCount = indices.length;

				ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(indicesLinesCount);
				indicesBuffer.put(indices);
				indicesBuffer.flip();

				VBOIID = glGenBuffers();
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIID);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

				isIndexBound = true;
			}
		}
	}

	public void calculateVertices(Vector3f[] verticesModel, Vector3f[] vertices, float[] verticesDraw, Vector3f offsetXYZ) {
		double radian = Math.toRadians(rotationZ);	

		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		float fX, fY, fZ;

		int indexCount = 0;
				
		for (int i = 0; i < vertices.length; i++) {
			v.x = verticesModel[i].x;
			v.y = verticesModel[i].y;
			v.z = verticesModel[i].z;

			dest1.w = 1f;
			dest1.x = 0f;
			dest1.y = 0f;
			dest1.z = 0f;

			dest2.w = 1f;
			dest2.x = 0f;
			dest2.y = 0f;
			dest2.z = 0f;

			float x, y, z;
			x = rotationXYZ.x ;
			y = rotationXYZ.y ;
			z = rotationXYZ.z ;
		
			x = x % 360.0f;
			y = y % 360.0f;
			z = z % 360.0f;

			dest2.rotate((float) Math.toRadians(x), (float) Math.toRadians(y), (float) Math.toRadians(z), dest1);

			dest1.transform(v);

			Vector3f vv = vertices[i];

				vv.x = v.x;
				vv.y = v.y;
				vv.z = v.z;
		}
		
		for (int i = 0; i < vertices.length; i++) {

			fX = vertices[i].x;
			fY = vertices[i].y;
			fZ = vertices[i].z;

			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX += offsetXYZ.x;
			dY += offsetXYZ.y;
			dZ += offsetXYZ.z;

			int newI = i * 3;
			verticesDraw[newI] = (dX);
			verticesDraw[newI + 1] = (dY);
			verticesDraw[newI + 2] = (dZ);			
		}	
	}

	public void draw() { 
		calculateVertices(verticesModel, vertices, verticesDraw,centerXYZ);
		
		if(isDraw)
		{drawCuboid();}		
	}

	public void drawCuboid() {		
		bindVertexData();
		glUseProgram(programId);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUniformMatrix4fv(GLMVPUniformLocation, false, manager_mvp.getInstance().get().get(FB));
		glUniform4f(GLRGBAUniformLocation, skinRed, skinGreen, skinBlue, 1.0f);

		glBindVertexArray(VAOID);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIID);
		glDrawElements(GL_TRIANGLES, indicesLinesCount, GL_UNSIGNED_BYTE, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);

		glUseProgram(0);

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
	}
	
	public void glCleanup() {
/*		glUseProgram(0);
		if (jointsProgramId != 0) {
			if (jointsVertexShaderId != 0) {
				glDetachShader(jointsProgramId, jointsVertexShaderId);
			}
			if (jointsFragmentShaderId != 0) {
				glDetachShader(jointsProgramId, jointsFragmentShaderId);
			}
			glDeleteProgram(jointsProgramId);
		}
*/
	}

	public void glConstruct() {
		try {
			try {
				programId = glCreateProgram();

				glCreateVertexShader("#version 130\nuniform mat4 MVP; in vec3 position; void main() { gl_Position =MVP* vec4(position, 1.0); }");

				glCreateFragmentShader("#version 130\nuniform vec4 RGBA; out vec4 fragColor;  void main() { fragColor = RGBA; }");

				glLink(programId);

				glUseProgram(programId);
				GLMVPUniformLocation = glGetUniformLocation(programId, "MVP");
				GLRGBAUniformLocation = glGetUniformLocation(programId, "RGBA");
				glUseProgram(0);

			} catch (Exception e) {
				System.out.println("exception caught:" + e.getMessage() + " " + e.getStackTrace());
			}

			if (programId == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in init " + e.getMessage() + " " + e.getStackTrace());
		}		
	}
	
	public void glCreateFragmentShader(String shaderCode) throws Exception {
		fragmentShaderId = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, programId);
	}
	
	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShader(String shaderCode) throws Exception {
		vertexShaderId = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, programId);
	}

	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}

	public void setCenterXYZ(float x, float y, float z) {
		centerXYZ.x = x;
		centerXYZ.y = y;
		centerXYZ.z = z;
	}

	public void setRGB(float r, float g, float b)
	{ skinRed = r; skinBlue = b; skinGreen = g;}
	
	public void setRotationXYZ(float x, float y, float z) {
		rotationXYZ.x = x;
		rotationXYZ.y = y;
		rotationXYZ.z = z;
	}

	public void setRotationXYZ(Vector3f nxyz) {
		rotationXYZ.x = nxyz.x;
		rotationXYZ.y = nxyz.y;
		rotationXYZ.z = nxyz.z;
	}

	public void setRotationZ(float x) {
		rotationZ=x;
	}
}
