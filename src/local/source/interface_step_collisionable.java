package local.source;
import org.joml.*;

public interface interface_step_collisionable {
	
	Vector3f centerXYZ = new Vector3f();
	Vector3f diameterXYZ = new Vector3f();

	public Vector3f getCenterXYZ();
	
	public Vector3f getDiameterXYZ();
	
	public void registerWithStepCollisionManager();
}
