package local.source;

import java.util.*;

import org.joml.Vector3f;

public class manager_engageables {

	private static manager_engageables instance = null;
	game_time gt =game_time.getInstance();
	List<interface_engageable> list = new ArrayList<interface_engageable>(); 	

	
	private manager_engageables()
	{}
	
	public void add(interface_engageable a)
	{ list.add(a);  }
	
	
	public static manager_engageables getInstance()
	{
		if(instance==null)
		{instance = new manager_engageables();}
		return instance;
	}
	
	
	public List<interface_engageable> getCollision(interface_engageable b) {
		List<interface_engageable> result = new ArrayList<interface_engageable>();

		for (interface_engageable a : list) {
			Vector3f aC = a.getCenterXYZ();
			Vector3f bC = b.getCenterXYZ();

			Vector3f aD = a.getEngageableDiameterXYZ();
			Vector3f bD = b.getEngageableDiameterXYZ();

			if (a != b) {
				float axr = aC.x + aD.x;
				float axl = aC.x - aD.x;

				float bxr = bC.x + bD.x;
				float bxl = bC.x - bD.x;

				if ((axl >= bxl && axl <= bxr) || (axr >= bxl && axr <= bxr) || (bxl >= axl && bxl <= axr) || (bxr >= axl && bxr <= axr)) {
					float ayr = aC.y + aD.y;
					float ayl = aC.y - aD.y;

					float byr = bC.y + bD.y;
					float byl = bC.y - bD.y;
					if ((ayl >= byl && ayl <= byr) || (ayr >= byl && ayr <= byr) || (byl >= ayl && byl <= ayr) || (byr >= ayl && byr <= ayr)) {
						float azr = aC.z + aD.z;
						float azl = aC.z - aD.z;

						float bzr = bC.z + bD.z;
						float bzl = bC.z - bD.z;
						if ((azl >= bzl && azl <= bzr) || (azr >= bzl && azr <= bzr) || (bzl >= azl && bzl <= azr) || (bzr >= azl && bzr <= azr)) {
							result.add(a);
						}
					}
				}
			}
		}

		return result;
	}
	

	public List<interface_engageable> getCollision(Vector3f center, Vector3f diameter,interface_engageable b) {
		
		List<interface_engageable> result = new ArrayList<interface_engageable>();
		
		for (interface_engageable a : list) {
			Vector3f aC = a.getCenterXYZ();
			Vector3f bC = center;

			Vector3f aD = a.getEngageableDiameterXYZ();
			Vector3f bD = diameter;

			if (a != b) 
			{
				float axr = aC.x + aD.x;
				float axl = aC.x - aD.x;

				float bxr = bC.x + bD.x;
				float bxl = bC.x - bD.x;

				if ((axl >= bxl && axl <= bxr) || (axr >= bxl && axr <= bxr) || (bxl >= axl && bxl <= axr) || (bxr >= axl && bxr <= axr)) {
					float ayr = aC.y + aD.y;
					float ayl = aC.y - aD.y;

					float byr = bC.y + bD.y;
					float byl = bC.y - bD.y;
					if ((ayl >= byl && ayl <= byr) || (ayr >= byl && ayr <= byr) || (byl >= ayl && byl <= ayr) || (byr >= ayl && byr <= ayr)) {
						float azr = aC.z + aD.z;
						float azl = aC.z - aD.z;

						float bzr = bC.z + bD.z;
						float bzl = bC.z - bD.z;
						if ((azl >= bzl && azl <= bzr) || (azr >= bzl && azr <= bzr) || (bzl >= azl && bzl <= azr) || (bzr >= azl && bzr <= azr)) {
							result.add(a);
						}
					}
				}
			}
		}
		return result;
	}
	
	
	
	public void remove(interface_engageable b)
	{
		for(interface_engageable a : list)
		{
			if(a==b)
			{
				list.remove(a);
				return;
			}
		}
	}
	
}
