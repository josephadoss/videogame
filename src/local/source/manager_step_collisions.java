package local.source;

import java.util.*;
import org.joml.*;

public class manager_step_collisions {

	// if running this gets expensive, consider breaking it out into a thread and just running it like five times a second
	
	private static manager_step_collisions instance = null;
	game_time gt = game_time.getInstance();
	List<interface_step_collisionable> list = new ArrayList<interface_step_collisionable>(); // these
																								// won't
																								// exist
																								// after
																								// being
																								// attacked
																								// to
																								// the
																								// point
																								// of
																								// death,
																								// so
																								// long
																								// term
																								// storage
																								// is
																								// bad
																								// idea



	private manager_step_collisions() {
	}

	public void add(interface_step_collisionable a) {
		list.add(a);
	}

	public static manager_step_collisions getInstance() {
		if (instance == null) {
			instance = new manager_step_collisions();
		}
		return instance;
	}

	public interface_step_collisionable getCollision(interface_step_collisionable b) { 
		for (interface_step_collisionable a : list) {
			Vector3f aC = a.getCenterXYZ();
			Vector3f bC = b.getCenterXYZ();

			Vector3f aD = a.getDiameterXYZ();
			Vector3f bD = b.getDiameterXYZ();

			if (a != b) {
				float axr = aC.x + aD.x;
				float axl = aC.x - aD.x;

				float bxr = bC.x + bD.x;
				float bxl = bC.x - bD.x;

				if ((axl >= bxl && axl <= bxr) || (axr >= bxl && axr <= bxr) || (bxl >= axl && bxl <= axr) || (bxr >= axl && bxr <= axr)) {
					float ayr = aC.y + aD.y;
					float ayl = aC.y - aD.y;

					float byr = bC.y + bD.y;
					float byl = bC.y - bD.y;
					if ((ayl >= byl && ayl <= byr) || (ayr >= byl && ayr <= byr) || (byl >= ayl && byl <= ayr) || (byr >= ayl && byr <= ayr)) {
						float azr = aC.z + aD.z;
						float azl = aC.z - aD.z;

						float bzr = bC.z + bD.z;
						float bzl = bC.z - bD.z;
						if ((azl >= bzl && azl <= bzr) || (azr >= bzl && azr <= bzr) || (bzl >= azl && bzl <= azr) || (bzr >= azl && bzr <= azr)) {
							return a;
						}
					}
				}
			}
		}

		return null;
	}
	

	public interface_step_collisionable getCollision(Vector3f center, Vector3f diameter,interface_engageable b) {
		for (interface_step_collisionable a : list) {
			Vector3f aC = a.getCenterXYZ();
			Vector3f bC = center;

			Vector3f aD = a.getDiameterXYZ();
			Vector3f bD = diameter;

			if (a != b) {
				float axr = aC.x + aD.x;
				float axl = aC.x - aD.x;

				float bxr = bC.x + bD.x;
				float bxl = bC.x - bD.x;

				if ((axl >= bxl && axl <= bxr) || (axr >= bxl && axr <= bxr) || (bxl >= axl && bxl <= axr) || (bxr >= axl && bxr <= axr)) {
					float ayr = aC.y + aD.y;
					float ayl = aC.y - aD.y;

					float byr = bC.y + bD.y;
					float byl = bC.y - bD.y;
					if ((ayl >= byl && ayl <= byr) || (ayr >= byl && ayr <= byr) || (byl >= ayl && byl <= ayr) || (byr >= ayl && byr <= ayr)) {
						float azr = aC.z + aD.z;
						float azl = aC.z - aD.z;

						float bzr = bC.z + bD.z;
						float bzl = bC.z - bD.z;
						if ((azl >= bzl && azl <= bzr) || (azr >= bzl && azr <= bzr) || (bzl >= azl && bzl <= azr) || (bzr >= azl && bzr <= azr)) {
							return a;
						}
					}
				}
			}
		}
		return null;
	}
}
