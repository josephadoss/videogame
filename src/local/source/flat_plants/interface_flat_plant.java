package local.source.flat_plants;

import org.joml.*;

// an actor is anything other than the landscape that can be drawn in the 3d part of the game
public interface interface_flat_plant {

	public String className = "interface_actor";
	
	public void draw();	
	
	public Vector3f getCenterXYZ();
	
	public void registerWithFlatPlantManager();
	
	public void setFacing(float x, float y, float z);
	
	public void setHeight(float h);
	
	public void setImage(String image);
	
//	public void setMVP(Matrix4f mvp);
	
	public void setXYZ(float x, float y, float z);
	
}
