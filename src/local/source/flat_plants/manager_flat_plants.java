package local.source.flat_plants;

import java.util.ArrayList;
import java.util.List;
import org.joml.*;

import local.source.game_time;

public class manager_flat_plants {

	private static manager_flat_plants instance = null;
	game_time gt = game_time.getInstance();
	List<interface_flat_plant> plants = new ArrayList<interface_flat_plant>();

	float x = 0;
	float y = 0;
	float z = 0;
	float collisionDetection = 40f;

	private manager_flat_plants() {
	}

	public void add(interface_flat_plant a) {
		plants.add(a);
	}

	public void draw() {
		for (interface_flat_plant a : plants) {
			Vector3f c = a.getCenterXYZ();
			if (c.x > (x - collisionDetection) && c.x < (x + collisionDetection)) {
				if (c.y > (y - collisionDetection) && c.y < (y + collisionDetection)) {
					if (c.z > (z - collisionDetection) && c.z < (z + collisionDetection)) {
						a.draw();
					}
				}
			}
		}
	}

	public static manager_flat_plants getInstance() {
		if (instance == null) {
			instance = new manager_flat_plants();
		}
		return instance;
	}

	public void remove(interface_flat_plant b) {
		for (interface_flat_plant a : plants) {
			if (a == b) {
				plants.remove(a);
				return;
			}
		}
	}

	public void removeAll() {
		for (interface_flat_plant a : plants) {
			plants.remove(a);
		}
	}

	public void setCenter(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public void setFacing(float x, float y, float z) {
		for (interface_flat_plant a : plants) {
			a.setFacing(x, y, z);
		}
	}

//	public void setMVP(Matrix4f MVP) {
//		for (interface_flat_plant a : plants) {
//			a.setMVP(MVP);
//		}
//	}

}
