package local.source.flat_plants;


import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_CLAMP_TO_BORDER;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VALIDATE_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glGetAttribLocation;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;
import static org.lwjgl.opengl.GL20.glGetProgrami;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform4f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;
import static org.lwjgl.stb.STBImage.*;

import java.nio.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;
import org.joml.Vector2f;
import org.lwjgl.BufferUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import local.source.globalStatic;
import local.source.manager_mvp;


public class flower1 implements interface_flat_plant {

	String className = "flower1";

	Vector3f centerXYZ = new Vector3f(0f, 0f, 0f);

	float widthY, heightZ, lengthX;

	//public Matrix4f MVP;
	public Vector3f rotationXYZ = new Vector3f();
	float rotationZ = 0f;
	Quaternionf dest1 = new Quaternionf();
	Quaternionf dest2 = new Quaternionf();
	Vector3f v = new Vector3f();

	int arrayDetail = 2; // this is how many columns of vertices, not how many
							// squares. 2 is one square. 3 is four squares
	// when doing rows and columns, row 0 is top, column 0 is left

	Vector3f[] verticesModelFront = new Vector3f[arrayDetail * arrayDetail];

	Vector2f[] whModelFront = new Vector2f[arrayDetail * arrayDetail];

	Vector3f[] verticesFront = new Vector3f[arrayDetail * arrayDetail];

	int[] indicesFront = new int[arrayDetail * arrayDetail * 6];

	int vertexNumbers = 5; // 3 or 5 3 is regular without textures 5 is with
							// textures
	float[] verticesDrawFront = new float[arrayDetail * arrayDetail * vertexNumbers];

	int indicesLinesCountFront;



	int VAOIDFront = -1;
	int VBOIDFront = -1;
	int VBOIIDFront = -1;


	public int GLMVPUniformLocationFront;
	public int GLRGBAUniformLocationFront;
	public FloatBuffer FBFront = BufferUtils.createFloatBuffer(20);
	public int programIdFront;
	public int vertexShaderIdFront;
	public int fragmentShaderIdFront;
	boolean isIndexBoundFront = false;
	public boolean isDrawFront = true;
	FloatBuffer verticesBufferFront;

	IntBuffer textureWidthFront = BufferUtils.createIntBuffer(1);
	IntBuffer textureHeightFront = BufferUtils.createIntBuffer(1);
	IntBuffer textureComponentsFront = BufferUtils.createIntBuffer(1);
	ByteBuffer textureDataFront;
	String textureSourcePath = "resources/flowers/flower1/skins/";
	String textureSourceFullPath= textureSourcePath+ "flower1blue1.png";
	int textureIDFront = -1;
	public int GLTextureCoordLocationFront;
	public int GLTextureImageUniformFront;

	
	public flower1()
	{		
		lengthX = 10f;
		widthY = 10f;
		heightZ = 10f;

/*		float height = heightZ / 2f;
		float width = widthY / 2f;
		float length = lengthX / 2f;*/

		if (isDrawFront ) {

			try {
				textureDataFront = stbi_load_from_memory(globalStatic.ioResourceToByteBuffer(textureSourceFullPath, 20 * 1024), textureWidthFront, textureHeightFront, textureComponentsFront, 4);
			} catch (Exception e) {
				System.out.println(className + " constructor : error thrown making the texture message:" + e.getMessage() + " stacktrace:" + e.getStackTrace());
			}
			

			this.glConstruct();
		}

		for (int i = 0; i < verticesModelFront.length; i++) {
			verticesModelFront[i] = new Vector3f(0f, 0f, 0f);
			verticesFront[i] = new Vector3f(0f, 0f, 0f);
			whModelFront[i] = new Vector2f(0f, 0f);
		}	
/*
		int indexFront = 0;
		for (int row = 1; row < (arrayDetail); row++) {
			for (int col = 1; col < (arrayDetail); col++) {
				// System.out.println("row:"+row+" col:"+col+"
				// arrayDetail:"+arrayDetail);
				int a = col - 1 + (row - 1) * arrayDetail; // System.out.println("a:"+a);
				int b = col + (row - 1) * arrayDetail; // System.out.println("b:"+b);
				int c = col + (row) * arrayDetail; // System.out.println("c:"+c);
				int d = col - 1 + (row) * arrayDetail; // System.out.println("d:"+d);

				indicesFront[indexFront++] = a;
				indicesFront[indexFront++] = b;
				indicesFront[indexFront++] = c;

				indicesFront[indexFront++] = a;
				indicesFront[indexFront++] = c;
				indicesFront[indexFront++] = d;
			}
		}	*/	
		
		// 0 bottom left
		verticesModelFront[0].x = 0;
		verticesModelFront[0].y = -.5f;
		verticesModelFront[0].z = 0;
		whModelFront[0].x = 1;
		whModelFront[0].y = 1;
		
		// 1 top left
		verticesModelFront[1].x = 0;
		verticesModelFront[1].y = -.5f;
		verticesModelFront[1].z = 1f;
		whModelFront[1].x = 1;
		whModelFront[1].y = 0;
		
		// 2 top right
		verticesModelFront[2].x = 0;
		verticesModelFront[2].y = .5f;
		verticesModelFront[2].z = 1f;
		whModelFront[2].x = 0;
		whModelFront[2].y = 0;
		
		// 3 bottom right
		verticesModelFront[3].x = 0;
		verticesModelFront[3].y = .5f;
		verticesModelFront[3].z = 0;
		whModelFront[3].x = 0;
		whModelFront[3].y = 1;
		
		indicesFront[0] = 0;
		indicesFront[1] = 1;
		indicesFront[2] = 2;
		indicesFront[3] = 2;
		indicesFront[4] = 3;
		indicesFront[5] = 0;

		registerWithFlatPlantManager();		
	}
	
	public void bindVertexDataFront() {

		if (isDrawFront) {
			if (verticesBufferFront == null) {
				verticesBufferFront = BufferUtils.createFloatBuffer(verticesDrawFront.length);
			}
			verticesBufferFront.put(verticesDrawFront).flip();

			if (textureIDFront == -1) {
				textureIDFront = glGenTextures();

				glBindTexture(GL_TEXTURE_2D, textureIDFront);
				// glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
				// glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

				// System.out.println("about to do glTexImage2D");
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureWidthFront.get(), textureHeightFront.get(), 0, GL_RGBA, GL_UNSIGNED_BYTE, textureDataFront);
				stbi_image_free(textureDataFront);
				// System.out.println("done with glTexImage2D");

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
				// GL_LINEAR);
				// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
				// GL_LINEAR);

				// System.out.println("texture width and height :
				// "+textureWidth.get() + " " +textureHeight.get());
				if (textureDataFront == null) {
					System.out.println("about to make texture with null textureData");
				}

			}

			if (VAOIDFront == -1) {
				VAOIDFront = glGenVertexArrays();
			}
			glBindVertexArray(VAOIDFront);

			if (VBOIDFront == -1) {
				VBOIDFront = glGenBuffers();
			}
			glBindBuffer(GL_ARRAY_BUFFER, VBOIDFront);
			glBufferData(GL_ARRAY_BUFFER, verticesBufferFront, GL_DYNAMIC_DRAW);
			// glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 20, 0);
			glVertexAttribPointer(1, 2, GL_FLOAT, false, 20, 12);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);

			if (!isIndexBoundFront) {
				indicesLinesCountFront = indicesFront.length;

				IntBuffer indicesBuffer = BufferUtils.createIntBuffer(indicesLinesCountFront);
				indicesBuffer.put(indicesFront);
				indicesBuffer.flip();

				VBOIIDFront = glGenBuffers();
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIIDFront);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

				isIndexBoundFront = true;
			}
		}
	}

	public void calculateVertices(Vector3f[] verticesModel, Vector2f[] whModel, Vector3f[] vertices, float[] verticesDraw, Vector3f offsetXYZ ) {

		double radian = Math.toRadians(rotationZ);

		float cosRadian = (float) Math.cos(radian);
		float sinRadian = (float) Math.sin(radian);

		float fX, fY, fZ;

		for (int i = 0; i < vertices.length; i++) {
			v.x = verticesModel[i].x;
			v.y = verticesModel[i].y;
			v.z = verticesModel[i].z;

			dest1.w = 1f;
			dest1.x = 0f;
			dest1.y = 0f;
			dest1.z = 0f;

			dest2.w = 1f;
			dest2.x = 0f;
			dest2.y = 0f;
			dest2.z = 0f;

			float x, y, z;
			x = rotationXYZ.x;// + rotationXYZ.x;// testX;
			y = rotationXYZ.y;// + rotationXYZ.y;//testY;
			z = rotationXYZ.z;// + rotationXYZ.z;//testZ;

			x = x % 360.0f;
			y = y % 360.0f;
			z = z % 360.0f;

			dest2.rotate((float) Math.toRadians(x), (float) Math.toRadians(y), (float) Math.toRadians(z), dest1);

			dest1.transform(v);

			Vector3f vv = vertices[i];

			vv.x = v.x;
			vv.y = v.y;
			vv.z = v.z;

		}

		float height = heightZ ;
		for (int i = 0; i < vertices.length; i++) {

			fX = vertices[i].x;// * lengthX/* X */;
			fY = vertices[i].y;// * widthY/* Y */;
			fZ = vertices[i].z;// * heightZ/* Z */;
	
			float dX = fX * cosRadian - fY * sinRadian;
			float dY = fY * cosRadian + fX * sinRadian;
			float dZ = fZ;

			dX *= height;
			dY *= height;
			dZ *= height;

			dX += offsetXYZ.x;
			dY += offsetXYZ.y;
			dZ += offsetXYZ.z;
	
			int newI = i * vertexNumbers;

			verticesDraw[newI] = (dX);
			verticesDraw[newI + 1] = (dY);
			verticesDraw[newI + 2] = (dZ);
			//System.out.println("i : " + i + " x : " + dX + " y : " + dY + " z : " + dZ);

			if (vertexNumbers == 5) { // this could be optimized to only run once
				verticesDraw[newI + 3] = whModel[i].x;// frow;
				verticesDraw[newI + 4] = whModel[i].y;// fcol;
			}
		}

	}

	public void draw() {
		if (isDrawFront) {
			calculateVertices(verticesModelFront, whModelFront, verticesFront, verticesDrawFront, centerXYZ);
			drawFront();
		}
	}

	public void drawFront() {
		
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_ALPHA_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND); 
		glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

		glUseProgram(programIdFront);
		bindVertexDataFront();

		glUniformMatrix4fv(GLMVPUniformLocationFront, false, manager_mvp.getInstance().get().get(FBFront));
		//glUniform4f(GLRGBAUniformLocationFront, skinRed, skinGreen, skinBlue, 1.0f);

		glBindVertexArray(VAOIDFront);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOIIDFront);
		glBindTexture(GL_TEXTURE_2D, textureIDFront);

		// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glDrawElements(GL_TRIANGLES, indicesLinesCountFront, GL_UNSIGNED_INT, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glBindVertexArray(0);

		glUseProgram(0);

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_ALPHA_TEST);
		glDisable(GL_BLEND);

	}

	public Vector3f getCenterXYZ()
	{return centerXYZ;}
	
	public void glCleanup() {
		/*
		 * glUseProgram(0); if (jointsProgramId != 0) { if (jointsVertexShaderId
		 * != 0) { glDetachShader(jointsProgramId, jointsVertexShaderId); } if
		 * (jointsFragmentShaderId != 0) { glDetachShader(jointsProgramId,
		 * jointsFragmentShaderId); } glDeleteProgram(jointsProgramId); }
		 */
	}

	public void glConstruct() {

		if (isDrawFront) {
			glConstructFront();
		}
	}

	public void glConstructFront() {
		try {
			try {
				programIdFront = glCreateProgram();

				glCreateVertexShaderFront("#version 130         \n"//
						+ "uniform mat4 MVP;                       \n"//
						+ "in vec3 position;                       \n"//
						+ "in vec2 texcoord;                       \n"//
						+ "out vec2 textureCoord;              \n"//
						+ "void main() {                           \n"//
						+ "     gl_Position =MVP* vec4(position, 1.0); \n"//
						+ "       textureCoord = texcoord;           \n"//
						+ "}");

				glCreateFragmentShaderFront("#version 130       \n"//
						+ "uniform vec4 RGBA; \n"//
						+ "in vec2 textureCoord;              \n"//
						+ "uniform sampler2D texImage;             \n"//
						+ "out vec4 out_color;                     \n"//
						+ "void main() {                           \n"//
						+ "   out_color= texture(texImage,textureCoord);\n"//
						+ " if(out_color.a<0.5f) \n" 
						+ " { discard;}" 
						+ "}   ");

				glLink(programIdFront);

				glUseProgram(programIdFront);
				GLMVPUniformLocationFront = glGetUniformLocation(programIdFront, "MVP");
				GLRGBAUniformLocationFront = glGetUniformLocation(programIdFront, "RGBA");
				GLTextureCoordLocationFront = glGetAttribLocation(programIdFront, "texcoord");
				GLTextureImageUniformFront = glGetUniformLocation(programIdFront, "texImage");

				glUseProgram(0);

				// glEnable(GL_PROGRAM_POINT_SIZE);

			} catch (Exception e) {
				System.out.println("exception caught in " + className + ":" + e.getMessage() + " " + e.getStackTrace());
			}

			if (programIdFront == 0) {
				throw new Exception("Could not create Shader");
			}

		} catch (Exception e) {
			System.out.println("exception caught in " + className + " : " + e.getMessage() + " " + e.getStackTrace());
		}
		// move();
	}
	
	public void glCreateFragmentShaderFront(String shaderCode) throws Exception {
		fragmentShaderIdFront = glCreateThisShader(shaderCode, GL_FRAGMENT_SHADER, programIdFront);
	}

	protected int glCreateThisShader(String shaderCode, int shaderType, int programID) throws Exception {
		int shaderId = glCreateShader(shaderType);
		if (shaderId == 0) {
			throw new Exception("Error in " + className + " creating shader. Code: " + shaderId);
		}
		glShaderSource(shaderId, shaderCode);
		glCompileShader(shaderId);
		if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
			throw new Exception("Error in " + className + " compiling Shader code: " + glGetShaderInfoLog(shaderId, 1024));
		}
		glAttachShader(programID, shaderId);
		return shaderId;
	}

	public void glCreateVertexShaderFront(String shaderCode) throws Exception {
		vertexShaderIdFront = glCreateThisShader(shaderCode, GL_VERTEX_SHADER, programIdFront);
	}

	public void glLink(int programId) throws Exception {
		glLinkProgram(programId);
		if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
			throw new Exception("Error in " + className + " linking Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
		glValidateProgram(programId);
		if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
			System.err.println("Warning in " + className + " validating Shader code: " + glGetProgramInfoLog(programId, 1024));
		}
	}

	public void registerWithFlatPlantManager()
	{
		manager_flat_plants.getInstance().add(this);		
	}
	
	
	public void setBodyHeight(float height) {
		this.heightZ = height;
		float headHeight = 1f / 8f;
		// float neck = headHeight * .2f;

		float l = headHeight * height;
		float w = headHeight * height;
		float h = headHeight * height * 1.5f;
	}


	public void setCenterXYZ(float x, float y, float z) {
		centerXYZ.x = x;
		centerXYZ.y = y;
		centerXYZ.z = z;
	}

	public void setFacing(float newX, float newY, float newZ)
	{
		float dX = newX - centerXYZ.x;
		float dY = newY - centerXYZ.y;
		rotationZ = ( (float) (Math.atan2(dY, dX) * 180.0f / Math.PI) ) + 180f;		
	}
	
	public void setFacingRotationZ(float x) {
		// degreeRotateY = x;
		rotationZ = x;
	}

	public void setHeight(float h)
	{ this.heightZ = h;}

	public void setImage(String image)
	{
		textureSourceFullPath= textureSourcePath+ image+".png";
		
		try {
			textureDataFront = stbi_load_from_memory(globalStatic.ioResourceToByteBuffer(textureSourceFullPath, 20 * 1024), textureWidthFront, textureHeightFront, textureComponentsFront, 4);
		} catch (Exception e) {
			System.out.println(className + " constructor : error thrown making the texture message:" + e.getMessage() + " stacktrace:" + e.getStackTrace());
		}
	}
	
//	public void setMVP(Matrix4f mvp) {
//		MVP = mvp; // System.out.println("set sword MVP");
//	}
	

	public void setRotationXYZ(float x, float y, float z) {
		rotationXYZ.x = x;
		rotationXYZ.y = y;
		rotationXYZ.z = z;
	}

	public void setRotationXYZ(Vector3f nxyz) {
		rotationXYZ.x = nxyz.x;
		rotationXYZ.y = nxyz.y;
		rotationXYZ.z = nxyz.z;
		// if(attack!=null)
		// {attack.setHandleXYZ(nxyz);}
	}

	public void setRotationZ(float x) {
		// degreeRotateY = x;
		rotationZ = x;
	}

	
	public void setXYZ(float x, float y, float z)
	{
		centerXYZ.x = x;
		centerXYZ.y = y;
		centerXYZ.z = z;
	}

}